#!/usr/bin/env ruby 

require 'net/http'
require 'uri'
require 'cgi'
require 'digest'


def web_search(q, key)
	q = CGI.escape(q)
	q = q.gsub!('%22', '"')
	cmd = "curl --header \"Ocp-Apim-Subscription-Key: #{key}\" https://api.cognitive.microsoft.com/bing/v5.0/search?q=#{q}"
	o = `#{cmd}`
	return o
end

=begin
def web_search(q)
	#puts q.inspect
	q = CGI.escape(q)
	#puts q.inspect
	q = q.gsub!('%22', '"')
	uri = URI.parse("https://api.cognitive.microsoft.com/bing/v5.0/search?mkt=en-US&q=#{q}")
	request = Net::HTTP::Get.new(uri)
	request["Ocp-Apim-Subscription-Key"] = "8a2f1d0e09174fe0aa1364d99543be68"
	puts uri.query
	#exit

	req_options = {
		use_ssl: uri.scheme == "https",
	}

	response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
		http.request(request)
	end

	return response 
end
=end

puts 'usage: ./web_search.rb PENDINGQUERIES OUTFILE BINGAPIKEY' if ARGV.size != 3
ifname = ARGV[0]
ofname = ARGV[1]
key = ARGV[2]
f = File.new(ofname, 'w+')
results = {} 
count = 0
File.readlines(ifname).each do |line|
	line = line.strip.gsub(',', '')
	sha1 = Digest::SHA1.hexdigest(line) 
	if results.has_key?(sha1) 
		#f.puts results[sha1].body
		f.puts results[sha1]
		# write to file
	else 
		results[sha1] = web_search(line, key)
		#f.puts results[sha1].body
		f.puts results[sha1]
	end
	puts results[sha1].size
	sleep 0.5
	count = count + 1
	exit if count == 1000
end
