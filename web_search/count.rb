#!/usr/bin/env ruby

require 'json'

queries = []
File.readlines('queries.txt').each do |line|
	queries << line.chomp
end

counts = []
File.readlines('results.txt').each do |line|
	result = JSON.parse(line)
	if result.has_key?("webPages")
		counts << result["webPages"]["totalEstimatedMatches"]
	else
		#puts result.inspect
	end
end

counts.each_with_index do |c, i|
	puts "#{queries[i]} \t #{c}"
end
