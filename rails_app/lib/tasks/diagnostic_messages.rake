namespace :dms do

	def reset_counters 
		DiagnosticMessage.find_each { |dm| DiagnosticMessage.reset_counters(dm.id, :examples) }
	end

	desc 'Update the examples_count column'
	task :update_examples_count => :environment do
		reset_counters
	end # task


	desc 'Convert the diagnostic message to a web search query'
	task :create_template => :environment do
		
	end # task


	desc 'Set or Update the Bing Search Result Count'
	task :update_bing_search_result_counts => :environment do
		puts 'update_bing_search_result_counts'
		count = 0
		dm_names = []
		dm_names = DiagnosticMessage.unscoped.uniq.pluck(:name)
		dm_names.each do |name|
			count = count + 1
			dm = DiagnosticMessage.find_by(name: name)
			set = false
			if dm.bing_search_result_count == 0
				set = dm.set_bing_search_result_count
			else 
				set = true
			end
			if set
				DiagnosticMessage.where(name: name).update_all(bing_search_result_count: dm.bing_search_result_count)
				puts "#{count}\tSUCCESS\t#{name}"
			else
				puts "#{count}\tERROR\t#{name}"
				puts "ERROR: #{dm.inspect}" 
				puts "ERROR: #{dm.errors.inspect}" 
			end # if
		end # each
	end # task


	desc 'Update bing search result count based of diagnostic messages with the same template.'
	task :update_bsrc_local => :environment do
		DiagnosticMessage.unscoped.order(:id).each do |dm|
			old = dm.bing_search_result_count
			new = dm.template.bing_search_result_count
			if old.nil? or old < new
				dm.bing_search_result_count = new
				if dm.save
					puts "SUCCESS:\t#{dm.id}\t#{old}\t#{new}" 
				else
					puts "ERROR Saving:\t#{dm.id}" 
				end
			else
					puts "NO CHANGE:\t#{dm.id}" 
			end
		end	
	end	


	desc 'Find diagnostic messages with an odd number of single quotes'
	task :odd_quotes => :environment do
		DiagnosticMessage.unscoped.order(:id).each do |dm|
			if !dm.message.nil?
				message = dm.message.dup
				indices = message.enum_for(:scan,/'/).map { Regexp.last_match.begin(0) }
				if indices.size % 2 != 0
					puts "#{dm.id}\t#{dm.message}"
				end 
			end
		end
	end


	desc 'Update the template id.'
	task :update_template_id_clang => :environment do
		DiagnosticMessage.unscoped.order(:id).each do |dm|
			old = dm.template_id
			cid = dm.compiler.id
			t = Template.find_by(compiler_id: cid, name: dm.name)
			if !t.nil?
				new = t.id
				if old.nil? or new != old
					dm.template_id = new
					if dm.save
						puts "SUCCESS\t#{dm.id}\t#{old}\t-->\t#{new}"
					else
						puts "ERROR Saving\t#{dm.id}"
						puts dm.inspect
						puts old.inspect
						puts new.inspect
						puts t.inspect
						puts dm.errors.inspect
						exit
					end
				else
					puts "NO CHANGE\t#{dm.id}"	
				end
			else 
				puts "ERROR Finding Template\t#{dm.id}"
			end
		end
	end


	desc 'Update the template id.'
	task :update_template_id_hard => :environment do
		DiagnosticMessage.unscoped.order(:id).each do |dm|
			old = dm.template_id
			q = dm.to_query_string
			cid = dm.compiler_id
			t = Template.find_by(compiler_id: cid, query_string: q) 
			if !t.nil?
				new = t.id
				if old.nil? or new != old
					dm.template_id = new
					if dm.save
						puts "SUCCESS\t#{dm.id}\t#{old}\t-->\t#{new}"
					else
						puts "ERROR Saving\t#{dm.id}"
						puts dm.inspect
						puts old.inspect
						puts new.inspect
						puts t.inspect
						puts dm.errors.inspect
						exit
					end
				else
					puts "NO CHANGE\t#{dm.id}"	
				end
			else 
				puts "ERROR Finding Template\t#{dm.id}"
			end
		end
	end

	desc 'Clean Diagnostic Messages'
	task :clean => :environment do
		reset_counters
		DiagnosticMessage.unscoped.order(:id).each do |dm|
			if dm.examples.size == 0
				puts "Deleting\t#{dm.id}"
				dm.destroy 
			end
		end
	end

end #namespace
