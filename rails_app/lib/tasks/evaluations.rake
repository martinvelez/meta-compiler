namespace :evaluations do

	desc 'Dump time data'
	task :dump_times => :environment do
		evals = Evaluation.order(:id)
		#puts 'id,email,snippet,group,time'
		puts 'Task,Group,Time'
		evals.each do |e|
			ts = e.tasks.sort_by do |t| 
				t[1]['order'].to_i
			end	
			ts = ts.to_h
			#puts ts.keys
			ts.each do |sid,info|
				time = -1
				timeout = info['timeout']
				if !timeout.nil? and timeout == 't'
					time = 300
				elsif timeout.nil? or timeout == 'f'
					s = info['started_at']
					if !s.nil?
						f = info['submitted_at']
						if !f.nil?
							#puts s
							#puts f
							time = DateTime.rfc3339(f) - DateTime.rfc3339(s)
							time = time * 1.days # convert to difference in seconds
						end
					end
				end
				if time != -1
					#puts "#{e.id},#{e.user.email},#{sid},#{info['group']},#{time}"
					puts "T#{sid},#{info['group']},#{time}"
					puts "All,#{info['group']},#{time}"
				else
					#puts "#{e.id},#{e.user.email},#{sid},#{info['group']},NA"
				end
			end
		end
	end # task

	desc 'Grade tasks'
	task :grade_tasks => :environment do
		evals = Evaluation.order(:id)		
		evals.each do |e|
			e.tasks.each do |sid,info|
				e.grades[sid] = 0 # incorrect
				timeout = info['timeout']
				if !timeout.nil? and timeout == 't'
					e.grades[sid] = -1 # timeout
				else
					if !info['submission'].nil?
						# compile
						compilation = info['submission'].compile
						if compilation[1].exitstatus == 0
							e.grades[sid] = 1 # correct
						end
					end
				end
			end	
			e.save
		end
	end


	desc 'Dump grades'
	task :dump_grades => :environment do
		evals = Evaluation.order(:id)		
		snippets = {}
		evals.each do |e|
			next if e.questionnaire.nil? or e.questionnaire.size < 12
			e.grades.each do |sid,grade|
				if !snippets.has_key?(sid)
					snippets[sid] = {
						overall: {correct: 0, incorrect: 0, timeout: 0},
						control: {correct: 0, incorrect: 0, timeout: 0},
						experimental: {correct: 0, incorrect: 0, timeout: 0}
					}
				end
				if grade.to_i == 0
					snippets[sid][:overall][:incorrect] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:incorrect] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:incorrect] += 1
					end
				elsif grade.to_i == 1
					snippets[sid][:overall][:correct] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:correct] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:correct] += 1
					end
				elsif grade.to_i == -1
					snippets[sid][:overall][:timeout] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:timeout] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:timeout] += 1
					end
				end
			end # each
		end # each
		snippets = snippets.sort_by { |sid,_| sid.to_i }.to_h
		puts snippets
		puts 'sid,oc,oi,ot,cc,ci,ct,ec,ei,et'
		snippets.each do |sid,info|
			print "#{sid},"
			print "#{info[:overall][:correct]},#{info[:overall][:incorrect]},#{info[:overall][:timeout]},"
			print "#{info[:control][:correct]},#{info[:control][:incorrect]},#{info[:control][:timeout]},"
			print "#{info[:experimental][:correct]},#{info[:experimental][:incorrect]},#{info[:experimental][:timeout]}"
			print "\n"
		end
	end # task


	desc 'Dump grades by participant'
	task :dump_grades_by_participant => :environment do
		evals = Evaluation.order(:id)		
		evals = {}
		evals.each do |e|
			next if e.questionnaire.nil? or e.questionnaire.size < 12
			e.grades.each do |sid,grade|
				if !snippets.has_key?(sid)
					snippets[sid] = {
						overall: {correct: 0, incorrect: 0, timeout: 0},
						control: {correct: 0, incorrect: 0, timeout: 0},
						experimental: {correct: 0, incorrect: 0, timeout: 0}
					}
				end
				if grade.to_i == 0
					snippets[sid][:overall][:incorrect] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:incorrect] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:incorrect] += 1
					end
				elsif grade.to_i == 1
					snippets[sid][:overall][:correct] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:correct] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:correct] += 1
					end
				elsif grade.to_i == -1
					snippets[sid][:overall][:timeout] += 1
					if e.tasks[sid.to_s]['group'] == 'control'
						snippets[sid][:control][:timeout] += 1
					elsif e.tasks[sid.to_s]['group'] == 'experimental'
						snippets[sid][:experimental][:timeout] += 1
					end
				end
			end # each
		end # each
		snippets = snippets.sort_by { |sid,_| sid.to_i }.to_h
		puts snippets
		puts 'sid,oc,oi,ot,cc,ci,ct,ec,ei,et'
		snippets.each do |sid,info|
			print "#{sid},"
			print "#{info[:overall][:correct]},#{info[:overall][:incorrect]},#{info[:overall][:timeout]},"
			print "#{info[:control][:correct]},#{info[:control][:incorrect]},#{info[:control][:timeout]},"
			print "#{info[:experimental][:correct]},#{info[:experimental][:incorrect]},#{info[:experimental][:timeout]}"
			print "\n"
		end
	end # task


	desc 'Dump helpfulness'
	task :dump_helpfulness => :environment do
		evals = Evaluation.order(:id)		
		snippets = {}
		puts 'Task,compiler,fix_examples'
		evals.each do |e|
			next if e.questionnaire.nil? or e.questionnaire.size < 12
			e.tasks.each do |sid,info|
				g = info['group']
				if !g.nil? and g == 'experimental'
					q = info['questionnaire']
					if !q.nil?
						if q['compiler_output'] != '4' and q['possible_patches'] != '4' 
							puts "#{sid},#{q['compiler_output']},#{q['possible_patches']}"
						end
					end
				end
			end # each
		end # each
	end # task


	desc 'Dump helpfulness v2'
	task :dump_helpfulness_2 => :environment do
		evals = Evaluation.order(:id)		
		snippets = {}
		puts 'Task,Info,Rating'
		evals.each do |e|
			next if e.questionnaire.nil? or e.questionnaire.size < 12
			e.tasks.each do |sid,info|
				next if sid == "2"
				g = info['group']
				if !g.nil? and g == 'experimental'
					q = info['questionnaire']
					if !q.nil?
						if q['compiler_output'] != '4' and q['possible_patches'] != '4' 
							puts "T#{sid},error_message,#{q['compiler_output']}"
							puts "T#{sid},fix_examples,#{q['possible_patches']}"
						end
					end
				end
			end # each
		end # each
	end # task

	
end #namespace
