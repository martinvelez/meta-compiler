namespace :compilers do

	def update_diagnostic_messages_count 
		Compiler.find_each { |c| Compiler.reset_counters(c.id, :diagnostic_messages) }
	end

	def update_templates_count 
		Compiler.find_each { |c| Compiler.reset_counters(c.id, :templates) }
	end

	def update_examples
		Compiler.find_each do |compiler|
			c = 0
			if !compiler.examples.nil? 
				c = compiler.examples.count
			end
			compiler.examples_count = c
			if compiler.save
				puts "SUCCESS:\tCOMPILER:\t#{compiler.id}"
			else
				puts "ERROR:\tCOMPILER:\t#{compiler.id}"
			end
		end
	end


	desc 'update the diagnostic_messages_count column'
	task :update_dms => :environment do
		update_diagnostic_messages_count()
	end # task
	
	desc 'update the templates_count column'
	task :update_templates => :environment do
		update_templates_count()
	end # task


	desc 'Update Examples Count'
	task :update_examples => :environment do
		puts __method__
		update_examples()
	end # task


	desc 'Update Summary By Selected Components'
	task :update_summary_selected => :environment do
		Compiler.find_each do |c|
			puts "c = #{c.name}"
			c.set_summary_by_selected_components
		end
	end # task
	

	desc 'Clean compilers'
	task :clean => :environment do
		# TODO
	end


end #namespace
