json.extract! template, :id, :name, :template, :compiler_id, :created_by, :created_at, :updated_at
json.url template_url(template, format: :json)