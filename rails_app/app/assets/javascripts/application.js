// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require turbolinks
//=require tether
// for debugging
//= require bootstrap-sprockets
// for faster compilation
// require bootstrap
//= require ace-rails-ap
//= require ace/theme-chrome
//= require ace/mode-python
//= require ace/mode-c_cpp
//= require ace/mode-java
//= require ace/mode-javascript
//= require ace/mode-ruby
//= require bootstrap-notify.min.js
//= require jquery-ui
//= require_tree .

$(document).ready(function() {
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
});

