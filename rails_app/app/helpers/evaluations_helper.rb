module EvaluationsHelper

  def task_submitted?(t)
		return true if !t['timeout'].nil?  
		return true if !t['submitted_at'].nil?  
    return false
  end 

  def tquestionnaire_complete?(q)
    return false if q.nil?
    return false if q['compiler_output'].nil?
    return false if q['compiler_output'].to_i == -1
    return false if q['possible_patches'].nil?
    return false if q['possible_patches'].to_i == -1
		return true
  end


end
