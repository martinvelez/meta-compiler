class EvaluationsController < ApplicationController
	before_action :authenticate_user!
  before_action :require_admin, only: [:show, :new, :edit, :update, :destroy]
  before_action :set_evaluation, only: [:show, :edit, :update, :questionnaire, :record_questionnaire, :tutorial, :record_tutorial_quiz, :tasks, :task, :task_finish, :task_questionnaire, :record_task_questionnaire, :questionnaire_final, :record_questionnaire_final]
	before_action :validate_consent, only: [:questionnaire, :tutorial, :tasks, :task, :task_questionnaire, :questionnaire_final]

	# GET /evaluations/consent
	def consent
		# check if the user has already signed consent	
		e = Evaluation.find_or_create_by(user_id: current_user.id)
		if !e.nil?
			Rails.logger.debug 'e.inspect'
			Rails.logger.debug e.inspect
			if valid_consent(e.consent)
				@consent = e.consent
				Rails.logger.debug 'valid consent'
				render 'consent_signed'
				return
			else 
				Rails.logger.debug 'invalid consent'
			end
		else 
			#error
		end
		# render consent 
	end

	# POST /evaluations/record_consent
	def record_consent
		params.require(:name)
		params.require(:date)
		params.require(:email)
		params.require(:code)
		e = Evaluation.find_by(user_id: current_user.id)
		if !e.nil?
			if valid_consent(e.consent)
				redirect_to evaluations_questionnaire_path, notice: 'Consent recorded.' 
				return
			else
				consent = {}
				consent[:name] = params[:name]
				consent[:date] = params[:date]
				consent[:email] = params[:email]
				consent[:code] = params[:code]
				e.consent = consent
				e.code = params[:code]
				if e.save
					redirect_to evaluations_questionnaire_path, notice: 'Consent recorded.' 
				else
					redirect_to evaluations_consent_path, notice: 'Consent not recorded.' 
				end
				return
			end
		end
		redirect_to evaluations_consent_path, notice: 'Consent not recorded.' 
	end


	# GET /evaluations/questionnaire
	def questionnaire
		#Rails.logger.debug __method__
		#Rails.logger.debug params.inspect
		@education = {}
		@education["Undergraduate (Freshman)"] = "u1"
		@education["Undergraduate (Sophomore)"] = "u2"
		@education["Undergraduate (Junior)"] = "u3"
		@education["Undergraduate (Senior)"] = "u4"
		@education["Graduate - 1st Year"] = "g1"
		@education["Graduate - 2nd Year"] = "g2"
		@education["Graduate - 3rd Year"] = "g3"
		@education["Graduate - 4th Year"] = "g4"
		@education["Graduate - 5th Year"] = "g5"
		@education["Graduate - 6th Year"] = "g6"
		@education["Other (Bachelor's Degree)"] = "bachelors"
		@education["Other (Masters Degree)"] = "masters"
		@education["Other (Ph.D. Degree)"] = "phd"
		@education["Other (All Others)"] = "other"
		@questionnaire = {}
		if !@evaluation.questionnaire.nil?
			@questionnaire = @evaluation.questionnaire
		end
		@started = Time.now
		# render questionnaire		
	end


	# POST /evaluations/record_questionnaire
	def record_questionnaire
		Rails.logger.debug __method__
		Rails.logger.debug params.inspect
		params.permit(:started, :gender, :student, :cppcourse, :cppexpyears, :cppexplevel, :education)
		q = {}
		q[:started] = params[:started]
		q[:education] = params[:education]
		q[:gender] = params[:gender]
		q[:cppcourse] = params[:cppcourse]
		q[:cppexplevel] = params[:cppexplevel]
		q[:cppexpyears] = params[:cppexpyears]
		q[:finished] = Time.now
		@evaluation.questionnaire = q	
		if !@evaluation.save
			redirect_to evaluations_questionnaire_path, notice: 'Questionnaire was not recorded.' 
		else
			redirect_to evaluations_tutorial_path, notice: 'Questionnaire recorded.' 
		end
	end


	# GET /evaluations/tutorial
	def tutorial
		Rails.logger.debug __method__
		@tutorial = {}
		if !@evaluation.tutorial_quiz.nil?
			@tutorial = @evaluation.tutorial_quiz
		end
		@tutorial["started"] = Time.now 
	end

	
	# POST /evaluations/record_tutorial_quiz
	def record_tutorial_quiz
		Rails.logger.debug __method__
		params.permit(:started, :confirmation)
		finished = Time.now
		tutorial = {}
		tutorial["started"] = params[:started]
		tutorial["confirmation"] = params[:confirmation]
		tutorial["finished"] = finished
		@evaluation.tutorial_quiz = tutorial	
		if !@evaluation.save
			redirect_to evaluations_tutorial_path, notice: 'Tutorial was not recorded.' 
		end
		redirect_to evaluations_tasks_path, notice: 'Tutorial quiz recorded.' 
	end


	# GET /evaluations/tasks
	def tasks
		Rails.logger.debug __method__
		max_order = 0
		if !@evaluation.tasks.empty?
			max_order = @evaluation.tasks.max_by { |sid,info| info["order"] }[1]["order"]
		end
		Snippet.order('RANDOM()').each do |s|
			sid_str = s.id.to_s
			if !@evaluation.tasks.has_key?(s.id.to_s)
				Rails.logger.debug "TASK missing #{s.id}"
				g = (rand(0..1) == 0 ? 'control' : 'experimental')
				@evaluation.tasks[sid_str] = {
					"started_at": nil,
					"submitted_at": nil,
					"submission": nil,
					"order": max_order,
					"group": g 
				}
				max_order = max_order + 1
			end # if has_key
		end
		@evaluation.tasks = @evaluation.tasks.sort_by { |sid,info| info["order"] }.to_h
		@evaluation.tasks.each do |sid,info|
			Rails.logger.debug "ORDER = #{info['order']}"
			Rails.logger.debug "Snippet = #{sid}"
			if !task_submitted?(info) 
				Rails.logger.debug 'TASK not submitted'
				@evaluation.next_task = sid # index of next task	
				break
			end
			if !tquestionnaire_complete?(info['questionnaire'])
				Rails.logger.debug 'TQuestionnaire not complete'
				Rails.logger.debug info['questionnaire']
				@evaluation.next_task = sid # index of next task	
				break
			end
			
		end
		@evaluation.save
	end


	def task_submitted?(t)
		return true if !t['timeout'].nil?
		return true if !t['submitted_at'].nil?
		return false
	end

	def tquestionnaire_complete?(q)
		Rails.logger.debug __method__
		Rails.logger.debug q
		return false if q.nil?
		return false if q['compiler_output'].nil?
		return false if q['compiler_output'].to_i == -1
		return false if q['possible_patches'].nil?
		return false if q['possible_patches'].to_i == -1
		return true
	end



	# GET /evaluations/task
	def task
		Rails.logger.debug __method__
		if @evaluation.tasks.empty?
			redirect_to evaluations_tasks_path, notice: 'Redirecting to list of tasks' 
		end
		@snippet = Snippet.find(@evaluation.next_task.to_s)
		sid_str = @snippet.id.to_s 
		@task = @evaluation.tasks[sid_str]
		Rails.logger.debug '@task'
		Rails.logger.debug @task
		if !@task["started_at"].nil? 
			if !@task["submitted_at"].nil?
				Rails.logger.debug 'Task already submitted!'
				redirect_to controller: 'evaluations', action: 'task_questionnaire', notice: 'Task already submitted.', snippet_id: sid_str
				return
			end
			started_at = DateTime.rfc3339(@task["started_at"])	
			Rails.logger.debug 'started_at'
			Rails.logger.debug started_at
			min_diff = (Time.now - started_at.to_time) / 60
			if min_diff > 5
				Rails.logger.debug 'Task time expired!'
				@evaluation.tasks[@snippet.id.to_s]["timeout"] = 't';
				@evaluation.save
				redirect_to controller: 'evaluations', action: 'task_questionnaire', snippet_id: sid_str, notice: 'Time expired'
				return
			end
		else
			@evaluation.tasks[sid_str]["started_at"] = Time.now
		end
		@cmd = "clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14"
		@code = @snippet.snippet
		@compiler_output = @snippet.compilation[0]
		@group = @evaluation.tasks[sid_str]['group']
		@evaluation.save
		@started_at = @evaluation.tasks[sid_str]["started_at"]
	end
	

	# POST /evaluations/task_finish
	def task_finish
		Rails.logger.debug __method__
		# TODO: check that submitted_at - started_at <= 5 minutes
		params.require(:code)
		params.require(:snippet_id)
		@snippet = Snippet.find(@evaluation.next_task.to_s)
		if !@snippet.nil?
			if @evaluation.tasks[@snippet.id.to_s]["submitted_at"].nil?
				started_at = DateTime.rfc3339(@evaluation.tasks[@snippet.id.to_s]["started_at"])	
				min_diff = (Time.now - started_at.to_time) / 60
				Rails.logger.debug 'min_diff'
				Rails.logger.debug min_diff
				if min_diff > 5
					@evaluation.tasks[@snippet.id.to_s]["timeout"] = 't';
				else
					@evaluation.tasks[@snippet.id.to_s]["submitted_at"] = Time.now
				end
				@evaluation.tasks[@snippet.id.to_s]["submission"] = params[:code]
				@evaluation.save
			end
		end
		redirect_to controller: 'evaluations', action: 'task_questionnaire', notice: 'Task completed.', snippet_id: @snippet.id
	end


	# GET /evaluations/task_questionnaire
	def task_questionnaire
		Rails.logger.debug __method__
		params.require(:snippet_id)
		@snippet = Snippet.find(params[:snippet_id])
		if @evaluation.tasks[@snippet.id.to_s].nil?
			redirect_to evaluations_tasks_path, notice: 'Invalid task.'	
		end
		if @evaluation.tasks[@snippet.id.to_s]["questionnaire"].nil?
			@questionnaire = {}
			@questionnaire["compiler_output"] = -1 
			@questionnaire["possible_patches"] = -1 
			@evaluation.tasks[@snippet.id.to_s]["questionnaire"] = @questionnaire
			@evaluation.save
		end
		@q= @evaluation.tasks[@snippet.id.to_s]["questionnaire"]
		@group = @evaluation.tasks[@snippet.id.to_s]["group"]
		@cmd = "clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14"
		@code = @snippet.snippet
		@compiler_output = @snippet.compilation[0]
		@scale = helpfulness_scale()
	end


	# POST /evaluations/record_task_questionnaire
	def record_task_questionnaire
		Rails.logger.debug __method__
		params.require(:snippet_id)
		params.require(:compiler_output)
		params.require(:possible_patches)
		@evaluation.tasks[params[:snippet_id]]["questionnaire"]["compiler_output"] = params[:compiler_output]
		@evaluation.tasks[params[:snippet_id]]["questionnaire"]["possible_patches"] = params[:possible_patches]
		@evaluation.save
		
		redirect_to evaluations_tasks_path, notice: 'Task questionnaire recorded.'	
	end


	# GET /evaluations/questionnaire_final
	def questionnaire_final
		Rails.logger.debug __method__
		
		@scale = helpfulness_scale()
		@q = {}
		if !@evaluation.questionnaire.nil?
			@q = @evaluation.questionnaire
			#Rails.logger.debug @q
		else
			redirect_to evaluations_questionnaire_path, notice: 'Please fill out questionnaire.' 
		end
		# render questionnaire		
	end


	# POST /evaluations/record_questionnaire_final
	def record_questionnaire_final
		Rails.logger.debug __method__
		Rails.logger.debug params.inspect
		params.permit(:overall_cem, :overall, :specific, :improvements, :cppexplevel) 
		q = {}
		q[:overall_cem] = params[:overall_cem]
		q[:overall] = params[:overall]
		q[:specific] = params[:specific]
		q[:improvements] = params[:improvements]
		q[:cppexplevel_final] = params[:cppexplevel_final]
		@evaluation.questionnaire.merge!(q)
		if !@evaluation.save
			redirect_to evaluations_questionnaire_final_path, notice: 'Final questionnaire was not recorded.' 
		else
			redirect_to evaluations_thank_you_path, notice: 'Final questionnaire recorded.' 
		end
	end



	# GET /evaluations/thank_you
	def thank_you
		Rails.logger.debug __method__
	end

	def times_report
		Rails.logger.debug __method__
		if current_user.admin?
    	@evaluations = Evaluation.order(:id)
		else
			redirect_to evaluations_consent_path, notice: 'Please read and sign consent form.' 
		end

	end


  # GET /evaluations
  # GET /evaluations.json
  def index
		if current_user.admin?
    	@evaluations = Evaluation.order(:id)
		else
			redirect_to evaluations_consent_path, notice: 'Please read and sign consent form.' 
		end
  end

  # GET /evaluations/1
  # GET /evaluations/1.json
  def show
		if current_user.admin?
    	@evaluation = Evaluation.find(params[:id])
		else
			redirect_to evaluations_consent_path, notice: 'Please read and sign consent form.' 
		end
  end

  # GET /evaluations/new
  def new
    @evaluation = Evaluation.new
  end

  # GET /evaluations/1/edit
  def edit
  end

  # POST /evaluations
  # POST /evaluations.json
  def create
    @evaluation = Evaluation.new(evaluation_params)

    respond_to do |format|
      if @evaluation.save
        format.html { redirect_to @evaluation, notice: 'Evaluation was successfully created.' }
        format.json { render :show, status: :created, location: @evaluation }
      else
        format.html { render :new }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evaluations/1
  # PATCH/PUT /evaluations/1.json
  def update
    respond_to do |format|
      if @evaluation.update(evaluation_params)
        format.html { redirect_to @evaluation, notice: 'Evaluation was successfully updated.' }
        format.json { render :show, status: :ok, location: @evaluation }
      else
        format.html { render :edit }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluations/1
  # DELETE /evaluations/1.json
  def destroy
    @evaluation = Evaluation.find(params[:id])
    @evaluation.destroy
    respond_to do |format|
      format.html { redirect_to evaluations_url, notice: 'Evaluation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evaluation
      #@evaluation = Evaluation.find(params[:id])
			@evaluation = Evaluation.find_by(user_id: current_user.id)
    end


		def validate_consent
			# Evaluation was created?
			if @evaluation.nil?
				redirect_to evaluations_consent_path, notice: 'Consent required.' 
			end
			# Consent valid?
			if !valid_consent(@evaluation.consent)
				Rails.logger.debug 'INVALID consent'
				redirect_to evaluations_consent_path, notice: 'Consent required.' 
			end
		end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evaluation_params
      params.require(:evaluation).permit(:user_id, :status, :code, :questionnaire, :tutorial_quiz)
    end

		def valid_consent(consent_hash)
			Rails.logger.debug __method__
			return false if consent_hash.nil?
			Rails.logger.debug 'consent_hash.inspect'
			Rails.logger.debug consent_hash.inspect
			name = consent_hash["name"]
			return false if name.nil?
			#Rails.logger.debug 'name'
			email = consent_hash["email"]
			return false if email.nil?
			#Rails.logger.debug 'email'
			date = consent_hash["date"]
			return false if date.nil?
			#Rails.logger.debug 'date'
			name_invalid = name.blank?
			email_invalid = email.blank?
			date_invalid = true 
			begin
				d, m, y = date.split("/")
				Date.valid_date? y.to_i, m.to_i, d.to_i
				date_invalid = false 
			rescue ArgumentError => err
				Rails.logger.debug err
				return false
			end
			valid = !name_invalid and !email_invalid and !date_invalid
			#Rails.logger.debug 'valid'
			#Rails.logger.debug valid.inspect
			return valid
		end


		def helpfulness_scale
			{
			'Not at all helpful': 0,
			'Somewhat helpful': 1,
			'Helpful': 2,
			'Very helpful': 3,
			'Not Applicable': 4
		}
		end



end
