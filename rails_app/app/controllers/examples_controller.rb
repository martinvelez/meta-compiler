# sort examples using overlap coefficient and bigrams
#require 'diffy'
 

class ExamplesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_example, only: [:show, :edit, :update, :destroy]

  # GET /examples
  # GET /examples.json
  def index
		@compiler = Compiler.find(params[:compiler_id])
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])
    @examples = Example.all
  end

	# GET /examples/random
  # GET /examples/random.json
	def random
		examples = Example.unscoped.select(:id, :buggy).order("RANDOM()").limit(1)
		@example = examples.first
		Rails.logger.debug @example.inspect
		@example.buggy = "//Example #{@example.id}\n#{@example.buggy}" 
		Rails.logger.debug @example.inspect

		respond_to do |format|
			format.json { render json: @example }
			#format.js { render json: @example, content_type: 'application/json' }
			#format.js { render json: @example, content_type: 'application/json' }
		end
	end

  # GET /examples/1
  # GET /examples/1.json
  def show
		@compiler = Compiler.find(params[:compiler_id])
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])
		@diff = Diffy::Diff.new(@example.buggy, @example.fixed).to_s(:html)
	end





  # GET /examples/1/show2
  # GET /examples/1/show2.json (?)
  def show2
    @example = Example.find(params[:example_id])
		@compiler = Compiler.find(params[:compiler_id])
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])
  end

  # GET /examples/new
  def new
		Rails.logger.debug 'ExamplesController.new'
		@compiler = Compiler.find(params[:compiler_id])
		Rails.logger.debug '@compiler.inspect'
		Rails.logger.debug @compiler.inspect
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])
		Rails.logger.debug '@dm.inspect'
		Rails.logger.debug @dm.inspect
    @example = Example.new
		Rails.logger.debug '@example.inspect'
		Rails.logger.debug @example.inspect
  end

  # GET /examples/1/edit
  def edit
		if current_user.id != @example.creator.id
			redirect_back(fallback_location: root_url)
		end
		@compiler = Compiler.find(params[:compiler_id])
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])
  end

  # POST /examples
  # POST /examples.json
  def create
		Rails.logger.debug 'ExamplesController.create'
    @example = Example.new(example_params)
		@example.diagnostic_message_id = params[:diagnostic_message_id]
		@example.created_by = current_user.id
		@compiler = Compiler.find(params[:compiler_id])
		@dm = DiagnosticMessage.find(params[:diagnostic_message_id])


    respond_to do |format|
      if @example.save
        format.html { redirect_to compiler_diagnostic_message_example_url(params[:compiler_id], params[:diagnostic_message_id], @example), notice: 'Example was successfully created.' }
        format.json { render :show, status: :created, location: @example }
      else
        format.html { render :new }
        format.json { render json: @example.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /examples/1
  # PATCH/PUT /examples/1.json
  def update
    respond_to do |format|
      if @example.update(example_params)
        format.html { redirect_to compiler_diagnostic_message_example_url(params[:compiler_id], params[:diagnostic_message_id], @example), flash: {notice: 'Example was successfully updated.' }}
        format.json { render :show, status: :ok, location: @example }
      else
        format.html { render :edit }
        format.json { render json: @example.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /examples/1
  # DELETE /examples/1.json
  def destroy
		if current_user.id != @example.creator.id	
			render nothing: true, status: :unauthorized
		end
    @example.destroy
    respond_to do |format|
      format.html { redirect_to examples_url, notice: 'Example was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_example
      @example = Example.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def example_params
      params.require(:example).permit(:diagnostic_message_id, :buggy, :fixed, :created_by)
    end
end
