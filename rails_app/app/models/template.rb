require 'net/http'


class Template < ApplicationRecord
  include PgSearch
  default_scope { order('patch_count DESC, examples_count DESC, diagnostic_messages_count DESC, sampled_search_result_count DESC, template ASC') }
	belongs_to :compiler, counter_cache: true
	belongs_to :creator, class_name: User, foreign_key: 'created_by'
	has_many :diagnostic_messages
	has_many :examples, through: :diagnostic_messages

	# 0 (the default) ignores the document length
	# 1 divides the rank by 1 + the logarithm of the document length
	# 2 divides the rank by the document length
	# 4 divides the rank by the mean harmonic distance between extents
	# 8 divides the rank by the number of unique words in document
	# 16 divides the rank by 1 + the logarithm of the number of unique words in document
	# 32 divides the rank by itself + 1

=begin
  pg_search_scope :search_by_template, 
									:against => :template, :using => {
                 		:tsearch => {
											:prefix => true, 					# match prefix
											:dictionary => "simple", 	# literal matching, no stemming
											:normalization => (2 + 16),
											:highlight => {
                        :start_sel => '<b>',
                        :stop_sel => '</b>'
                      }
										}
                  }
=end
	pg_search_scope :search_by_template, 
									:against => :template, :using => {
                 		:trigram => {
										}
                  }

	# A proxy for "importance" is "web search popularity".
	#
	# This method:
	# Convert message to query string
	# Use the Bing Web Search API to automatically search
	# Parse the JSON reponse
	# Get the total estimated matches
	# 
	# return true if all steps succeed
	# return false otherwise
	def update_web_search
		Rails.logger.debug __method__
		q = self.web_search.keys.sample
		if self.web_search[q].class == Hash \
			and self.web_search[q].has_key?('bing_response_code') \
			and !self.web_search[q]['bing_response_code'].nil?
			return false
		end
		response = search_web(q)
		self.web_search[q] = {}
		# <Net::HTTPOK 200 OK readbody=true>
		self.web_search[q]['bing_response_code'] = response.code		
		# {"_type": "SearchResponse", "webPages": {"webSearchUrl": "https:\/\/www.bing.com\/cr?IG=843576B0148445688D0B617CB9A0EADE&CID=36B5A80EB0FB6B0C3472A2EFB1A46AD4&rd=1&h=YHDVuwHRdl5iOmgWrzycOhkBC34D-8HKAOKQz77VlSs&v=1&r=https%3a%2f%2fwww.bing.com%2fsearch%3fq%3d%2522no%2bviable%2bconversion%2bfrom%2522%2b%2522to%2522&p=DevEx,5292.1", "totalEstimatedMatches": 56700, "value": ...	
		self.web_search[q]['bing_response_body'] = response.body.encode('UTF-8',invalid: :replace, undef: :replace)
		self.web_search[q]['bing_totalEstimatedMatches_present'] = false
		self.web_search[q]['bing_webPages_present'] = false

		Rails.logger.debug ('Before JSON.parse')	
		result = JSON.parse(response.body)
		Rails.logger.debug ('After JSON.parse')	
		if result.has_key?("webPages")
			self.web_search[q]['bing_webPages_present'] = true
			Rails.logger.debug ('bing_webPages_present')	
			if result["webPages"].has_key?('totalEstimatedMatches')
				Rails.logger.debug ('totalEstimatedMatches')	
				self.web_search[q]['bing_totalEstimatedMatches_present'] = true
				self.web_search[q]['bing_totalEstimatedMatches'] = result['webPages']['totalEstimatedMatches'].to_i 
				
			end	
		end 	

		if self.save
			Rails.logger.debug "SUCCESS: Updated Web Search."
			return true
		end	

		Rails.logger.debug "ERROR: Failed to save count."
		return false
	end


	# Make a call to the Bing Web Search Api v5 using the curl command
	#
	# TODO: Figure out how to make exact queries, e.g., "expected" "after"
	# params: none
	def search_web_curl(query)
		Rails.logger.debug __method__
		key = ENV['BING_KEY']
		q = CGI.escape(query)
		q = q.gsub!('%22', '"')
		cmd = "curl --header \"Ocp-Apim-Subscription-Key: #{key}\" https://api.cognitive.microsoft.com/bing/v5.0/search?q=#{q}&mkt=en-us"
		Rails.logger.debug 'cmd'
		Rails.logger.debug cmd
		o = `#{cmd}`
		return o
	end


	# Make a call to the Bing Web Search Api v5
	# Documentation: https://dev.cognitive.microsoft.com/docs/services/56b43eeccf5ff8098cef3807/operations/56b4447dcf5ff8098cef380d
	#
	# need response status and reponse body
	# params: String query 
	def search_web(query)
		sleep(5)
		Rails.logger.debug __method__
		uri = URI('https://api.cognitive.microsoft.com/bing/v5.0/search')
		Rails.logger.debug 'uri'
		uri.query = URI.encode_www_form({
				# Request parameters
				'q' => query
				#'count' => '10',
				#'offset' => '0',
				#'mkt' => 'en-us'
				#'safesearch' => 'Moderate'
		})

		request = Net::HTTP::Get.new(uri.request_uri)
		Rails.logger.debug 'request'
		# Request headers
		request['Ocp-Apim-Subscription-Key'] = ENV['BING_KEY']
		# Request body
		request.body = "{body}"

		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
			http.request(request)
		end
		Rails.logger.debug 'response'
		
		return response
	end # search_web


	def sample_query
		processed_queries = self.web_search.select { |q, h| h.class == Hash }
		processed_queries = processed_queries.select { |q,h| h['bing_response_code'].to_i == 200 }
		Rails.logger.debug processed_queries
		q = processed_queries.keys.sample
		Rails.logger.debug q
		if !q.nil?
			self.sampled_query = q
			self.sampled_search_result_count = 0 
			if self.web_search[q].has_key?('bing_totalEstimatedMatches')
				self.sampled_search_result_count = self.web_search[q]['bing_totalEstimatedMatches']
			end
		end
		if self.save
			Rails.logger.debug 'SUCCESS: sampled query.'
			return true
		end
		Rails.logger.debug 'ERROR: failed to sample query.'
		return false 
	end

	def set_patches
		patches = {All: 0}
		self.examples.each do |e|
			if patches.has_key?(e.patch)
				patches[e.patch] += 1
			else
				patches[e.patch] = 1
			end
			patches[:All] += 1
		end
		self.patches = patches.sort_by { |p,count| count * -1 }.to_h
	end

	def set_patch_count
		self.set_patches # refresh patches
		self.patch_count = (self.patches.keys - ['All']).size
		Rails.logger.debug 'self.patch_count'
		Rails.logger.debug self.patch_count
		self.save
	end

end
