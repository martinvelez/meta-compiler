class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	devise :omniauthable, :omniauth_providers => [:google_oauth2, :facebook]
	has_many :examples
	has_many :diagnostic_examples
	has_many :evaluations

	def self.from_omniauth(auth)
		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
			user.email = auth.info.email
			user.password = Devise.friendly_token[0,20]
			user.name = auth.info.name   # assuming the user model has a name
			user.first_name = user.name.split(' ')[0]   # assuming the user model has a name
			user.last_name = user.name.split(' ')[1]   # assuming the user model has a name
			user.image_url = auth.info.image # assuming the user model has an image
			#user.skip_confirmation!
		end
	end
end
