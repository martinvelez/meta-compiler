class Compiler < ApplicationRecord
	has_many :diagnostic_messages
	has_many :examples, through: :diagnostic_messages
	has_many :templates
	belongs_to :creator, class_name: User, foreign_key: 'created_by'

	def templates_with_examples
		return self.templates.select { |t| t.examples.size > 0 }
	end

	def templates_without_examples
		return self.templates.select { |t| t.examples.size == 0 }
	end

	def select_by_component(components=['Lex','Parse','Sema','CodeGen'])
		ts = self.templates.select do |t|
			!(t.components & components).empty?
		end
		return ts
	end

	def summarize_by_component(components=['Lex','Parse','Sema','CodeGen'])
		Rails.logger.debug __method__
		ts = select_by_component(components)
		Rails.logger.debug 'ts.size'
		Rails.logger.debug ts.size
		summary = { All: {examples: 0, total: 0, covered: 0, coverage: 0.0} }
		ts.each do |t|
			covered = (t.examples.size > 0)
			t.components.each do |c|
				next if !components.include?(c)
				if summary.has_key?(c)
					summary[c][:examples] = summary[c][:examples] + t.examples.size
					summary[c][:total] = summary[c][:total] + 1
					summary[c][:covered] = summary[c][:covered] + 1 if covered
				else
					summary[c] = {}
					summary[c][:examples] = t.examples.size
					summary[c][:total] = 1
					summary[c][:covered] = 1 if covered
				end
			end
			summary[:All][:examples] = summary[:All][:examples] + t.examples.size
			summary[:All][:total] = summary[:All][:total] + 1
			summary[:All][:covered] = summary[:All][:covered] + 1 if covered
		end
		summary.each do |c,h|
			h[:coverage] = (h[:covered].to_f / h[:total]) * 100
			h[:coverage] = h[:coverage].round(1)
		end
		return summary
	end


	def set_summary_by_selected_components
		Rails.logger.debug __method__
		self.summary_by_selected_components = self.summarize_by_component.to_json
		self.save
	end

end
