require 'test_helper'

class DiagnosticMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diagnostic_message = diagnostic_messages(:one)
  end

  test "should get index" do
    get diagnostic_messages_url
    assert_response :success
  end

  test "should get new" do
    get new_diagnostic_message_url
    assert_response :success
  end

  test "should create diagnostic_message" do
    assert_difference('DiagnosticMessage.count') do
      post diagnostic_messages_url, params: { diagnostic_message: { compiler_id: @diagnostic_message.compiler_id, created_by: @diagnostic_message.created_by, message: @diagnostic_message.message, name: @diagnostic_message.name } }
    end

    assert_redirected_to diagnostic_message_url(DiagnosticMessage.last)
  end

  test "should show diagnostic_message" do
    get diagnostic_message_url(@diagnostic_message)
    assert_response :success
  end

  test "should get edit" do
    get edit_diagnostic_message_url(@diagnostic_message)
    assert_response :success
  end

  test "should update diagnostic_message" do
    patch diagnostic_message_url(@diagnostic_message), params: { diagnostic_message: { compiler_id: @diagnostic_message.compiler_id, created_by: @diagnostic_message.created_by, message: @diagnostic_message.message, name: @diagnostic_message.name } }
    assert_redirected_to diagnostic_message_url(@diagnostic_message)
  end

  test "should destroy diagnostic_message" do
    assert_difference('DiagnosticMessage.count', -1) do
      delete diagnostic_message_url(@diagnostic_message)
    end

    assert_redirected_to diagnostic_messages_url
  end
end
