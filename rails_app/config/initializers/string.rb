require 'open3'


class String

def tokenizer_output(pl = 'c++')
		cmd = "clang++-3.9 -cc1 -dump-tokens -Wfatal-errors -stdlib=libc++ -std=c++14 -x c++ -"
		stdout_and_stderr_str, status = Open3.capture2e(cmd, stdin_data: self)
		#Rails.logger.debug 'stdout_and_stderr_str'
		#Rails.logger.debug stdout_and_stderr_str
		return stdout_and_stderr_str 
	end

	
	# Tokenize C++ Code using clang++
	def tokenize(pl = 'c++')
		#Rails.logger.debug __method__
		tokens = []
		self.tokenizer_output.lines.each do |line|
			if line =~ /Loc/
				tokens << line.split.first
			end
			#Rails.logger.debug 'tokens'
			#Rails.logger.debug tokens 
		end
		return tokens
	end	

	def lexemize(pl = 'c++')
		#Rails.logger.debug __method__
		lexemes = []
		#Rails.logger.debug 'stdout_and_stderr_str'
		#Rails.logger.debug stdout_and_stderr_str
		self.tokenizer_output.lines.each do |line|
			if line =~ /Loc/
				line.gsub!(/\[StartOfLine\]/,'')
				line.gsub!(/\[LeadingSpace\]/,'')
				line.gsub!(/Loc=<.*>/,'')
				line.strip!
				line.sub!(/^\w+\s/,'')
				line[0] = ''
				line[-1] = ''
				#Rails.logger.debug line
				lexemes << line
			end
		end
		return lexemes
	end

	


	# sudo apt-get install libc++-dev
	# SHOULD: clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14
	# CURRENT: clang++-3.9 -c -Wfatal-errors -std=c++14 -x c++ -
	def compile(pl = 'c++')
		#Rails.logger.debug __method__
		cmd = "clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14 -x c++ -"
		stdout_and_stderr_str, status = Open3.capture2e(cmd, stdin_data: self)
		#Rails.logger.debug stdout_and_stderr_str
		return stdout_and_stderr_str, status
	end


	def extract_diagnostic_message(pl = 'c++')
		compiler_output = self
		#Rails.logger.debug __method__
		return nil if compiler_output.nil?
		# parse line with "error:" to get diagnostic message
		dm = ""
		compiler_output.each_line do |line|
			if line =~ /error\:/ 
				dm = line.split("error:")[1].strip
			end                                                                             
		end 

		return dm		
	end


	# sudo apt-get install clang-format-3.9
	def format(style = 'Chromium')
		#Rails.logger.debug __method__
		cmd = "clang-format-3.9 -style=#{style}"
		#Rails.logger.debug cmd
		stdout_and_stderr_str, status = Open3.capture2e(cmd, stdin_data: self)
		#Rails.logger.debug stdout_and_stderr_str 
		return stdout_and_stderr_str
	end


	def to_template
		Rails.logger.debug __method__
		delim = '*****'

		copy = self.dup
		indices = copy.enum_for(:scan,/'/).map { Regexp.last_match.begin(0) }
		if indices.size % 2 != 0
			p_of_i = []
			if !copy.index("function's").nil?
				p_of_i = indices[1..2]	
			elsif !copy.index("constructor's").nil?
				p_of_i = indices[1..2]	
			elsif !copy.index("attribute's").nil?
				p_of_i = indices[0..1]	
			else 
				return nil
			end
			copy[p_of_i[0]..p_of_i[1]] = delim
			Rails.logger.debug "copy"
			Rails.logger.debug "#{copy.inspect}"
		else
			while !copy.index("'").nil?
				p_of_i = copy.enum_for(:scan,/'/).map { Regexp.last_match.begin(0) }.first(2)
				copy[p_of_i[0]..p_of_i[1]] = delim
				Rails.logger.debug "copy"
				Rails.logger.debug "#{copy.inspect}"
			end
		end
		phrases = copy.split(delim)
		phrases.map! { |phrase| phrase.strip } 
		Rails.logger.debug "#{self}"
		#Rails.logger.debug "#{copy}"
		Rails.logger.debug "#{phrases.inspect}"
		#phrases.map! do |phrase|
		#	if !phrase.blank?
		#		phrase.prepend('"')
		#		phrase << '"'
		#	end
		#end
		phrases = phrases.compact
		Rails.logger.debug "#{phrases.inspect}"
		return phrases.join(' ')
	end

	def fix_str_to_html
		parts = self.split(' ')
		parts.map! do |p| 
			if p == 'Insert'
				p = p.gsub('Insert',"<span style='color:green'>Insert</span>&nbsp;")
			elsif p == 'Delete'
				p = p.gsub('Delete',"<span style='color:red'>Delete</span>&nbsp;")
			elsif p == 'and'
				p = p + '&nbsp;'
			else
				p = "<span class='token'>" + escapeHTML(p) + "</span>&nbsp;"
			end
		end 
		parts.join(' ')
	end

	def autoexperiment(patch)
		insts = patch.split(', ')
		inst = insts[0]
		return 0 if inst.nil?
		op = inst.split(' ')[0]
		token = inst.split(' ')[1]
		tokens = self.tokenize('c++')
		lexemes = self.lexemize('c++')
		abstract_tokens = ['char_constant',
			'identifier','numeric_constant','string_literal','utf16_string_literal', 'wide_string_literal']
		if op == 'Delete'
			status = 0
			if !abstract_tokens.include?(token)
				
				Rails.logger.debug "token = #{token}"
				begin 
					lexeme = token.cpp_token_to_lexeme
					re = Regexp.new(Regexp.escape(lexeme))
					#Thread.abort_on_exception = true
					#threads = []
					self.enum_for(:scan, re).map { Regexp.last_match.begin(0) }.each do |idx|
						#begin 
							#threads << Thread.new do
								#Rails.logger.debug "idx = #{idx}"
								tmp = self.dup
								tmp[idx..(idx+(lexeme.size-1))] = ''
									
								compilation = tmp.compile
								if compilation[1].exitstatus == 0
									#Rails.logger.debug '########## COMPILES #########'
									#Rails.logger.debug 'BUGGY'
									#Rails.logger.debug self
									#Rails.logger.debug 'FIXED'
									#Rails.logger.debug tmp
									#raise 'compiles'
									return 1
								end # if
							#end # Thread
						#rescue Exception => e
							#return 1
						#end
					end # enum_for
					#begin
					#	threads.each do |t|
					#		t.abort_on_exception
					#		t.join
					#	end
					#rescue Exception => e
					#	return 1
					#end
				rescue Exception => e
					Rails.logger.debug "ERROR: coverting #{token} to lexeme"
				end
			else # abstract token
				#threads = []
				tokens.each_with_index do |t,i|
					next if t != token	
					re = Regexp.new(Regexp.escape(lexemes[i]))
					self.enum_for(:scan, re).map { Regexp.last_match.begin(0) }.each do |idx|
						#begin 
						#	threads << Thread.new do
								#Rails.logger.debug "idx = #{idx}"
								tmp = self.dup
								tmp[idx..(idx+lexemes[i].size-1)] = ''
								
								compilation = tmp.compile
								if compilation[1].exitstatus == 0
									#Rails.logger.debug '########## COMPILES #########'
									#Rails.logger.debug 'BUGGY'
									#Rails.logger.debug self
									#Rails.logger.debug 'FIXED'
									#Rails.logger.debug tmp
									#raise 'compiles'
									return 1
								end # if
							#end # Thread
						#rescue Exception => e
							#return 1
						#end # begin
					end # enum_for
				end # tokens.each		
				#begin
				#	threads.each do |t|
				#		t.abort_on_exception
				#		t.join
				#	end
				#rescue Exception => e
				#	return 1
				#end
			end # if !abstract token
		elsif op == 'Insert'
			if !abstract_tokens.include?(token)
				Rails.logger.debug "token = #{token}"
				lexeme = token.cpp_token_to_lexeme
				Rails.logger.debug "lexeme = #{lexeme}"
				re = Regexp.new(Regexp.escape(lexeme))
				#threads = []
				#Thread.abort_on_exception = true

				#Rails.logger.debug 'SELF START' 
				#Rails.logger.debug lexemes.size
				#Rails.logger.debug 'SELF END' 
				begin	
					results = Parallel.map(0..lexemes.size, in_threads: 8) do |i|	
						nlexemes = lexemes.dup
						nlexemes.insert(i,lexeme)
						loc = self.index(lexemes.first)
						tmp = "//ORIGINAL HEADER\n"
						tmp += self.first(loc)
						tmp += "//MODIFIED\n"
						tmp += nlexemes.join(' ')
						
						compilation = tmp.compile
						if compilation[1].exitstatus == 0
=begin
							Rails.logger.debug '########## COMPILES #########'
							Rails.logger.debug 'BUGGY'
							Rails.logger.debug self
							Rails.logger.debug 'FIXED'
							Rails.logger.debug tmp
=end
							#raise Parallel::Break	
							return 1
						else
=begin
							if lexeme == 'struct'
								Rails.logger.debug "LOC = #{loc}"
								Rails.logger.debug 'TMP START' 
								Rails.logger.debug tmp
								Rails.logger.debug 'TMP END' 
							end
=end
						end # if
					end # each lexeme
				rescue Exception => e
					Rails.logger.debug "--------------------EXCEPTION"
					Rails.logger.debug e
					return 1
				end
				return 0
			else # abstract token
				# TODO
			end # if !abstract token
		elsif op == 'Replace'
			# TODO
		end


		#Rails.logger.debug '########## DOES NOT FIX #########'
		return 0
	end

	def cpp_token_to_lexeme

		'string_literal'
		h = {
			'__alignof':'__alignof',
			'__builtin_offsetof':'__builtin_offsetof',
			'_Complex':'_Complex',
			'__func__':'__func__',
			'__null':'__null',
			alignas:'alignas',
			asm:'asm',
			auto:'auto',
			bool:'bool',
			catch:'catch',
			char:'char',
			# abstract
			'char_constant':'char_constant',
			class:'class',
			const:'const',
			constexpr:'constexpr',
			decltype:'decltype',
			delete:'delete',
			double:'double',
			enum:'enum',
			explicit:'explicit',
			extern:'extern',
			false:'false',
			float:'float',
			for:'for',
			friend: 'friend',
			goto:'goto',
			if: 'if',
			# abstract
			identifier: 'identifier', 
			inline: 'inline',
			int: 'int',
			long: 'long',
			mutable:'mutable',
			namespace: 'namespace',
			new: 'new',
			noexcept: 'noexcept',
			nullptr: 'nullptr',
			# abstract
			'numeric_constant':'numeric_constant',
			operator:'operator',
			'__PRETTY_FUNCTION__':'__PRETTY_FUNCTION__',
			private:'private',
			protected:'protected',
			public:'public',
			return:'return',
			short:'short',
			signed:'signed',
			sizeof:'sizeof',
			static:'static',
			'static_assert':'static_assert',
			# abstract
			'string_literal':'string_literal', 
			struct:'struct',
			switch:'switch',
			template:'template',
			this:'this',
			throw:'throw',
			true:'true',
			typeid:'typeid',
			typedef:'typedef',
			typename:'typename',
			typeof:'typeof',
			union:'union',
			unsigned:'unsigned',
			using:'using',
			# abstract
			'utf16_string_literal': 'utf16_string_literal',
			virtual:'virtual',
			void:'void',
			volatile:'volatile',
			'wchar_t':'wchar_t',
			while:'while'
		}
		return self if h.has_key?(self.to_sym)

if self == "l_paren"
return '('
elsif self == 'r_paren'
return ')'
elsif self == 'amp'
return '&'
elsif self == 'ampamp'
return '&&'
elsif self == 'ampequal'
return '&='
elsif self == 'semi'
return ';'
elsif self == 'l_square'
return '['
elsif self == 'r_square'
return ']'
elsif self == 'l_brace'
return '{'
elsif self == 'r_brace'
return '}'
elsif self == 'star'
return '*'
elsif self == 'starequal'
return '*='
elsif self == 'less'
return '<'
elsif self == 'lessless'
return '<<'
elsif self == 'lesslessequal'
return '<<='
elsif self == 'lessequal'
return '<='
elsif self == 'greater'
return '>'
elsif self == 'greatergreater'
return '>>'
elsif self == 'greatergreaterequal'
return '>>='
elsif self == 'greaterequal'
return '>='
elsif self == 'colon'
return ':'
elsif self == 'coloncolon'
return '::'
elsif self == 'comma'
return ','
elsif self == 'equal'
return '='
elsif self == 'equalequal'
return '=='
elsif self == 'period'
return '.'
elsif self == 'periodstar'
return '.*'
elsif self == 'exclaim'
return '!'
elsif self == 'exclaimequal'
return '!='
elsif self == 'question'
return '?'
elsif self == 'plus'
return '+'
elsif self == 'plusplus'
return '++'
elsif self == 'plusequal'
return '+='
elsif self == 'minus'
return '-'
elsif self == 'minusminus'
return '--'
elsif self == 'minusequal'
return '-='
elsif self == 'arrow'
return '->'
elsif self == 'arrowstar'
return '->*'
elsif self == 'pipe'
return '|'
elsif self == 'pipepipe'
return '||'
elsif self == 'pipeequal'
return '|='
elsif self == 'slash'
return '/'
elsif self == 'caret'
return '^'
elsif self == 'percent'
return '%'
elsif self == 'tilde'
return '~'
elsif self == 'ellipsis'
return '...'
elsif self == '__attribute'
return '__attribute__'
end
	end

end
