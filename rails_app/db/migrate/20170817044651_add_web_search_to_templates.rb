class AddWebSearchToTemplates < ActiveRecord::Migration[5.0]
  def change
		add_column :templates, :web_search, :json, default: {}
  end
end
