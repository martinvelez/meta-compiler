class AddBingSearchResultCountToDiagnosticMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :diagnostic_messages, :bing_search_result_count, :integer, default: 0
  end
end
