class AddTasksToEvaluation < ActiveRecord::Migration[5.0]
  def change
		add_column :evaluations, :tasks, :json, default: {}
  end
end
