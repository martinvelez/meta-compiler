class AddTemplateIdToDiagnosticMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :diagnostic_messages, :template_id, :integer
  end
end
