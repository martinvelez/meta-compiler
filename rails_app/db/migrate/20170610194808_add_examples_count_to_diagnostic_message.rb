class AddExamplesCountToDiagnosticMessage < ActiveRecord::Migration[5.0]
  def change
		add_column :diagnostic_messages, :examples_count, :integer, default: 0
  end
end
