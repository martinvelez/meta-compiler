class AddGradesToEvaluation < ActiveRecord::Migration[5.0]
  def change
		add_column :evaluations, :grades, :json, default: {}
  end
end
