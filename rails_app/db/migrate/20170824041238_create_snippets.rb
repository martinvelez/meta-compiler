class CreateSnippets < ActiveRecord::Migration[5.0]
  def change
    create_table :snippets do |t|
      t.text :snippet
      t.integer :diagnostic_message_id
      t.string :tokens, array: true, default: []
      t.json :compilation
      t.text :source
			t.integer :created_by

      t.timestamps
    end
  end
end
