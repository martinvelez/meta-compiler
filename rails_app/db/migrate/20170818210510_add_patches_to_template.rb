class AddPatchesToTemplate < ActiveRecord::Migration[5.0]
  def change
		add_column :templates, :patches, :json, default: {}
  end
end
