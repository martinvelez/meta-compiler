class AddSummaryByComponentsToCompiler < ActiveRecord::Migration[5.0]
  def change
    add_column :compilers, :summary_by_components, :json, default: {}
    add_column :compilers, :summary_by_selected_components, :json, default: {}
  end
end
