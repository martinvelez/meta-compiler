class AddNextTaskToEvaluation < ActiveRecord::Migration[5.0]
  def change
    add_column :evaluations, :next_task, :integer
  end
end
