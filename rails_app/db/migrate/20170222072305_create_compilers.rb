class CreateCompilers < ActiveRecord::Migration[5.0]
  def change
    create_table :compilers do |t|
      t.string :family
      t.string :name
      t.text :description
      t.string :url
      t.integer :created_by

      t.timestamps
    end
  end
end
