class AddQueryStringToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :templates, :query_string, :text
  end
end
