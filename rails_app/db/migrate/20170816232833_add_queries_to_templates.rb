class AddQueriesToTemplates < ActiveRecord::Migration[5.0]
  def change
		add_column :templates, :queries, :text, array: true, default: []
		add_column :templates, :search_result_counts, :integer, array: true, default: [] 
		add_column :templates, :sampled_query, :text
		add_column :templates, :sampled_search_result_count, :integer, default: 0
		remove_column :templates, :query_string
		remove_column :templates, :bing_search_result_count
  end
end
