class AddDiagnosticMessagesCountToCompiler < ActiveRecord::Migration[5.0]
  def change
    add_column :compilers, :diagnostic_messages_count, :integer
  end
end
