class AddCompilationToExamples < ActiveRecord::Migration[5.0]
  def change
		add_column :examples, :buggy_compilation_output, :text
		add_column :examples, :buggy_compilation_status, :integer
		add_column :examples, :fixed_compilation_output, :text
		add_column :examples, :fixed_compilation_status, :integer
  end
end
