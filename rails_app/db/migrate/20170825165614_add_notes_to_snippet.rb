class AddNotesToSnippet < ActiveRecord::Migration[5.0]
  def change
    add_column :snippets, :notes, :text
  end
end
