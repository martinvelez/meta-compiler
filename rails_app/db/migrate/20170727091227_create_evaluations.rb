class CreateEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :evaluations do |t|
      t.integer :user_id
      t.integer :status
      t.string :code
      t.json :questionnaire
      t.json :tutorial_quiz

      t.timestamps
    end
  end
end
