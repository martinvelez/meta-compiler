class AddBingSearchResultCountToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :templates, :bing_search_result_count, :integer
  end
end
