class CreateExamples < ActiveRecord::Migration[5.0]
  def change
    create_table :examples do |t|
      t.integer :diagnostic_message_id
      t.text :buggy
      t.text :fixed
      t.integer :created_by

      t.timestamps
    end
  end
end
