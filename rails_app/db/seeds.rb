# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.find_or_create_by(email: 'mvelez999@yahoo.com', first_name: 'Martin', last_name: 'Velez')
user.password = ENV['ADMIN_PASSWORD']
user.password_confirmation = ENV['ADMIN_PASSWORD']
user.save!


c = Compiler.find_or_create_by(name: "clang++-3.9")
c.description = %{The goal of the Clang project is to create a new C based language front-end: C, C++, Objective C/C++, OpenCL C and others for the LLVM compiler. You can get and build the source today.}
c.created_by = user.id
c.family = "Clang"
c.save!

d = DiagnosticMessage.find_or_create_by(compiler_id: c.id, name: "err_expected_semi_after_expr", message: "expected ';' after expression", created_by: user.id)


buggy = %{#include <iostream>

using namespace std;

int main()
{
	cout << "Hello World!" << endl
	return 0;
}}

fixed = %{#include <iostream>

using namespace std;

int main()
{
	cout << "Hello World!" << endl;
	return 0;
}}

Example.create(diagnostic_message_id: d.id, buggy: buggy, fixed: fixed, created_by: user.id) 
