#include <iostream>
#include <functional>
#include <type_traits>

template<typename TFunc, typename Func, typename TRet, typename... TArgs>
static std::enable_if_t<!std::is_void<TRet>::value, TRet>
	Invoker(TArgs&&... Args) {
	return Func(1, std::forward<TArgs>(Args)...);
}

template<typename TFunc, typename Func, typename TRet, typename... TArgs>
static std::enable_if_t<std::is_void<TRet>::value, TRet>
	Invoker(TArgs&&... Args) {
	Func(1, std::forward<TArgs>(Args)...);
}

template<typename TFunc, typename... TArgs, typename TRet = std::result_of_t<TFunc(int, TArgs...)>>
void* make_invoker_ptr(std::function<TFunc(int, TArgs...)> Func) {
	auto fn = Invoker<TFunc, Func, TRet, TArgs...>;
	return reinterpret_cast<void*>(fn);
}

template<typename TFunc, typename... TArgs, typename TRet = std::result_of_t<TFunc(int, TArgs...)>>
TRet (*make_invoker_fn(std::function<TFunc(int, TArgs...)> Func))(TArgs&&...) {
	return Invoker<TFunc, Func, TRet, TArgs...>;
}

void f1(int a) {
	std::cout << "f1: " << a << std::endl;
}
void f2(int a, int b) {
	std::cout << "f2: " << a << ' ' << b << std::endl;
}
int f3(int a) {
	std::cout << "f3: " << a << std::endl;
	return 3;
}
int f4(int a, int b) {
	std::cout << "f4: " << a << ' ' << b << std::endl;
	return 4;
}

int main() {
	make_invoker_ptr<decltype(f1)*>(f1); //What is wrong with this???
	make_invoker_ptr<decltype(f2)*, int>(f2);
	make_invoker_ptr<decltype(f3)*>(f3);
	make_invoker_ptr<decltype(f4)*, int>(f4);

	auto fn1 = make_invoker_fn<decltype(f1)*>(f1);
	auto fn2 = make_invoker_fn<decltype(f2)*, int>(f2);
	auto fn3 = make_invoker_fn<decltype(f3)*>(f3);
	auto fn4 = make_invoker_fn<decltype(f4)*, int>(f4);

	//fn1();
	//fn2(2);
	//fn3();
	//fn4(4);

	return 0;
}