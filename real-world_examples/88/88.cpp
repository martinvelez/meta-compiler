//New empty tree
struct node *newTreeNode(int data)
{
    //New tree nodes
    node *node = new node;
    //New data node
    node->data = data;
    //New left node
    node->left = nullptr;
    //New right node
    node->right = nullptr;

    return node;
}
