Put the real-world examples of buggy code you find here.

1. Copy-paste the compiler error into Google.
2. Inspect each retrieved web page for code snippets.
3. Keep only those examples that require a one-token level fix.


=====
ID: 100
Message: NA
Source: NA
=====
None
=====
ID: 99
Message: invalid operands to binary expression ('const char *' and 'int')
Source: https://cboard.cprogramming.com/c-programming/142274-why-does-compiler-say-invalid-operands-binary-have-char*-int.html
=====
#include <stdio.h>

int main() {
  int num1; /*Variable to hold num*/
  printf("Please enter a number:\n");
  scanf("%d" &num1);
  if (num1 % 2 == 0) /*If clause to determine if number is prime*/
  {
    printf("Number %d is a prime number.\n", num1);
  } else {
    printf("Number %d is not a prime number.\n", num1);
  }
  return 0; /*Exits the program*/
}
=====
ID: 98
Message: NA
Source: NA
=====
None
=====
ID: 97
Message: cannot initialize a parameter of type 'int *' with an rvalue of type 'int (*)[20]'
Source: https://stackoverflow.com/questions/40276731/cannot-initialize-a-parameter-of-type-int-with-an-rvalue-of-type-int-20
=====
template <typename T>
class SelectionSort {
 public:
  void stuffNum(T* object, int size, int min, int max) {
    for (int i = 0; i < size; i++) {
      object[i] = 5;
    }
  }
};

int main() {
  SelectionSort<int> sorterInt;

  int test_array[20];
  sorterInt.stuffNum(&test_array, 20, 1, 200);
}
=====
ID: 96
Message: NA
Source: NA
=====
None
=====
ID: 95
Message: must use 'struct' tag to refer to type 'node' in this scope
Source: https://stackoverflow.com/questions/30053403/error-about-a-node-in-c
=====
struct node {
  int data;
  struct node* left;
  struct node* right;
};

// New empty tree
struct node* newTreeNode(int data) {
  // New tree nodes
  node* node = new node;
  // New data node
  node->data = data;
  // New left node
  node->left = nullptr;
  // New right node
  node->right = nullptr;

  return node;
}
=====
ID: 94
Message: pack expansion does not contain any unexpanded parameter packs
Source: https://github.com/t-crest/patmos-clang/blob/master/test/CXX/expr/expr.prim/expr.prim.lambda/p23.cpp
=====
template <typename T, int... Values>
void bogus_expansions(T x) {
  auto l1 = [x...] {};
  auto l2 = [Values...] {};
}
=====
ID: 93
Message: declaration of reference variable 'var' requires an initializer
Source: http://www.thegeekstuff.com/2013/05/cpp-reference-variable/
=====
#include <iostream>

int main(void) {
  int& var;

  /* Assume some logic here*/

  return 0;
}
=====
ID: 92
Message: NA
Source: NA
=====
None
=====
ID: 91
Message: expected ';' after expression
Source: https://stackoverflow.com/questions/38163457/errors-expected-after-expression-and-expression-result-unused
=====
#include <stdio.h>

int main(void) {
  printf("How tall do you want your pyramid to be?\n");
  int height = 10;

  if (height > 23 && height < 1) {
    printf("Please use a positive number no greater than 23:\n");
  } else
    (height > 0 && height <= 23) { printf("Thanks!\n"); }
}
=====
ID: 90
Message: NA
Source: NA
=====
None




= Log of findings =

```
* 70:
* 71:
	[] worked by removing `windows.h` and commenting `Sleep()` calls
[x] 72:
[x] 73:
[x] 74:
[x] 75:
[x] 76:
	[!] The metacompiler shows no possible fixes for the example
	[+] It does now.  Insert "static" before "T pi".  Correct suggestion is 17th out of 30.
[ ] 77:
[x] 78:
[ ] 79: Very short error message
[x] 80:
[x] 81:
    [!] No fix available
[.] 82: Dumb example from CS50 stack exchange
    [!] No fix available
[x] 83:
    [!] No fix available
[ ] 84: Could not find in first page
[.] 85: Not really a real-world example
[ ] 86: Not meaningful if not exact error message
[ ] 87: Not found in first page
[x] 88:
[ ] 89: Could not find a C++ example in the first page (mostly objective-c)
[x] 90:
    [!] crashed martin's website
[.] 91:
    [ ] copied from [https://ideone.com/hsdqJy]
    [ ] with two modifications to get to the right error
[.] 92: found in a very esoteric place. (was in search results) [https://github.com/Microsoft/clang/blob/master/test/SemaCXX/references.cpp]
[x] 93:
    [ ] second search result worked
    [!] No fixes available
[x] 94: added `using namespace std`
    [ ] first few search results didn't work.
    [ ] multi-file
[.] 95: Got a slightly different error message `use of class template 'Foo' requires template arguments`
    [ ] used 2nd search result. could not trigger with first result.
[.] 96: could trigger almost the same error. also added a main function
[x] 97: worked by adding class definition
[x] 98: worked by including `string`
    [ ] multi file
```
