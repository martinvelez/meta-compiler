template < class T > struct A;

template <> struct A < int > 
{ 
  static int const _Traits;
};

template <> int const A < int >::_Traits = 1;

