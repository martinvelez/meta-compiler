#pragma once


void someFunction();

class Foo
{

public:
    inline void doStuff() { someFunction(); }

};

//COMPILER OUTPUT
/*
$ clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14
<stdin>:1:9: warning: #pragma once in main file [-Wpragma-once-outside-header]
#pragma once
        ^
1 warning generated.
*/
