class A
{
public:
    int foo();
};

class B
{
public:
    int A::foo(){ return 0; }; //error: non-friend class member 'foo' cannot have a qualified name
};

int main(){ }