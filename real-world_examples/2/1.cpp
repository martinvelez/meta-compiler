#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "Library.h" // include header of Library class
#include "Game.h" // include header of Game class
#include "DVD.h" // include header of DVD class
#include "Book.h" // include header of Book class
using namespace std;

void Library::insertGame( char gameName[], char platform[], int c){
    strcpy( collection[numGames].name, gameName);
    strcpy( collection[numGames].platform, platform);
    collection[numGames].copies = c;
    cout << "Game added to collection.\n";
    ++numGames;
}

void Library::deleteGame( char gameName[]){
    int i;
    for( i = 0; i < numGames; i++){
        if( strcmp( gameName, collection[i].name) == 0){
            collection[i].copies--;
            cout << "Game deleted from collection.\n";
            return;
        }
    }
    cout << "Game not found.\n";
}

void Library::insertDVD( char dvdName[], char director[], int c){
    strcpy( collection1[numDVDs].name, dvdName);
    strcpy( collection1[numDVDs].director, director);
    collection1[numDVDs].copies = c;
    cout << "DVD added to collection.\n";
    ++numDVDs;
}

void Library::deleteDVD( char dvdName[]){
    int i;
    for( i = 0; i < numDVDs; i++){
        if( strcmp( dvdName, collection1[i].name) == 0){
            collection1[i].copies--;
            cout << "DVD deleted from collection.\n";
            return;
        }
    }
    cout << "DVD not found.\n";
}

void Library::insertBook( char bookName[], char author[], int c){
    strcpy( collection2[numBooks].name, bookName);
    strcpy( collection2[numBooks].author, author);
    collection2[numBooks].copies = c;
    cout << "Book added to collection.\n";
    ++numBooks;
}

void Library::deleteBook( char bookName[]){
    int i;
    for( i = 0; i < numBooks; i++){
        if( strcmp( bookName, collection2[i].name) == 0){
            collection2[i].copies--;
            cout << "Book deleted from collection.\n";
            return;
        }
    }
    cout << "Book not found.\n";
}

Game *Library::search( char gameName[]){
    int i;
    for( i = 0; i < numGames; i++){
        if( strcmp( gameName, collection[i].name) == 0)
            return &collection[i];
    }
    return NULL;
}

DVD *Library::search( char dvdName[]){
    int i;
    for( i = 0; i < numDVDs; i++){
        if( strcmp( dvdName, collection1[i].name) == 0)
            return &collection1[i];
    }
    return NULL;
}

Book *Library::search( char bookName[]){
    int i;
    for( i = 0; i < numBooks; i++){
        if( strcmp( bookName, collection2[i].name) == 0)
            return &collection2[i];
    }
    return NULL;
}

int main(){

    Library lib;


    while( 1 ){

        char mainSelect;
        char gameOption, name[30], platform[30], copies[10];
        char dvdOption;
        char bookOption;

        // Ask the user to select an option
        cout << "\nMain Menu:"<<endl;
        cout << "D for DVDs"<<endl;
        cout << "G for Games"<<endl;
        cout << "B for Books"<<endl;
        cout << "E to exit from the system"<<endl;

        // Read user selection
        cin.getline( name, 80);
        mainSelect = name[0];

        // Switch statement to select between the options
        switch (mainSelect){
           case 'd': case 'D':
              break;
           case 'g': case 'G':
               break;
           case 'b': case 'B':
               break;
           case 'e': case 'E':
               exit(0);
               break;
        }

        if (mainSelect == 'd','D'){

           cout << "\nEnter your option:"<<endl;
           cout << "A to add a new DVD"<<endl;
           cout << "D to delete a DVD"<<endl;
           cout << "S to search for a DVD"<<endl;
           cout << "E to exit from the system"<<endl;

           cin.getline( name, 80);
           dvdOption = name[0];

        switch (dvdOption){

           case 'a': case 'A':
              cout << "Enter Name of DVD: ";
              cin.getline( name, 80);
              cout << "Enter Director of DVD: ";
              cin.getline(director, 80);
              cout << "Enter no of copies: ";
              cin.getline(copies, 80);
              lib.insertDVD( name, director, atoi(copies));
              break;
           case 'd': case 'D':
              cout << "Enter Name of DVD:\n";
              cin.getline(name, 80);
              lib.deleteDVD(name);
              break;
           case 's': case 'S':
              cout << "Enter Name of DVD:\n";
              cin.getline(name, 80);
              Game *item;
              item = lib.search( name );
              if( item != NULL){
                cout << "DVD found\n" << item->name << endl << item->platform << endl << item->copies << endl;
            }
              else
              cout << "DVD not found\n";
              break;
           case 'e': case 'E':
              exit(0);
              break;
        }
        }

        else if (mainSelect == 'g','G'){
            cout << "\nEnter your option:"<<endl;
            cout << "A to add a new game"<<endl;
            cout << "D to delete a game"<<endl;
            cout << "S to search for a game"<<endl;
            cout << "E to exit from the system"<<endl;

            cin.getline( name, 80);
            gameOption = name[0];

        switch (gameOption){

           case 'a': case 'A':
             cout << "Enter Name of Game: ";
             cin.getline( name, 80);
             cout << "Enter game platform: ";
             cin.getline(platform, 80);
             cout << "Enter no of copies: ";
             cin.getline(copies, 80);
             lib.insertGame( name, platform, atoi(copies));
             break;
           case 'd': case 'D':
             cout << "Enter Name of Game:\n";
             cin.getline(name, 80);
             lib.deleteGame(name);
             break;
           case 's': case 'S':
             cout << "Enter Name of Game:\n";
             cin.getline(name, 80);
             Game *item;
             item = lib.search( name );
             if( item != NULL){
             cout << "Game found\n" << item->name << endl << item->platform << endl << item->copies << endl;
             }
             else
             cout << "Game not found\n";
             break;
             case 'e': case 'E':
             exit(0);
             break;
        }
        }
        }
    }
    return 0;
}
