\section{Overview}
\label{sec:overview}

\begin{figure*}[t]
  \centering
  \input{./figures/fig-overview.tex }
  \caption{\textsc{CompAssist} Architecture: The generator synthesizes example
fixes from a collection of compilable seed programs.  The search engine
retrieves and ranks example fixes given a user program as a query.  The web user
interface presents example fixes and suggest possible patches when the user
program fails to compile.}
  \label{fig:arch}
\end{figure*}



%A database of compilation repair examples enables a variety of applications.

\textsc{CompAssist}'s user interface, shown in \autoref{fig:t10}, is a web
application that compiles a program, extracts the compiler error message from
the compiler output, retrieves related repair examples, and suggests a list of
possible patches.  It ranks the patches to show those are most likely to fix
the user program.  For each patch, it provides repair examples which may help
the programmer understand the suggested patch, and determine if it is
applicable to his/her particular error.  It ranks the repair examples to show
those that have more in common with the user program first.

To illustrate how a programmer benefits from \textsc{CompAssist}, let us
discuss a real-world programmer, whom we shall call Mary.  Mary wrote the small
\texttt{C++} program shown in \autoref{fig:t10}; she may be just learning
inheritance, an important concept in \texttt{C++}.  Her program fails to
compile due to a defect involving inheritance.  The actual
defect is that the \lstinline[language=C++,basicstyle=\ttfamily]{CAT} class
inherits through private inheritance, which is the default behavior.  Instead,
it should inherit through public inheritance, as Mary confirms in her answer.
In other words, she needed to insert
\lstinline[language=C++,basicstyle=\ttfamily]{public} at line 12 before
\lstinline[language=C++,basicstyle=\ttfamily]{Animal}.


Like many programmers, she posts her code and the compiler error to
StackOverflow, and asks for
help.\footnote{https://stackoverflow.com/questions/27594593/cannot-cast-subclass-to-its-private-base-class/27594850}
Other users try but fail to help her.  One offers help about casting.  Another
asks for additional information, which is probably unnecessary based on her
post.  Eventually, Mary posts that she was able to resolve the compiler error
on her own and provides her solution.

Now suppose that instead she uses \textsc{CompAssist} for help.  She
copy-pastes her program into the code editor.  Then, she clicks ``Compile''.
The first patch suggestion is the correct patch, ``Insert
\lstinline[language=C++,basicstyle=\ttfamily]{public}''.  Mary resolves her
compiler error in a matter of seconds instead of minutes, based from the time
difference between her question and answer.  We expect \textsc{CompAssist} to
be helpful to a programmer in scenarios like this where the programmer reads
the compiler output but still fails to fix the defect.  In these cases, the
suggested patches and repair examples would simply be additional information.  


\autoref{fig:arch} shows \textsc{CompAssist}'s architecture.  The frontend is
supported by two backend components 1) an offline generator that synthesizes
repair examples from a corpus of compilable programs and stores them in a
database, and 2) an online search engine that retrieves and ranks repair
examples in response to user queries.
