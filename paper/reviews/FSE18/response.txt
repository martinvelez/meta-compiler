We appreciate the feedback, which helps improve our exposition.  As
the reviews commented, our main technical contribution is indeed a
novel, clever approach to synthesizing minimal compilation repair
examples. This response focuses on clarifying the main
questions/misinterpretations.


(1) Importance

We believe this work addresses an important, unsolved problem. Besides
the cited studies confirming its importance for beginners, we cited a
Google study showing its relevance even for professional developers.
Huawei, our collaborator, shared similar stories, which motivated this
work, especially for C++.  The Google study also highlights C++ errors
are different from Java ones. Our goal is to support any
language/compiler, and we tackle the key challenge of automatically
generating/reducing/ranking repair examples.


(2) GenProg, PAR

As the paper discussed, techniques like GenProg and PAR are unsuited
because compiler errors don't lead to good fitness functions.
Reference [A1] suggested by R3 confirms that automated repair tools
help with logic bugs, but not compiler errors. CompAssist's goal is
also broader than repair because, even when repair fails, it can
provide minimal repair examples. It doesn't depend on manual effort
and data like PAR or Prophet, and complements such tools by providing
seed patches.


(3) Coverage

The fact that 51% error coverage for a production C++ compiler can be
obtained fully automatically in ten days shows its promise. The 1,686
potential errors were extracted from Clang's source, thus some errors
may be unreachable, and additional ones can be covered with more
seeds/time/mutations. We discussed our effort in finding error
messages via web search, which shows diverse errors are covered,
including those without online help.


(4) Ranking

Our ranking scheme provides a good initial point, which can be
dynamically refined leveraging usage data. Hence, we didn't evaluate
it formally.  Auto-experimentation usually takes under a minute and is
feasible due to the vastly reduced search space.  The user can read
the compiler output and/or browse suggestions ranked wrt code
similarity.  A patch fixing the error is moved to the top of the
search results along with its examples.  The paper discussed how to
deal with programs of arbitrary size, e.g., by restricting
auto-experimentation to code regions around error lines indicated by
the compiler.


(5) User study

Its goal is to determine whether repair examples are helpful, which
the study confirmed.  The number of tasks mirrors that of [A2] to
allow users to complete within one hour.  The result that users found
examples helpful in "5 out of 9" tasks may have been misinterpreted
--- we didn't expect users to find examples helpful in all 9 tasks as
programmers don't need help with all errors.  Indeed, if they find an
error message helpful, they likely don't need additional help and
categorize repair examples unhelpful.


(6) Technical clarifications 

- We consider multiple errors, but consider one at a time by
  instructing the compiler to exit on the first fatal error. We then
  fix each successive fatal error.

- Our study snippets are real-world programs found online and range
  2-29 LoC (average 14.4).


[A1] https://dl.acm.org/citation.cfm?id=3106262
[A2] https://dl.acm.org/citation.cfm?id=3097437
