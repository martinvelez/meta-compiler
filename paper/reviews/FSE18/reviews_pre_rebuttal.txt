----------------------- REVIEW 1 ---------------------
PAPER: 288
TITLE: CompAssist: Synthesis of Minimal Compilation Repair Examples
AUTHORS: Martin Velez, Chengnian Sun, Nima Joharizadeh, Haichuan Wang and Zhendong Su

Overall score: -2 (reject)

----------- Summary -----------
This paper proposes a method to help developers fix compilation errors by
providing patches together with error messages. The idea is to create a pool
of compilation-error-fixing patches by taking compiling programs and mutating
them to create non-compiling variants, and associating the compilation error
message each variant produces with the patch to fix it. Then, using that pool
when a program results in a compilation error, by using that error to
retrieve a set of patches and prioritize them using some heuristics to show
the developer examples that may repair the compilation error.

The approach is evaluated by showing that the mutation process can create
examples that cover 51% of the possible compiler errors, that delta debugging
can get patches to 5 of fewer lines 60% of the time. A user study with 14
students showed that the tool was not helpful. Only half the time (5 out of 9
tasks) did the users give the tool a "somewhat helpful" or better rating, on
a scale of "not helpful at all", "somewhat helpful", "helpful", and "very
helpful." In a user study like this, with the tool set up to win because the
users clearly know they're evaluating the tool, these are not encouraging
results.

----------- Detailed evaluation -----------
This paper is, unfortunately, below the ESEC-FSE bar. Let me start by saying
that the problem of fixing compilation errors is not a deep problem worth
significant effort. There is ongoing research on automatically fixing
semantic bugs; fixing compilation bugs is a oversimplification of a serious
problem. Sure, it's possible to create tools that perhaps can help beginner
programmers understand compiler error messages, but it really isn't what
premier venue research is about. The paper cites several papers up front to
motivate compilation errors as an important challenge, but those papers only
say that beginners have trouble understanding compiler messages, and that
even experienced developers sometimes make compilation errors.

The paper starts out saying that automatic bug repair techniques (like
GenProg and PAR) won't work for compilation errors. Why not? The paper claims
that because those techniques rely on a fitness function and a test suite,
they cannot repair compilation errors. That's simply not true. To adapt those
techniques to the compilation problem, the fitness function would need to
change, and instead of test suites, the compiler's errors would be the
oracle. The tools would need to be adapted, but certainly, they could work in
this space. After all, compilation errors are basically a simple version of
semantic bugs.

In fact, Eclipse already has something that is similar to PAR's templates
that can be used to automatically fix compilation errors. They are called
QuickFix suggestions, and then are static, patern-based repairs for
compilation errors. See 
https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Fconcepts%2Fconcept-quickfix-assist.htm
There is also recent work called QuickFix Scout that uses QuickFix
suggestions to automatically find fixes that repair compilation errors. See
"Speculative Analysis of Integrated Development Environment Recommendations"
by Muslu et al. in OOPSLA'12. This is related work.  

Essentially, what this paper contributes is a clever way to automatically
generate more suggestions like QuickFix suggestions already built into
Eclipse. This is perhaps a worthwhile tool contribution.

The paper needs to be honest about its findings. The introduction makes it
sound like the users found the tool helpful. But in reality, only half the
time did the users say the tool was a 2 or better on a 1-4 scale. This is not
very promising, and it doesn't help the paper to pretend that the results are
better than they are. Even the coverage results are not very promising, as
the technique is only able to cover 51% of the possible compiler errors.

I found the paper easy to read but somewhat sloppy. For example, Definition 1
is loose. c could be any program? \Delta could be anything? Why isn't the
Compilation Repair Example indexed by e? I believe the authors meant for the
Compilation Repair Example to be defined much tighter, otherwise any
non-compiling program u and any other (potentially unrelated) compiling
program c satisfy the definition.

It would be better if the figures referenced a single running example. For
example, the patch in figure 3 has nothing to do with the error in figure 1.

I found figure 2 not very helpful. What are the stars? I do not think this
figure added anything to the paper.

----------- Strengths and weaknesses -----------
+ The paper comes with a working tool.
+ The idea for automatically generating a mapping from compilation error
 messages to patches is clever.
- The problem the paper tackles is just not very deep.
- The results are not encouraging, both in terms of coverage and in terms of
 user's responses.
- Important related work is missing.

----------- Questions to the authors -----------
I have no questions for the authors.

----------------------- REVIEW 2 ---------------------
PAPER: 288
TITLE: CompAssist: Synthesis of Minimal Compilation Repair Examples
AUTHORS: Martin Velez, Chengnian Sun, Nima Joharizadeh, Haichuan Wang and Zhendong Su

Overall score: -1 (weak reject)

----------- Summary -----------
The paper presents a fuzz-and-reduce approach to generate compilation repair examples. The goal is to help developers fixing some compilation errors by showing examples. The tool implementing this approach is called CompAssist. It tries to introduce compilation errors in correct programs by applying mutation operators (the fuzz step). Then, it reduces the program in the second step. Finally, an online technique is proposed to search and present relevant candidate repairs with examples to users. The approach is evaluated on a C++ compiler (Clang++). CompAssist contains examples for 51.4% of all possible compilation errors of Clang++. Almost 60% of the repair examples contain at most 5 lines of code. Some compilation errors have more than one repair example. The approach is evaluated in a user study involving 14 participants and 9 tasks. Users indicate that CompAssist helps fixing the compilation errors in 5 out of 9 tasks.

----------- Detailed evaluation -----------
(page 2) “the programmer reads the compiler output but still fails to fix the defect.”
Clang++ contains 1,686 compilation error messages according to the paper. It is not clear when CompAssist is most helpful. How often do not developers know how to fix a compilation error? It is important to identify and discuss the subset of compilation errors in each compiler that is trivial for developers to fix them.

What are the mutation operators considered in the fuzz step? Is there a subset of mutation operators that yields most of the distinct compilation errors? What happens if a mutation operator yields more than one compilation error? Do you include this example for each compilation error message?

(page 5) “We used a subset of 25,240 compilable programs for C and C++. We ran Algorithm 1 for 10 days”
Give more details about the subset of compilable programs used. How many lines of code do they have? Do they use all C/C++ keywords? How many mutant operators does the approach apply? How many times does it successfully introduce a compilation error? It is important to give much more evidences that Algorithm 2 helps reducing programs. Does the algorithm reduce at least 50% of all evaluated programs?

What can we do to include more examples for the compilation error messages not covered yet? Should we consider high-order mutation operators? Is it possible to reduce the number of lines of code for 40.4% of the examples containing more than 5 lines of code?

A previous work finds a number of bugs in C compilers. Can they influence your results? Do you evaluate programs containing C constructions with undefined and/or unspecified behaviors?

@inproceedings{Yang:2011,  
author = {Yang, Xuejun and Chen, Yang and Eide, Eric and Regehr, John},  
title = {Finding and Understanding Bugs in C Compilers},  
booktitle = {Proceedings of the 32nd ACM SIGPLAN Conference on Programming Language Design and Implementation},  
series = {PLDI '11},  
year = {2011},  
pages = {283--294}
} 

(page 5) “We inspected Clang’s source code to obtain a complete list of kinds of errors and their corresponding templates”
Give more details about the process of finding compilation error messages, and defining the error messages templates.

Show much more details (LOC,…) about the nine programs considered in Table 4. Is the considered set of tasks in the user study representative? Do they only have one compilation error? How do you select them? When is a task considered correct or incorrect? Is it possible that a developer yield a compilable program, but it was not the expected program?

- (page 5) “Auto-experimentation: We test each patch on the user program using brute-force.” 
How long does this step take in each program in the user study presented in Section 6? Does it always find a solution? Show more evidences about the quality of Auto-experimentation.

Minor comments
- (page 1) candiates => candidates
- (page 2) Section 3 and 4 => Sections 3 and 4
- Discuss about the time and space complexity of Algorithms 1 and 2.
- (page 5) “we compute the overlap coefficient [27] and subtract the Levenshtein distance [19] between uuser and u.” => Describe in more details.
- (page 5) “Based on pilot studies” => Describe in more details.
- (page 5) “We test each patch on the user program using brute-force” => How long does this step take in the subjects used in Section 6? Do you consider C/C++ programs containing ifdefs?
- (page 6) “Table 2 shows the ten errors” => Five errors?
- (page 6) “Table 3 shows the top ten errors” => Five errors?
- Why is not T2 considered in Table 4?
- Fix the open single quote symbol in LaTeX in Table 4.
- (page 7) “each of the 10 C++ programs” => 9?
- (page 7) “Submissions after 5 were” => 5 minutes?
- (page 8) “As we discussed in ??”
- (page 8) “English explanations” => English explanations.
- (page 9) “Fourth” => Third?
- (page 10) “the size of the user program can become irrelevant if restrict to a small region.” => Is it simple to identify a region?
- References: Include pages in [10, 36]

----------- Strengths and weaknesses -----------
(+) The paper is easy to read and follow.
(+) It evaluates the proposed solution with users. 
(-) It does not discuss how often developers do not know how to fix compilation errors.
(-) It does not discuss some important aspects of the approach and evaluation (fuzz, reduce, auto experimentation, subjects, tasks).

----------- Questions to the authors -----------
- How often do not developers know how to fix a compilation error? 
- What are the mutation operators considered in the fuzz step? Give more details about the fuzz and reduce steps in Section 5. See some related questions in my review.
- Is the set of tasks considered in the user study representative? How do you select them? Can you give more details about the nine programs used? Show more evidences about the quality of Auto-experimentation. See some related questions in my review.

----------------------- REVIEW 3 ---------------------
PAPER: 288
TITLE: CompAssist: Synthesis of Minimal Compilation Repair Examples
AUTHORS: Martin Velez, Chengnian Sun, Nima Joharizadeh, Haichuan Wang and Zhendong Su

Overall score: -1 (weak reject)

----------- Summary -----------
Compilation error messages are not always easy to understand. To
address this problem, this paper proposes a way to suggest a minimal
example patch to help programmers understand the compilation error at
hand. Example patches are searched for in a pre-built database of
patches. Given a compilation error message E, patches fixing the same
kind of error as E are shown to the users in a ranked order. To build
a database of patches, an arbitrary compilable program c is randomly
mutated into an uncompilable program u, and the difference between c
and u (after c and u are reduced to their minimal versions, using a
customized delta debugging) is stored in the database as an example
patch. The proposed system is evaluated in terms of the coverage of
compilation errors, the simplicity of repair examples, and the user
acceptance.

----------- Detailed evaluation -----------
* Novelty: 

I like the simple and novel idea proposed in this paper. The basic
idea is akin to mutation testing, in the sense that code is
deliberately broken to generate patches (instead of tests). Simple
ideas often work well in practice, and this paper also has such a
potential.

* Completeness: 

The online search part of this paper seems to me incomplete. In
particular, the auto-experimentation part looks rudimentary at this
point. The described auto-experimentation process assumes a single
compilation error; if a compilation error disappears after applying a
searched example patch P, then the score of P is updated (it is not
described in the paper how the score is updated, though). However,
real-world programs often have multiple compilation errors, and I
doubt whether the suggested automation scheme would work for those
programs. Even for single-error cases, the current automation scheme
looks too simple to be used for sizable programs. Currently, the
entire program is subject to changes for a candidate patch, without
restricting the program locations to where a candidate patch is
applied. Note that for many real-world programs, compilation is not
done instantly. I suppose the authors could easily make their
auto-experimentation feature more scalable, for example by making use
of the error line information in the compilation error message,
although this too would not always work.

Speaking of scalability, I doubt whether the suggested ranking formula
would work for large code. According to the description in the paper,
the entire uncompilable code is compared with a small example program
in the database. Wouldn't it be enough and better to make comparison
only with a problematic region? Perhaps, do the authors implicitly
assume that their target users are only novice students? Even if that
is the case, this paper does not provide evaluation for
auto-experimentation and ranking. I think evaluating them properly is
very important, considering their potential high impact on the
users. Users would not appreciate reading spurious example patches
that cannot fix the compilation error.


* Evaluation: 

This is the weakest part of this paper. As mentioned, the
auto-experimentation and ranking are not evaluated at all. In
comparison, prior related work [1,14,40] reports repairability (how
often a suggested patch fixes the error). The evaluation described in
Section 5 only covers the offline patch example generation part,
leaving out the online search part (while Section 6 addresses this
part, its scope is limited). Considering the conceptual similarity of
this work to mutation testing, one important research question that I
think should be addressed is whether the "artificial" patches
generated from deliberately broken code can actually be used to fix
real-world program errors. For this, the evaluation of
auto-experimentation and ranking is critical. For example, an
experiment could be performed to see how often top-N ranked patches
actually fix the compilation errors of the real-world programs in a
benchmark. Of course, fixing compilation errors does not necessarily
lead to correct code. Simply deleting a problematic expression or a
statement can often make the error go away. Thus, it will be also
interesting to investigate this avenue as well, making use of
test-suites and correct versions; one can observe whether the correct
(ground-truth) version and the automatically fixed version pass the
same tests.

The user study given in Section 6 seems useful and interesting. Also,
the study seems carefully designed and administered. The problem is
that the number of participants (14) is too small, in my
opinion. These 14 participants are again divided into the control
group and the experimental group. I doubt if there is any statistical
significance in any of the results reported in the user study. The
authors do not report on statistical significance.

The number of programs used for the user study (9) is also quite
small. In comparison, prior earlier work is evaluated with hundreds to
thousands of programs [1,14,40]. While in the user study, it would be
difficult to use the same number of programs, sampling only 9 errors
out of 867 errors that can be covered by the tool seems too limited.


* Related work: 

In Section 8.2 (Automated Program Repair), semantics-based repair
approaches are not described. Note that it is generally considered
that search-based and semantics-based approaches are two main distinct
approaches to automated program repair. If the authors target to fix
student programs, I think the following paper should also be
mentioned. 

A feasibility study of using automated program repair for introductory
programming assignments. ESEC/SIGSOFT FSE 2017

* Typos:

In page 8, "As we discussed in ??"

----------- Strengths and weaknesses -----------
+ Simple and good idea
+ The paper is easy to follow.

- Insufficient evaluation, in particular on auto-experimentation and
ranking
- A small sample size of the user study (both participants and error messages)
- Missing related work

----------- Questions to the authors -----------
1. How do you update the score with the result of
auto-experimentation?  

2. Do you consider multiple compilation errors?

3. In Table 4, why did you skip over T2? 

4. What is the size of the snippets used for the user study?

5. How did you divide the study participants into the control and
experimental groups? Why do the numbers of these groups vary across
the tasks?

------------------------------------------------------
