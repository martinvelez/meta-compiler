package edu.ucdavis.error.fuzzer.repository;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.message.CompilerError;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.util.FileUtility;
import org.apache.commons.io.FileUtils;
import org.pmw.tinylog.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;

public class CompilerErrorRepository {

  private final Map<String, List<ErrorInstance>> savedErrorMessages;

  private final Set<String> unhandledErrorMessages;

  private final File repoRootFolder;

  private final File unhandledErrorMessageFile;

  private static final String MSG_PREFIX = "MSG_";

  private final int maxInstancesPerErrorMsg;

  public Iterable<String> getSavedErrorMessages() {
    return this.savedErrorMessages.keySet();
  }

  public Stream<ErrorInstance> stremErrorInstances() {
    return savedErrorMessages.values().stream().flatMap(list -> list.stream());
  }

  public ConcurrentLinkedQueue toErrorInstanceQueue() {
    final ConcurrentLinkedQueue<ErrorInstance> queue = new ConcurrentLinkedQueue<>();
    stremErrorInstances().forEach(queue::add);
    return queue;
  }

  public Iterable<ErrorInstance> getSavedErrorInstances(String errorMsg) {
    final List<ErrorInstance> result = this.savedErrorMessages.get(errorMsg);
    return result == null ? Collections.emptyList() : result;
  }

  static boolean isValidErrorInstanceFolderName(String folderName) {
    return folderName.matches("[0-9]+");
  }

  private static List<ErrorInstance> loadErrorInstances(File errorMsgFolder) {
    List<ErrorInstance> result = new ArrayList<>();
    for (File instanceFolder : errorMsgFolder.listFiles()) {
      if (!instanceFolder.isDirectory()) {
        continue;
      }
      if (!isValidErrorInstanceFolderName(instanceFolder.getName())) {
        continue;
      }
      final ErrorInstance errorInstance = ErrorInstance.load(instanceFolder);
      result.add(errorInstance);
    }
    return result;
  }

  private static Map<String, List<ErrorInstance>> loadSavedErrorMessages(File root) {
    final Map<String, List<ErrorInstance>> result = new HashMap<>();

    for (File errorMsgFolder : root.listFiles()) {
      if (!errorMsgFolder.isDirectory()) {
        continue;
      }
      final String folderName = errorMsgFolder.getName();
      if (!folderName.startsWith(MSG_PREFIX)) {
        continue;
      }
      result.put(folderName, loadErrorInstances(errorMsgFolder));
    }
    return result;
  }

  public static CompilerErrorRepository loadExistingErrorRepository(
      File repoRootFolder, int maxInstancesPerErrorMsg) {
    checkArgument(
        repoRootFolder.isDirectory(),
        "The specified repo root folder %s is not a directory",
        repoRootFolder);
    return new CompilerErrorRepository(repoRootFolder, maxInstancesPerErrorMsg);
  }

  public static CompilerErrorRepository createOrLoadErrorRepository(
      File repoRootFolder, int maxInstancesPerErrorMsg) {
    if (!repoRootFolder.exists()) {
      repoRootFolder.mkdirs();
    }
    return new CompilerErrorRepository(repoRootFolder, maxInstancesPerErrorMsg);
  }

  private CompilerErrorRepository(File repoRootFolder, int maxInstancesPerErrorMsg) {
    checkArgument(
        repoRootFolder.isDirectory(), "The repo root folder %s does not exist.", repoRootFolder);
    checkArgument(
        maxInstancesPerErrorMsg >= 0,
        "The maxInstancesPerErrorMsg should be non-positive: %s",
        maxInstancesPerErrorMsg);
    Logger.info("Loading error repository from {}", repoRootFolder);
    this.repoRootFolder = repoRootFolder;
    this.maxInstancesPerErrorMsg = maxInstancesPerErrorMsg;
    this.unhandledErrorMessageFile = new File(this.repoRootFolder, "unhandled_error_messages.txt");
    this.savedErrorMessages = loadSavedErrorMessages(repoRootFolder);
    unhandledErrorMessages = loadSavedUnhandledErrorMessages(this.unhandledErrorMessageFile);

    Logger.info("The root of the error repository is {}", this.repoRootFolder);
    Logger.info("The max number of instances per error msg is {}", this.maxInstancesPerErrorMsg);
    Logger.info("    {} existing error message folders", this.savedErrorMessages.size());
    Logger.info("    {} existing error instances", this.countTotalErrorInstances());
  }

  private int countTotalErrorInstances() {
    return this.savedErrorMessages.entrySet().stream().mapToInt(e -> e.getValue().size()).sum();
  }

  private static Set<String> loadSavedUnhandledErrorMessages(File unhandledErrorMessageFile) {
    final HashSet<String> result = new HashSet<>();
    if (!unhandledErrorMessageFile.exists()) {
      try {
        unhandledErrorMessageFile.createNewFile();
      } catch (IOException e) {
        throw new RuntimeException("cannot create file " + unhandledErrorMessageFile, e);
      }
      return result;
    }
    try (BufferedReader reader = new BufferedReader(new FileReader(unhandledErrorMessageFile))) {

      for (String line = reader.readLine(); line != null; line = reader.readLine()) {
        line = line.trim();
        if (line.isEmpty()) {
          continue;
        }
        result.add(line);
      }
      return result;
    } catch (IOException e) {
      throw new RuntimeException("fail to read the file " + unhandledErrorMessageFile, e);
    }
  }

  private static String normalizeErrorMsg(String errorMsgSignature) {
    final StringBuilder builder = new StringBuilder("MSG_");
    final int length = errorMsgSignature.length();
    for (int i = 0; i < length; ++i) {
      final char c = errorMsgSignature.charAt(i);
      if (Character.isLetterOrDigit(c) || c == '.' || c == '_' || c == '-') {
        builder.append(c);
      } else {
        builder.append('_');
      }
    }
    return builder.toString();
  }

  public synchronized void saveUnhandledErrorMessage(String errorMsg) {
    if (this.unhandledErrorMessages.contains(errorMsg)) {
      return;
    } else {
      this.unhandledErrorMessages.add(errorMsg);
    }
    try {
      FileUtils.writeStringToFile(
          this.unhandledErrorMessageFile, errorMsg + "\n", Charsets.UTF_8, true);
    } catch (IOException e) {
      throw new RuntimeException(
          "cannot writeTokensToFile unhandled error msg to file " + this.unhandledErrorMessageFile,
          e);
    }
  }

  /**
   * @param compilerError
   * @return null if the error has existed.
   */
  public synchronized Optional<ErrorInstance> saveErrorInstance(
      AbstractMutant mutant, CompilerError compilerError, SourceFile seedFile, File mutantFile) {
    final String normalizedErrorMsg = normalizeErrorMsg(compilerError.getSignature());

    List<ErrorInstance> instanceList = this.savedErrorMessages.get(normalizedErrorMsg);
    if (instanceList == null) {
      instanceList = new ArrayList<>();
      this.savedErrorMessages.put(normalizedErrorMsg, instanceList);
    }

    final int numOfInstances = instanceList.size();
    if (numOfInstances >= this.maxInstancesPerErrorMsg) {
      Logger.info(
          "There are already {} instances for error msg {}, max is {}",
          numOfInstances,
          normalizedErrorMsg,
          this.maxInstancesPerErrorMsg);
      Logger.info(
          "There are {} instances of error msg {}, exceeding the max {}",
          numOfInstances,
          normalizedErrorMsg,
          this.maxInstancesPerErrorMsg);
      return Optional.empty();
    }

    final String seedContentWithoutWhitespaces =
        FileUtility.readFileAndDiscardWhitespaces(seedFile.getAbsoluteFile());
    for (ErrorInstance instance : instanceList) {
      if (seedContentWithoutWhitespaces.equals(instance.getGoodFileContentWithNoWhitespaces())) {
        // this seed file has triggered this file, thus returning.
        Logger.info(
            "The seed file {} has triggered an instance of the error msg {}",
            seedFile,
            normalizedErrorMsg);
        return Optional.empty();
      }
    }

    final File errorParentFolder = new File(this.repoRootFolder, normalizedErrorMsg);
    if (!errorParentFolder.exists()) {
      errorParentFolder.mkdirs();
    }

    final String instanceID = Strings.padStart(String.valueOf(numOfInstances), 3, '0');
    final File instanceFolder = new File(errorParentFolder, instanceID);
    Preconditions.checkState(isValidErrorInstanceFolderName(instanceFolder.getName()));
    if (!instanceFolder.exists()) {
      instanceFolder.mkdirs();
    }

    final ErrorInstance instance =
        ErrorInstance.create(instanceFolder, mutant, compilerError, seedFile, mutantFile);
    instanceList.add(instance);
    final ErrorInstance.IntegrityReport report = instance.checkIntegrity();
    if (report.hasProblems()) {
      report.logProblems();
      Preconditions.checkState(false, "The newly created error instance should have no problem");
    }
    return Optional.of(instance);
  }
}
