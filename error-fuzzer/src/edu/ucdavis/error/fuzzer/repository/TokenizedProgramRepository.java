package edu.ucdavis.error.fuzzer.repository;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.token.AutoLoadableTokenizedProgram;
import edu.ucdavis.error.fuzzer.token.TokenizedProgram;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/** Created by Chengnian Sun on 4/2/17. */
public class TokenizedProgramRepository {

  public static class SourceProgramWithTokens {
    private final SourceFile sourceFile;
    private final AutoLoadableTokenizedProgram tokensHolder;

    private SourceProgramWithTokens(
        SourceFile sourceFile, AutoLoadableTokenizedProgram tokensHolder) {
      this.sourceFile = sourceFile;
      this.tokensHolder = tokensHolder;
    }

    public SourceFile getSourceFile() {
      return sourceFile;
    }

    public TokenizedProgram getTokenizedProgram() {
      return this.tokensHolder.get();
    }

    public ImmutableList<TokenProgramSegment> getTokenList() {
      return this.getTokenizedProgram().getTokens();
    }
  }

  public static TokenizedProgramRepository createRepository(
      ImmutableList<SourceFile> sourceFiles, File repoFolder) {
    repoFolder.mkdirs();

    Arrays.stream(repoFolder.listFiles())
        .forEach(
            file -> {
              Logger.warn("Deleting existing token file {}", file);
              Files.fileTreeTraverser().preOrderTraversal(file).forEach(File::delete);
            });
    final AtomicInteger tokenFileID = new AtomicInteger(0);
    final ImmutableList.Builder<SourceProgramWithTokens> builder = ImmutableList.builder();
    for (SourceFile sourceFile : sourceFiles) {
      try {
        Logger.info("tokenizing program {}", sourceFile);
        final TokenizedProgram program =
            TokenizedProgram.tokenizeSourceProgram(sourceFile.getAbsoluteFile());
        if (program.getTokens().size() < 4) {
          Logger.warn(
              "There are too few tokens ({}) in the program ({}), thus stop mutating it.",
              program.getTokens().size(),
              sourceFile.getFileName());
          continue;
        }
        final int tokenFileId = tokenFileID.incrementAndGet();
        final File tokenFile =
            new File(repoFolder, Strings.padStart(Integer.toString(tokenFileId), 6, '0'));
        program.writeTokensToFile(tokenFile);
        builder.add(
            new SourceProgramWithTokens(sourceFile, new AutoLoadableTokenizedProgram(tokenFile)));
      } catch (Throwable e) {
        Logger.debug(e, "failed to tokenize the source file {}", sourceFile);
        continue;
      }
    }
    return new TokenizedProgramRepository(builder.build());
  }

  private final ImmutableList<SourceProgramWithTokens> programs;

  public TokenizedProgramRepository(ImmutableList<SourceProgramWithTokens> programs) {
    this.programs = programs;
    Logger.info("There are {} files in the {}", programs.size(), this.getClass());
  }

  public ImmutableList<SourceProgramWithTokens> getPrograms() {
    return programs;
  }
}
