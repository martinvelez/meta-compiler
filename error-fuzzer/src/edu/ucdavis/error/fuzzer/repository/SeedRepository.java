package edu.ucdavis.error.fuzzer.repository;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.util.ClangFormat;
import edu.ucdavis.error.fuzzer.util.RandomUtil;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.google.common.base.Preconditions.checkArgument;
import static edu.ucdavis.error.fuzzer.compiler.SourceFile.isCSourceFile;
import static edu.ucdavis.error.fuzzer.compiler.SourceFile.isCppSourceFile;

/**
 * Created by Chengnian Sun on 4/1/17.
 *
 * <p>This class represents all the seed files used for fuzzing.
 */
public class SeedRepository {

  private final ImmutableList<SourceFile> seeds;

  private SeedRepository(ImmutableList<SourceFile> seeds) {
    this.seeds = seeds;
    Logger.info("There are {} seed files in the seed repository", this.seeds.size());
  }

  public ImmutableList<SourceFile> getSeeds() {
    return seeds;
  }

  public static class Builder {

    private FluentIterable<File> files;

    public Builder(File seedRoot) {
      checkArgument(seedRoot.isDirectory(), "The seed root %s is NOT a directory.");
      this.files =
          Files.fileTreeTraverser()
              .preOrderTraversal(seedRoot)
              .filter(File::isFile)
              .filter(f -> isCppSourceFile(f.getName()) || isCSourceFile(f.getName()));
    }

    public Builder filter(Predicate<File> filter) {
      this.files = this.files.filter(filter);
      return this;
    }

    public Builder sample(double samplingRatio) {
      Preconditions.checkState(
          samplingRatio >= 0 && samplingRatio <= 1, "Invalid sampling ration %s", samplingRatio);
      if (samplingRatio < 1) {
        List<File> copy = this.files.copyInto(new ArrayList<>());
        Collections.shuffle(copy, RandomUtil.v().getRandom());
        this.files = FluentIterable.from(copy);
        return limit((int) (copy.size() * samplingRatio));
      }
      return this;
    }

    /**
     * Filter the files of large size.
     *
     * @param upperByteLimit the limit of file size in bytes.
     * @return
     */
    public Builder discardLargeFiles(int upperByteLimit) {
      this.files = this.files.filter(f -> f.length() <= upperByteLimit);
      return this;
    }

    public Builder shuffle() {
      List<File> copy = this.files.copyInto(new ArrayList<>());
      Collections.shuffle(copy, RandomUtil.v().getRandom());
      this.files = FluentIterable.from(copy);
      return this;
    }

    public Builder limit(int count) {
      this.files = this.files.limit(count);
      return this;
    }

    public SeedRepository build() {
      return new SeedRepository(
          this.files
              .transform(
                  file -> {
                    ClangFormat.formatSourceFile(file);
                    return file;
                  })
              .transform(SourceFile::new)
              .toList());
    }
  }
}
