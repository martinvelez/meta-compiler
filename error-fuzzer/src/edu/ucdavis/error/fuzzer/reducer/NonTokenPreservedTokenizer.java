package edu.ucdavis.error.fuzzer.reducer;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.token.TokenizedProgram;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.NonTokenProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;

import java.io.File;
import java.io.IOException;

/**
 * This tokenizer not only tokenizes a program into a sequence of tokens, but also reserves the
 * non-tokens (e.g., spaces, comments, and other non-tokens).
 *
 * <p>Created by Chengnian Sun on 4/27/17.
 */
public class NonTokenPreservedTokenizer {

  public static ImmutableList<IProgramSegment> tokenize(File sourceFile)
      throws ExpansionOverlapsBoundaryException, ParsingFailureException, IOException {
    final ImmutableList<TokenProgramSegment> regularTokens =
        TokenizedProgram.tokenizeSourceProgram(sourceFile).getTokens();
    final String sourceText = Files.asCharSource(sourceFile, Charsets.UTF_8).read();
    if (sourceText.isEmpty()) {
      return ImmutableList.of();
    }
    ImmutableList.Builder<IProgramSegment> builder = ImmutableList.builder();
    int start = 0;
    for (TokenProgramSegment token : regularTokens) {
      final int offset = token.getOffset();
      final int endOffset = token.getEndOffset();
      Preconditions.checkState(start <= offset, "start=%s, offset=%s", start, offset);
      if (start < offset) {
        builder.add(new NonTokenProgramSegment(sourceText.substring(start, offset)));
      }
      builder.add(token);
      start = endOffset;
    }
    if (start < sourceText.length()) {
      builder.add(new NonTokenProgramSegment(sourceText.substring(start)));
    }
    return builder.build();
  }

  public static void main(String[] args)
      throws ExpansionOverlapsBoundaryException, IOException, ParsingFailureException {
    ImmutableList<IProgramSegment> tokenize = tokenize(new File
            ("test-data/error-instances/MSG_no_type_named_.__in_" +
                    "._/000/reduction/good_reduced_formatted.C"));
    for (IProgramSegment segment : tokenize) {
      System.out.print(segment.getLexeme());
    }
  }
}
