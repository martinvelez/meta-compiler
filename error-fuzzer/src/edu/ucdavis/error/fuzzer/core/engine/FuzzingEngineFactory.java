package edu.ucdavis.error.fuzzer.core.engine;

import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;

import java.io.File;
import java.util.Optional;

public class FuzzingEngineFactory {

  private final Class<? extends AbstractFuzzingEngine> klass;

  public FuzzingEngineFactory(Class<? extends AbstractFuzzingEngine> klass) {
    super();
    this.klass = klass;
  }

  public Optional<? extends AbstractFuzzingEngine> createEngine(
      SourceFile seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager) {
//    try {
////      if (this.klass.equals(IdentifierSubstituionFuzzingEngine.class)) {
////        return Optional.of(new IdentifierSubstituionFuzzingEngine(
////            seedFile, rootFolder, errorRepo, crashingBugPackager));
////      } else
//      if (this.klass.equals(RandomTokenManipulationFuzzingEngine.class)) {
//        return Optional.of(new RandomTokenManipulationFuzzingEngine(
//            seedFile, rootFolder, errorRepo, crashingBugPackager));
//      }
//    } catch (ParsingFailureException | ExpansionOverlapsBoundaryException | IOException e) {
//      return Optional.empty();
//    }

    throw new RuntimeException("unhandled engine type: " + this.klass);
  }
}
