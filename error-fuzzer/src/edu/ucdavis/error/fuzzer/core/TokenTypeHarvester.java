package edu.ucdavis.error.fuzzer.core;

import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.token.TokenizedProgram;
import edu.ucdavis.error.fuzzer.token.cdt.CDTTokenizer;
import edu.ucdavis.error.fuzzer.util.FileUtility;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

/** Created by neo on 3/28/17. */
public class TokenTypeHarvester {

  private final File originalProgramFolder;

  private final File tokenizedProgramFolder;

  public TokenTypeHarvester(File originalProgramFolder, File tokenizedProgramFolder) {
    this.originalProgramFolder = originalProgramFolder;
    this.tokenizedProgramFolder = tokenizedProgramFolder;
  }

  public void run() {
    List<File> seedFiles =
        FileUtility.globSourceFiles(this.originalProgramFolder)
            .stream()
            .filter(f -> f.length() < 1024 * 200)
            .map(File::getAbsoluteFile)
            .collect(Collectors.toList());
    for (File file : seedFiles) {
      System.out.println(file);
      try {
        final TokenizedProgram program = new TokenizedProgram(new CDTTokenizer(new SourceFile(file)).getTokens());
        final URI relativePath = this.originalProgramFolder.toURI().relativize(file.toURI());
        program.writeTokensToFile(new File(this.tokenizedProgramFolder, relativePath.toString()));
      } catch (Exception e) {
      }
    }
  }

  public static void main(String[] args)
      throws ExpansionOverlapsBoundaryException, ParsingFailureException {
    new TokenTypeHarvester(new File("gcc-seed-programs"), new File("tokenized-gcc-seed-programs"))
        .run();
  }
}
