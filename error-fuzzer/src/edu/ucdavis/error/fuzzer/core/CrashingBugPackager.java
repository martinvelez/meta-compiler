package edu.ucdavis.error.fuzzer.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarOutputStream;
import org.pmw.tinylog.Logger;

import com.google.common.base.Charsets;

import edu.ucdavis.error.fuzzer.compiler.AbstractCompilerTestConfig;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.Shell.CmdOutput;

public class CrashingBugPackager {

//  private static final File REDUCTION_SCRIPT = new File("scripts/r.sh");
  private final File crashingBugFolder;

  private static void tar(Map<File, String> fileAndTarEntryNames, File tarFile)
      throws IOException {
    FileOutputStream dest = new FileOutputStream(tarFile);
    TarOutputStream out = new TarOutputStream(new BufferedOutputStream(dest));

    for (Map.Entry<File, String> entry : fileAndTarEntryNames.entrySet()) {
      out.putNextEntry(new TarEntry(entry.getKey(), entry.getValue()));
      BufferedInputStream origin = new BufferedInputStream(
          new FileInputStream(entry.getKey()));
      int count;
      byte data[] = new byte[2048];

      while ((count = origin.read(data)) != -1) {
        out.write(data, 0, count);
      }

      out.flush();
      origin.close();
    }

    out.close();
  }

  private static void collectFileAndTarEntryNames(Map<File, String> result,
      File file, File base) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        collectFileAndTarEntryNames(result, child, base);
      }
    } else {
      result.put(file, relativizePath(base, file));
    }
  }

  /**
   * tar the folder into a tar file
   * 
   * @param folder
   * @param tarFile
   * @throws IOException
   */
  private static void tar(File folder, File tarFile) throws IOException {
    Map<File, String> fileAndTarEntryNames = new HashMap<>();

    File base = folder.getAbsoluteFile().getParentFile();
    collectFileAndTarEntryNames(fileAndTarEntryNames, folder, base);
    tar(fileAndTarEntryNames, tarFile);
  }

  private static String relativizePath(File base, File file) {
    return base.toURI().relativize(file.toURI()).getPath();
  }

  public static void main(String[] args) throws IOException {
    // File folder = new File(
    // "/home/neo/workspace/meta-compiler/meta-compiler/error-engine/src/edu/ucdavis/error/engine/token/");
    // File file = new File(
    // "/home/neo/workspace/meta-compiler/meta-compiler/error-engine/src/edu/ucdavis/error/engine/token/TokenUtility.java");
    // System.out.println(folder.toURI().relativize(file.toURI()).getPath());
    tar(new File("src"), new File("test.tgz"));

    // List<File> files = Arrays.asList(
    // file,
    // new File(
    // "/home/neo/workspace/meta-compiler/meta-compiler/error-engine/build.xml"));
    // File result = new File("test.tgz");
    // tar(files, result);
  }

  public CrashingBugPackager(File crashingBugFolder) {
    super();
    this.crashingBugFolder = crashingBugFolder;
    if (!this.crashingBugFolder.exists()) {
      this.crashingBugFolder.mkdirs();
    }
  }

  public File getCrashingBugFolder() {
    return crashingBugFolder;
  }

  private void saveCompileScript(AbstractCompilerTestConfig compilerConfig,
      File folder) throws IOException {
    final StringBuilder builder = new StringBuilder();
    builder.append("#!/usr/bin/env bash").append("\n");
    builder.append("if [[ $# != 2 ]] ; then ").append("\n");
    builder.append("  echo \"Usage: $0 <source file> <output file>\"")
        .append("\n");
    builder.append("  exit 1").append("\n");
    builder.append("fi").append("\n");
    builder.append(compilerConfig.getCompilerCmd()).append(" $1 -o $2")
        .append("\n");
    builder.append("exit $?").append("\n");

    final File scriptFile = new File(folder,
        compilerConfig.getCompiler() + ".compile.sh");
    FileUtils.write(scriptFile, builder, Charsets.UTF_8);
    scriptFile.setExecutable(true);
  }

  public void packageCrashingBug(SourceFile originalSource,
      File bugTriggeringMutant, AbstractCompilerTestConfig compilerConfig,
      CmdOutput crashingOutput) {
    this.packageBug(true, originalSource, bugTriggeringMutant, compilerConfig,
        crashingOutput);
  }

  public void packagePerformanceBug(SourceFile originalSource,
      File bugTriggeringMutant, AbstractCompilerTestConfig compilerConfig,
      CmdOutput crashingOutput) {
    this.packageBug(false, originalSource, bugTriggeringMutant, compilerConfig,
        crashingOutput);
  }

  private void packageBug(final boolean isCrashing, SourceFile originalSource,
      File bugTriggeringMutant, AbstractCompilerTestConfig compilerConfig,
      CmdOutput crashingOutput) {
    final StringBuilder folderName = new StringBuilder();
    final Date now = new Date();
    folderName.append(new SimpleDateFormat("yyyyMMdd").format(now)).append("-");
    folderName.append(compilerConfig.getCompiler()).append("-");
    folderName.append(compilerConfig.getFlags().replaceAll("\\s+", ""))
        .append("-");
    folderName.append(isCrashing ? "build" : "perf").append("-")
        .append(new SimpleDateFormat("HHmmss").format(now));

    final File folder = new File(this.crashingBugFolder, folderName.toString());
    folder.mkdirs();
    Logger.info("Packaging bug to folder {}", folder);

    try {
      // copy original file
      FileUtils.copyFile(originalSource.getAbsoluteFile(),
          new File(folder, "orig" + originalSource.getFileNameExtension()));
      // copy mutated file
      FileUtils.copyFile(bugTriggeringMutant,
          new File(folder, "t" + originalSource.getFileNameExtension()));

      // writeTokensToFile crashing output
      FileUtils.write(new File(folder, "outchk.txt"),
          crashingOutput.getStderr(), Charsets.UTF_8);

      // create reduction folder;
      File reductionFolder = new File(folder, "reduction");
      reductionFolder.mkdirs();
      FileUtils.copyFile(bugTriggeringMutant, new File(reductionFolder,
          "small" + originalSource.getFileNameExtension()));
      // // r.sh
      // File reductionScript = new File(reductionFolder, "r.sh");
      // FileUtility.copyFile(REDUCTION_SCRIPT, reductionScript);
      this.saveCompileScript(compilerConfig, folder);
      File tarFile = new File(this.crashingBugFolder,
          folder.getName() + ".tgz");
      tar(folder, tarFile);
      Logger.info("Packaged bug to a tar file {}", tarFile);
    } catch (IOException e) {
      Logger.error(e, "Failed to package the bug at {}", folder);
    }
  }

}
