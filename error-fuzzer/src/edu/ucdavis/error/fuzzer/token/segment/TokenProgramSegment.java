package edu.ucdavis.error.fuzzer.token.segment;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/** Created by Chengnian Sun on 4/27/17. */
public class TokenProgramSegment extends AbstractProgramSegment {

  private static final int INVALID_OFFSET = -1;

  public static TokenProgramSegment createTokenWithValidOffsets(
      String lexeme, int type, int offset, int endOffset) {
    checkArgument(offset >= 0, "offset=%s", offset);
    checkArgument(endOffset >= 0, "endOffset=%s", endOffset);
    return new TokenProgramSegment(lexeme, type, offset, endOffset);
  }

  public static TokenProgramSegment createTokenWithNoOffsets(String lexeme, int type) {
    return new TokenProgramSegment(lexeme, type, INVALID_OFFSET, INVALID_OFFSET);
  }

  private final int type;
  private final int offset;
  private final int endOffset;

  //  public TokenProgramSegment(String lexeme, int type) {
  //    this(lexeme, type, Integer.MIN_VALUE, Integer.MIN_VALUE);
  //  }

  private TokenProgramSegment(String lexeme, int type, int offset, int endOffset) {
    super(lexeme);
    this.type = type;
    this.offset = offset;
    this.endOffset = endOffset;
  }

  public int getTokenType() {
    return type;
  }

  public int getOffset() {
    checkState(this.offset >= 0, "This token has no associated offset information.");
    return offset;
  }

  public int getEndOffset() {
    checkState(this.endOffset >= 0, "This token has no associated offset information");
    return endOffset;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    TokenProgramSegment that = (TokenProgramSegment) o;

    return type == that.type;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + type;
    return result;
  }
}
