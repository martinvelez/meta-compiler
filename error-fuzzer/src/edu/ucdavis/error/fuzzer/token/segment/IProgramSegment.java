package edu.ucdavis.error.fuzzer.token.segment;

/**
 * This class abstracts a segment in a program. A segment can be
 * <li>Regular tokens: tokens that constitute a program
 * <li>Empty strings: just empty strings.
 * <li>Non tokens: other texts that are neither regular tokens nor empty strings.
 */
public interface IProgramSegment {

  String getLexeme();
}
