package edu.ucdavis.error.fuzzer.message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pmw.tinylog.Logger;

import com.google.common.collect.ImmutableList;

/*
 * http://clang.llvm.org/docs/InternalsManual.html#formatting-a-diagnostic-argument
 */
public class ClangErrorMsgTemplateParser {

  private final static Pattern PATTERN_PERCENTAGE = Pattern.compile("%%");

  private final static Pattern PATTERN_SIMPLE_PARAMETER = Pattern
      .compile("%[0-9]");

  private final static Pattern PATTERN_S = Pattern.compile("%s[0-9]");

  private final static Pattern PATTERN_ORDINAL = Pattern
      .compile("%ordinal[0-9]");

  private final static Pattern PATTERN_OBJCCLASS = Pattern
      .compile("%objcclass[0-9]");

  private final static Pattern PATTERN_OBJCINSTANCE = Pattern
      .compile("%objcinstance[0-9]");

  private final static Pattern PATTERN_Q = Pattern.compile("%q[0-9]");

  private final static Pattern PATTERN_SELECT = Pattern
      .compile("%select\\{([^{}]+?)\\}[0-9]");

  private final static Pattern PATTERN_PLURAL = Pattern
      .compile("%plural\\{([^{}]+?)\\}[0-9]");

  private final static Pattern PATTERN_DIFF = Pattern
      .compile("%diff\\{([^{}]+?)\\}[0-9].[0-9]");

  // public static void main(String[] args) {
  // {
  // Matcher matcher = PATTERN_PERCENTAGE.matcher("ss%%ss%%ss%%");
  // while (matcher.find()) {
  // System.out.println(matcher.start() + "--" + matcher.end());
  // }
  // }
  // {
  // Matcher matcher = PATTERN_SIMPLE_PARAMETER.matcher("%1%2sss%3");
  // while (matcher.find()) {
  // System.out.println(matcher.start() + "--" + matcher.end());
  // }
  // }
  // }

  private final String template;

  private final ImmutableList<Pattern> patterns;

  /**
   * Java Metacharacters
   * 
   * <([{\^-=$!|]})?*+.>
   * 
   * @param template
   * @return
   */
  private static String preEscape(String template) {
    // final String metaChars = "\\?*+.";
    final String metaChars = "<([^-=!])?*+.>";
    for (char c : metaChars.toCharArray()) {
      template = template.replaceAll(Pattern.quote(String.valueOf(c)),
          Matcher.quoteReplacement("\\" + c));
    }
    return template;
  }

  private static String postEscape(String template) {
    final String metaChars = "{}";
    for (char c : metaChars.toCharArray()) {
      template = template.replaceAll("\\" + c,
          Matcher.quoteReplacement("\\" + c));
    }
    return template;
  }

  public ClangErrorMsgTemplateParser(String template) {
    this.template = preEscape(template);

    ImmutableList.Builder<Pattern> builder = ImmutableList.builder();
    for (String s : parse(this.template)) {
      final String escaped = postEscape(s);
      builder.add(Pattern.compile(escaped));
    }
    this.patterns = builder.build();
  }

  public String getOriginalTemplate() {
    return this.template;
  }

  public ImmutableList<Pattern> getPatterns() {
    return this.patterns;
  }

  private static class ReplacementEdit {

    private final int regionStart;

    private final int regionEnd;

    private final String[] alternatives;

    public ReplacementEdit(int regionStart, int regionEnd,
        String[] alternatives) {
      super();
      this.regionStart = regionStart;
      this.regionEnd = regionEnd;
      this.alternatives = alternatives;
    }

  }

  private static void editStringRecursive(String s, List<ReplacementEdit> edits,
      List<Integer> alternativeSelection, List<String> result) {
    final int editCount = edits.size();
    if (editCount == alternativeSelection.size()) {
      final StringBuilder builder = new StringBuilder();

      int i = 0;
      for (int editIndex = 0; editIndex < edits.size(); ++editIndex) {
        final ReplacementEdit currentEdit = edits.get(editIndex);
        for (; i < currentEdit.regionStart; ++i) {
          builder.append(s.charAt(i));
        }
        assert (i == currentEdit.regionStart);
        builder.append(
            currentEdit.alternatives[alternativeSelection.get(editIndex)]);
        i = currentEdit.regionEnd;
      }
      for (; i < s.length(); ++i) {
        builder.append(s.charAt(i));
      }
      assert (i == s.length());
      result.add(builder.toString());
      return;
    }

    final ReplacementEdit currentEdit = edits.get(alternativeSelection.size());
    for (int i = 0; i < currentEdit.alternatives.length; ++i) {
      alternativeSelection.add(i);
      editStringRecursive(s, edits, alternativeSelection, result);
      alternativeSelection.remove(alternativeSelection.size() - 1);
    }
  }

  private static List<String> editString(String s,
      List<ReplacementEdit> edits) {
    if (edits.isEmpty()) {
      return Arrays.asList(s);
    }
    List<String> result = new ArrayList<>();
    List<Integer> alternativeSelection = new ArrayList<>();
    editStringRecursive(s, edits, alternativeSelection, result);
    return result;
  }

  private static List<String> internalExpand(List<String> messages,
      Pattern pattern, String replacement) {
    List<String> result = new ArrayList<>();
    for (String msg : messages) {
      result.add(msg.replaceAll(pattern.pattern(), replacement));
    }
    return result;
  }

  private static List<String> internalExpand(List<String> messages,
      Pattern pattern) {
    return internalExpand(messages, pattern, ".+");
  }

  static List<String> expandS(List<String> messages) {
    return internalExpand(messages, PATTERN_S, ".?");
  }

  static List<String> expandOrdinal(List<String> messages) {
    return internalExpand(messages, PATTERN_ORDINAL);
  }

  static List<String> expandQ(List<String> messages) {
    return internalExpand(messages, PATTERN_Q);
  }

  static List<String> expandObjcclass(List<String> messages) {
    return internalExpand(messages, PATTERN_OBJCCLASS);
  }

  static List<String> expandObjcinstance(List<String> messages) {
    return internalExpand(messages, PATTERN_OBJCINSTANCE);
  }

  static List<String> expandPlural(List<String> concretization) {
    List<String> result = new ArrayList<>();
    for (String string : concretization) {
      final Matcher pluralMatcher = PATTERN_PLURAL.matcher(string);
      List<ReplacementEdit> edits = new ArrayList<>();
      while (pluralMatcher.find()) {
        final int start = pluralMatcher.start();
        final int end = pluralMatcher.end();
        assert (pluralMatcher.groupCount() == 1);
        final String[] alternatives = pluralMatcher.group(1).split("\\|");
        for (int i = 0; i < alternatives.length; ++i) {
          final String alternative = alternatives[i];
          alternatives[i] = alternative.substring(alternative.indexOf(':') + 1);
        }
        edits.add(new ReplacementEdit(start, end, alternatives));
      }
      if (edits.isEmpty()) {
        result.add(string);
      } else {
        result.addAll(editString(string, edits));
      }
    }
    return result;
  }

  static List<String> expandDiff(List<String> concretization) {
    List<String> result = new ArrayList<>();

    for (String string : concretization) {
      final Matcher selectMatcher = PATTERN_DIFF.matcher(string);
      List<ReplacementEdit> edits = new ArrayList<>();
      while (selectMatcher.find()) {
        final int start = selectMatcher.start();
        final int end = selectMatcher.end();
        assert (selectMatcher.groupCount() == 1);
        final String[] alternatives = selectMatcher.group(1).split("\\|");
        for (int i = 0; i < alternatives.length; ++i) {
          final String alt = alternatives[i];
          alternatives[i] = alt.replaceAll("\\$", ".+");
        }
        edits.add(new ReplacementEdit(start, end, alternatives));
      }
      if (edits.isEmpty()) {
        result.add(string);
      } else {
        result.addAll(editString(string, edits));
      }
    }

    return result;
  }

  static List<String> expandSelect(List<String> concretization) {
    List<String> result = new ArrayList<>();

    for (String string : concretization) {
      final Matcher selectMatcher = PATTERN_SELECT.matcher(string);
      List<ReplacementEdit> edits = new ArrayList<>();
      while (selectMatcher.find()) {
        final int start = selectMatcher.start();
        final int end = selectMatcher.end();
        assert selectMatcher.groupCount() == 1 : string;
        final String[] alternatives = selectMatcher.group(1).split("\\|");
        edits.add(new ReplacementEdit(start, end, alternatives));
      }
      if (edits.isEmpty()) {
        result.add(string);
      } else {
        result.addAll(editString(string, edits));
      }
    }

    return result;
  }

  // private static ImmutableList<Pattern> parseToPatterns(String template) {
  // // List<Pattern> result = new ArrayList<>();
  // ImmutableList.Builder<Pattern> builder = ImmutableList.builder();
  // for (String s : load(template)) {
  // builder.add(Pattern.compile(s));
  // }
  // return builder.build();
  // }

  static List<String> parse(String template) {
    // process select
    Logger.debug("Parsing template {}", template);
    List<String> prev = new ArrayList<>();
    prev.add(template);
    do {
      List<String> messages = expandSelect(prev);
      messages = expandPlural(messages);
      messages = expandDiff(messages);
      if (messages.equals(prev)) {
        break;
      } else {
        prev = messages;
      }
    } while (true);

    prev = expandOrdinal(prev);
    prev = expandObjcclass(prev);
    prev = expandObjcinstance(prev);
    prev = expandQ(prev);
    prev = expandS(prev);
    prev = internalExpand(prev, PATTERN_SIMPLE_PARAMETER);
    prev = internalExpand(prev, PATTERN_PERCENTAGE, "%");
    final Set<String> set = new TreeSet<>(prev);
    prev.clear();
    prev.addAll(set);
    return prev;
  }

  // // private int index;
  // //
  // // private final String template;
  // private CharacterStream stream;
  //
  // private final List<Pattern> convertedRegexps;
  //
  // public ClangErrorMsgTemplateParser(String template) {
  // this.stream = new CharacterStream(template);
  // this.convertedRegexps = this.parseTemplate();
  // }
  //
  // private List<Pattern> parseTemplate() {
  // List<List<String>> segments = this.parseMessageToSegments();
  // }
  //
  // private List<List<String>> parseMessageToSegments() {
  //
  // List<List<String>> result = new ArrayList<>();
  // final StringBuilder builder = new StringBuilder();
  // while (stream.hasNextChar()) {
  // final char c = this.stream.nextChar();
  // if (c != '%') {
  // builder.append(c);
  // continue;
  // }
  // assert (c == '%');
  // if (consumeIfIsEscapedPercentageSymbol() != null) {
  // builder.append('%');
  // continue;
  // }
  // }
  //
  // for (int i = 0; i < length; ++i) {
  // final char c = string.charAt(i);
  // if (c != '%') {
  // builder.append(c);
  // continue;
  // }
  //
  // assert (c == '%');
  // Preconditions.checkState(i + 1 < length);
  // if (consumeIfIsEscapedPercentageSymbol()) {
  // builder.append('%');
  // continue;
  // }
  // if (isSimpleParameter(string, i)) {
  // // %1, %2, ..., %9
  // builder.append(".+");
  // continue;
  // }
  // if (isS_format(string, i)) {
  // builder.append("s?");
  // continue;
  // }
  // if (isOrdinal(string, i)) {
  //
  // }
  //
  // }
  // return result;
  // }
  // //
  // // private static boolean containsStringAtIndex(String s, int startIndex,
  // // String substring) {
  // // assert (s != null);
  // // assert (substring != null);
  // // final int length = s.length();
  // // final int subLength = substring.length();
  // //
  // // if (subLength + startIndex > length) {
  // // return false;
  // // }
  // //
  // // for (int i = 0; i < subLength; ++i) {
  // // if (s.charAt(i + startIndex) != substring.charAt(i)) {
  // // return false;
  // // }
  // // }
  // // return true;
  // // }
  //
  // private boolean startsWith(CharacterStream stream, String niddle,
  // boolean requireExtraTailingChar) {
  // final int length = niddle.length();
  // for (int i = 0; i < length; ++i) {
  // final char c = niddle.charAt(i);
  // if (!stream.hasNextChar()) {
  // return false;
  // }
  // final char other = stream.nextChar();
  // if (c != other) {
  // return false;
  // }
  // }
  // if (requireExtraTailingChar) {
  //
  // }
  // }
  //
  // private static boolean isWithinOneToNine(char c) {
  // return '0' <= c && c <= '9';
  // }
  //
  // private String consumeIfIsEscapedPercentageSymbol() {
  // CharacterStream fork = this.stream.fork();
  //
  //
  // final int index = this.getCurrentIndex();
  // if ()
  // assert (s.charAt(startIndex) == '%');
  // if (startIndex + 1 < s.length()) {
  // return s.charAt(startIndex + 1) == '%';
  // }
  // return false;
  // }
  //
  // private static boolean isSimpleParameter(String s, int startIndex) {
  // assert (s.charAt(startIndex) == '%');
  // if (startIndex + 1 < s.length()) {
  // return isWithinOneToNine(s.charAt(startIndex + 1));
  // }
  // return false;
  // }
  //
  // private static boolean isS_format(String s, int startIndex) {
  // assert (s.charAt(startIndex) == '%');
  // if (startIndex + 2 < s.length()) {
  // return s.charAt(startIndex + 1) == 's'
  // && isWithinOneToNine(s.charAt(startIndex + 2));
  // }
  // return false;
  // }
  //
  // private static boolean isOrdinal(String s, int startIndex) {
  // assert (s.charAt(startIndex) == '%');
  // if (!containsStringAtIndex(s, startIndex, "%ordinal")) {
  // return false;
  // }
  //
  // }

}
