package edu.ucdavis.error.fuzzer.util;

import java.io.File;

import com.google.common.collect.ImmutableList;

/**
 * This class checkes whether a command exists on the environment variable PATH.
 * 
 * @author neo
 *
 */
public class PathEnvUtil {

  private static final ImmutableList<File> PATH_DIRECTORIES;

  static {
    ImmutableList.Builder<File> builder = ImmutableList.builder();
    final String pathEnv = System.getenv("PATH");
    if (pathEnv == null || pathEnv.isEmpty()) {
      throw new RuntimeException(
          "The environment variable PATH does not exist");
    }
    for (String dir : pathEnv.split(File.pathSeparator)) {
      final File folder = new File(dir);
      if (folder.exists() && folder.isDirectory())
        builder.add(folder);
    }
    PATH_DIRECTORIES = builder.build();
  }

  public static boolean existCommand(String commandName) {
    for (File dir : PATH_DIRECTORIES) {
      final File cmd = new File(dir, commandName);
      if (cmd.exists() && cmd.isFile()) {
        return true;
      }
    }
    return false;
  }

  public static void main(String[] args) {
    final boolean result = existCommand("sls");
    System.out.println(result);
  }

}
