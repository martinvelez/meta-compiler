

template <typename T> struct A {
  template <class T2> void f1(T, T2);
  template <class T2> void f2(T, T2);
};

template <> template <class X1> void A<int>::f1(int, X1);

template <> template <> void A<int>::f2<char>(int, char);

template <> template <> void A<int>::f1(int, char);
