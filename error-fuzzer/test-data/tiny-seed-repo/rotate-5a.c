

typedef long unsigned int size_t;
typedef int wchar_t;
typedef struct {
  int quot;
  int rem;
} div_t;

typedef struct {
  long int quot;
  long int rem;
} ldiv_t;

__extension__ typedef struct {
  long long int quot;
  long long int rem;
} lldiv_t;
extern size_t __ctype_get_mb_cur_max(void) __attribute__((__nothrow__));

extern double atof(const char *__nptr) __attribute__((__nothrow__))
__attribute__((__pure__)) __attribute__((__nonnull__(1)));

extern int atoi(const char *__nptr) __attribute__((__nothrow__))
__attribute__((__pure__)) __attribute__((__nonnull__(1)));

extern long int atol(const char *__nptr) __attribute__((__nothrow__))
__attribute__((__pure__)) __attribute__((__nonnull__(1)));

__extension__ extern long long int atoll(const char *__nptr)
    __attribute__((__nothrow__)) __attribute__((__pure__))
    __attribute__((__nonnull__(1)));

extern double strtod(const char *__restrict __nptr, char **__restrict __endptr)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));

extern float strtof(const char *__restrict __nptr, char **__restrict __endptr)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));

extern long double strtold(const char *__restrict __nptr,
                           char **__restrict __endptr)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));

extern long int strtol(const char *__restrict __nptr,
                       char **__restrict __endptr, int __base)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));

extern unsigned long int strtoul(const char *__restrict __nptr,
                                 char **__restrict __endptr, int __base)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
__extension__ extern long long int
strtoll(const char *__restrict __nptr, char **__restrict __endptr, int __base)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));

__extension__ extern unsigned long long int
strtoull(const char *__restrict __nptr, char **__restrict __endptr, int __base)
    __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern int rand(void) __attribute__((__nothrow__));

extern void srand(unsigned int __seed) __attribute__((__nothrow__));
extern void *malloc(size_t __size) __attribute__((__nothrow__))
__attribute__((__malloc__));

extern void *calloc(size_t __nmemb, size_t __size) __attribute__((__nothrow__))
__attribute__((__malloc__));
extern void *realloc(void *__ptr, size_t __size) __attribute__((__nothrow__))
__attribute__((__warn_unused_result__));

extern void free(void *__ptr) __attribute__((__nothrow__));
extern void *aligned_alloc(size_t __alignment, size_t __size)
    __attribute__((__nothrow__)) __attribute__((__malloc__));

extern void abort(void) __attribute__((__nothrow__))
__attribute__((__noreturn__));

extern int atexit(void (*__func)(void)) __attribute__((__nothrow__))
__attribute__((__nonnull__(1)));

extern int at_quick_exit(void (*__func)(void)) __attribute__((__nothrow__))
__attribute__((__nonnull__(1)));
extern void exit(int __status) __attribute__((__nothrow__))
__attribute__((__noreturn__));

extern void quick_exit(int __status) __attribute__((__nothrow__))
__attribute__((__noreturn__));

extern void _Exit(int __status) __attribute__((__nothrow__))
__attribute__((__noreturn__));

extern char *getenv(const char *__name) __attribute__((__nothrow__))
__attribute__((__nonnull__(1)));
extern int system(const char *__command);
typedef int (*__compar_fn_t)(const void *, const void *);
extern void *bsearch(const void *__key, const void *__base, size_t __nmemb,
                     size_t __size, __compar_fn_t __compar)
    __attribute__((__nonnull__(1, 2, 5)));

extern void qsort(void *__base, size_t __nmemb, size_t __size,
                  __compar_fn_t __compar) __attribute__((__nonnull__(1, 4)));
extern int abs(int __x) __attribute__((__nothrow__)) __attribute__((__const__));
extern long int labs(long int __x) __attribute__((__nothrow__))
__attribute__((__const__));

__extension__ extern long long int llabs(long long int __x)
    __attribute__((__nothrow__)) __attribute__((__const__));

extern div_t div(int __numer, int __denom) __attribute__((__nothrow__))
__attribute__((__const__));
extern ldiv_t ldiv(long int __numer, long int __denom)
    __attribute__((__nothrow__)) __attribute__((__const__));

__extension__ extern lldiv_t lldiv(long long int __numer, long long int __denom)
    __attribute__((__nothrow__)) __attribute__((__const__));
extern int mblen(const char *__s, size_t __n) __attribute__((__nothrow__));

extern int mbtowc(wchar_t *__restrict __pwc, const char *__restrict __s,
                  size_t __n) __attribute__((__nothrow__));

extern int wctomb(char *__s, wchar_t __wchar) __attribute__((__nothrow__));

extern size_t mbstowcs(wchar_t *__restrict __pwcs, const char *__restrict __s,
                       size_t __n) __attribute__((__nothrow__));

extern size_t wcstombs(char *__restrict __s, const wchar_t *__restrict __pwcs,
                       size_t __n) __attribute__((__nothrow__));
static __inline int __get_cpuid(unsigned int __level, unsigned int *__eax,
                                unsigned int *__ebx, unsigned int *__ecx,
                                unsigned int *__edx) {
  __asm("  xchgq  %%rbx,%q1\n"
        "  cpuid\n"
        "  xchgq  %%rbx,%q1"
        : "=a"(*__eax), "=r"(*__ebx), "=c"(*__ecx), "=d"(*__edx)
        : "0"(__level));
  return 1;
}

static __inline int __get_cpuid_max(unsigned int __level, unsigned int *__sig) {
  unsigned int __eax, __ebx, __ecx, __edx;
  __asm("  xchgq  %%rbx,%q1\n"
        "  cpuid\n"
        "  xchgq  %%rbx,%q1"
        : "=a"(__eax), "=r"(__ebx), "=c"(__ecx), "=d"(__edx)
        : "0"(__level));
  if (__sig)
    *__sig = __ebx;
  return __eax;
}
typedef long long __m64 __attribute__((__vector_size__(8)));

typedef int __v2si __attribute__((__vector_size__(8)));
typedef short __v4hi __attribute__((__vector_size__(8)));
typedef char __v8qi __attribute__((__vector_size__(8)));

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_empty(void) {
  __builtin_ia32_emms();
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cvtsi32_si64(int __i) {
  return (__m64)__builtin_ia32_vec_init_v2si(__i, 0);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cvtsi64_si32(__m64 __m) {
  return __builtin_ia32_vec_ext_v2si((__v2si)__m, 0);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cvtsi64_m64(long long __i) {
  return (__m64)__i;
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cvtm64_si64(__m64 __m) {
  return (long long)__m;
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_packs_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_packsswb((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_packs_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_packssdw((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_packs_pu16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_packuswb((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpackhi_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpckhbw((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpackhi_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpckhwd((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpackhi_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpckhdq((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpacklo_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpcklbw((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpacklo_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpcklwd((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_unpacklo_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_punpckldq((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_add_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_add_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_add_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddd((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_adds_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddsb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_adds_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddsw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_adds_pu8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddusb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_adds_pu16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_paddusw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sub_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sub_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sub_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubd((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_subs_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubsb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_subs_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubsw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_subs_pu8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubusb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_subs_pu16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_psubusw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_madd_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pmaddwd((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_mulhi_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pmulhw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_mullo_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pmullw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sll_pi16(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psllw((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_slli_pi16(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psllwi((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sll_pi32(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_pslld((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_slli_pi32(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_pslldi((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sll_si64(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psllq(__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_slli_si64(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psllqi(__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sra_pi16(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psraw((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srai_pi16(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psrawi((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_sra_pi32(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psrad((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srai_pi32(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psradi((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srl_pi16(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psrlw((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srli_pi16(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psrlwi((__v4hi)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srl_pi32(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psrld((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srli_pi32(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psrldi((__v2si)__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srl_si64(__m64 __m, __m64 __count) {
  return (__m64)__builtin_ia32_psrlq(__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_srli_si64(__m64 __m, int __count) {
  return (__m64)__builtin_ia32_psrlqi(__m, __count);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_and_si64(__m64 __m1, __m64 __m2) {
  return __builtin_ia32_pand(__m1, __m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_andnot_si64(__m64 __m1, __m64 __m2) {
  return __builtin_ia32_pandn(__m1, __m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_or_si64(__m64 __m1, __m64 __m2) {
  return __builtin_ia32_por(__m1, __m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_xor_si64(__m64 __m1, __m64 __m2) {
  return __builtin_ia32_pxor(__m1, __m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpeq_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpeqb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpeq_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpeqw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpeq_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpeqd((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpgt_pi8(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpgtb((__v8qi)__m1, (__v8qi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpgt_pi16(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpgtw((__v4hi)__m1, (__v4hi)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_cmpgt_pi32(__m64 __m1, __m64 __m2) {
  return (__m64)__builtin_ia32_pcmpgtd((__v2si)__m1, (__v2si)__m2);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_setzero_si64(void) {
  return (__m64){0LL};
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set_pi32(int __i1, int __i0) {
  return (__m64)__builtin_ia32_vec_init_v2si(__i0, __i1);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set_pi16(short __s3, short __s2, short __s1, short __s0) {
  return (__m64)__builtin_ia32_vec_init_v4hi(__s0, __s1, __s2, __s3);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set_pi8(char __b7, char __b6, char __b5, char __b4, char __b3,
                char __b2, char __b1, char __b0) {
  return (__m64)__builtin_ia32_vec_init_v8qi(__b0, __b1, __b2, __b3, __b4, __b5,
                                             __b6, __b7);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set1_pi32(int __i) {
  return _mm_set_pi32(__i, __i);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set1_pi16(short __w) {
  return _mm_set_pi16(__w, __w, __w, __w);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_set1_pi8(char __b) {
  return _mm_set_pi8(__b, __b, __b, __b, __b, __b, __b, __b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_setr_pi32(int __i0, int __i1) {
  return _mm_set_pi32(__i1, __i0);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_setr_pi16(short __w0, short __w1, short __w2, short __w3) {
  return _mm_set_pi16(__w3, __w2, __w1, __w0);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("mmx")))
    _mm_setr_pi8(char __b0, char __b1, char __b2, char __b3, char __b4,
                 char __b5, char __b6, char __b7) {
  return _mm_set_pi8(__b7, __b6, __b5, __b4, __b3, __b2, __b1, __b0);
}

typedef int __v4si __attribute__((__vector_size__(16)));
typedef float __v4sf __attribute__((__vector_size__(16)));
typedef float __m128 __attribute__((__vector_size__(16)));

extern int posix_memalign(void **__memptr, size_t __alignment, size_t __size);
static __inline__ void *__attribute__((__always_inline__, __nodebug__,
                                       __malloc__))
_mm_malloc(size_t __size, size_t __align) {
  if (__align == 1) {
    return malloc(__size);
  }

  if (!(__align & (__align - 1)) && __align < sizeof(void *))
    __align = sizeof(void *);

  void *__mallocedMemory;

  if (posix_memalign(&__mallocedMemory, __align, __size))
    return 0;

  return __mallocedMemory;
}

static __inline__ void __attribute__((__always_inline__, __nodebug__))
_mm_free(void *__p) {
  free(__p);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_add_ss(__m128 __a, __m128 __b) {
  __a[0] += __b[0];
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_add_ps(__m128 __a, __m128 __b) {
  return __a + __b;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sub_ss(__m128 __a, __m128 __b) {
  __a[0] -= __b[0];
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sub_ps(__m128 __a, __m128 __b) {
  return __a - __b;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_mul_ss(__m128 __a, __m128 __b) {
  __a[0] *= __b[0];
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_mul_ps(__m128 __a, __m128 __b) {
  return __a * __b;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_div_ss(__m128 __a, __m128 __b) {
  __a[0] /= __b[0];
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_div_ps(__m128 __a, __m128 __b) {
  return __a / __b;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sqrt_ss(__m128 __a) {
  __m128 __c = __builtin_ia32_sqrtss(__a);
  return (__m128){__c[0], __a[1], __a[2], __a[3]};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sqrt_ps(__m128 __a) {
  return __builtin_ia32_sqrtps(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_rcp_ss(__m128 __a) {
  __m128 __c = __builtin_ia32_rcpss(__a);
  return (__m128){__c[0], __a[1], __a[2], __a[3]};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_rcp_ps(__m128 __a) {
  return __builtin_ia32_rcpps(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_rsqrt_ss(__m128 __a) {
  __m128 __c = __builtin_ia32_rsqrtss(__a);
  return (__m128){__c[0], __a[1], __a[2], __a[3]};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_rsqrt_ps(__m128 __a) {
  return __builtin_ia32_rsqrtps(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_min_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_minss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_min_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_minps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_max_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_maxss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_max_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_maxps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_and_ps(__m128 __a, __m128 __b) {
  return (__m128)((__v4si)__a & (__v4si)__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_andnot_ps(__m128 __a, __m128 __b) {
  return (__m128)(~(__v4si)__a & (__v4si)__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_or_ps(__m128 __a, __m128 __b) {
  return (__m128)((__v4si)__a | (__v4si)__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_xor_ps(__m128 __a, __m128 __b) {
  return (__m128)((__v4si)__a ^ (__v4si)__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpeq_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpeqss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpeq_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpeqps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmplt_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpltss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmplt_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpltps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmple_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpless(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmple_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpleps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpgt_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_shufflevector(__a, __builtin_ia32_cmpltss(__b, __a),
                                         4, 1, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpgt_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpltps(__b, __a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpge_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_shufflevector(__a, __builtin_ia32_cmpless(__b, __a),
                                         4, 1, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpge_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpleps(__b, __a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpneq_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpneqss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpneq_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpneqps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnlt_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnltss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnlt_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnltps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnle_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnless(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnle_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnleps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpngt_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_shufflevector(__a, __builtin_ia32_cmpnltss(__b, __a),
                                         4, 1, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpngt_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnltps(__b, __a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnge_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_shufflevector(__a, __builtin_ia32_cmpnless(__b, __a),
                                         4, 1, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpnge_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpnleps(__b, __a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpord_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpordss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpord_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpordps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpunord_ss(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpunordss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cmpunord_ps(__m128 __a, __m128 __b) {
  return (__m128)__builtin_ia32_cmpunordps(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comieq_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comieq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comilt_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comilt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comile_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comile(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comigt_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comigt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comige_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comige(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_comineq_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_comineq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomieq_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomieq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomilt_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomilt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomile_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomile(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomigt_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomigt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomige_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomige(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_ucomineq_ss(__m128 __a, __m128 __b) {
  return __builtin_ia32_ucomineq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtss_si32(__m128 __a) {
  return __builtin_ia32_cvtss2si(__a);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvt_ss2si(__m128 __a) {
  return _mm_cvtss_si32(__a);
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtss_si64(__m128 __a) {
  return __builtin_ia32_cvtss2si64(__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtps_pi32(__m128 __a) {
  return (__m64)__builtin_ia32_cvtps2pi(__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvt_ps2pi(__m128 __a) {
  return _mm_cvtps_pi32(__a);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvttss_si32(__m128 __a) {
  return __a[0];
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtt_ss2si(__m128 __a) {
  return _mm_cvttss_si32(__a);
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvttss_si64(__m128 __a) {
  return __a[0];
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvttps_pi32(__m128 __a) {
  return (__m64)__builtin_ia32_cvttps2pi(__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtt_ps2pi(__m128 __a) {
  return _mm_cvttps_pi32(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtsi32_ss(__m128 __a, int __b) {
  __a[0] = __b;
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvt_si2ss(__m128 __a, int __b) {
  return _mm_cvtsi32_ss(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtsi64_ss(__m128 __a, long long __b) {
  __a[0] = __b;
  return __a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpi32_ps(__m128 __a, __m64 __b) {
  return __builtin_ia32_cvtpi2ps(__a, (__v2si)__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvt_pi2ps(__m128 __a, __m64 __b) {
  return _mm_cvtpi32_ps(__a, __b);
}

static __inline__ float
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtss_f32(__m128 __a) {
  return __a[0];
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_loadh_pi(__m128 __a, const __m64 *__p) {
  typedef float __mm_loadh_pi_v2f32 __attribute__((__vector_size__(8)));
  struct __mm_loadh_pi_struct {
    __mm_loadh_pi_v2f32 __u;
  } __attribute__((__packed__, __may_alias__));
  __mm_loadh_pi_v2f32 __b = ((struct __mm_loadh_pi_struct *)__p)->__u;
  __m128 __bb = __builtin_shufflevector(__b, __b, 0, 1, 0, 1);
  return __builtin_shufflevector(__a, __bb, 0, 1, 4, 5);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_loadl_pi(__m128 __a, const __m64 *__p) {
  typedef float __mm_loadl_pi_v2f32 __attribute__((__vector_size__(8)));
  struct __mm_loadl_pi_struct {
    __mm_loadl_pi_v2f32 __u;
  } __attribute__((__packed__, __may_alias__));
  __mm_loadl_pi_v2f32 __b = ((struct __mm_loadl_pi_struct *)__p)->__u;
  __m128 __bb = __builtin_shufflevector(__b, __b, 0, 1, 0, 1);
  return __builtin_shufflevector(__a, __bb, 4, 5, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_load_ss(const float *__p) {
  struct __mm_load_ss_struct {
    float __u;
  } __attribute__((__packed__, __may_alias__));
  float __u = ((struct __mm_load_ss_struct *)__p)->__u;
  return (__m128){__u, 0, 0, 0};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_load1_ps(const float *__p) {
  struct __mm_load1_ps_struct {
    float __u;
  } __attribute__((__packed__, __may_alias__));
  float __u = ((struct __mm_load1_ps_struct *)__p)->__u;
  return (__m128){__u, __u, __u, __u};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_load_ps(const float *__p) {
  return *(__m128 *)__p;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_loadu_ps(const float *__p) {
  struct __loadu_ps {
    __m128 __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_ps *)__p)->__v;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_loadr_ps(const float *__p) {
  __m128 __a = _mm_load_ps(__p);
  return __builtin_shufflevector(__a, __a, 3, 2, 1, 0);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_undefined_ps() {
  return (__m128)__builtin_ia32_undef128();
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_set_ss(float __w) {
  return (__m128){__w, 0, 0, 0};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_set1_ps(float __w) {
  return (__m128){__w, __w, __w, __w};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_set_ps1(float __w) {
  return _mm_set1_ps(__w);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_set_ps(float __z, float __y, float __x, float __w) {
  return (__m128){__w, __x, __y, __z};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_setr_ps(float __z, float __y, float __x, float __w) {
  return (__m128){__z, __y, __x, __w};
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_setzero_ps(void) {
  return (__m128){0, 0, 0, 0};
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_storeh_pi(__m64 *__p, __m128 __a) {
  __builtin_ia32_storehps((__v2si *)__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_storel_pi(__m64 *__p, __m128 __a) {
  __builtin_ia32_storelps((__v2si *)__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_store_ss(float *__p, __m128 __a) {
  struct __mm_store_ss_struct {
    float __u;
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_store_ss_struct *)__p)->__u = __a[0];
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_storeu_ps(float *__p, __m128 __a) {
  __builtin_ia32_storeups(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_store1_ps(float *__p, __m128 __a) {
  __a = __builtin_shufflevector(__a, __a, 0, 0, 0, 0);
  _mm_storeu_ps(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_store_ps1(float *__p, __m128 __a) {
  return _mm_store1_ps(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_store_ps(float *__p, __m128 __a) {
  *(__m128 *)__p = __a;
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_storer_ps(float *__p, __m128 __a) {
  __a = __builtin_shufflevector(__a, __a, 3, 2, 1, 0);
  _mm_store_ps(__p, __a);
}
static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_stream_pi(__m64 *__p, __m64 __a) {
  __builtin_ia32_movntq(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_stream_ps(float *__p, __m128 __a) {
  __builtin_ia32_movntps(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sfence(void) {
  __builtin_ia32_sfence();
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_extract_pi16(__m64 __a, int __n) {
  __v4hi __b = (__v4hi)__a;
  return (unsigned short)__b[__n & 3];
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_insert_pi16(__m64 __a, int __d, int __n) {
  __v4hi __b = (__v4hi)__a;
  __b[__n & 3] = __d;
  return (__m64)__b;
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_max_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pmaxsw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_max_pu8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pmaxub((__v8qi)__a, (__v8qi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_min_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pminsw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_min_pu8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pminub((__v8qi)__a, (__v8qi)__b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_movemask_pi8(__m64 __a) {
  return __builtin_ia32_pmovmskb((__v8qi)__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_mulhi_pu16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pmulhuw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_maskmove_si64(__m64 __d, __m64 __n, char *__p) {
  __builtin_ia32_maskmovq((__v8qi)__d, (__v8qi)__n, __p);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_avg_pu8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pavgb((__v8qi)__a, (__v8qi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_avg_pu16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pavgw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_sad_pu8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_psadbw((__v8qi)__a, (__v8qi)__b);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_getcsr(void) {
  return __builtin_ia32_stmxcsr();
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_setcsr(unsigned int __i) {
  __builtin_ia32_ldmxcsr(__i);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_unpackhi_ps(__m128 __a, __m128 __b) {
  return __builtin_shufflevector(__a, __b, 2, 6, 3, 7);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_unpacklo_ps(__m128 __a, __m128 __b) {
  return __builtin_shufflevector(__a, __b, 0, 4, 1, 5);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_move_ss(__m128 __a, __m128 __b) {
  return __builtin_shufflevector(__a, __b, 4, 1, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_movehl_ps(__m128 __a, __m128 __b) {
  return __builtin_shufflevector(__a, __b, 6, 7, 2, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_movelh_ps(__m128 __a, __m128 __b) {
  return __builtin_shufflevector(__a, __b, 0, 1, 4, 5);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpi16_ps(__m64 __a) {
  __m64 __b, __c;
  __m128 __r;

  __b = _mm_setzero_si64();
  __b = _mm_cmpgt_pi16(__b, __a);
  __c = _mm_unpackhi_pi16(__a, __b);
  __r = _mm_setzero_ps();
  __r = _mm_cvtpi32_ps(__r, __c);
  __r = _mm_movelh_ps(__r, __r);
  __c = _mm_unpacklo_pi16(__a, __b);
  __r = _mm_cvtpi32_ps(__r, __c);

  return __r;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpu16_ps(__m64 __a) {
  __m64 __b, __c;
  __m128 __r;

  __b = _mm_setzero_si64();
  __c = _mm_unpackhi_pi16(__a, __b);
  __r = _mm_setzero_ps();
  __r = _mm_cvtpi32_ps(__r, __c);
  __r = _mm_movelh_ps(__r, __r);
  __c = _mm_unpacklo_pi16(__a, __b);
  __r = _mm_cvtpi32_ps(__r, __c);

  return __r;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpi8_ps(__m64 __a) {
  __m64 __b;

  __b = _mm_setzero_si64();
  __b = _mm_cmpgt_pi8(__b, __a);
  __b = _mm_unpacklo_pi8(__a, __b);

  return _mm_cvtpi16_ps(__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpu8_ps(__m64 __a) {
  __m64 __b;

  __b = _mm_setzero_si64();
  __b = _mm_unpacklo_pi8(__a, __b);

  return _mm_cvtpi16_ps(__b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtpi32x2_ps(__m64 __a, __m64 __b) {
  __m128 __c;

  __c = _mm_setzero_ps();
  __c = _mm_cvtpi32_ps(__c, __b);
  __c = _mm_movelh_ps(__c, __c);

  return _mm_cvtpi32_ps(__c, __a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtps_pi16(__m128 __a) {
  __m64 __b, __c;

  __b = _mm_cvtps_pi32(__a);
  __a = _mm_movehl_ps(__a, __a);
  __c = _mm_cvtps_pi32(__a);

  return _mm_packs_pi32(__b, __c);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_cvtps_pi8(__m128 __a) {
  __m64 __b, __c;

  __b = _mm_cvtps_pi16(__a);
  __c = _mm_setzero_si64();

  return _mm_packs_pi16(__b, __c);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse")))
    _mm_movemask_ps(__m128 __a) {
  return __builtin_ia32_movmskps(__a);
}

typedef double __m128d __attribute__((__vector_size__(16)));
typedef long long __m128i __attribute__((__vector_size__(16)));

typedef double __v2df __attribute__((__vector_size__(16)));
typedef long long __v2di __attribute__((__vector_size__(16)));
typedef short __v8hi __attribute__((__vector_size__(16)));
typedef char __v16qi __attribute__((__vector_size__(16)));

typedef signed char __v16qs __attribute__((__vector_size__(16)));

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("f16c")))
    _mm_cvtph_ps(__m128i __a) {
  return (__m128)__builtin_ia32_vcvtph2ps((__v8hi)__a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_sd(__m128d __a, __m128d __b) {
  __a[0] += __b[0];
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_pd(__m128d __a, __m128d __b) {
  return __a + __b;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_sd(__m128d __a, __m128d __b) {
  __a[0] -= __b[0];
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_pd(__m128d __a, __m128d __b) {
  return __a - __b;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mul_sd(__m128d __a, __m128d __b) {
  __a[0] *= __b[0];
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mul_pd(__m128d __a, __m128d __b) {
  return __a * __b;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_div_sd(__m128d __a, __m128d __b) {
  __a[0] /= __b[0];
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_div_pd(__m128d __a, __m128d __b) {
  return __a / __b;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sqrt_sd(__m128d __a, __m128d __b) {
  __m128d __c = __builtin_ia32_sqrtsd(__b);
  return (__m128d){__c[0], __a[1]};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sqrt_pd(__m128d __a) {
  return __builtin_ia32_sqrtpd(__a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_min_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_minsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_min_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_minpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_max_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_maxsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_max_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_maxpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_and_pd(__m128d __a, __m128d __b) {
  return (__m128d)((__v4si)__a & (__v4si)__b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_andnot_pd(__m128d __a, __m128d __b) {
  return (__m128d)(~(__v4si)__a & (__v4si)__b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_or_pd(__m128d __a, __m128d __b) {
  return (__m128d)((__v4si)__a | (__v4si)__b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_xor_pd(__m128d __a, __m128d __b) {
  return (__m128d)((__v4si)__a ^ (__v4si)__b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpeq_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpeqpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmplt_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpltpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmple_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmplepd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpgt_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpltpd(__b, __a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpge_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmplepd(__b, __a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpord_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpordpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpunord_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpunordpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpneq_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpneqpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnlt_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnltpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnle_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnlepd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpngt_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnltpd(__b, __a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnge_pd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnlepd(__b, __a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpeq_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpeqsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmplt_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpltsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmple_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmplesd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpgt_sd(__m128d __a, __m128d __b) {
  __m128d __c = __builtin_ia32_cmpltsd(__b, __a);
  return (__m128d){__c[0], __a[1]};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpge_sd(__m128d __a, __m128d __b) {
  __m128d __c = __builtin_ia32_cmplesd(__b, __a);
  return (__m128d){__c[0], __a[1]};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpord_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpordsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpunord_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpunordsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpneq_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpneqsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnlt_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnltsd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnle_sd(__m128d __a, __m128d __b) {
  return (__m128d)__builtin_ia32_cmpnlesd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpngt_sd(__m128d __a, __m128d __b) {
  __m128d __c = __builtin_ia32_cmpnltsd(__b, __a);
  return (__m128d){__c[0], __a[1]};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpnge_sd(__m128d __a, __m128d __b) {
  __m128d __c = __builtin_ia32_cmpnlesd(__b, __a);
  return (__m128d){__c[0], __a[1]};
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comieq_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdeq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comilt_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdlt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comile_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdle(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comigt_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdgt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comige_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdge(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_comineq_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_comisdneq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomieq_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdeq(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomilt_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdlt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomile_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdle(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomigt_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdgt(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomige_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdge(__a, __b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_ucomineq_sd(__m128d __a, __m128d __b) {
  return __builtin_ia32_ucomisdneq(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtpd_ps(__m128d __a) {
  return __builtin_ia32_cvtpd2ps(__a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtps_pd(__m128 __a) {
  return __builtin_ia32_cvtps2pd(__a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtepi32_pd(__m128i __a) {
  return __builtin_ia32_cvtdq2pd((__v4si)__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtpd_epi32(__m128d __a) {
  return __builtin_ia32_cvtpd2dq(__a);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsd_si32(__m128d __a) {
  return __builtin_ia32_cvtsd2si(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsd_ss(__m128 __a, __m128d __b) {
  __a[0] = __b[0];
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi32_sd(__m128d __a, int __b) {
  __a[0] = __b;
  return __a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtss_sd(__m128d __a, __m128 __b) {
  __a[0] = __b[0];
  return __a;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvttpd_epi32(__m128d __a) {
  return (__m128i)__builtin_ia32_cvttpd2dq(__a);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvttsd_si32(__m128d __a) {
  return __a[0];
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtpd_pi32(__m128d __a) {
  return (__m64)__builtin_ia32_cvtpd2pi(__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvttpd_pi32(__m128d __a) {
  return (__m64)__builtin_ia32_cvttpd2pi(__a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtpi32_pd(__m64 __a) {
  return __builtin_ia32_cvtpi2pd((__v2si)__a);
}

static __inline__ double
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsd_f64(__m128d __a) {
  return __a[0];
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_load_pd(double const *__dp) {
  return *(__m128d *)__dp;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_load1_pd(double const *__dp) {
  struct __mm_load1_pd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  double __u = ((struct __mm_load1_pd_struct *)__dp)->__u;
  return (__m128d){__u, __u};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadr_pd(double const *__dp) {
  __m128d __u = *(__m128d *)__dp;
  return __builtin_shufflevector(__u, __u, 1, 0);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadu_pd(double const *__dp) {
  struct __loadu_pd {
    __m128d __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_pd *)__dp)->__v;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_load_sd(double const *__dp) {
  struct __mm_load_sd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  double __u = ((struct __mm_load_sd_struct *)__dp)->__u;
  return (__m128d){__u, 0};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadh_pd(__m128d __a, double const *__dp) {
  struct __mm_loadh_pd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  double __u = ((struct __mm_loadh_pd_struct *)__dp)->__u;
  return (__m128d){__a[0], __u};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadl_pd(__m128d __a, double const *__dp) {
  struct __mm_loadl_pd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  double __u = ((struct __mm_loadl_pd_struct *)__dp)->__u;
  return (__m128d){__u, __a[1]};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_undefined_pd() {
  return (__m128d)__builtin_ia32_undef128();
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_sd(double __w) {
  return (__m128d){__w, 0};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_pd(double __w) {
  return (__m128d){__w, __w};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_pd(double __w, double __x) {
  return (__m128d){__x, __w};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setr_pd(double __w, double __x) {
  return (__m128d){__w, __x};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setzero_pd(void) {
  return (__m128d){0, 0};
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_move_sd(__m128d __a, __m128d __b) {
  return (__m128d){__b[0], __a[1]};
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_store_sd(double *__dp, __m128d __a) {
  struct __mm_store_sd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_store_sd_struct *)__dp)->__u = __a[0];
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_store1_pd(double *__dp, __m128d __a) {
  struct __mm_store1_pd_struct {
    double __u[2];
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_store1_pd_struct *)__dp)->__u[0] = __a[0];
  ((struct __mm_store1_pd_struct *)__dp)->__u[1] = __a[0];
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_store_pd(double *__dp, __m128d __a) {
  *(__m128d *)__dp = __a;
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storeu_pd(double *__dp, __m128d __a) {
  __builtin_ia32_storeupd(__dp, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storer_pd(double *__dp, __m128d __a) {
  __a = __builtin_shufflevector(__a, __a, 1, 0);
  *(__m128d *)__dp = __a;
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storeh_pd(double *__dp, __m128d __a) {
  struct __mm_storeh_pd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_storeh_pd_struct *)__dp)->__u = __a[1];
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storel_pd(double *__dp, __m128d __a) {
  struct __mm_storeh_pd_struct {
    double __u;
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_storeh_pd_struct *)__dp)->__u = __a[0];
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_epi8(__m128i __a, __m128i __b) {
  return (__m128i)((__v16qi)__a + (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_epi16(__m128i __a, __m128i __b) {
  return (__m128i)((__v8hi)__a + (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_epi32(__m128i __a, __m128i __b) {
  return (__m128i)((__v4si)__a + (__v4si)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_si64(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_paddq(__a, __b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_add_epi64(__m128i __a, __m128i __b) {
  return __a + __b;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_adds_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_paddsb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_adds_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_paddsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_adds_epu8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_paddusb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_adds_epu16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_paddusw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_avg_epu8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pavgb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_avg_epu16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pavgw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_madd_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmaddwd128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_max_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmaxsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_max_epu8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmaxub128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_min_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pminsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_min_epu8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pminub128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mulhi_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmulhw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mulhi_epu16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmulhuw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mullo_epi16(__m128i __a, __m128i __b) {
  return (__m128i)((__v8hi)__a * (__v8hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mul_su32(__m64 __a, __m64 __b) {
  return __builtin_ia32_pmuludq((__v2si)__a, (__v2si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mul_epu32(__m128i __a, __m128i __b) {
  return __builtin_ia32_pmuludq128((__v4si)__a, (__v4si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sad_epu8(__m128i __a, __m128i __b) {
  return __builtin_ia32_psadbw128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_epi8(__m128i __a, __m128i __b) {
  return (__m128i)((__v16qi)__a - (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_epi16(__m128i __a, __m128i __b) {
  return (__m128i)((__v8hi)__a - (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_epi32(__m128i __a, __m128i __b) {
  return (__m128i)((__v4si)__a - (__v4si)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_si64(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_psubq(__a, __b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sub_epi64(__m128i __a, __m128i __b) {
  return __a - __b;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_subs_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psubsb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_subs_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psubsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_subs_epu8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psubusb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_subs_epu16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psubusw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_and_si128(__m128i __a, __m128i __b) {
  return __a & __b;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_andnot_si128(__m128i __a, __m128i __b) {
  return ~__a & __b;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_or_si128(__m128i __a, __m128i __b) {
  return __a | __b;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_xor_si128(__m128i __a, __m128i __b) {
  return __a ^ __b;
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_slli_epi16(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_psllwi128((__v8hi)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sll_epi16(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_psllw128((__v8hi)__a, (__v8hi)__count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_slli_epi32(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_pslldi128((__v4si)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sll_epi32(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_pslld128((__v4si)__a, (__v4si)__count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_slli_epi64(__m128i __a, int __count) {
  return __builtin_ia32_psllqi128(__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sll_epi64(__m128i __a, __m128i __count) {
  return __builtin_ia32_psllq128(__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srai_epi16(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_psrawi128((__v8hi)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sra_epi16(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_psraw128((__v8hi)__a, (__v8hi)__count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srai_epi32(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_psradi128((__v4si)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_sra_epi32(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_psrad128((__v4si)__a, (__v4si)__count);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srli_epi16(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_psrlwi128((__v8hi)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srl_epi16(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_psrlw128((__v8hi)__a, (__v8hi)__count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srli_epi32(__m128i __a, int __count) {
  return (__m128i)__builtin_ia32_psrldi128((__v4si)__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srl_epi32(__m128i __a, __m128i __count) {
  return (__m128i)__builtin_ia32_psrld128((__v4si)__a, (__v4si)__count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srli_epi64(__m128i __a, int __count) {
  return __builtin_ia32_psrlqi128(__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_srl_epi64(__m128i __a, __m128i __count) {
  return __builtin_ia32_psrlq128(__a, __count);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpeq_epi8(__m128i __a, __m128i __b) {
  return (__m128i)((__v16qi)__a == (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpeq_epi16(__m128i __a, __m128i __b) {
  return (__m128i)((__v8hi)__a == (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpeq_epi32(__m128i __a, __m128i __b) {
  return (__m128i)((__v4si)__a == (__v4si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpgt_epi8(__m128i __a, __m128i __b) {

  return (__m128i)((__v16qs)__a > (__v16qs)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpgt_epi16(__m128i __a, __m128i __b) {
  return (__m128i)((__v8hi)__a > (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmpgt_epi32(__m128i __a, __m128i __b) {
  return (__m128i)((__v4si)__a > (__v4si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmplt_epi8(__m128i __a, __m128i __b) {
  return _mm_cmpgt_epi8(__b, __a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmplt_epi16(__m128i __a, __m128i __b) {
  return _mm_cmpgt_epi16(__b, __a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cmplt_epi32(__m128i __a, __m128i __b) {
  return _mm_cmpgt_epi32(__b, __a);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi64_sd(__m128d __a, long long __b) {
  __a[0] = __b;
  return __a;
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsd_si64(__m128d __a) {
  return __builtin_ia32_cvtsd2si64(__a);
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvttsd_si64(__m128d __a) {
  return __a[0];
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtepi32_ps(__m128i __a) {
  return __builtin_ia32_cvtdq2ps((__v4si)__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtps_epi32(__m128 __a) {
  return (__m128i)__builtin_ia32_cvtps2dq(__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvttps_epi32(__m128 __a) {
  return (__m128i)__builtin_ia32_cvttps2dq(__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi32_si128(int __a) {
  return (__m128i)(__v4si){__a, 0, 0, 0};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi64_si128(long long __a) {
  return (__m128i){__a, 0};
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi128_si32(__m128i __a) {
  __v4si __b = (__v4si)__a;
  return __b[0];
}

static __inline__ long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_cvtsi128_si64(__m128i __a) {
  return __a[0];
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_load_si128(__m128i const *__p) {
  return *__p;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadu_si128(__m128i const *__p) {
  struct __loadu_si128 {
    __m128i __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_si128 *)__p)->__v;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_loadl_epi64(__m128i const *__p) {
  struct __mm_loadl_epi64_struct {
    long long __u;
  } __attribute__((__packed__, __may_alias__));
  return (__m128i){((struct __mm_loadl_epi64_struct *)__p)->__u, 0};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_undefined_si128() {
  return (__m128i)__builtin_ia32_undef128();
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_epi64x(long long __q1, long long __q0) {
  return (__m128i){__q0, __q1};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_epi64(__m64 __q1, __m64 __q0) {
  return (__m128i){(long long)__q0, (long long)__q1};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_epi32(int __i3, int __i2, int __i1, int __i0) {
  return (__m128i)(__v4si){__i0, __i1, __i2, __i3};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_epi16(short __w7, short __w6, short __w5, short __w4, short __w3,
                  short __w2, short __w1, short __w0) {
  return (__m128i)(__v8hi){__w0, __w1, __w2, __w3, __w4, __w5, __w6, __w7};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set_epi8(char __b15, char __b14, char __b13, char __b12, char __b11,
                 char __b10, char __b9, char __b8, char __b7, char __b6,
                 char __b5, char __b4, char __b3, char __b2, char __b1,
                 char __b0) {
  return (__m128i)(__v16qi){__b0,  __b1,  __b2,  __b3, __b4,  __b5,
                            __b6,  __b7,  __b8,  __b9, __b10, __b11,
                            __b12, __b13, __b14, __b15};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_epi64x(long long __q) {
  return (__m128i){__q, __q};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_epi64(__m64 __q) {
  return (__m128i){(long long)__q, (long long)__q};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_epi32(int __i) {
  return (__m128i)(__v4si){__i, __i, __i, __i};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_epi16(short __w) {
  return (__m128i)(__v8hi){__w, __w, __w, __w, __w, __w, __w, __w};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_set1_epi8(char __b) {
  return (__m128i)(__v16qi){__b, __b, __b, __b, __b, __b, __b, __b,
                            __b, __b, __b, __b, __b, __b, __b, __b};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setr_epi64(__m64 __q0, __m64 __q1) {
  return (__m128i){(long long)__q0, (long long)__q1};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setr_epi32(int __i0, int __i1, int __i2, int __i3) {
  return (__m128i)(__v4si){__i0, __i1, __i2, __i3};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setr_epi16(short __w0, short __w1, short __w2, short __w3, short __w4,
                   short __w5, short __w6, short __w7) {
  return (__m128i)(__v8hi){__w0, __w1, __w2, __w3, __w4, __w5, __w6, __w7};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setr_epi8(char __b0, char __b1, char __b2, char __b3, char __b4,
                  char __b5, char __b6, char __b7, char __b8, char __b9,
                  char __b10, char __b11, char __b12, char __b13, char __b14,
                  char __b15) {
  return (__m128i)(__v16qi){__b0,  __b1,  __b2,  __b3, __b4,  __b5,
                            __b6,  __b7,  __b8,  __b9, __b10, __b11,
                            __b12, __b13, __b14, __b15};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_setzero_si128(void) {
  return (__m128i){0LL, 0LL};
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_store_si128(__m128i *__p, __m128i __b) {
  *__p = __b;
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storeu_si128(__m128i *__p, __m128i __b) {
  __builtin_ia32_storedqu((char *)__p, (__v16qi)__b);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_maskmoveu_si128(__m128i __d, __m128i __n, char *__p) {
  __builtin_ia32_maskmovdqu((__v16qi)__d, (__v16qi)__n, __p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_storel_epi64(__m128i *__p, __m128i __a) {
  struct __mm_storel_epi64_struct {
    long long __u;
  } __attribute__((__packed__, __may_alias__));
  ((struct __mm_storel_epi64_struct *)__p)->__u = __a[0];
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_stream_pd(double *__p, __m128d __a) {
  __builtin_ia32_movntpd(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_stream_si128(__m128i *__p, __m128i __a) {
  __builtin_ia32_movntdq(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_stream_si32(int *__p, int __a) {
  __builtin_ia32_movnti(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_stream_si64(long long *__p, long long __a) {
  __builtin_ia32_movnti64(__p, __a);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_clflush(void const *__p) {
  __builtin_ia32_clflush(__p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_lfence(void) {
  __builtin_ia32_lfence();
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_mfence(void) {
  __builtin_ia32_mfence();
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_packs_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_packsswb128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_packs_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_packssdw128((__v4si)__a, (__v4si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_packus_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_packuswb128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_extract_epi16(__m128i __a, int __imm) {
  __v8hi __b = (__v8hi)__a;
  return (unsigned short)__b[__imm & 7];
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_insert_epi16(__m128i __a, int __b, int __imm) {
  __v8hi __c = (__v8hi)__a;
  __c[__imm & 7] = __b;
  return (__m128i)__c;
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_movemask_epi8(__m128i __a) {
  return __builtin_ia32_pmovmskb128((__v16qi)__a);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpackhi_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector(
      (__v16qi)__a, (__v16qi)__b, 8, 16 + 8, 9, 16 + 9, 10, 16 + 10, 11,
      16 + 11, 12, 16 + 12, 13, 16 + 13, 14, 16 + 14, 15, 16 + 15);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpackhi_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector((__v8hi)__a, (__v8hi)__b, 4, 8 + 4, 5,
                                          8 + 5, 6, 8 + 6, 7, 8 + 7);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpackhi_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector((__v4si)__a, (__v4si)__b, 2, 4 + 2, 3,
                                          4 + 3);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpackhi_epi64(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector(__a, __b, 1, 2 + 1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpacklo_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector(
      (__v16qi)__a, (__v16qi)__b, 0, 16 + 0, 1, 16 + 1, 2, 16 + 2, 3, 16 + 3, 4,
      16 + 4, 5, 16 + 5, 6, 16 + 6, 7, 16 + 7);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpacklo_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector((__v8hi)__a, (__v8hi)__b, 0, 8 + 0, 1,
                                          8 + 1, 2, 8 + 2, 3, 8 + 3);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpacklo_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector((__v4si)__a, (__v4si)__b, 0, 4 + 0, 1,
                                          4 + 1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpacklo_epi64(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_shufflevector(__a, __b, 0, 2 + 0);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_movepi64_pi64(__m128i __a) {
  return (__m64)__a[0];
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_movpi64_epi64(__m64 __a) {
  return (__m128i){(long long)__a, 0};
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_move_epi64(__m128i __a) {
  return __builtin_shufflevector(__a, (__m128i){0}, 0, 2);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpackhi_pd(__m128d __a, __m128d __b) {
  return __builtin_shufflevector(__a, __b, 1, 2 + 1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_unpacklo_pd(__m128d __a, __m128d __b) {
  return __builtin_shufflevector(__a, __b, 0, 2 + 0);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_movemask_pd(__m128d __a) {
  return __builtin_ia32_movmskpd(__a);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castpd_ps(__m128d __a) {
  return (__m128)__a;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castpd_si128(__m128d __a) {
  return (__m128i)__a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castps_pd(__m128 __a) {
  return (__m128d)__a;
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castps_si128(__m128 __a) {
  return (__m128i)__a;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castsi128_ps(__m128i __a) {
  return (__m128)__a;
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_castsi128_pd(__m128i __a) {
  return (__m128d)__a;
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse2")))
    _mm_pause(void) {
  __builtin_ia32_pause();
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_lddqu_si128(__m128i const *__p) {
  return (__m128i)__builtin_ia32_lddqu((char const *)__p);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_addsub_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_addsubps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_hadd_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_haddps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_hsub_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_hsubps(__a, __b);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_movehdup_ps(__m128 __a) {
  return __builtin_shufflevector(__a, __a, 1, 1, 3, 3);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_moveldup_ps(__m128 __a) {
  return __builtin_shufflevector(__a, __a, 0, 0, 2, 2);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_addsub_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_addsubpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_hadd_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_haddpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_hsub_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_hsubpd(__a, __b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_movedup_pd(__m128d __a) {
  return __builtin_shufflevector(__a, __a, 0, 0);
}
static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_monitor(void const *__p, unsigned __extensions, unsigned __hints) {
  __builtin_ia32_monitor((void *)__p, __extensions, __hints);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("sse3")))
    _mm_mwait(unsigned __extensions, unsigned __hints) {
  __builtin_ia32_mwait(__extensions, __hints);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_pi8(__m64 __a) {
  return (__m64)__builtin_ia32_pabsb((__v8qi)__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_epi8(__m128i __a) {
  return (__m128i)__builtin_ia32_pabsb128((__v16qi)__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_pi16(__m64 __a) {
  return (__m64)__builtin_ia32_pabsw((__v4hi)__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_epi16(__m128i __a) {
  return (__m128i)__builtin_ia32_pabsw128((__v8hi)__a);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_pi32(__m64 __a) {
  return (__m64)__builtin_ia32_pabsd((__v2si)__a);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_abs_epi32(__m128i __a) {
  return (__m128i)__builtin_ia32_pabsd128((__v4si)__a);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadd_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phaddw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadd_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phaddd128((__v4si)__a, (__v4si)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadd_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phaddw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadd_pi32(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phaddd((__v2si)__a, (__v2si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadds_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phaddsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hadds_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phaddsw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsub_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phsubw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsub_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phsubd128((__v4si)__a, (__v4si)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsub_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phsubw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsub_pi32(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phsubd((__v2si)__a, (__v2si)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsubs_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_phsubsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_hsubs_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_phsubsw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_maddubs_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmaddubsw128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_maddubs_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pmaddubsw((__v8qi)__a, (__v8qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_mulhrs_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pmulhrsw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_mulhrs_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pmulhrsw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_shuffle_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_pshufb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_shuffle_pi8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_pshufb((__v8qi)__a, (__v8qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_epi8(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psignb128((__v16qi)__a, (__v16qi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_epi16(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psignw128((__v8hi)__a, (__v8hi)__b);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_epi32(__m128i __a, __m128i __b) {
  return (__m128i)__builtin_ia32_psignd128((__v4si)__a, (__v4si)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_pi8(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_psignb((__v8qi)__a, (__v8qi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_pi16(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_psignw((__v4hi)__a, (__v4hi)__b);
}

static __inline__ __m64
    __attribute__((__always_inline__, __nodebug__, __target__("ssse3")))
    _mm_sign_pi32(__m64 __a, __m64 __b) {
  return (__m64)__builtin_ia32_psignd((__v2si)__a, (__v2si)__b);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_blendv_pd(__m128d __V1, __m128d __V2, __m128d __M) {
  return (__m128d)__builtin_ia32_blendvpd((__v2df)__V1, (__v2df)__V2,
                                          (__v2df)__M);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_blendv_ps(__m128 __V1, __m128 __V2, __m128 __M) {
  return (__m128)__builtin_ia32_blendvps((__v4sf)__V1, (__v4sf)__V2,
                                         (__v4sf)__M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_blendv_epi8(__m128i __V1, __m128i __V2, __m128i __M) {
  return (__m128i)__builtin_ia32_pblendvb128((__v16qi)__V1, (__v16qi)__V2,
                                             (__v16qi)__M);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_mullo_epi32(__m128i __V1, __m128i __V2) {
  return (__m128i)((__v4si)__V1 * (__v4si)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_mul_epi32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pmuldq128((__v4si)__V1, (__v4si)__V2);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_stream_load_si128(__m128i const *__V) {
  return (__m128i)__builtin_ia32_movntdqa((const __v2di *)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_min_epi8(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pminsb128((__v16qi)__V1, (__v16qi)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_max_epi8(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pmaxsb128((__v16qi)__V1, (__v16qi)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_min_epu16(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pminuw128((__v8hi)__V1, (__v8hi)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_max_epu16(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pmaxuw128((__v8hi)__V1, (__v8hi)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_min_epi32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pminsd128((__v4si)__V1, (__v4si)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_max_epi32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pmaxsd128((__v4si)__V1, (__v4si)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_min_epu32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pminud128((__v4si)__V1, (__v4si)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_max_epu32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_pmaxud128((__v4si)__V1, (__v4si)__V2);
}
static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_testz_si128(__m128i __M, __m128i __V) {
  return __builtin_ia32_ptestz128((__v2di)__M, (__v2di)__V);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_testc_si128(__m128i __M, __m128i __V) {
  return __builtin_ia32_ptestc128((__v2di)__M, (__v2di)__V);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_testnzc_si128(__m128i __M, __m128i __V) {
  return __builtin_ia32_ptestnzc128((__v2di)__M, (__v2di)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cmpeq_epi64(__m128i __V1, __m128i __V2) {
  return (__m128i)((__v2di)__V1 == (__v2di)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi8_epi16(__m128i __V) {

  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v16qs)__V, (__v16qs)__V, 0, 1, 2, 3, 4, 5, 6,
                              7),
      __v8hi);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi8_epi32(__m128i __V) {

  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v16qs)__V, (__v16qs)__V, 0, 1, 2, 3), __v4si);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi8_epi64(__m128i __V) {

  typedef signed char __v16qs __attribute__((__vector_size__(16)));
  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v16qs)__V, (__v16qs)__V, 0, 1), __v2di);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi16_epi32(__m128i __V) {
  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v8hi)__V, (__v8hi)__V, 0, 1, 2, 3), __v4si);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi16_epi64(__m128i __V) {
  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v8hi)__V, (__v8hi)__V, 0, 1), __v2di);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepi32_epi64(__m128i __V) {
  return (__m128i) __builtin_convertvector(
      __builtin_shufflevector((__v4si)__V, (__v4si)__V, 0, 1), __v2di);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu8_epi16(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxbw128((__v16qi)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu8_epi32(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxbd128((__v16qi)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu8_epi64(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxbq128((__v16qi)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu16_epi32(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxwd128((__v8hi)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu16_epi64(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxwq128((__v8hi)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_cvtepu32_epi64(__m128i __V) {
  return (__m128i)__builtin_ia32_pmovzxdq128((__v4si)__V);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_packus_epi32(__m128i __V1, __m128i __V2) {
  return (__m128i)__builtin_ia32_packusdw128((__v4si)__V1, (__v4si)__V2);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.1")))
    _mm_minpos_epu16(__m128i __V) {
  return (__m128i)__builtin_ia32_phminposuw128((__v8hi)__V);
}
static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.2")))
    _mm_cmpgt_epi64(__m128i __V1, __m128i __V2) {
  return (__m128i)((__v2di)__V1 > (__v2di)__V2);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.2")))
    _mm_crc32_u8(unsigned int __C, unsigned char __D) {
  return __builtin_ia32_crc32qi(__C, __D);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.2")))
    _mm_crc32_u16(unsigned int __C, unsigned short __D) {
  return __builtin_ia32_crc32hi(__C, __D);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.2")))
    _mm_crc32_u32(unsigned int __C, unsigned int __D) {
  return __builtin_ia32_crc32si(__C, __D);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("sse4.2")))
    _mm_crc32_u64(unsigned long long __C, unsigned long long __D) {
  return __builtin_ia32_crc32di(__C, __D);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("aes")))
    _mm_aesenc_si128(__m128i __V, __m128i __R) {
  return (__m128i)__builtin_ia32_aesenc128(__V, __R);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("aes")))
    _mm_aesenclast_si128(__m128i __V, __m128i __R) {
  return (__m128i)__builtin_ia32_aesenclast128(__V, __R);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("aes")))
    _mm_aesdec_si128(__m128i __V, __m128i __R) {
  return (__m128i)__builtin_ia32_aesdec128(__V, __R);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("aes")))
    _mm_aesdeclast_si128(__m128i __V, __m128i __R) {
  return (__m128i)__builtin_ia32_aesdeclast128(__V, __R);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("aes")))
    _mm_aesimc_si128(__m128i __V) {
  return (__m128i)__builtin_ia32_aesimc128(__V);
}

typedef double __v4df __attribute__((__vector_size__(32)));
typedef float __v8sf __attribute__((__vector_size__(32)));
typedef long long __v4di __attribute__((__vector_size__(32)));
typedef int __v8si __attribute__((__vector_size__(32)));
typedef short __v16hi __attribute__((__vector_size__(32)));
typedef char __v32qi __attribute__((__vector_size__(32)));

typedef signed char __v32qs __attribute__((__vector_size__(32)));

typedef float __m256 __attribute__((__vector_size__(32)));
typedef double __m256d __attribute__((__vector_size__(32)));
typedef long long __m256i __attribute__((__vector_size__(32)));

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_add_pd(__m256d __a, __m256d __b) {
  return __a + __b;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_add_ps(__m256 __a, __m256 __b) {
  return __a + __b;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_sub_pd(__m256d __a, __m256d __b) {
  return __a - __b;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_sub_ps(__m256 __a, __m256 __b) {
  return __a - __b;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_addsub_pd(__m256d __a, __m256d __b) {
  return (__m256d)__builtin_ia32_addsubpd256((__v4df)__a, (__v4df)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_addsub_ps(__m256 __a, __m256 __b) {
  return (__m256)__builtin_ia32_addsubps256((__v8sf)__a, (__v8sf)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_div_pd(__m256d __a, __m256d __b) {
  return __a / __b;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_div_ps(__m256 __a, __m256 __b) {
  return __a / __b;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_max_pd(__m256d __a, __m256d __b) {
  return (__m256d)__builtin_ia32_maxpd256((__v4df)__a, (__v4df)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_max_ps(__m256 __a, __m256 __b) {
  return (__m256)__builtin_ia32_maxps256((__v8sf)__a, (__v8sf)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_min_pd(__m256d __a, __m256d __b) {
  return (__m256d)__builtin_ia32_minpd256((__v4df)__a, (__v4df)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_min_ps(__m256 __a, __m256 __b) {
  return (__m256)__builtin_ia32_minps256((__v8sf)__a, (__v8sf)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_mul_pd(__m256d __a, __m256d __b) {
  return __a * __b;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_mul_ps(__m256 __a, __m256 __b) {
  return __a * __b;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_sqrt_pd(__m256d __a) {
  return (__m256d)__builtin_ia32_sqrtpd256((__v4df)__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_sqrt_ps(__m256 __a) {
  return (__m256)__builtin_ia32_sqrtps256((__v8sf)__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_rsqrt_ps(__m256 __a) {
  return (__m256)__builtin_ia32_rsqrtps256((__v8sf)__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_rcp_ps(__m256 __a) {
  return (__m256)__builtin_ia32_rcpps256((__v8sf)__a);
}
static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_and_pd(__m256d __a, __m256d __b) {
  return (__m256d)((__v4di)__a & (__v4di)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_and_ps(__m256 __a, __m256 __b) {
  return (__m256)((__v8si)__a & (__v8si)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_andnot_pd(__m256d __a, __m256d __b) {
  return (__m256d)(~(__v4di)__a & (__v4di)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_andnot_ps(__m256 __a, __m256 __b) {
  return (__m256)(~(__v8si)__a & (__v8si)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_or_pd(__m256d __a, __m256d __b) {
  return (__m256d)((__v4di)__a | (__v4di)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_or_ps(__m256 __a, __m256 __b) {
  return (__m256)((__v8si)__a | (__v8si)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_xor_pd(__m256d __a, __m256d __b) {
  return (__m256d)((__v4di)__a ^ (__v4di)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_xor_ps(__m256 __a, __m256 __b) {
  return (__m256)((__v8si)__a ^ (__v8si)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_hadd_pd(__m256d __a, __m256d __b) {
  return (__m256d)__builtin_ia32_haddpd256((__v4df)__a, (__v4df)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_hadd_ps(__m256 __a, __m256 __b) {
  return (__m256)__builtin_ia32_haddps256((__v8sf)__a, (__v8sf)__b);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_hsub_pd(__m256d __a, __m256d __b) {
  return (__m256d)__builtin_ia32_hsubpd256((__v4df)__a, (__v4df)__b);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_hsub_ps(__m256 __a, __m256 __b) {
  return (__m256)__builtin_ia32_hsubps256((__v8sf)__a, (__v8sf)__b);
}

static __inline __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_permutevar_pd(__m128d __a, __m128i __c) {
  return (__m128d)__builtin_ia32_vpermilvarpd((__v2df)__a, (__v2di)__c);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_permutevar_pd(__m256d __a, __m256i __c) {
  return (__m256d)__builtin_ia32_vpermilvarpd256((__v4df)__a, (__v4di)__c);
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_permutevar_ps(__m128 __a, __m128i __c) {
  return (__m128)__builtin_ia32_vpermilvarps((__v4sf)__a, (__v4si)__c);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_permutevar_ps(__m256 __a, __m256i __c) {
  return (__m256)__builtin_ia32_vpermilvarps256((__v8sf)__a, (__v8si)__c);
}
static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_blendv_pd(__m256d __a, __m256d __b, __m256d __c) {
  return (__m256d)__builtin_ia32_blendvpd256((__v4df)__a, (__v4df)__b,
                                             (__v4df)__c);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_blendv_ps(__m256 __a, __m256 __b, __m256 __c) {
  return (__m256)__builtin_ia32_blendvps256((__v8sf)__a, (__v8sf)__b,
                                            (__v8sf)__c);
}
static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_extract_epi32(__m256i __a, const int __imm) {
  __v8si __b = (__v8si)__a;
  return __b[__imm & 7];
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_extract_epi16(__m256i __a, const int __imm) {
  __v16hi __b = (__v16hi)__a;
  return __b[__imm & 15];
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_extract_epi8(__m256i __a, const int __imm) {
  __v32qi __b = (__v32qi)__a;
  return __b[__imm & 31];
}

static __inline long long
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_extract_epi64(__m256i __a, const int __imm) {
  __v4di __b = (__v4di)__a;
  return __b[__imm & 3];
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_insert_epi32(__m256i __a, int __b, int const __imm) {
  __v8si __c = (__v8si)__a;
  __c[__imm & 7] = __b;
  return (__m256i)__c;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_insert_epi16(__m256i __a, int __b, int const __imm) {
  __v16hi __c = (__v16hi)__a;
  __c[__imm & 15] = __b;
  return (__m256i)__c;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_insert_epi8(__m256i __a, int __b, int const __imm) {
  __v32qi __c = (__v32qi)__a;
  __c[__imm & 31] = __b;
  return (__m256i)__c;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_insert_epi64(__m256i __a, long long __b, int const __imm) {
  __v4di __c = (__v4di)__a;
  __c[__imm & 3] = __b;
  return (__m256i)__c;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtepi32_pd(__m128i __a) {
  return (__m256d)__builtin_ia32_cvtdq2pd256((__v4si)__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtepi32_ps(__m256i __a) {
  return (__m256)__builtin_ia32_cvtdq2ps256((__v8si)__a);
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtpd_ps(__m256d __a) {
  return (__m128)__builtin_ia32_cvtpd2ps256((__v4df)__a);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtps_epi32(__m256 __a) {
  return (__m256i)__builtin_ia32_cvtps2dq256((__v8sf)__a);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtps_pd(__m128 __a) {
  return (__m256d)__builtin_ia32_cvtps2pd256((__v4sf)__a);
}

static __inline __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvttpd_epi32(__m256d __a) {
  return (__m128i)__builtin_ia32_cvttpd2dq256((__v4df)__a);
}

static __inline __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvtpd_epi32(__m256d __a) {
  return (__m128i)__builtin_ia32_cvtpd2dq256((__v4df)__a);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_cvttps_epi32(__m256 __a) {
  return (__m256i)__builtin_ia32_cvttps2dq256((__v8sf)__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_movehdup_ps(__m256 __a) {
  return __builtin_shufflevector(__a, __a, 1, 1, 3, 3, 5, 5, 7, 7);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_moveldup_ps(__m256 __a) {
  return __builtin_shufflevector(__a, __a, 0, 0, 2, 2, 4, 4, 6, 6);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_movedup_pd(__m256d __a) {
  return __builtin_shufflevector(__a, __a, 0, 0, 2, 2);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_unpackhi_pd(__m256d __a, __m256d __b) {
  return __builtin_shufflevector(__a, __b, 1, 5, 1 + 2, 5 + 2);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_unpacklo_pd(__m256d __a, __m256d __b) {
  return __builtin_shufflevector(__a, __b, 0, 4, 0 + 2, 4 + 2);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_unpackhi_ps(__m256 __a, __m256 __b) {
  return __builtin_shufflevector(__a, __b, 2, 10, 2 + 1, 10 + 1, 6, 14, 6 + 1,
                                 14 + 1);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_unpacklo_ps(__m256 __a, __m256 __b) {
  return __builtin_shufflevector(__a, __b, 0, 8, 0 + 1, 8 + 1, 4, 12, 4 + 1,
                                 12 + 1);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testz_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_vtestzpd((__v2df)__a, (__v2df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testc_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_vtestcpd((__v2df)__a, (__v2df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testnzc_pd(__m128d __a, __m128d __b) {
  return __builtin_ia32_vtestnzcpd((__v2df)__a, (__v2df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testz_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_vtestzps((__v4sf)__a, (__v4sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testc_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_vtestcps((__v4sf)__a, (__v4sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_testnzc_ps(__m128 __a, __m128 __b) {
  return __builtin_ia32_vtestnzcps((__v4sf)__a, (__v4sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testz_pd(__m256d __a, __m256d __b) {
  return __builtin_ia32_vtestzpd256((__v4df)__a, (__v4df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testc_pd(__m256d __a, __m256d __b) {
  return __builtin_ia32_vtestcpd256((__v4df)__a, (__v4df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testnzc_pd(__m256d __a, __m256d __b) {
  return __builtin_ia32_vtestnzcpd256((__v4df)__a, (__v4df)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testz_ps(__m256 __a, __m256 __b) {
  return __builtin_ia32_vtestzps256((__v8sf)__a, (__v8sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testc_ps(__m256 __a, __m256 __b) {
  return __builtin_ia32_vtestcps256((__v8sf)__a, (__v8sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testnzc_ps(__m256 __a, __m256 __b) {
  return __builtin_ia32_vtestnzcps256((__v8sf)__a, (__v8sf)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testz_si256(__m256i __a, __m256i __b) {
  return __builtin_ia32_ptestz256((__v4di)__a, (__v4di)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testc_si256(__m256i __a, __m256i __b) {
  return __builtin_ia32_ptestc256((__v4di)__a, (__v4di)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_testnzc_si256(__m256i __a, __m256i __b) {
  return __builtin_ia32_ptestnzc256((__v4di)__a, (__v4di)__b);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_movemask_pd(__m256d __a) {
  return __builtin_ia32_movmskpd256((__v4df)__a);
}

static __inline int
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_movemask_ps(__m256 __a) {
  return __builtin_ia32_movmskps256((__v8sf)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_zeroall(void) {
  __builtin_ia32_vzeroall();
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_zeroupper(void) {
  __builtin_ia32_vzeroupper();
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_broadcast_ss(float const *__a) {
  float __f = *__a;
  return (__m128)(__v4sf){__f, __f, __f, __f};
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_broadcast_sd(double const *__a) {
  double __d = *__a;
  return (__m256d)(__v4df){__d, __d, __d, __d};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_broadcast_ss(float const *__a) {
  float __f = *__a;
  return (__m256)(__v8sf){__f, __f, __f, __f, __f, __f, __f, __f};
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_broadcast_pd(__m128d const *__a) {
  return (__m256d)__builtin_ia32_vbroadcastf128_pd256(__a);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_broadcast_ps(__m128 const *__a) {
  return (__m256)__builtin_ia32_vbroadcastf128_ps256(__a);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_load_pd(double const *__p) {
  return *(__m256d *)__p;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_load_ps(float const *__p) {
  return *(__m256 *)__p;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu_pd(double const *__p) {
  struct __loadu_pd {
    __m256d __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_pd *)__p)->__v;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu_ps(float const *__p) {
  struct __loadu_ps {
    __m256 __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_ps *)__p)->__v;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_load_si256(__m256i const *__p) {
  return *__p;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu_si256(__m256i const *__p) {
  struct __loadu_si256 {
    __m256i __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_si256 *)__p)->__v;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_lddqu_si256(__m256i const *__p) {
  return (__m256i)__builtin_ia32_lddqu256((char const *)__p);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_store_pd(double *__p, __m256d __a) {
  *(__m256d *)__p = __a;
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_store_ps(float *__p, __m256 __a) {
  *(__m256 *)__p = __a;
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu_pd(double *__p, __m256d __a) {
  __builtin_ia32_storeupd256(__p, (__v4df)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu_ps(float *__p, __m256 __a) {
  __builtin_ia32_storeups256(__p, (__v8sf)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_store_si256(__m256i *__p, __m256i __a) {
  *__p = __a;
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu_si256(__m256i *__p, __m256i __a) {
  __builtin_ia32_storedqu256((char *)__p, (__v32qi)__a);
}

static __inline __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_maskload_pd(double const *__p, __m128i __m) {
  return (__m128d)__builtin_ia32_maskloadpd((const __v2df *)__p, (__v2di)__m);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_maskload_pd(double const *__p, __m256i __m) {
  return (__m256d)__builtin_ia32_maskloadpd256((const __v4df *)__p,
                                               (__v4di)__m);
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_maskload_ps(float const *__p, __m128i __m) {
  return (__m128)__builtin_ia32_maskloadps((const __v4sf *)__p, (__v4si)__m);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_maskload_ps(float const *__p, __m256i __m) {
  return (__m256)__builtin_ia32_maskloadps256((const __v8sf *)__p, (__v8si)__m);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_maskstore_ps(float *__p, __m256i __m, __m256 __a) {
  __builtin_ia32_maskstoreps256((__v8sf *)__p, (__v8si)__m, (__v8sf)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_maskstore_pd(double *__p, __m128i __m, __m128d __a) {
  __builtin_ia32_maskstorepd((__v2df *)__p, (__v2di)__m, (__v2df)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_maskstore_pd(double *__p, __m256i __m, __m256d __a) {
  __builtin_ia32_maskstorepd256((__v4df *)__p, (__v4di)__m, (__v4df)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm_maskstore_ps(float *__p, __m128i __m, __m128 __a) {
  __builtin_ia32_maskstoreps((__v4sf *)__p, (__v4si)__m, (__v4sf)__a);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_stream_si256(__m256i *__a, __m256i __b) {
  __builtin_ia32_movntdq256((__v4di *)__a, (__v4di)__b);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_stream_pd(double *__a, __m256d __b) {
  __builtin_ia32_movntpd256(__a, (__v4df)__b);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_stream_ps(float *__p, __m256 __a) {
  __builtin_ia32_movntps256(__p, (__v8sf)__a);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_undefined_pd() {
  return (__m256d)__builtin_ia32_undef256();
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_undefined_ps() {
  return (__m256)__builtin_ia32_undef256();
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_undefined_si256() {
  return (__m256i)__builtin_ia32_undef256();
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_pd(double __a, double __b, double __c, double __d) {
  return (__m256d){__d, __c, __b, __a};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_ps(float __a, float __b, float __c, float __d, float __e,
                  float __f, float __g, float __h) {
  return (__m256){__h, __g, __f, __e, __d, __c, __b, __a};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_epi32(int __i0, int __i1, int __i2, int __i3, int __i4, int __i5,
                     int __i6, int __i7) {
  return (__m256i)(__v8si){__i7, __i6, __i5, __i4, __i3, __i2, __i1, __i0};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_epi16(short __w15, short __w14, short __w13, short __w12,
                     short __w11, short __w10, short __w09, short __w08,
                     short __w07, short __w06, short __w05, short __w04,
                     short __w03, short __w02, short __w01, short __w00) {
  return (__m256i)(__v16hi){__w00, __w01, __w02, __w03, __w04, __w05,
                            __w06, __w07, __w08, __w09, __w10, __w11,
                            __w12, __w13, __w14, __w15};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_epi8(char __b31, char __b30, char __b29, char __b28, char __b27,
                    char __b26, char __b25, char __b24, char __b23, char __b22,
                    char __b21, char __b20, char __b19, char __b18, char __b17,
                    char __b16, char __b15, char __b14, char __b13, char __b12,
                    char __b11, char __b10, char __b09, char __b08, char __b07,
                    char __b06, char __b05, char __b04, char __b03, char __b02,
                    char __b01, char __b00) {
  return (__m256i)(__v32qi){__b00, __b01, __b02, __b03, __b04, __b05, __b06,
                            __b07, __b08, __b09, __b10, __b11, __b12, __b13,
                            __b14, __b15, __b16, __b17, __b18, __b19, __b20,
                            __b21, __b22, __b23, __b24, __b25, __b26, __b27,
                            __b28, __b29, __b30, __b31};
}

static __inline __m256i __attribute__((__always_inline__, __nodebug__,
                                       __target__("avx")))
_mm256_set_epi64x(long long __a, long long __b, long long __c, long long __d) {
  return (__m256i)(__v4di){__d, __c, __b, __a};
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_pd(double __a, double __b, double __c, double __d) {
  return (__m256d){__a, __b, __c, __d};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_ps(float __a, float __b, float __c, float __d, float __e,
                   float __f, float __g, float __h) {
  return (__m256){__a, __b, __c, __d, __e, __f, __g, __h};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_epi32(int __i0, int __i1, int __i2, int __i3, int __i4,
                      int __i5, int __i6, int __i7) {
  return (__m256i)(__v8si){__i0, __i1, __i2, __i3, __i4, __i5, __i6, __i7};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_epi16(short __w15, short __w14, short __w13, short __w12,
                      short __w11, short __w10, short __w09, short __w08,
                      short __w07, short __w06, short __w05, short __w04,
                      short __w03, short __w02, short __w01, short __w00) {
  return (__m256i)(__v16hi){__w15, __w14, __w13, __w12, __w11, __w10,
                            __w09, __w08, __w07, __w06, __w05, __w04,
                            __w03, __w02, __w01, __w00};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_epi8(char __b31, char __b30, char __b29, char __b28, char __b27,
                     char __b26, char __b25, char __b24, char __b23, char __b22,
                     char __b21, char __b20, char __b19, char __b18, char __b17,
                     char __b16, char __b15, char __b14, char __b13, char __b12,
                     char __b11, char __b10, char __b09, char __b08, char __b07,
                     char __b06, char __b05, char __b04, char __b03, char __b02,
                     char __b01, char __b00) {
  return (__m256i)(__v32qi){__b31, __b30, __b29, __b28, __b27, __b26, __b25,
                            __b24, __b23, __b22, __b21, __b20, __b19, __b18,
                            __b17, __b16, __b15, __b14, __b13, __b12, __b11,
                            __b10, __b09, __b08, __b07, __b06, __b05, __b04,
                            __b03, __b02, __b01, __b00};
}

static __inline __m256i __attribute__((__always_inline__, __nodebug__,
                                       __target__("avx")))
_mm256_setr_epi64x(long long __a, long long __b, long long __c, long long __d) {
  return (__m256i)(__v4di){__a, __b, __c, __d};
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_pd(double __w) {
  return (__m256d){__w, __w, __w, __w};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_ps(float __w) {
  return (__m256){__w, __w, __w, __w, __w, __w, __w, __w};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_epi32(int __i) {
  return (__m256i)(__v8si){__i, __i, __i, __i, __i, __i, __i, __i};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_epi16(short __w) {
  return (__m256i)(__v16hi){__w, __w, __w, __w, __w, __w, __w, __w,
                            __w, __w, __w, __w, __w, __w, __w, __w};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_epi8(char __b) {
  return (__m256i)(__v32qi){__b, __b, __b, __b, __b, __b, __b, __b,
                            __b, __b, __b, __b, __b, __b, __b, __b,
                            __b, __b, __b, __b, __b, __b, __b, __b,
                            __b, __b, __b, __b, __b, __b, __b, __b};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set1_epi64x(long long __q) {
  return (__m256i)(__v4di){__q, __q, __q, __q};
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setzero_pd(void) {
  return (__m256d){0, 0, 0, 0};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setzero_ps(void) {
  return (__m256){0, 0, 0, 0, 0, 0, 0, 0};
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setzero_si256(void) {
  return (__m256i){0LL, 0LL, 0LL, 0LL};
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castpd_ps(__m256d __a) {
  return (__m256)__a;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castpd_si256(__m256d __a) {
  return (__m256i)__a;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castps_pd(__m256 __a) {
  return (__m256d)__a;
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castps_si256(__m256 __a) {
  return (__m256i)__a;
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castsi256_ps(__m256i __a) {
  return (__m256)__a;
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castsi256_pd(__m256i __a) {
  return (__m256d)__a;
}

static __inline __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castpd256_pd128(__m256d __a) {
  return __builtin_shufflevector(__a, __a, 0, 1);
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castps256_ps128(__m256 __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, 2, 3);
}

static __inline __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castsi256_si128(__m256i __a) {
  return __builtin_shufflevector(__a, __a, 0, 1);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castpd128_pd256(__m128d __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, -1, -1);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castps128_ps256(__m128 __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, 2, 3, -1, -1, -1, -1);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_castsi128_si256(__m128i __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, -1, -1);
}
static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu2_m128(float const *__addr_hi, float const *__addr_lo) {
  struct __loadu_ps {
    __m128 __v;
  } __attribute__((__packed__, __may_alias__));

  __m256 __v256 = _mm256_castps128_ps256(((struct __loadu_ps *)__addr_lo)->__v);
  return __extension__({
    (__m256) __builtin_shufflevector(
        (__v8sf)(__m256)(__v256),
        (__v8sf)_mm256_castps128_ps256(
            (__m128)(((struct __loadu_ps *)__addr_hi)->__v)),
        (((1) & 1) ? 0 : 8), (((1) & 1) ? 1 : 9), (((1) & 1) ? 2 : 10),
        (((1) & 1) ? 3 : 11), (((1) & 1) ? 8 : 4), (((1) & 1) ? 9 : 5),
        (((1) & 1) ? 10 : 6), (((1) & 1) ? 11 : 7));
  });
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu2_m128d(double const *__addr_hi, double const *__addr_lo) {
  struct __loadu_pd {
    __m128d __v;
  } __attribute__((__packed__, __may_alias__));

  __m256d __v256 =
      _mm256_castpd128_pd256(((struct __loadu_pd *)__addr_lo)->__v);
  return __extension__({
    (__m256d)
        __builtin_shufflevector((__v4df)(__m256d)(__v256),
                                (__v4df)_mm256_castpd128_pd256((__m128d)(
                                    ((struct __loadu_pd *)__addr_hi)->__v)),
                                (((1) & 1) ? 0 : 4), (((1) & 1) ? 1 : 5),
                                (((1) & 1) ? 4 : 2), (((1) & 1) ? 5 : 3));
  });
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_loadu2_m128i(__m128i const *__addr_hi, __m128i const *__addr_lo) {
  struct __loadu_si128 {
    __m128i __v;
  } __attribute__((__packed__, __may_alias__));
  __m256i __v256 =
      _mm256_castsi128_si256(((struct __loadu_si128 *)__addr_lo)->__v);
  return __extension__({
    (__m256i)
        __builtin_shufflevector((__v4di)(__m256i)(__v256),
                                (__v4di)_mm256_castsi128_si256((__m128i)(
                                    ((struct __loadu_si128 *)__addr_hi)->__v)),
                                (((1) & 1) ? 0 : 4), (((1) & 1) ? 1 : 5),
                                (((1) & 1) ? 4 : 2), (((1) & 1) ? 5 : 3));
  });
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu2_m128(float *__addr_hi, float *__addr_lo, __m256 __a) {
  __m128 __v128;

  __v128 = _mm256_castps256_ps128(__a);
  __builtin_ia32_storeups(__addr_lo, __v128);
  __v128 = __extension__({
    (__m128) __builtin_shufflevector((__v8sf)(__m256)(__a),
                                     (__v8sf)(_mm256_setzero_ps()),
                                     (((1) & 1) ? 4 : 0), (((1) & 1) ? 5 : 1),
                                     (((1) & 1) ? 6 : 2), (((1) & 1) ? 7 : 3));
  });
  __builtin_ia32_storeups(__addr_hi, __v128);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu2_m128d(double *__addr_hi, double *__addr_lo, __m256d __a) {
  __m128d __v128;

  __v128 = _mm256_castpd256_pd128(__a);
  __builtin_ia32_storeupd(__addr_lo, __v128);
  __v128 = __extension__({
    (__m128d) __builtin_shufflevector((__v4df)(__m256d)(__a),
                                      (__v4df)(_mm256_setzero_pd()),
                                      (((1) & 1) ? 2 : 0), (((1) & 1) ? 3 : 1));
  });
  __builtin_ia32_storeupd(__addr_hi, __v128);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_storeu2_m128i(__m128i *__addr_hi, __m128i *__addr_lo, __m256i __a) {
  __m128i __v128;

  __v128 = _mm256_castsi256_si128(__a);
  __builtin_ia32_storedqu((char *)__addr_lo, (__v16qi)__v128);
  __v128 = __extension__({
    (__m128i) __builtin_shufflevector((__v4di)(__m256i)(__a),
                                      (__v4di)(_mm256_setzero_si256()),
                                      (((1) & 1) ? 2 : 0), (((1) & 1) ? 3 : 1));
  });
  __builtin_ia32_storedqu((char *)__addr_hi, (__v16qi)__v128);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_m128(__m128 __hi, __m128 __lo) {
  return (__m256)__builtin_shufflevector(__lo, __hi, 0, 1, 2, 3, 4, 5, 6, 7);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_m128d(__m128d __hi, __m128d __lo) {
  return (__m256d)_mm256_set_m128((__m128)__hi, (__m128)__lo);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_set_m128i(__m128i __hi, __m128i __lo) {
  return (__m256i)_mm256_set_m128((__m128)__hi, (__m128)__lo);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_m128(__m128 __lo, __m128 __hi) {
  return _mm256_set_m128(__hi, __lo);
}

static __inline __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_m128d(__m128d __lo, __m128d __hi) {
  return (__m256d)_mm256_set_m128((__m128)__hi, (__m128)__lo);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx")))
    _mm256_setr_m128i(__m128i __lo, __m128i __hi) {
  return (__m256i)_mm256_set_m128((__m128)__hi, (__m128)__lo);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_abs_epi8(__m256i __a) {
  return (__m256i)__builtin_ia32_pabsb256((__v32qi)__a);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_abs_epi16(__m256i __a) {
  return (__m256i)__builtin_ia32_pabsw256((__v16hi)__a);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_abs_epi32(__m256i __a) {
  return (__m256i)__builtin_ia32_pabsd256((__v8si)__a);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_packs_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_packsswb256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_packs_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_packssdw256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_packus_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_packuswb256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_packus_epi32(__m256i __V1, __m256i __V2) {
  return (__m256i)__builtin_ia32_packusdw256((__v8si)__V1, (__v8si)__V2);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_add_epi8(__m256i __a, __m256i __b) {
  return (__m256i)((__v32qi)__a + (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_add_epi16(__m256i __a, __m256i __b) {
  return (__m256i)((__v16hi)__a + (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_add_epi32(__m256i __a, __m256i __b) {
  return (__m256i)((__v8si)__a + (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_add_epi64(__m256i __a, __m256i __b) {
  return __a + __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_adds_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_paddsb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_adds_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_paddsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_adds_epu8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_paddusb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_adds_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_paddusw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_and_si256(__m256i __a, __m256i __b) {
  return __a & __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_andnot_si256(__m256i __a, __m256i __b) {
  return ~__a & __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_avg_epu8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pavgb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_avg_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pavgw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_blendv_epi8(__m256i __V1, __m256i __V2, __m256i __M) {
  return (__m256i)__builtin_ia32_pblendvb256((__v32qi)__V1, (__v32qi)__V2,
                                             (__v32qi)__M);
}
static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpeq_epi8(__m256i __a, __m256i __b) {
  return (__m256i)((__v32qi)__a == (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpeq_epi16(__m256i __a, __m256i __b) {
  return (__m256i)((__v16hi)__a == (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpeq_epi32(__m256i __a, __m256i __b) {
  return (__m256i)((__v8si)__a == (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpeq_epi64(__m256i __a, __m256i __b) {
  return (__m256i)(__a == __b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpgt_epi8(__m256i __a, __m256i __b) {

  return (__m256i)((__v32qs)__a > (__v32qs)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpgt_epi16(__m256i __a, __m256i __b) {
  return (__m256i)((__v16hi)__a > (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpgt_epi32(__m256i __a, __m256i __b) {
  return (__m256i)((__v8si)__a > (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cmpgt_epi64(__m256i __a, __m256i __b) {
  return (__m256i)(__a > __b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hadd_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phaddw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hadd_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phaddd256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hadds_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phaddsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hsub_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phsubw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hsub_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phsubd256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_hsubs_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_phsubsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_maddubs_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaddubsw256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_madd_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaddwd256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxsb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxsd256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epu8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxub256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxuw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_max_epu32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmaxud256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminsb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminsd256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epu8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminub256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminuw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_min_epu32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pminud256((__v8si)__a, (__v8si)__b);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_movemask_epi8(__m256i __a) {
  return __builtin_ia32_pmovmskb256((__v32qi)__a);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi8_epi16(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxbw256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi8_epi32(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxbd256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi8_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxbq256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi16_epi32(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxwd256((__v8hi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi16_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxwq256((__v8hi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepi32_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovsxdq256((__v4si)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu8_epi16(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxbw256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu8_epi32(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxbd256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu8_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxbq256((__v16qi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu16_epi32(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxwd256((__v8hi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu16_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxwq256((__v8hi)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_cvtepu32_epi64(__m128i __V) {
  return (__m256i)__builtin_ia32_pmovzxdq256((__v4si)__V);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mul_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmuldq256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mulhrs_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmulhrsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mulhi_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmulhuw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mulhi_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pmulhw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mullo_epi16(__m256i __a, __m256i __b) {
  return (__m256i)((__v16hi)__a * (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mullo_epi32(__m256i __a, __m256i __b) {
  return (__m256i)((__v8si)__a * (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_mul_epu32(__m256i __a, __m256i __b) {
  return __builtin_ia32_pmuludq256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_or_si256(__m256i __a, __m256i __b) {
  return __a | __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sad_epu8(__m256i __a, __m256i __b) {
  return __builtin_ia32_psadbw256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_shuffle_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_pshufb256((__v32qi)__a, (__v32qi)__b);
}
static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sign_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psignb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sign_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psignw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sign_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psignd256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_slli_epi16(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_psllwi256((__v16hi)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sll_epi16(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_psllw256((__v16hi)__a, (__v8hi)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_slli_epi32(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_pslldi256((__v8si)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sll_epi32(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_pslld256((__v8si)__a, (__v4si)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_slli_epi64(__m256i __a, int __count) {
  return __builtin_ia32_psllqi256(__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sll_epi64(__m256i __a, __m128i __count) {
  return __builtin_ia32_psllq256(__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srai_epi16(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_psrawi256((__v16hi)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sra_epi16(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_psraw256((__v16hi)__a, (__v8hi)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srai_epi32(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_psradi256((__v8si)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sra_epi32(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_psrad256((__v8si)__a, (__v4si)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srli_epi16(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_psrlwi256((__v16hi)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srl_epi16(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_psrlw256((__v16hi)__a, (__v8hi)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srli_epi32(__m256i __a, int __count) {
  return (__m256i)__builtin_ia32_psrldi256((__v8si)__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srl_epi32(__m256i __a, __m128i __count) {
  return (__m256i)__builtin_ia32_psrld256((__v8si)__a, (__v4si)__count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srli_epi64(__m256i __a, int __count) {
  return __builtin_ia32_psrlqi256(__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srl_epi64(__m256i __a, __m128i __count) {
  return __builtin_ia32_psrlq256(__a, __count);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sub_epi8(__m256i __a, __m256i __b) {
  return (__m256i)((__v32qi)__a - (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sub_epi16(__m256i __a, __m256i __b) {
  return (__m256i)((__v16hi)__a - (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sub_epi32(__m256i __a, __m256i __b) {
  return (__m256i)((__v8si)__a - (__v8si)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sub_epi64(__m256i __a, __m256i __b) {
  return __a - __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_subs_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psubsb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_subs_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psubsw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_subs_epu8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psubusb256((__v32qi)__a, (__v32qi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_subs_epu16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_psubusw256((__v16hi)__a, (__v16hi)__b);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpackhi_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(
      (__v32qi)__a, (__v32qi)__b, 8, 32 + 8, 9, 32 + 9, 10, 32 + 10, 11,
      32 + 11, 12, 32 + 12, 13, 32 + 13, 14, 32 + 14, 15, 32 + 15, 24, 32 + 24,
      25, 32 + 25, 26, 32 + 26, 27, 32 + 27, 28, 32 + 28, 29, 32 + 29, 30,
      32 + 30, 31, 32 + 31);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpackhi_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(
      (__v16hi)__a, (__v16hi)__b, 4, 16 + 4, 5, 16 + 5, 6, 16 + 6, 7, 16 + 7,
      12, 16 + 12, 13, 16 + 13, 14, 16 + 14, 15, 16 + 15);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpackhi_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector((__v8si)__a, (__v8si)__b, 2, 8 + 2, 3,
                                          8 + 3, 6, 8 + 6, 7, 8 + 7);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpackhi_epi64(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(__a, __b, 1, 4 + 1, 3, 4 + 3);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpacklo_epi8(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(
      (__v32qi)__a, (__v32qi)__b, 0, 32 + 0, 1, 32 + 1, 2, 32 + 2, 3, 32 + 3, 4,
      32 + 4, 5, 32 + 5, 6, 32 + 6, 7, 32 + 7, 16, 32 + 16, 17, 32 + 17, 18,
      32 + 18, 19, 32 + 19, 20, 32 + 20, 21, 32 + 21, 22, 32 + 22, 23, 32 + 23);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpacklo_epi16(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(
      (__v16hi)__a, (__v16hi)__b, 0, 16 + 0, 1, 16 + 1, 2, 16 + 2, 3, 16 + 3, 8,
      16 + 8, 9, 16 + 9, 10, 16 + 10, 11, 16 + 11);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpacklo_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector((__v8si)__a, (__v8si)__b, 0, 8 + 0, 1,
                                          8 + 1, 4, 8 + 4, 5, 8 + 5);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_unpacklo_epi64(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_shufflevector(__a, __b, 0, 4 + 0, 2, 4 + 2);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_xor_si256(__m256i __a, __m256i __b) {
  return __a ^ __b;
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_stream_load_si256(__m256i const *__V) {
  return (__m256i)__builtin_ia32_movntdqa256((const __v4di *)__V);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastss_ps(__m128 __X) {
  return (__m128)__builtin_shufflevector((__v4sf)__X, (__v4sf)__X, 0, 0, 0, 0);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastsd_pd(__m128d __a) {
  return __builtin_shufflevector(__a, __a, 0, 0);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastss_ps(__m128 __X) {
  return (__m256)__builtin_shufflevector((__v4sf)__X, (__v4sf)__X, 0, 0, 0, 0,
                                         0, 0, 0, 0);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastsd_pd(__m128d __X) {
  return (__m256d)__builtin_shufflevector((__v2df)__X, (__v2df)__X, 0, 0, 0, 0);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastsi128_si256(__m128i __X) {
  return (__m256i)__builtin_shufflevector(__X, __X, 0, 1, 0, 1);
}
static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastb_epi8(__m128i __X) {
  return (__m256i)__builtin_shufflevector(
      (__v16qi)__X, (__v16qi)__X, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastw_epi16(__m128i __X) {
  return (__m256i)__builtin_shufflevector((__v8hi)__X, (__v8hi)__X, 0, 0, 0, 0,
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastd_epi32(__m128i __X) {
  return (__m256i)__builtin_shufflevector((__v4si)__X, (__v4si)__X, 0, 0, 0, 0,
                                          0, 0, 0, 0);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_broadcastq_epi64(__m128i __X) {
  return (__m256i)__builtin_shufflevector(__X, __X, 0, 0, 0, 0);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastb_epi8(__m128i __X) {
  return (__m128i)__builtin_shufflevector((__v16qi)__X, (__v16qi)__X, 0, 0, 0,
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                          0);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastw_epi16(__m128i __X) {
  return (__m128i)__builtin_shufflevector((__v8hi)__X, (__v8hi)__X, 0, 0, 0, 0,
                                          0, 0, 0, 0);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastd_epi32(__m128i __X) {
  return (__m128i)__builtin_shufflevector((__v4si)__X, (__v4si)__X, 0, 0, 0, 0);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_broadcastq_epi64(__m128i __X) {
  return (__m128i)__builtin_shufflevector(__X, __X, 0, 0);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_permutevar8x32_epi32(__m256i __a, __m256i __b) {
  return (__m256i)__builtin_ia32_permvarsi256((__v8si)__a, (__v8si)__b);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_permutevar8x32_ps(__m256 __a, __m256i __b) {
  return (__m256)__builtin_ia32_permvarsf256((__v8sf)__a, (__v8si)__b);
}
static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_maskload_epi32(int const *__X, __m256i __M) {
  return (__m256i)__builtin_ia32_maskloadd256((const __v8si *)__X, (__v8si)__M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_maskload_epi64(long long const *__X, __m256i __M) {
  return (__m256i)__builtin_ia32_maskloadq256((const __v4di *)__X, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_maskload_epi32(int const *__X, __m128i __M) {
  return (__m128i)__builtin_ia32_maskloadd((const __v4si *)__X, (__v4si)__M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_maskload_epi64(long long const *__X, __m128i __M) {
  return (__m128i)__builtin_ia32_maskloadq((const __v2di *)__X, (__v2di)__M);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_maskstore_epi32(int *__X, __m256i __M, __m256i __Y) {
  __builtin_ia32_maskstored256((__v8si *)__X, (__v8si)__M, (__v8si)__Y);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_maskstore_epi64(long long *__X, __m256i __M, __m256i __Y) {
  __builtin_ia32_maskstoreq256((__v4di *)__X, __M, __Y);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_maskstore_epi32(int *__X, __m128i __M, __m128i __Y) {
  __builtin_ia32_maskstored((__v4si *)__X, (__v4si)__M, (__v4si)__Y);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_maskstore_epi64(long long *__X, __m128i __M, __m128i __Y) {
  __builtin_ia32_maskstoreq((__v2di *)__X, __M, __Y);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sllv_epi32(__m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_psllv8si((__v8si)__X, (__v8si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_sllv_epi32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_psllv4si((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_sllv_epi64(__m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_psllv4di(__X, __Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_sllv_epi64(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_psllv2di(__X, __Y);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srav_epi32(__m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_psrav8si((__v8si)__X, (__v8si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_srav_epi32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_psrav4si((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srlv_epi32(__m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_psrlv8si((__v8si)__X, (__v8si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_srlv_epi32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_psrlv4si((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm256_srlv_epi64(__m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_psrlv4di(__X, __Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx2")))
    _mm_srlv_epi64(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_psrlv2di(__X, __Y);
}

static __inline __m256
    __attribute__((__always_inline__, __nodebug__, __target__("f16c")))
    _mm256_cvtph_ps(__m128i __a) {
  return (__m256)__builtin_ia32_vcvtph2ps256((__v8hi)__a);
}

static __inline__ unsigned short __attribute__((__always_inline__, __nodebug__))
__tzcnt_u16(unsigned short __X) {
  return __X ? __builtin_ctzs(__X) : 16;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __andn_u32(unsigned int __X, unsigned int __Y) {
  return ~__X & __Y;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __bextr_u32(unsigned int __X, unsigned int __Y) {
  return __builtin_ia32_bextr_u32(__X, __Y);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    _bextr_u32(unsigned int __X, unsigned int __Y, unsigned int __Z) {
  return __builtin_ia32_bextr_u32(__X, ((__Y & 0xff) | ((__Z & 0xff) << 8)));
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsi_u32(unsigned int __X) {
  return __X & -__X;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsmsk_u32(unsigned int __X) {
  return __X ^ (__X - 1);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsr_u32(unsigned int __X) {
  return __X & (__X - 1);
}

static __inline__ unsigned int __attribute__((__always_inline__, __nodebug__))
__tzcnt_u32(unsigned int __X) {
  return __X ? __builtin_ctz(__X) : 32;
}
static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __andn_u64(unsigned long long __X, unsigned long long __Y) {
  return ~__X & __Y;
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __bextr_u64(unsigned long long __X, unsigned long long __Y) {
  return __builtin_ia32_bextr_u64(__X, __Y);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    _bextr_u64(unsigned long long __X, unsigned int __Y, unsigned int __Z) {
  return __builtin_ia32_bextr_u64(__X, ((__Y & 0xff) | ((__Z & 0xff) << 8)));
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsi_u64(unsigned long long __X) {
  return __X & -__X;
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsmsk_u64(unsigned long long __X) {
  return __X ^ (__X - 1);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi")))
    __blsr_u64(unsigned long long __X) {
  return __X & (__X - 1);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__))
    __tzcnt_u64(unsigned long long __X) {
  return __X ? __builtin_ctzll(__X) : 64;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _bzhi_u32(unsigned int __X, unsigned int __Y) {
  return __builtin_ia32_bzhi_si(__X, __Y);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _pdep_u32(unsigned int __X, unsigned int __Y) {
  return __builtin_ia32_pdep_si(__X, __Y);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _pext_u32(unsigned int __X, unsigned int __Y) {
  return __builtin_ia32_pext_si(__X, __Y);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _bzhi_u64(unsigned long long __X, unsigned long long __Y) {
  return __builtin_ia32_bzhi_di(__X, __Y);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _pdep_u64(unsigned long long __X, unsigned long long __Y) {
  return __builtin_ia32_pdep_di(__X, __Y);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _pext_u64(unsigned long long __X, unsigned long long __Y) {
  return __builtin_ia32_pext_di(__X, __Y);
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("bmi2")))
    _mulx_u64(unsigned long long __X, unsigned long long __Y,
              unsigned long long *__P) {
  unsigned __int128 __res = (unsigned __int128)__X * __Y;
  *__P = (unsigned long long)(__res >> 64);
  return (unsigned long long)__res;
}

static __inline__ unsigned short
    __attribute__((__always_inline__, __nodebug__, __target__("lzcnt")))
    __lzcnt16(unsigned short __X) {
  return __X ? __builtin_clzs(__X) : 16;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("lzcnt")))
    __lzcnt32(unsigned int __X) {
  return __X ? __builtin_clz(__X) : 32;
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("lzcnt")))
    _lzcnt_u32(unsigned int __X) {
  return __X ? __builtin_clz(__X) : 32;
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("lzcnt")))
    __lzcnt64(unsigned long long __X) {
  return __X ? __builtin_clzll(__X) : 64;
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("lzcnt")))
    _lzcnt_u64(unsigned long long __X) {
  return __X ? __builtin_clzll(__X) : 64;
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmadd_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmadd_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmadd_ss(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddss(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmadd_sd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsub_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmsubps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsub_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmsubpd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsub_ss(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmsubss(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsub_sd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmsubsd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmadd_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmaddps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmadd_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmaddpd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmadd_ss(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmaddss(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmadd_sd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmaddsd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmsub_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmsubps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmsub_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmsubpd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmsub_ss(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmsubss(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fnmsub_sd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmsubsd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmaddsub_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddsubps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmaddsub_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsubpd(__A, __B, __C);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsubadd_ps(__m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmsubaddps(__A, __B, __C);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm_fmsubadd_pd(__m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmsubaddpd(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmadd_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmadd_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmsub_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmsubps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmsub_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmsubpd256(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fnmadd_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfnmaddps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fnmadd_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfnmaddpd256(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fnmsub_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfnmsubps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fnmsub_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfnmsubpd256(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmaddsub_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddsubps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmaddsub_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256(__A, __B, __C);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmsubadd_ps(__m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmsubaddps256(__A, __B, __C);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("fma")))
    _mm256_fmsubadd_pd(__m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmsubaddpd256(__A, __B, __C);
}

typedef double __v8df __attribute__((__vector_size__(64)));
typedef float __v16sf __attribute__((__vector_size__(64)));
typedef long long __v8di __attribute__((__vector_size__(64)));
typedef int __v16si __attribute__((__vector_size__(64)));

typedef float __m512 __attribute__((__vector_size__(64)));
typedef double __m512d __attribute__((__vector_size__(64)));
typedef long long __m512i __attribute__((__vector_size__(64)));

typedef unsigned char __mmask8;
typedef unsigned short __mmask16;
static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_setzero_si512(void) {
  return (__m512i)(__v8di){0, 0, 0, 0, 0, 0, 0, 0};
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_undefined_pd() {
  return (__m512d)__builtin_ia32_undef512();
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_undefined() {
  return (__m512)__builtin_ia32_undef512();
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_undefined_ps() {
  return (__m512)__builtin_ia32_undef512();
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_undefined_epi32() {
  return (__m512i)__builtin_ia32_undef512();
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_set1_epi32(__mmask16 __M, int __A) {
  return (__m512i)__builtin_ia32_pbroadcastd512_gpr_mask(
      __A, (__v16si)_mm512_setzero_si512(), __M);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_set1_epi64(__mmask8 __M, long long __A) {

  return (__m512i)__builtin_ia32_pbroadcastq512_gpr_mask(
      __A, (__v8di)_mm512_setzero_si512(), __M);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_setzero_ps(void) {
  return (__m512){0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
}
static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_setzero_pd(void) {
  return (__m512d){0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_set1_ps(float __w) {
  return (__m512){__w, __w, __w, __w, __w, __w, __w, __w,
                  __w, __w, __w, __w, __w, __w, __w, __w};
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_set1_pd(double __w) {
  return (__m512d){__w, __w, __w, __w, __w, __w, __w, __w};
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_set1_epi32(int __s) {
  return (__m512i)(__v16si){__s, __s, __s, __s, __s, __s, __s, __s,
                            __s, __s, __s, __s, __s, __s, __s, __s};
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_set1_epi64(long long __d) {
  return (__m512i)(__v8di){__d, __d, __d, __d, __d, __d, __d, __d};
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_broadcastss_ps(__m128 __X) {
  float __f = __X[0];
  return (__v16sf){__f, __f, __f, __f, __f, __f, __f, __f,
                   __f, __f, __f, __f, __f, __f, __f, __f};
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_broadcastsd_pd(__m128d __X) {
  double __d = __X[0];
  return (__v8df){__d, __d, __d, __d, __d, __d, __d, __d};
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_castpd256_pd512(__m256d __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, 2, 3, -1, -1, -1, -1);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_castps256_ps512(__m256 __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, 2, 3, 4, 5, 6, 7, -1, -1, -1,
                                 -1, -1, -1, -1, -1);
}

static __inline __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_castpd512_pd128(__m512d __a) {
  return __builtin_shufflevector(__a, __a, 0, 1);
}

static __inline __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_castps512_ps128(__m512 __a) {
  return __builtin_shufflevector(__a, __a, 0, 1, 2, 3);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_and_epi32(__m512i __a, __m512i __b) {
  return __a & __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_and_epi32(__m512i __src, __mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pandd512_mask((__v16si)__a, (__v16si)__b,
                                               (__v16si)__src, (__mmask16)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_and_epi32(__mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pandd512_mask((__v16si)__a, (__v16si)__b,
                                               (__v16si)_mm512_setzero_si512(),
                                               (__mmask16)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_and_epi64(__m512i __a, __m512i __b) {
  return __a & __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_and_epi64(__m512i __src, __mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pandq512_mask((__v8di)__a, (__v8di)__b,
                                               (__v8di)__src, (__mmask8)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_and_epi64(__mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pandq512_mask(
      (__v8di)__a, (__v8di)__b, (__v8di)_mm512_setzero_si512(), (__mmask8)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_andnot_epi32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnd512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_andnot_epi32(__m512i __W, __mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnd512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)__W, (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_andnot_epi32(__mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnd512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_andnot_epi64(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_andnot_epi64(__m512i __W, __mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnq512_mask((__v8di)__A, (__v8di)__B,
                                                (__v8di)__W, __U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_andnot_epi64(__mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pandnq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_pd(), __U);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_or_epi32(__m512i __a, __m512i __b) {
  return __a | __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_or_epi32(__m512i __src, __mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pord512_mask((__v16si)__a, (__v16si)__b,
                                              (__v16si)__src, (__mmask16)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_or_epi32(__mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pord512_mask((__v16si)__a, (__v16si)__b,
                                              (__v16si)_mm512_setzero_si512(),
                                              (__mmask16)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_or_epi64(__m512i __a, __m512i __b) {
  return __a | __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_or_epi64(__m512i __src, __mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_porq512_mask((__v8di)__a, (__v8di)__b,
                                              (__v8di)__src, (__mmask8)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_or_epi64(__mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_porq512_mask(
      (__v8di)__a, (__v8di)__b, (__v8di)_mm512_setzero_si512(), (__mmask8)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_xor_epi32(__m512i __a, __m512i __b) {
  return __a ^ __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_xor_epi32(__m512i __src, __mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pxord512_mask((__v16si)__a, (__v16si)__b,
                                               (__v16si)__src, (__mmask16)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_xor_epi32(__mmask16 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pxord512_mask((__v16si)__a, (__v16si)__b,
                                               (__v16si)_mm512_setzero_si512(),
                                               (__mmask16)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_xor_epi64(__m512i __a, __m512i __b) {
  return __a ^ __b;
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_xor_epi64(__m512i __src, __mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pxorq512_mask((__v8di)__a, (__v8di)__b,
                                               (__v8di)__src, (__mmask8)__k);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_xor_epi64(__mmask8 __k, __m512i __a, __m512i __b) {
  return (__m512i)__builtin_ia32_pxorq512_mask(
      (__v8di)__a, (__v8di)__b, (__v8di)_mm512_setzero_si512(), (__mmask8)__k);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_and_si512(__m512i __a, __m512i __b) {
  return __a & __b;
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_or_si512(__m512i __a, __m512i __b) {
  return __a | __b;
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_xor_si512(__m512i __a, __m512i __b) {
  return __a ^ __b;
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_add_pd(__m512d __a, __m512d __b) {
  return __a + __b;
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_add_ps(__m512 __a, __m512 __b) {
  return __a + __b;
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mul_pd(__m512d __a, __m512d __b) {
  return __a * __b;
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mul_ps(__m512 __a, __m512 __b) {
  return __a * __b;
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sub_pd(__m512d __a, __m512d __b) {
  return __a - __b;
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sub_ps(__m512 __a, __m512 __b) {
  return __a - __b;
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_add_epi64(__m512i __A, __m512i __B) {
  return (__m512i)((__v8di)__A + (__v8di)__B);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_add_epi64(__m512i __W, __mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddq512_mask((__v8di)__A, (__v8di)__B,
                                               (__v8di)__W, (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_add_epi64(__mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sub_epi64(__m512i __A, __m512i __B) {
  return (__m512i)((__v8di)__A - (__v8di)__B);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_sub_epi64(__m512i __W, __mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubq512_mask((__v8di)__A, (__v8di)__B,
                                               (__v8di)__W, (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_sub_epi64(__mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_add_epi32(__m512i __A, __m512i __B) {
  return (__m512i)((__v16si)__A + (__v16si)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_add_epi32(__m512i __W, __mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddd512_mask((__v16si)__A, (__v16si)__B,
                                               (__v16si)__W, (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_add_epi32(__mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddd512_mask((__v16si)__A, (__v16si)__B,
                                               (__v16si)_mm512_setzero_si512(),
                                               (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sub_epi32(__m512i __A, __m512i __B) {
  return (__m512i)((__v16si)__A - (__v16si)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_sub_epi32(__m512i __W, __mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubd512_mask((__v16si)__A, (__v16si)__B,
                                               (__v16si)__W, (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_sub_epi32(__mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubd512_mask((__v16si)__A, (__v16si)__B,
                                               (__v16si)_mm512_setzero_si512(),
                                               (__mmask16)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_pd(__m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_maxpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_ps(__m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_maxps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)-1, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_max_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_maxss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_max_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_maxss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_max_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_maxsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_max_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_maxsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_epi32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsd512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_epu32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxud512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_epi64(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_max_epu64(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxuq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_pd(__m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_minpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_ps(__m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_minps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)-1, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_min_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_minss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_min_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_minss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_min_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_minsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_min_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_minsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_epi32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsd512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_epu32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminud512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)_mm512_setzero_si512(),
                                                (__mmask16)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_epi64(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_min_epu64(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminuq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mul_epi32(__m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuldq512_mask(
      (__v16si)__X, (__v16si)__Y, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_mul_epi32(__m512i __W, __mmask8 __M, __m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuldq512_mask((__v16si)__X, (__v16si)__Y,
                                                (__v8di)__W, __M);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_mul_epi32(__mmask8 __M, __m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuldq512_mask(
      (__v16si)__X, (__v16si)__Y, (__v8di)_mm512_setzero_si512(), __M);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mul_epu32(__m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuludq512_mask(
      (__v16si)__X, (__v16si)__Y, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_mul_epu32(__m512i __W, __mmask8 __M, __m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuludq512_mask((__v16si)__X, (__v16si)__Y,
                                                 (__v8di)__W, __M);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_mul_epu32(__mmask8 __M, __m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmuludq512_mask(
      (__v16si)__X, (__v16si)__Y, (__v8di)_mm512_setzero_si512(), __M);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mullo_epi32(__m512i __A, __m512i __B) {
  return (__m512i)((__v16si)__A * (__v16si)__B);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_mullo_epi32(__mmask16 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulld512_mask(
      (__v16si)__A, (__v16si)__B, (__v16si)_mm512_setzero_si512(), __M);
}

static __inline __m512i __attribute__((__always_inline__, __nodebug__,
                                       __target__("avx512f")))
_mm512_mask_mullo_epi32(__m512i __W, __mmask16 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulld512_mask((__v16si)__A, (__v16si)__B,
                                                (__v16si)__W, __M);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sqrt_pd(__m512d __a) {
  return (__m512d)__builtin_ia32_sqrtpd512_mask(
      (__v8df)__a, (__v8df)_mm512_setzero_pd(), (__mmask8)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_sqrt_ps(__m512 __a) {
  return (__m512)__builtin_ia32_sqrtps512_mask(
      (__v16sf)__a, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_rsqrt14_pd(__m512d __A) {
  return (__m512d)__builtin_ia32_rsqrt14pd512_mask(
      (__v8df)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_rsqrt14_ps(__m512 __A) {
  return (__m512)__builtin_ia32_rsqrt14ps512_mask(
      (__v16sf)__A, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_rsqrt14_ss(__m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_rsqrt14ss(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_rsqrt14_sd(__m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_rsqrt14sd(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_rcp14_pd(__m512d __A) {
  return (__m512d)__builtin_ia32_rcp14pd512_mask(
      (__v8df)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_rcp14_ps(__m512 __A) {
  return (__m512)__builtin_ia32_rcp14ps512_mask(
      (__v16sf)__A, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1);
}
static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_rcp14_ss(__m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_rcp14ss((__v4sf)__A, (__v4sf)__B,
                                        (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_rcp14_sd(__m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_rcp14sd(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_floor_ps(__m512 __A) {
  return (__m512)__builtin_ia32_rndscaleps_mask((__v16sf)__A, (0x00 | 0x01),
                                                (__v16sf)__A, -1, 0x04);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_floor_pd(__m512d __A) {
  return (__m512d)__builtin_ia32_rndscalepd_mask((__v8df)__A, (0x00 | 0x01),
                                                 (__v8df)__A, -1, 0x04);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_ceil_ps(__m512 __A) {
  return (__m512)__builtin_ia32_rndscaleps_mask((__v16sf)__A, (0x00 | 0x02),
                                                (__v16sf)__A, -1, 0x04);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_ceil_pd(__m512d __A) {
  return (__m512d)__builtin_ia32_rndscalepd_mask((__v8df)__A, (0x00 | 0x02),
                                                 (__v8df)__A, -1, 0x04);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_abs_epi64(__m512i __A) {
  return (__m512i)__builtin_ia32_pabsq512_mask(
      (__v8di)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_abs_epi32(__m512i __A) {
  return (__m512i)__builtin_ia32_pabsd512_mask(
      (__v16si)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)-1);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_add_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_addss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_add_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_addss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_add_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_addsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_add_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_addsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_add_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_addpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_add_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_addpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_add_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_addps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__W, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_add_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_addps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)__U, 0x04);
}
static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_sub_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_subss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_sub_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_subss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_sub_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_subsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_sub_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_subsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_sub_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_subpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_sub_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_subpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_sub_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_subps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__W, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_sub_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_subps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)__U, 0x04);
}
static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_mul_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_mulss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_mul_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_mulss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_mul_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_mulsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_mul_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_mulsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_mul_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_mulpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_mul_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_mulpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_mul_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_mulps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__W, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_mul_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_mulps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)__U, 0x04);
}
static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_div_ss(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_divss_round((__v4sf)__A, (__v4sf)__B,
                                            (__v4sf)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_div_ss(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_divss_round(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_mask_div_sd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_divsd_round((__v2df)__A, (__v2df)__B,
                                             (__v2df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm_maskz_div_sd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_divsd_round(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_div_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_divpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__W, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_div_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_divpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)_mm512_setzero_pd(),
                                               (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_div_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_divps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__W, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_div_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_divps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)_mm512_setzero_ps(),
                                              (__mmask16)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmadd_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmadd_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fmadd_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask3(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fmadd_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_maskz(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmsub_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmsub_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fmsub_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_maskz(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fnmadd_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      -(__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask3_fnmadd_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask3(
      -(__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_maskz_fnmadd_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_maskz(
      -(__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fnmsub_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_mask(
      -(__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_maskz_fnmsub_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddpd512_maskz(
      -(__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)__U, 0x04);
}
static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmadd_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmadd_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fmadd_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfmaddps512_mask3(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fmadd_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_maskz(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmsub_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmsub_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fmsub_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_maskz(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fnmadd_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      -(__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fnmadd_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfmaddps512_mask3(
      -(__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fnmadd_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_maskz(
      -(__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fnmsub_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_mask(
      -(__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_fnmsub_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddps512_maskz(
      -(__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmaddsub_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_fmaddsub_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask3_fmaddsub_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_mask3(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_maskz_fmaddsub_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_maskz(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmsubadd_pd(__m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_mask(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)-1, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask_fmsubadd_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_mask(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_maskz_fmsubadd_pd(__mmask8 __U, __m512d __A, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfmaddsubpd512_maskz(
      (__v8df)__A, (__v8df)__B, -(__v8df)__C, (__mmask8)__U, 0x04);
}
static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmaddsub_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmaddsub_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512f")))
_mm512_mask3_fmaddsub_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfmaddsubps512_mask3(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512f")))
_mm512_maskz_fmaddsub_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_maskz(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_fmsubadd_ps(__m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_mask(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)-1, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fmsubadd_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_mask(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512f")))
_mm512_maskz_fmsubadd_ps(__mmask16 __U, __m512 __A, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfmaddsubps512_maskz(
      (__v16sf)__A, (__v16sf)__B, -(__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fmsub_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfmsubpd512_mask3(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fmsub_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfmsubps512_mask3(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask3_fmsubadd_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfmsubaddpd512_mask3(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512f")))
_mm512_mask3_fmsubadd_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfmsubaddps512_mask3(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fnmadd_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfnmaddpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fnmadd_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfnmaddps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fnmsub_pd(__m512d __A, __mmask8 __U, __m512d __B, __m512d __C) {
  return (__m512d)__builtin_ia32_vfnmsubpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}

static __inline__ __m512d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512f")))
_mm512_mask3_fnmsub_pd(__m512d __A, __m512d __B, __m512d __C, __mmask8 __U) {
  return (__m512d)__builtin_ia32_vfnmsubpd512_mask3(
      (__v8df)__A, (__v8df)__B, (__v8df)__C, (__mmask8)__U, 0x04);
}
static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_fnmsub_ps(__m512 __A, __mmask16 __U, __m512 __B, __m512 __C) {
  return (__m512)__builtin_ia32_vfnmsubps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask3_fnmsub_ps(__m512 __A, __m512 __B, __m512 __C, __mmask16 __U) {
  return (__m512)__builtin_ia32_vfnmsubps512_mask3(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)__C, (__mmask16)__U, 0x04);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_permutex2var_epi32(__m512i __A, __m512i __I, __m512i __B) {
  return (__m512i)__builtin_ia32_vpermt2vard512_mask(
      (__v16si)__I, (__v16si)__A, (__v16si)__B, (__mmask16)-1);
}
static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_permutex2var_epi64(__m512i __A, __m512i __I, __m512i __B) {
  return (__m512i)__builtin_ia32_vpermt2varq512_mask((__v8di)__I, (__v8di)__A,
                                                     (__v8di)__B, (__mmask8)-1);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_permutex2var_pd(__m512d __A, __m512i __I, __m512d __B) {
  return (__m512d)__builtin_ia32_vpermt2varpd512_mask(
      (__v8di)__I, (__v8df)__A, (__v8df)__B, (__mmask8)-1);
}
static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_permutex2var_ps(__m512 __A, __m512i __I, __m512 __B) {
  return (__m512)__builtin_ia32_vpermt2varps512_mask(
      (__v16si)__I, (__v16sf)__A, (__v16sf)__B, (__mmask16)-1);
}
static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_blend_pd(__mmask8 __U, __m512d __A, __m512d __W) {
  return (__m512d)__builtin_ia32_blendmpd_512_mask((__v8df)__A, (__v8df)__W,
                                                   (__mmask8)__U);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_blend_ps(__mmask16 __U, __m512 __A, __m512 __W) {
  return (__m512)__builtin_ia32_blendmps_512_mask((__v16sf)__A, (__v16sf)__W,
                                                  (__mmask16)__U);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_blend_epi64(__mmask8 __U, __m512i __A, __m512i __W) {
  return (__m512i)__builtin_ia32_blendmq_512_mask((__v8di)__A, (__v8di)__W,
                                                  (__mmask8)__U);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_blend_epi32(__mmask16 __U, __m512i __A, __m512i __W) {
  return (__m512i)__builtin_ia32_blendmd_512_mask((__v16si)__A, (__v16si)__W,
                                                  (__mmask16)__U);
}
static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvttps_epu32(__m512 __A) {
  return (__m512i)__builtin_ia32_cvttps2udq512_mask(
      (__v16sf)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)-1, 0x04);
}
static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvtepi32_pd(__m256i __A) {
  return (__m512d)__builtin_ia32_cvtdq2pd512_mask(
      (__v8si)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvtepu32_pd(__m256i __A) {
  return (__m512d)__builtin_ia32_cvtudq2pd512_mask(
      (__v8si)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}
static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvtph_ps(__m256i __A) {
  return (__m512)__builtin_ia32_vcvtph2ps512_mask(
      (__v16hi)__A, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1, 0x04);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvttps_epi32(__m512 __a) {
  return (__m512i)__builtin_ia32_cvttps2dq512_mask(
      (__v16sf)__a, (__v16si)_mm512_setzero_si512(), (__mmask16)-1, 0x04);
}

static __inline __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cvttpd_epi32(__m512d __a) {
  return (__m256i)__builtin_ia32_cvttpd2dq512_mask(
      (__v8df)__a, (__v8si)_mm256_setzero_si256(), (__mmask8)-1, 0x04);
}
static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_unpackhi_pd(__m512d __a, __m512d __b) {
  return __builtin_shufflevector(__a, __b, 1, 9, 1 + 2, 9 + 2, 1 + 4, 9 + 4,
                                 1 + 6, 9 + 6);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_unpacklo_pd(__m512d __a, __m512d __b) {
  return __builtin_shufflevector(__a, __b, 0, 8, 0 + 2, 8 + 2, 0 + 4, 8 + 4,
                                 0 + 6, 8 + 6);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_unpackhi_ps(__m512 __a, __m512 __b) {
  return __builtin_shufflevector(__a, __b, 2, 18, 3, 19, 2 + 4, 18 + 4, 3 + 4,
                                 19 + 4, 2 + 8, 18 + 8, 3 + 8, 19 + 8, 2 + 12,
                                 18 + 12, 3 + 12, 19 + 12);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_unpacklo_ps(__m512 __a, __m512 __b) {
  return __builtin_shufflevector(__a, __b, 0, 16, 1, 17, 0 + 4, 16 + 4, 1 + 4,
                                 17 + 4, 0 + 8, 16 + 8, 1 + 8, 17 + 8, 0 + 12,
                                 16 + 12, 1 + 12, 17 + 12);
}

static __inline __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_test_epi32_mask(__m512i __A, __m512i __B) {
  return (__mmask16)__builtin_ia32_ptestmd512((__v16si)__A, (__v16si)__B,
                                              (__mmask16)-1);
}

static __inline __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_test_epi64_mask(__m512i __A, __m512i __B) {
  return (__mmask8)__builtin_ia32_ptestmq512((__v8di)__A, (__v8di)__B,
                                             (__mmask8)-1);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_loadu_epi32(__mmask16 __U, void const *__P) {
  return (__m512i)__builtin_ia32_loaddqusi512_mask(
      (const __v16si *)__P, (__v16si)_mm512_setzero_si512(), (__mmask16)__U);
}

static __inline __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_loadu_epi64(__mmask8 __U, void const *__P) {
  return (__m512i)__builtin_ia32_loaddqudi512_mask(
      (const __v8di *)__P, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_loadu_ps(__mmask16 __U, void const *__P) {
  return (__m512)__builtin_ia32_loadups512_mask(
      (const __v16sf *)__P, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_loadu_pd(__mmask8 __U, void const *__P) {
  return (__m512d)__builtin_ia32_loadupd512_mask(
      (const __v8df *)__P, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_load_ps(__mmask16 __U, void const *__P) {
  return (__m512)__builtin_ia32_loadaps512_mask(
      (const __v16sf *)__P, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_maskz_load_pd(__mmask8 __U, void const *__P) {
  return (__m512d)__builtin_ia32_loadapd512_mask(
      (const __v8df *)__P, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_loadu_pd(double const *__p) {
  struct __loadu_pd {
    __m512d __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_pd *)__p)->__v;
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_loadu_ps(float const *__p) {
  struct __loadu_ps {
    __m512 __v;
  } __attribute__((__packed__, __may_alias__));
  return ((struct __loadu_ps *)__p)->__v;
}

static __inline __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_load_ps(double const *__p) {
  return (__m512)__builtin_ia32_loadaps512_mask(
      (const __v16sf *)__p, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1);
}

static __inline __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_load_pd(float const *__p) {
  return (__m512d)__builtin_ia32_loadapd512_mask(
      (const __v8df *)__p, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_storeu_epi64(void *__P, __mmask8 __U, __m512i __A) {
  __builtin_ia32_storedqudi512_mask((__v8di *)__P, (__v8di)__A, (__mmask8)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_storeu_epi32(void *__P, __mmask16 __U, __m512i __A) {
  __builtin_ia32_storedqusi512_mask((__v16si *)__P, (__v16si)__A,
                                    (__mmask16)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_storeu_pd(void *__P, __mmask8 __U, __m512d __A) {
  __builtin_ia32_storeupd512_mask((__v8df *)__P, (__v8df)__A, (__mmask8)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_storeu_pd(void *__P, __m512d __A) {
  __builtin_ia32_storeupd512_mask((__v8df *)__P, (__v8df)__A, (__mmask8)-1);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_storeu_ps(void *__P, __mmask16 __U, __m512 __A) {
  __builtin_ia32_storeups512_mask((__v16sf *)__P, (__v16sf)__A, (__mmask16)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_storeu_ps(void *__P, __m512 __A) {
  __builtin_ia32_storeups512_mask((__v16sf *)__P, (__v16sf)__A, (__mmask16)-1);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_store_pd(void *__P, __mmask8 __U, __m512d __A) {
  __builtin_ia32_storeapd512_mask((__v8df *)__P, (__v8df)__A, (__mmask8)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_store_pd(void *__P, __m512d __A) {
  *(__m512d *)__P = __A;
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_store_ps(void *__P, __mmask16 __U, __m512 __A) {
  __builtin_ia32_storeaps512_mask((__v16sf *)__P, (__v16sf)__A, (__mmask16)__U);
}

static __inline void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_store_ps(void *__P, __m512 __A) {
  *(__m512 *)__P = __A;
}

static __inline __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_knot(__mmask16 __M) {
  return __builtin_ia32_knothi(__M);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpeq_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqd512_mask((__v16si)__a, (__v16si)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpeq_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqd512_mask((__v16si)__a, (__v16si)__b,
                                                   __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpeq_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 0,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpeq_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 0,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpeq_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq512_mask((__v8di)__a, (__v8di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpeq_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq512_mask((__v8di)__a, (__v8di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpeq_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpeq_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 0,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpge_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 5,
                                                (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpge_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 5,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpge_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 5,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpge_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 5,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpge_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpge_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 5,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpge_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpge_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 5,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpgt_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtd512_mask((__v16si)__a, (__v16si)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpgt_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtd512_mask((__v16si)__a, (__v16si)__b,
                                                   __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpgt_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 6,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpgt_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 6,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpgt_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq512_mask((__v8di)__a, (__v8di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpgt_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq512_mask((__v8di)__a, (__v8di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpgt_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpgt_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 6,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmple_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 2,
                                                (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmple_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 2,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmple_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 2,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmple_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 2,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmple_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmple_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 2,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmple_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmple_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 2,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmplt_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 1,
                                                (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmplt_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 1,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmplt_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 1,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmplt_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 1,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmplt_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmplt_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 1,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmplt_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmplt_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 1,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpneq_epi32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 4,
                                                (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpneq_epi32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_cmpd512_mask((__v16si)__a, (__v16si)__b, 4,
                                                __u);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpneq_epu32_mask(__m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 4,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpneq_epu32_mask(__mmask16 __u, __m512i __a, __m512i __b) {
  return (__mmask16)__builtin_ia32_ucmpd512_mask((__v16si)__a, (__v16si)__b, 4,
                                                 __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpneq_epi64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpneq_epi64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_cmpq512_mask((__v8di)__a, (__v8di)__b, 4,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_cmpneq_epu64_mask(__m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512f")))
    _mm512_mask_cmpneq_epu64_mask(__mmask8 __u, __m512i __a, __m512i __b) {
  return (__mmask8)__builtin_ia32_ucmpq512_mask((__v8di)__a, (__v8di)__b, 4,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_cmpeq_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqd128_mask((__v4si)__a, (__v4si)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_mask_cmpeq_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqd128_mask((__v4si)__a, (__v4si)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpeq_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpeq_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 0,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_cmpeq_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqd256_mask((__v8si)__a, (__v8si)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_mask_cmpeq_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqd256_mask((__v8si)__a, (__v8si)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpeq_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpeq_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 0,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_cmpeq_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq128_mask((__v2di)__a, (__v2di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_mask_cmpeq_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq128_mask((__v2di)__a, (__v2di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpeq_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpeq_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 0,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_cmpeq_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq256_mask((__v4di)__a, (__v4di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_mask_cmpeq_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqq256_mask((__v4di)__a, (__v4di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpeq_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpeq_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 0,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpge_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpge_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 5,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpge_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpge_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 5,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpge_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpge_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 5,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpge_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpge_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 5,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpge_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpge_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 5,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpge_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpge_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 5,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpge_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpge_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 5,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpge_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpge_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 5,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_cmpgt_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtd128_mask((__v4si)__a, (__v4si)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_mask_cmpgt_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtd128_mask((__v4si)__a, (__v4si)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpgt_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpgt_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 6,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_cmpgt_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtd256_mask((__v8si)__a, (__v8si)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_mask_cmpgt_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtd256_mask((__v8si)__a, (__v8si)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpgt_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpgt_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 6,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_cmpgt_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq128_mask((__v2di)__a, (__v2di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm_mask_cmpgt_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq128_mask((__v2di)__a, (__v2di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpgt_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpgt_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 6,
                                                __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_cmpgt_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq256_mask((__v4di)__a, (__v4di)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl, avx512bw")))
_mm256_mask_cmpgt_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtq256_mask((__v4di)__a, (__v4di)__b,
                                                  __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpgt_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpgt_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 6,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmple_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmple_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 2,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmple_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmple_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 2,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmple_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmple_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 2,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmple_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmple_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 2,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmple_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmple_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 2,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmple_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmple_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 2,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmple_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmple_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 2,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmple_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmple_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 2,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmplt_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmplt_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 1,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmplt_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmplt_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 1,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmplt_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmplt_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 1,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmplt_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmplt_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 1,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmplt_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmplt_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 1,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmplt_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmplt_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 1,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmplt_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmplt_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 1,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmplt_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmplt_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 1,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpneq_epi32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpneq_epi32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpd128_mask((__v4si)__a, (__v4si)__b, 4,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpneq_epu32_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpneq_epu32_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpd128_mask((__v4si)__a, (__v4si)__b, 4,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpneq_epi32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpneq_epi32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpd256_mask((__v8si)__a, (__v8si)__b, 4,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpneq_epu32_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpneq_epu32_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpd256_mask((__v8si)__a, (__v8si)__b, 4,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpneq_epi64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpneq_epi64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpq128_mask((__v2di)__a, (__v2di)__b, 4,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cmpneq_epu64_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cmpneq_epu64_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpq128_mask((__v2di)__a, (__v2di)__b, 4,
                                                __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpneq_epi64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpneq_epi64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_cmpq256_mask((__v4di)__a, (__v4di)__b, 4,
                                               __u);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cmpneq_epu64_mask(__m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cmpneq_epu64_mask(__mmask8 __u, __m256i __a, __m256i __b) {
  return (__mmask8)__builtin_ia32_ucmpq256_mask((__v4di)__a, (__v4di)__b, 4,
                                                __u);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_add_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddd256_mask((__v8si)__A, (__v8si)__B,
                                               (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_add_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_add_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddq256_mask((__v4di)__A, (__v4di)__B,
                                               (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_add_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sub_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubd256_mask((__v8si)__A, (__v8si)__B,
                                               (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sub_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sub_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubq256_mask((__v4di)__A, (__v4di)__B,
                                               (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sub_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_add_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddd128_mask((__v4si)__A, (__v4si)__B,
                                               (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_add_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_add_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddq128_mask((__v2di)__A, (__v2di)__B,
                                               (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_add_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sub_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubd128_mask((__v4si)__A, (__v4si)__B,
                                               (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sub_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sub_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubq128_mask((__v2di)__A, (__v2di)__B,
                                               (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sub_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_mul_epi32(__m256i __W, __mmask8 __M, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmuldq256_mask((__v8si)__X, (__v8si)__Y,
                                                (__v4di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_mul_epi32(__mmask8 __M, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmuldq256_mask(
      (__v8si)__X, (__v8si)__Y, (__v4di)_mm256_setzero_si256(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_mul_epi32(__m128i __W, __mmask8 __M, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmuldq128_mask((__v4si)__X, (__v4si)__Y,
                                                (__v2di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_mul_epi32(__mmask8 __M, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmuldq128_mask(
      (__v4si)__X, (__v4si)__Y, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_mul_epu32(__m256i __W, __mmask8 __M, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmuludq256_mask((__v8si)__X, (__v8si)__Y,
                                                 (__v4di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_mul_epu32(__mmask8 __M, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmuludq256_mask(
      (__v8si)__X, (__v8si)__Y, (__v4di)_mm256_setzero_si256(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_mul_epu32(__m128i __W, __mmask8 __M, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmuludq128_mask((__v4si)__X, (__v4si)__Y,
                                                 (__v2di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_mul_epu32(__mmask8 __M, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmuludq128_mask(
      (__v4si)__X, (__v4si)__Y, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_mullo_epi32(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulld256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask_mullo_epi32(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulld256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_mullo_epi32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulld128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_mullo_epi32(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulld128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_and_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandd256_mask((__v8si)__A, (__v8si)__B,
                                               (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_and_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_and_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandd128_mask((__v4si)__A, (__v4si)__B,
                                               (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_and_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask_andnot_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandnd256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_andnot_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandnd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_andnot_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandnd128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_andnot_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandnd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_or_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pord256_mask((__v8si)__A, (__v8si)__B,
                                              (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_or_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pord256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_or_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pord128_mask((__v4si)__A, (__v4si)__B,
                                              (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_or_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pord128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_xor_epi32(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pxord256_mask((__v8si)__A, (__v8si)__B,
                                               (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_xor_epi32(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pxord256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_xor_epi32(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pxord128_mask((__v4si)__A, (__v4si)__B,
                                               (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_xor_epi32(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pxord128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_and_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandq256_mask((__v4di)__A, (__v4di)__B,
                                               (__v4di)__W, __U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_and_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_pd(), __U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_and_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandq128_mask((__v2di)__A, (__v2di)__B,
                                               (__v2di)__W, __U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_and_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandq128_mask((__v2di)__A, (__v2di)__B,
                                               (__v2di)_mm_setzero_pd(), __U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask_andnot_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandnq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, __U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_andnot_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pandnq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_pd(), __U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_andnot_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandnq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, __U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_andnot_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pandnq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)_mm_setzero_pd(), __U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_or_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_porq256_mask((__v4di)__A, (__v4di)__B,
                                              (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_or_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_porq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_or_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_porq128_mask((__v2di)__A, (__v2di)__B,
                                              (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_or_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_porq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_xor_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pxorq256_mask((__v4di)__A, (__v4di)__B,
                                               (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_xor_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pxorq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_xor_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pxorq128_mask((__v2di)__A, (__v2di)__B,
                                               (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_xor_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pxorq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmadd_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_mask((__v2df)__A, (__v2df)__B,
                                                  (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmadd_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfmaddpd128_mask3((__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmadd_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_maskz((__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmsub_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_mask((__v2df)__A, (__v2df)__B,
                                                  -(__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmsub_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_maskz((__v2df)__A, (__v2df)__B,
                                                   -(__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fnmadd_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfmaddpd128_mask3(-(__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fnmadd_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_maskz(-(__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fnmsub_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddpd128_maskz(-(__v2df)__A, (__v2df)__B,
                                                   -(__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmadd_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_mask((__v4df)__A, (__v4df)__B,
                                                  (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmadd_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfmaddpd256_mask3((__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmadd_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_maskz((__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmsub_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_mask((__v4df)__A, (__v4df)__B,
                                                  -(__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmsub_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_maskz((__v4df)__A, (__v4df)__B,
                                                   -(__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask3_fnmadd_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfmaddpd256_mask3(-(__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_maskz_fnmadd_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_maskz(-(__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_maskz_fnmsub_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddpd256_maskz(-(__v4df)__A, (__v4df)__B,
                                                   -(__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmadd_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_mask((__v4sf)__A, (__v4sf)__B,
                                                 (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmadd_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfmaddps128_mask3((__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmadd_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_maskz((__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmsub_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_mask((__v4sf)__A, (__v4sf)__B,
                                                 -(__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmsub_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_maskz((__v4sf)__A, (__v4sf)__B,
                                                  -(__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fnmadd_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfmaddps128_mask3(-(__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fnmadd_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_maskz(-(__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fnmsub_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddps128_maskz(-(__v4sf)__A, (__v4sf)__B,
                                                  -(__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmadd_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_mask((__v8sf)__A, (__v8sf)__B,
                                                 (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmadd_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfmaddps256_mask3((__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmadd_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_maskz((__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmsub_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_mask((__v8sf)__A, (__v8sf)__B,
                                                 -(__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmsub_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_maskz((__v8sf)__A, (__v8sf)__B,
                                                  -(__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fnmadd_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfmaddps256_mask3(-(__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fnmadd_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_maskz(-(__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fnmsub_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddps256_maskz(-(__v8sf)__A, (__v8sf)__B,
                                                  -(__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmaddsub_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsubpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmaddsub_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfmaddsubpd128_mask3(
      (__v2df)__A, (__v2df)__B, (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmaddsub_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsubpd128_maskz(
      (__v2df)__A, (__v2df)__B, (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmsubadd_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsubpd128_mask(
      (__v2df)__A, (__v2df)__B, -(__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmsubadd_pd(__mmask8 __U, __m128d __A, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfmaddsubpd128_maskz(
      (__v2df)__A, (__v2df)__B, -(__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask_fmaddsub_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask3_fmaddsub_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256_mask3(
      (__v4df)__A, (__v4df)__B, (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_maskz_fmaddsub_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256_maskz(
      (__v4df)__A, (__v4df)__B, (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask_fmsubadd_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256_mask(
      (__v4df)__A, (__v4df)__B, -(__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_maskz_fmsubadd_pd(__mmask8 __U, __m256d __A, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfmaddsubpd256_maskz(
      (__v4df)__A, (__v4df)__B, -(__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmaddsub_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddsubps128_mask((__v4sf)__A, (__v4sf)__B,
                                                    (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmaddsub_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfmaddsubps128_mask3(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmaddsub_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddsubps128_maskz(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fmsubadd_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddsubps128_mask(
      (__v4sf)__A, (__v4sf)__B, -(__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_fmsubadd_ps(__mmask8 __U, __m128 __A, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfmaddsubps128_maskz(
      (__v4sf)__A, (__v4sf)__B, -(__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmaddsub_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddsubps256_mask((__v8sf)__A, (__v8sf)__B,
                                                    (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmaddsub_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfmaddsubps256_mask3(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmaddsub_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddsubps256_maskz(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fmsubadd_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddsubps256_mask(
      (__v8sf)__A, (__v8sf)__B, -(__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_fmsubadd_ps(__mmask8 __U, __m256 __A, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfmaddsubps256_maskz(
      (__v8sf)__A, (__v8sf)__B, -(__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmsub_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfmsubpd128_mask3((__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmsub_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfmsubpd256_mask3((__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmsub_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfmsubps128_mask3((__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmsub_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfmsubps256_mask3((__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmsubadd_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfmsubaddpd128_mask3(
      (__v2df)__A, (__v2df)__B, (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask3_fmsubadd_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfmsubaddpd256_mask3(
      (__v4df)__A, (__v4df)__B, (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fmsubadd_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfmsubaddps128_mask3(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fmsubadd_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfmsubaddps256_mask3(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fnmadd_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmaddpd128_mask((__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fnmadd_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfnmaddpd256_mask((__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fnmadd_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmaddps128_mask((__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fnmadd_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfnmaddps256_mask((__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fnmsub_pd(__m128d __A, __mmask8 __U, __m128d __B, __m128d __C) {
  return (__m128d)__builtin_ia32_vfnmsubpd128_mask((__v2df)__A, (__v2df)__B,
                                                   (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fnmsub_pd(__m128d __A, __m128d __B, __m128d __C, __mmask8 __U) {
  return (__m128d)__builtin_ia32_vfnmsubpd128_mask3((__v2df)__A, (__v2df)__B,
                                                    (__v2df)__C, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fnmsub_pd(__m256d __A, __mmask8 __U, __m256d __B, __m256d __C) {
  return (__m256d)__builtin_ia32_vfnmsubpd256_mask((__v4df)__A, (__v4df)__B,
                                                   (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm256_mask3_fnmsub_pd(__m256d __A, __m256d __B, __m256d __C, __mmask8 __U) {
  return (__m256d)__builtin_ia32_vfnmsubpd256_mask3((__v4df)__A, (__v4df)__B,
                                                    (__v4df)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_fnmsub_ps(__m128 __A, __mmask8 __U, __m128 __B, __m128 __C) {
  return (__m128)__builtin_ia32_vfnmsubps128_mask((__v4sf)__A, (__v4sf)__B,
                                                  (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask3_fnmsub_ps(__m128 __A, __m128 __B, __m128 __C, __mmask8 __U) {
  return (__m128)__builtin_ia32_vfnmsubps128_mask3((__v4sf)__A, (__v4sf)__B,
                                                   (__v4sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_fnmsub_ps(__m256 __A, __mmask8 __U, __m256 __B, __m256 __C) {
  return (__m256)__builtin_ia32_vfnmsubps256_mask((__v8sf)__A, (__v8sf)__B,
                                                  (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask3_fnmsub_ps(__m256 __A, __m256 __B, __m256 __C, __mmask8 __U) {
  return (__m256)__builtin_ia32_vfnmsubps256_mask3((__v8sf)__A, (__v8sf)__B,
                                                   (__v8sf)__C, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_add_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_addpd128_mask((__v2df)__A, (__v2df)__B,
                                               (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_add_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_addpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_add_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_addpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_add_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_addpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_add_ps(__m128 __W, __mmask16 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_addps128_mask((__v4sf)__A, (__v4sf)__B,
                                              (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_add_ps(__mmask16 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_addps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_add_ps(__m256 __W, __mmask16 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_addps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_add_ps(__mmask16 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_addps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_blend_epi32(__mmask8 __U, __m128i __A, __m128i __W) {
  return (__m128i)__builtin_ia32_blendmd_128_mask((__v4si)__A, (__v4si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_blend_epi32(__mmask8 __U, __m256i __A, __m256i __W) {
  return (__m256i)__builtin_ia32_blendmd_256_mask((__v8si)__A, (__v8si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_blend_pd(__mmask8 __U, __m128d __A, __m128d __W) {
  return (__m128d)__builtin_ia32_blendmpd_128_mask((__v2df)__A, (__v2df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_blend_pd(__mmask8 __U, __m256d __A, __m256d __W) {
  return (__m256d)__builtin_ia32_blendmpd_256_mask((__v4df)__A, (__v4df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_blend_ps(__mmask8 __U, __m128 __A, __m128 __W) {
  return (__m128)__builtin_ia32_blendmps_128_mask((__v4sf)__A, (__v4sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_blend_ps(__mmask8 __U, __m256 __A, __m256 __W) {
  return (__m256)__builtin_ia32_blendmps_256_mask((__v8sf)__A, (__v8sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_blend_epi64(__mmask8 __U, __m128i __A, __m128i __W) {
  return (__m128i)__builtin_ia32_blendmq_128_mask((__v2di)__A, (__v2di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_blend_epi64(__mmask8 __U, __m256i __A, __m256i __W) {
  return (__m256i)__builtin_ia32_blendmq_256_mask((__v4di)__A, (__v4di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compress_pd(__m128d __W, __mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_compressdf128_mask((__v2df)__A, (__v2df)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_compress_pd(__mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_compressdf128_mask(
      (__v2df)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compress_pd(__m256d __W, __mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_compressdf256_mask((__v4df)__A, (__v4df)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_compress_pd(__mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_compressdf256_mask(
      (__v4df)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compress_epi64(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_compressdi128_mask((__v2di)__A, (__v2di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_compress_epi64(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_compressdi128_mask(
      (__v2di)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compress_epi64(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_compressdi256_mask((__v4di)__A, (__v4di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_compress_epi64(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_compressdi256_mask(
      (__v4di)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compress_ps(__m128 __W, __mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_compresssf128_mask((__v4sf)__A, (__v4sf)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_compress_ps(__mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_compresssf128_mask(
      (__v4sf)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compress_ps(__m256 __W, __mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_compresssf256_mask((__v8sf)__A, (__v8sf)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_compress_ps(__mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_compresssf256_mask(
      (__v8sf)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compress_epi32(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_compresssi128_mask((__v4si)__A, (__v4si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_compress_epi32(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_compresssi128_mask(
      (__v4si)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compress_epi32(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_compresssi256_mask((__v8si)__A, (__v8si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_compress_epi32(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_compresssi256_mask(
      (__v8si)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compressstoreu_pd(void *__P, __mmask8 __U, __m128d __A) {
  __builtin_ia32_compressstoredf128_mask((__v2df *)__P, (__v2df)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compressstoreu_pd(void *__P, __mmask8 __U, __m256d __A) {
  __builtin_ia32_compressstoredf256_mask((__v4df *)__P, (__v4df)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compressstoreu_epi64(void *__P, __mmask8 __U, __m128i __A) {
  __builtin_ia32_compressstoredi128_mask((__v2di *)__P, (__v2di)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compressstoreu_epi64(void *__P, __mmask8 __U, __m256i __A) {
  __builtin_ia32_compressstoredi256_mask((__v4di *)__P, (__v4di)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compressstoreu_ps(void *__P, __mmask8 __U, __m128 __A) {
  __builtin_ia32_compressstoresf128_mask((__v4sf *)__P, (__v4sf)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compressstoreu_ps(void *__P, __mmask8 __U, __m256 __A) {
  __builtin_ia32_compressstoresf256_mask((__v8sf *)__P, (__v8sf)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_compressstoreu_epi32(void *__P, __mmask8 __U, __m128i __A) {
  __builtin_ia32_compressstoresi128_mask((__v4si *)__P, (__v4si)__A,
                                         (__mmask8)__U);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_compressstoreu_epi32(void *__P, __mmask8 __U, __m256i __A) {
  __builtin_ia32_compressstoresi256_mask((__v8si *)__P, (__v8si)__A,
                                         (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtepi32_pd(__m128d __W, __mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtdq2pd128_mask((__v4si)__A, (__v2df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtepi32_pd(__mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtdq2pd128_mask(
      (__v4si)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtepi32_pd(__m256d __W, __mmask8 __U, __m128i __A) {
  return (__m256d)__builtin_ia32_cvtdq2pd256_mask((__v4si)__A, (__v4df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtepi32_pd(__mmask8 __U, __m128i __A) {
  return (__m256d)__builtin_ia32_cvtdq2pd256_mask(
      (__v4si)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtepi32_ps(__m128 __W, __mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtdq2ps128_mask((__v4si)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtepi32_ps(__mmask16 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtdq2ps128_mask(
      (__v4si)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtepi32_ps(__m256 __W, __mmask8 __U, __m256i __A) {
  return (__m256)__builtin_ia32_cvtdq2ps256_mask((__v8si)__A, (__v8sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtepi32_ps(__mmask16 __U, __m256i __A) {
  return (__m256)__builtin_ia32_cvtdq2ps256_mask(
      (__v8si)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtpd_epi32(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2dq128_mask((__v2df)__A, (__v4si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtpd_epi32(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2dq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtpd_epi32(__m128i __W, __mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvtpd2dq256_mask((__v4df)__A, (__v4si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtpd_epi32(__mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvtpd2dq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtpd_ps(__m128 __W, __mmask8 __U, __m128d __A) {
  return (__m128)__builtin_ia32_cvtpd2ps_mask((__v2df)__A, (__v4sf)__W,
                                              (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtpd_ps(__mmask8 __U, __m128d __A) {
  return (__m128)__builtin_ia32_cvtpd2ps_mask(
      (__v2df)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtpd_ps(__m128 __W, __mmask8 __U, __m256d __A) {
  return (__m128)__builtin_ia32_cvtpd2ps256_mask((__v4df)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtpd_ps(__mmask8 __U, __m256d __A) {
  return (__m128)__builtin_ia32_cvtpd2ps256_mask(
      (__v4df)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvtpd_epu32(__m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtpd_epu32(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq128_mask((__v2df)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtpd_epu32(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvtpd_epu32(__m256d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtpd_epu32(__m128i __W, __mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq256_mask((__v4df)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtpd_epu32(__mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvtpd2udq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtps_epi32(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2dq128_mask((__v4sf)__A, (__v4si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtps_epi32(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2dq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtps_epi32(__m256i __W, __mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvtps2dq256_mask((__v8sf)__A, (__v8si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtps_epi32(__mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvtps2dq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtps_pd(__m128d __W, __mmask8 __U, __m128 __A) {
  return (__m128d)__builtin_ia32_cvtps2pd128_mask((__v4sf)__A, (__v2df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtps_pd(__mmask8 __U, __m128 __A) {
  return (__m128d)__builtin_ia32_cvtps2pd128_mask(
      (__v4sf)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtps_pd(__m256d __W, __mmask8 __U, __m128 __A) {
  return (__m256d)__builtin_ia32_cvtps2pd256_mask((__v4sf)__A, (__v4df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtps_pd(__mmask8 __U, __m128 __A) {
  return (__m256d)__builtin_ia32_cvtps2pd256_mask(
      (__v4sf)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvtps_epu32(__m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2udq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtps_epu32(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2udq128_mask((__v4sf)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtps_epu32(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2udq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvtps_epu32(__m256 __A) {
  return (__m256i)__builtin_ia32_cvtps2udq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtps_epu32(__m256i __W, __mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvtps2udq256_mask((__v8sf)__A, (__v8si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtps_epu32(__mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvtps2udq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvttpd_epi32(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2dq128_mask((__v2df)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvttpd_epi32(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2dq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvttpd_epi32(__m128i __W, __mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvttpd2dq256_mask((__v4df)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvttpd_epi32(__mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvttpd2dq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvttpd_epu32(__m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvttpd_epu32(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq128_mask((__v2df)__A, (__v4si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvttpd_epu32(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq128_mask(
      (__v2df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvttpd_epu32(__m256d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvttpd_epu32(__m128i __W, __mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq256_mask((__v4df)__A, (__v4si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvttpd_epu32(__mmask8 __U, __m256d __A) {
  return (__m128i)__builtin_ia32_cvttpd2udq256_mask(
      (__v4df)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvttps_epi32(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2dq128_mask((__v4sf)__A, (__v4si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvttps_epi32(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2dq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvttps_epi32(__m256i __W, __mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvttps2dq256_mask((__v8sf)__A, (__v8si)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvttps_epi32(__mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvttps2dq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvttps_epu32(__m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2udq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvttps_epu32(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2udq128_mask((__v4sf)__A, (__v4si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvttps_epu32(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2udq128_mask(
      (__v4sf)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvttps_epu32(__m256 __A) {
  return (__m256i)__builtin_ia32_cvttps2udq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvttps_epu32(__m256i __W, __mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvttps2udq256_mask((__v8sf)__A, (__v8si)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvttps_epu32(__mmask8 __U, __m256 __A) {
  return (__m256i)__builtin_ia32_cvttps2udq256_mask(
      (__v8sf)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvtepu32_pd(__m128i __A) {
  return (__m128d)__builtin_ia32_cvtudq2pd128_mask(
      (__v4si)__A, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtepu32_pd(__m128d __W, __mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtudq2pd128_mask((__v4si)__A, (__v2df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtepu32_pd(__mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtudq2pd128_mask(
      (__v4si)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvtepu32_pd(__m128i __A) {
  return (__m256d)__builtin_ia32_cvtudq2pd256_mask(
      (__v4si)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtepu32_pd(__m256d __W, __mmask8 __U, __m128i __A) {
  return (__m256d)__builtin_ia32_cvtudq2pd256_mask((__v4si)__A, (__v4df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtepu32_pd(__mmask8 __U, __m128i __A) {
  return (__m256d)__builtin_ia32_cvtudq2pd256_mask(
      (__v4si)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_cvtepu32_ps(__m128i __A) {
  return (__m128)__builtin_ia32_cvtudq2ps128_mask(
      (__v4si)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_cvtepu32_ps(__m128 __W, __mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtudq2ps128_mask((__v4si)__A, (__v4sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_cvtepu32_ps(__mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtudq2ps128_mask(
      (__v4si)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_cvtepu32_ps(__m256i __A) {
  return (__m256)__builtin_ia32_cvtudq2ps256_mask(
      (__v8si)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_cvtepu32_ps(__m256 __W, __mmask8 __U, __m256i __A) {
  return (__m256)__builtin_ia32_cvtudq2ps256_mask((__v8si)__A, (__v8sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_cvtepu32_ps(__mmask8 __U, __m256i __A) {
  return (__m256)__builtin_ia32_cvtudq2ps256_mask(
      (__v8si)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_div_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_divpd_mask((__v2df)__A, (__v2df)__B,
                                            (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_div_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_divpd_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_div_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_divpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_div_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_divpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_div_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_divps_mask((__v4sf)__A, (__v4sf)__B,
                                           (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_div_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_divps_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_div_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_divps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_div_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_divps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expand_pd(__m128d __W, __mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_expanddf128_mask((__v2df)__A, (__v2df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expand_pd(__mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_expanddf128_mask(
      (__v2df)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expand_pd(__m256d __W, __mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_expanddf256_mask((__v4df)__A, (__v4df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expand_pd(__mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_expanddf256_mask(
      (__v4df)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expand_epi64(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_expanddi128_mask((__v2di)__A, (__v2di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expand_epi64(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_expanddi128_mask(
      (__v2di)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expand_epi64(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_expanddi256_mask((__v4di)__A, (__v4di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expand_epi64(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_expanddi256_mask(
      (__v4di)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expandloadu_pd(__m128d __W, __mmask8 __U, void const *__P) {
  return (__m128d)__builtin_ia32_expandloaddf128_mask(
      (__v2df *)__P, (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expandloadu_pd(__mmask8 __U, void const *__P) {
  return (__m128d)__builtin_ia32_expandloaddf128_mask(
      (__v2df *)__P, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expandloadu_pd(__m256d __W, __mmask8 __U, void const *__P) {
  return (__m256d)__builtin_ia32_expandloaddf256_mask(
      (__v4df *)__P, (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expandloadu_pd(__mmask8 __U, void const *__P) {
  return (__m256d)__builtin_ia32_expandloaddf256_mask(
      (__v4df *)__P, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expandloadu_epi64(__m128i __W, __mmask8 __U, void const *__P) {
  return (__m128i)__builtin_ia32_expandloaddi128_mask(
      (__v2di *)__P, (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expandloadu_epi64(__mmask8 __U, void const *__P) {
  return (__m128i)__builtin_ia32_expandloaddi128_mask(
      (__v2di *)__P, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expandloadu_epi64(__m256i __W, __mmask8 __U, void const *__P) {
  return (__m256i)__builtin_ia32_expandloaddi256_mask(
      (__v4di *)__P, (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expandloadu_epi64(__mmask8 __U, void const *__P) {
  return (__m256i)__builtin_ia32_expandloaddi256_mask(
      (__v4di *)__P, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expandloadu_ps(__m128 __W, __mmask8 __U, void const *__P) {
  return (__m128)__builtin_ia32_expandloadsf128_mask((__v4sf *)__P, (__v4sf)__W,
                                                     (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expandloadu_ps(__mmask8 __U, void const *__P) {
  return (__m128)__builtin_ia32_expandloadsf128_mask(
      (__v4sf *)__P, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expandloadu_ps(__m256 __W, __mmask8 __U, void const *__P) {
  return (__m256)__builtin_ia32_expandloadsf256_mask((__v8sf *)__P, (__v8sf)__W,
                                                     (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expandloadu_ps(__mmask8 __U, void const *__P) {
  return (__m256)__builtin_ia32_expandloadsf256_mask(
      (__v8sf *)__P, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expandloadu_epi32(__m128i __W, __mmask8 __U, void const *__P) {
  return (__m128i)__builtin_ia32_expandloadsi128_mask(
      (__v4si *)__P, (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expandloadu_epi32(__mmask8 __U, void const *__P) {
  return (__m128i)__builtin_ia32_expandloadsi128_mask(
      (__v4si *)__P, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expandloadu_epi32(__m256i __W, __mmask8 __U, void const *__P) {
  return (__m256i)__builtin_ia32_expandloadsi256_mask(
      (__v8si *)__P, (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expandloadu_epi32(__mmask8 __U, void const *__P) {
  return (__m256i)__builtin_ia32_expandloadsi256_mask(
      (__v8si *)__P, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expand_ps(__m128 __W, __mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_expandsf128_mask((__v4sf)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expand_ps(__mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_expandsf128_mask(
      (__v4sf)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expand_ps(__m256 __W, __mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_expandsf256_mask((__v8sf)__A, (__v8sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expand_ps(__mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_expandsf256_mask(
      (__v8sf)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_expand_epi32(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_expandsi128_mask((__v4si)__A, (__v4si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_expand_epi32(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_expandsi128_mask(
      (__v4si)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_expand_epi32(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_expandsi256_mask((__v8si)__A, (__v8si)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_expand_epi32(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_expandsi256_mask(
      (__v8si)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_getexp_pd(__m128d __A) {
  return (__m128d)__builtin_ia32_getexppd128_mask(
      (__v2df)__A, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_getexp_pd(__m128d __W, __mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_getexppd128_mask((__v2df)__A, (__v2df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_getexp_pd(__mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_getexppd128_mask(
      (__v2df)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_getexp_pd(__m256d __A) {
  return (__m256d)__builtin_ia32_getexppd256_mask(
      (__v4df)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_getexp_pd(__m256d __W, __mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_getexppd256_mask((__v4df)__A, (__v4df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_getexp_pd(__mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_getexppd256_mask(
      (__v4df)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_getexp_ps(__m128 __A) {
  return (__m128)__builtin_ia32_getexpps128_mask(
      (__v4sf)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_getexp_ps(__m128 __W, __mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_getexpps128_mask((__v4sf)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_getexp_ps(__mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_getexpps128_mask(
      (__v4sf)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_getexp_ps(__m256 __A) {
  return (__m256)__builtin_ia32_getexpps256_mask(
      (__v8sf)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_getexp_ps(__m256 __W, __mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_getexpps256_mask((__v8sf)__A, (__v8sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_getexp_ps(__mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_getexpps256_mask(
      (__v8sf)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_maxpd_mask((__v2df)__A, (__v2df)__B,
                                            (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_maxpd_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_maxpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_maxpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_maxps_mask((__v4sf)__A, (__v4sf)__B,
                                           (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_maxps_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_maxps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_maxps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_minpd_mask((__v2df)__A, (__v2df)__B,
                                            (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_minpd_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_minpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_minpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_minps_mask((__v4sf)__A, (__v4sf)__B,
                                           (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_minps_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_minps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_minps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_mul_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_mulpd_mask((__v2df)__A, (__v2df)__B,
                                            (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_mul_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_mulpd_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_mul_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_mulpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_mul_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_mulpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_mul_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_mulps_mask((__v4sf)__A, (__v4sf)__B,
                                           (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_mul_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_mulps_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_mul_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_mulps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_mul_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_mulps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_abs_epi32(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsd128_mask((__v4si)__A, (__v4si)__W,
                                               (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_abs_epi32(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsd128_mask(
      (__v4si)__A, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_abs_epi32(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsd256_mask((__v8si)__A, (__v8si)__W,
                                               (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_abs_epi32(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsd256_mask(
      (__v8si)__A, (__v8si)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_abs_epi64(__m128i __A) {
  return (__m128i)__builtin_ia32_pabsq128_mask(
      (__v2di)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_abs_epi64(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsq128_mask((__v2di)__A, (__v2di)__W,
                                               (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_abs_epi64(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsq128_mask(
      (__v2di)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_abs_epi64(__m256i __A) {
  return (__m256i)__builtin_ia32_pabsq256_mask(
      (__v4di)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_abs_epi64(__m256i __W, __mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsq256_mask((__v4di)__A, (__v4di)__W,
                                               (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_abs_epi64(__mmask8 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsq256_mask(
      (__v4di)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_epi32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_epi32(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsd128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_epi32(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_epi32(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsd256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_epi64(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_epi64(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_max_epi64(__m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_epi64(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_epi64(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_max_epi64(__m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_epu32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxud128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_epu32(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxud128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_epu32(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxud256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_epu32(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxud256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_max_epu64(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxuq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_max_epu64(__m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxuq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_max_epu64(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxuq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_max_epu64(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxuq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_max_epu64(__m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxuq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_max_epu64(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxuq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_epi32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsd128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_epi32(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsd128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_epi32(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsd256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_epi32(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsd256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_min_epi64(__m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_epi64(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_epi64(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_min_epi64(__m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_epi64(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_epi64(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_epu32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminud128_mask(
      (__v4si)__A, (__v4si)__B, (__v4si)_mm_setzero_si128(), __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_epu32(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminud128_mask((__v4si)__A, (__v4si)__B,
                                                (__v4si)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_epu32(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminud256_mask(
      (__v8si)__A, (__v8si)__B, (__v8si)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_epu32(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminud256_mask((__v8si)__A, (__v8si)__B,
                                                (__v8si)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_min_epu64(__m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminuq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_min_epu64(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminuq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, __M);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_min_epu64(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminuq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_min_epu64(__m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminuq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_min_epu64(__m256i __W, __mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminuq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_min_epu64(__mmask8 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminuq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), __M);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_scalef_pd(__m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_scalefpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_scalef_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_scalefpd128_mask((__v2df)__A, (__v2df)__B,
                                                  (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_scalef_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_scalefpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_scalef_pd(__m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_scalefpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_scalef_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_scalefpd256_mask((__v4df)__A, (__v4df)__B,
                                                  (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_scalef_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_scalefpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_scalef_ps(__m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_scalefps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_scalef_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_scalefps128_mask((__v4sf)__A, (__v4sf)__B,
                                                 (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_scalef_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_scalefps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_scalef_ps(__m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_scalefps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_scalef_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_scalefps256_mask((__v8sf)__A, (__v8sf)__B,
                                                 (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_scalef_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_scalefps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}
static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sqrt_pd(__m128d __W, __mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_sqrtpd128_mask((__v2df)__A, (__v2df)__W,
                                                (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sqrt_pd(__mmask8 __U, __m128d __A) {
  return (__m128d)__builtin_ia32_sqrtpd128_mask(
      (__v2df)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sqrt_pd(__m256d __W, __mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_sqrtpd256_mask((__v4df)__A, (__v4df)__W,
                                                (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sqrt_pd(__mmask8 __U, __m256d __A) {
  return (__m256d)__builtin_ia32_sqrtpd256_mask(
      (__v4df)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sqrt_ps(__m128 __W, __mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_sqrtps128_mask((__v4sf)__A, (__v4sf)__W,
                                               (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sqrt_ps(__mmask8 __U, __m128 __A) {
  return (__m128)__builtin_ia32_sqrtps128_mask(
      (__v4sf)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sqrt_ps(__m256 __W, __mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_sqrtps256_mask((__v8sf)__A, (__v8sf)__W,
                                               (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sqrt_ps(__mmask8 __U, __m256 __A) {
  return (__m256)__builtin_ia32_sqrtps256_mask(
      (__v8sf)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sub_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_subpd128_mask((__v2df)__A, (__v2df)__B,
                                               (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sub_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_subpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sub_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_subpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sub_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_subpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_sub_ps(__m128 __W, __mmask16 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_subps128_mask((__v4sf)__A, (__v4sf)__B,
                                              (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_sub_ps(__mmask16 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_subps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_sub_ps(__m256 __W, __mmask16 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_subps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_sub_ps(__mmask16 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_subps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask2_permutex2var_epi32(__m128i __A, __m128i __I, __mmask8 __U,
                                 __m128i __B) {
  return (__m128i)__builtin_ia32_vpermi2vard128_mask(
      (__v4si)__A, (__v4si)__I, (__v4si)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask2_permutex2var_epi32(__m256i __A, __m256i __I, __mmask8 __U,
                                    __m256i __B) {
  return (__m256i)__builtin_ia32_vpermi2vard256_mask(
      (__v8si)__A, (__v8si)__I, (__v8si)__B, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm_mask2_permutex2var_pd(__m128d __A, __m128i __I, __mmask8 __U, __m128d __B) {
  return (__m128d)__builtin_ia32_vpermi2varpd128_mask(
      (__v2df)__A, (__v2di)__I, (__v2df)__B, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask2_permutex2var_pd(__m256d __A, __m256i __I, __mmask8 __U,
                                 __m256d __B) {
  return (__m256d)__builtin_ia32_vpermi2varpd256_mask(
      (__v4df)__A, (__v4di)__I, (__v4df)__B, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl")))
_mm_mask2_permutex2var_ps(__m128 __A, __m128i __I, __mmask8 __U, __m128 __B) {
  return (__m128)__builtin_ia32_vpermi2varps128_mask(
      (__v4sf)__A, (__v4si)__I, (__v4sf)__B, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask2_permutex2var_ps(__m256 __A, __m256i __I, __mmask8 __U,
                                 __m256 __B) {
  return (__m256)__builtin_ia32_vpermi2varps256_mask(
      (__v8sf)__A, (__v8si)__I, (__v8sf)__B, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask2_permutex2var_epi64(__m128i __A, __m128i __I, __mmask8 __U,
                                 __m128i __B) {
  return (__m128i)__builtin_ia32_vpermi2varq128_mask(
      (__v2di)__A, (__v2di)__I, (__v2di)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask2_permutex2var_epi64(__m256i __A, __m256i __I, __mmask8 __U,
                                    __m256i __B) {
  return (__m256i)__builtin_ia32_vpermi2varq256_mask(
      (__v4di)__A, (__v4di)__I, (__v4di)__B, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_permutex2var_epi32(__m128i __A, __m128i __I, __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2vard128_mask((__v4si)__I, (__v4si)__A,
                                                     (__v4si)__B, (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_permutex2var_epi32(__m128i __A, __mmask8 __U, __m128i __I,
                                __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2vard128_mask(
      (__v4si)__I, (__v4si)__A, (__v4si)__B, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_permutex2var_epi32(__mmask8 __U, __m128i __A, __m128i __I,
                                 __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2vard128_maskz(
      (__v4si)__I, (__v4si)__A, (__v4si)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_permutex2var_epi32(__m256i __A, __m256i __I, __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2vard256_mask((__v8si)__I, (__v8si)__A,
                                                     (__v8si)__B, (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_permutex2var_epi32(__m256i __A, __mmask8 __U, __m256i __I,
                                   __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2vard256_mask(
      (__v8si)__I, (__v8si)__A, (__v8si)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_permutex2var_epi32(__mmask8 __U, __m256i __A, __m256i __I,
                                    __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2vard256_maskz(
      (__v8si)__I, (__v8si)__A, (__v8si)__B, (__mmask8)__U);
}

static __inline__ __m128d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_permutex2var_pd(__m128d __A, __m128i __I, __m128d __B) {
  return (__m128d)__builtin_ia32_vpermt2varpd128_mask(
      (__v2di)__I, (__v2df)__A, (__v2df)__B, (__mmask8)-1);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm_mask_permutex2var_pd(__m128d __A, __mmask8 __U, __m128i __I, __m128d __B) {
  return (__m128d)__builtin_ia32_vpermt2varpd128_mask(
      (__v2di)__I, (__v2df)__A, (__v2df)__B, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl")))
_mm_maskz_permutex2var_pd(__mmask8 __U, __m128d __A, __m128i __I, __m128d __B) {
  return (__m128d)__builtin_ia32_vpermt2varpd128_maskz(
      (__v2di)__I, (__v2df)__A, (__v2df)__B, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_permutex2var_pd(__m256d __A, __m256i __I, __m256d __B) {
  return (__m256d)__builtin_ia32_vpermt2varpd256_mask(
      (__v4di)__I, (__v4df)__A, (__v4df)__B, (__mmask8)-1);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_permutex2var_pd(__m256d __A, __mmask8 __U, __m256i __I,
                                __m256d __B) {
  return (__m256d)__builtin_ia32_vpermt2varpd256_mask(
      (__v4di)__I, (__v4df)__A, (__v4df)__B, (__mmask8)__U);
}

static __inline__ __m256d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_permutex2var_pd(__mmask8 __U, __m256d __A, __m256i __I,
                                 __m256d __B) {
  return (__m256d)__builtin_ia32_vpermt2varpd256_maskz(
      (__v4di)__I, (__v4df)__A, (__v4df)__B, (__mmask8)__U);
}

static __inline__ __m128
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_permutex2var_ps(__m128 __A, __m128i __I, __m128 __B) {
  return (__m128)__builtin_ia32_vpermt2varps128_mask((__v4si)__I, (__v4sf)__A,
                                                     (__v4sf)__B, (__mmask8)-1);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl")))
_mm_mask_permutex2var_ps(__m128 __A, __mmask8 __U, __m128i __I, __m128 __B) {
  return (__m128)__builtin_ia32_vpermt2varps128_mask(
      (__v4si)__I, (__v4sf)__A, (__v4sf)__B, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl")))
_mm_maskz_permutex2var_ps(__mmask8 __U, __m128 __A, __m128i __I, __m128 __B) {
  return (__m128)__builtin_ia32_vpermt2varps128_maskz(
      (__v4si)__I, (__v4sf)__A, (__v4sf)__B, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_permutex2var_ps(__m256 __A, __m256i __I, __m256 __B) {
  return (__m256)__builtin_ia32_vpermt2varps256_mask((__v8si)__I, (__v8sf)__A,
                                                     (__v8sf)__B, (__mmask8)-1);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl")))
_mm256_mask_permutex2var_ps(__m256 __A, __mmask8 __U, __m256i __I, __m256 __B) {
  return (__m256)__builtin_ia32_vpermt2varps256_mask(
      (__v8si)__I, (__v8sf)__A, (__v8sf)__B, (__mmask8)__U);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_permutex2var_ps(__mmask8 __U, __m256 __A, __m256i __I,
                                 __m256 __B) {
  return (__m256)__builtin_ia32_vpermt2varps256_maskz(
      (__v8si)__I, (__v8sf)__A, (__v8sf)__B, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_permutex2var_epi64(__m128i __A, __m128i __I, __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varq128_mask((__v2di)__I, (__v2di)__A,
                                                     (__v2di)__B, (__mmask8)-1);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_mask_permutex2var_epi64(__m128i __A, __mmask8 __U, __m128i __I,
                                __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varq128_mask(
      (__v2di)__I, (__v2di)__A, (__v2di)__B, (__mmask8)__U);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm_maskz_permutex2var_epi64(__mmask8 __U, __m128i __A, __m128i __I,
                                 __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varq128_maskz(
      (__v2di)__I, (__v2di)__A, (__v2di)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_permutex2var_epi64(__m256i __A, __m256i __I, __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varq256_mask((__v4di)__I, (__v4di)__A,
                                                     (__v4di)__B, (__mmask8)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_mask_permutex2var_epi64(__m256i __A, __mmask8 __U, __m256i __I,
                                   __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varq256_mask(
      (__v4di)__I, (__v4di)__A, (__v4di)__B, (__mmask8)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512vl")))
    _mm256_maskz_permutex2var_epi64(__mmask8 __U, __m256i __A, __m256i __I,
                                    __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varq256_maskz(
      (__v4di)__I, (__v4di)__A, (__v4di)__B, (__mmask8)__U);
}

typedef unsigned int __mmask32;
typedef unsigned long long __mmask64;
typedef char __v64qi __attribute__((__vector_size__(64)));
typedef short __v32hi __attribute__((__vector_size__(64)));

static __inline __v64qi
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_setzero_qi(void) {
  return (__v64qi){0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}

static __inline __v32hi
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_setzero_hi(void) {
  return (__v32hi){0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpeq_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_pcmpeqb512_mask((__v64qi)__a, (__v64qi)__b,
                                                   (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpeq_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_pcmpeqb512_mask((__v64qi)__a, (__v64qi)__b,
                                                   __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpeq_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 0,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpeq_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 0,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpeq_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_pcmpeqw512_mask((__v32hi)__a, (__v32hi)__b,
                                                   (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpeq_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_pcmpeqw512_mask((__v32hi)__a, (__v32hi)__b,
                                                   __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpeq_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 0,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpeq_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 0,
                                                 __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpge_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 5,
                                                (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpge_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 5,
                                                __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpge_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 5,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpge_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 5,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpge_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 5,
                                                (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpge_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 5,
                                                __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpge_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 5,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpge_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 5,
                                                 __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpgt_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_pcmpgtb512_mask((__v64qi)__a, (__v64qi)__b,
                                                   (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpgt_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_pcmpgtb512_mask((__v64qi)__a, (__v64qi)__b,
                                                   __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpgt_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 6,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpgt_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 6,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpgt_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_pcmpgtw512_mask((__v32hi)__a, (__v32hi)__b,
                                                   (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpgt_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_pcmpgtw512_mask((__v32hi)__a, (__v32hi)__b,
                                                   __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpgt_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 6,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpgt_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 6,
                                                 __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmple_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 2,
                                                (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmple_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 2,
                                                __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmple_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 2,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmple_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 2,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmple_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 2,
                                                (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmple_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 2,
                                                __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmple_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 2,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmple_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 2,
                                                 __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmplt_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 1,
                                                (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmplt_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 1,
                                                __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmplt_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 1,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmplt_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 1,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmplt_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 1,
                                                (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmplt_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 1,
                                                __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmplt_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 1,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmplt_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 1,
                                                 __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpneq_epi8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 4,
                                                (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpneq_epi8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_cmpb512_mask((__v64qi)__a, (__v64qi)__b, 4,
                                                __u);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpneq_epu8_mask(__m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 4,
                                                 (__mmask64)-1);
}

static __inline__ __mmask64
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpneq_epu8_mask(__mmask64 __u, __m512i __a, __m512i __b) {
  return (__mmask64)__builtin_ia32_ucmpb512_mask((__v64qi)__a, (__v64qi)__b, 4,
                                                 __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpneq_epi16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 4,
                                                (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpneq_epi16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_cmpw512_mask((__v32hi)__a, (__v32hi)__b, 4,
                                                __u);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cmpneq_epu16_mask(__m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 4,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cmpneq_epu16_mask(__mmask32 __u, __m512i __a, __m512i __b) {
  return (__mmask32)__builtin_ia32_ucmpw512_mask((__v32hi)__a, (__v32hi)__b, 4,
                                                 __u);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_add_epi8(__m512i __A, __m512i __B) {
  return (__m512i)((__v64qi)__A + (__v64qi)__B);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_add_epi8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddb512_mask((__v64qi)__A, (__v64qi)__B,
                                               (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_add_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_sub_epi8(__m512i __A, __m512i __B) {
  return (__m512i)((__v64qi)__A - (__v64qi)__B);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_sub_epi8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubb512_mask((__v64qi)__A, (__v64qi)__B,
                                               (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_sub_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_add_epi16(__m512i __A, __m512i __B) {
  return (__m512i)((__v32hi)__A + (__v32hi)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_add_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddw512_mask((__v32hi)__A, (__v32hi)__B,
                                               (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_add_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_sub_epi16(__m512i __A, __m512i __B) {
  return (__m512i)((__v32hi)__A - (__v32hi)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_sub_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubw512_mask((__v32hi)__A, (__v32hi)__B,
                                               (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_sub_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mullo_epi16(__m512i __A, __m512i __B) {
  return (__m512i)((__v32hi)__A * (__v32hi)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_mullo_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmullw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_mullo_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmullw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_blend_epi8(__mmask64 __U, __m512i __A, __m512i __W) {
  return (__m512i)__builtin_ia32_blendmb_512_mask((__v64qi)__A, (__v64qi)__W,
                                                  (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_blend_epi16(__mmask32 __U, __m512i __A, __m512i __W) {
  return (__m512i)__builtin_ia32_blendmw_512_mask((__v32hi)__A, (__v32hi)__W,
                                                  (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_abs_epi8(__m512i __A) {
  return (__m512i)__builtin_ia32_pabsb512_mask(
      (__v64qi)__A, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_abs_epi8(__m512i __W, __mmask64 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_pabsb512_mask((__v64qi)__A, (__v64qi)__W,
                                               (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_abs_epi8(__mmask64 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_pabsb512_mask(
      (__v64qi)__A, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_abs_epi16(__m512i __A) {
  return (__m512i)__builtin_ia32_pabsw512_mask(
      (__v32hi)__A, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_abs_epi16(__m512i __W, __mmask32 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_pabsw512_mask((__v32hi)__A, (__v32hi)__W,
                                               (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_abs_epi16(__mmask32 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_pabsw512_mask(
      (__v32hi)__A, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_packs_epi32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packssdw512_mask(
      (__v16si)__A, (__v16si)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_packs_epi32(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packssdw512_mask(
      (__v16si)__A, (__v16si)__B, (__v32hi)_mm512_setzero_hi(), __M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_packs_epi32(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packssdw512_mask((__v16si)__A, (__v16si)__B,
                                                  (__v32hi)__W, __M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_packs_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packsswb512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_packs_epi16(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packsswb512_mask((__v32hi)__A, (__v32hi)__B,
                                                  (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_packs_epi16(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packsswb512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v64qi)_mm512_setzero_qi(), __M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_packus_epi32(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packusdw512_mask(
      (__v16si)__A, (__v16si)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_packus_epi32(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packusdw512_mask(
      (__v16si)__A, (__v16si)__B, (__v32hi)_mm512_setzero_hi(), __M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_packus_epi32(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packusdw512_mask((__v16si)__A, (__v16si)__B,
                                                  (__v32hi)__W, __M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_packus_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packuswb512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_packus_epi16(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packuswb512_mask((__v32hi)__A, (__v32hi)__B,
                                                  (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_packus_epi16(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_packuswb512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_adds_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_adds_epi8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsb512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_adds_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_adds_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_adds_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_adds_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_adds_epu8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_adds_epu8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusb512_mask((__v64qi)__A, (__v64qi)__B,
                                                 (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_adds_epu8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_adds_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_adds_epu16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusw512_mask((__v32hi)__A, (__v32hi)__B,
                                                 (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_adds_epu16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_paddusw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_avg_epu8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_avg_epu8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgb512_mask((__v64qi)__A, (__v64qi)__B,
                                               (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_avg_epu8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_avg_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_avg_epu16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgw512_mask((__v32hi)__A, (__v32hi)__B,
                                               (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_avg_epu16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pavgw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_max_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_max_epi8(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_max_epi8(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsb512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_max_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_max_epi16(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_max_epi16(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxsw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_max_epu8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxub512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_max_epu8(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxub512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_max_epu8(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxub512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_max_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_max_epu16(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_max_epu16(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaxuw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_min_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_min_epi8(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_min_epi8(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsb512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_min_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_min_epi16(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_min_epi16(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminsw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_min_epu8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminub512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_min_epu8(__mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminub512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_min_epu8(__m512i __W, __mmask64 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminub512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_min_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_min_epu16(__mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__M);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_min_epu16(__m512i __W, __mmask32 __M, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pminuw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_shuffle_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pshufb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_shuffle_epi8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pshufb512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_shuffle_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pshufb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_subs_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_subs_epi8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsb512_mask((__v64qi)__A, (__v64qi)__B,
                                                (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_subs_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_subs_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_subs_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_subs_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_subs_epu8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_subs_epu8(__m512i __W, __mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusb512_mask((__v64qi)__A, (__v64qi)__B,
                                                 (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_subs_epu8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusb512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_subs_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_subs_epu16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusw512_mask((__v32hi)__A, (__v32hi)__B,
                                                 (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_subs_epu16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_psubusw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask2_permutex2var_epi16(__m512i __A, __m512i __I, __mmask32 __U,
                                    __m512i __B) {
  return (__m512i)__builtin_ia32_vpermi2varhi512_mask(
      (__v32hi)__A, (__v32hi)__I, (__v32hi)__B, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_permutex2var_epi16(__m512i __A, __m512i __I, __m512i __B) {
  return (__m512i)__builtin_ia32_vpermt2varhi512_mask(
      (__v32hi)__I, (__v32hi)__A, (__v32hi)__B, (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_permutex2var_epi16(__m512i __A, __mmask32 __U, __m512i __I,
                                   __m512i __B) {
  return (__m512i)__builtin_ia32_vpermt2varhi512_mask(
      (__v32hi)__I, (__v32hi)__A, (__v32hi)__B, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_permutex2var_epi16(__mmask32 __U, __m512i __A, __m512i __I,
                                    __m512i __B) {
  return (__m512i)__builtin_ia32_vpermt2varhi512_maskz(
      (__v32hi)__I, (__v32hi)__A, (__v32hi)__B, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mulhrs_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhrsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_mulhrs_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhrsw512_mask((__v32hi)__A, (__v32hi)__B,
                                                  (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_mulhrs_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhrsw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mulhi_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_mulhi_epi16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhw512_mask((__v32hi)__A, (__v32hi)__B,
                                                (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_mulhi_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mulhi_epu16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_mulhi_epu16(__m512i __W, __mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhuw512_mask((__v32hi)__A, (__v32hi)__B,
                                                 (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_mulhi_epu16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmulhuw512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maddubs_epi16(__m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmaddubsw512_mask(
      (__v64qi)__X, (__v64qi)__Y, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_maddubs_epi16(__m512i __W, __mmask32 __U, __m512i __X,
                              __m512i __Y) {
  return (__m512i)__builtin_ia32_pmaddubsw512_mask(
      (__v64qi)__X, (__v64qi)__Y, (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_maddubs_epi16(__mmask32 __U, __m512i __X, __m512i __Y) {
  return (__m512i)__builtin_ia32_pmaddubsw512_mask(
      (__v64qi)__X, (__v64qi)__Y, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_madd_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaddwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v16si)_mm512_setzero_si512(),
      (__mmask16)-1);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512bw")))
_mm512_mask_madd_epi16(__m512i __W, __mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaddwd512_mask((__v32hi)__A, (__v32hi)__B,
                                                 (__v16si)__W, (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_madd_epi16(__mmask16 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmaddwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v16si)_mm512_setzero_si512(),
      (__mmask16)__U);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cvtsepi16_epi8(__m512i __A) {
  return (__m256i)__builtin_ia32_pmovswb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), (__mmask32)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cvtsepi16_epi8(__m256i __O, __mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovswb512_mask((__v32hi)__A, (__v32qi)__O,
                                                 __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_cvtsepi16_epi8(__mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovswb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cvtusepi16_epi8(__m512i __A) {
  return (__m256i)__builtin_ia32_pmovuswb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), (__mmask32)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cvtusepi16_epi8(__m256i __O, __mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovuswb512_mask((__v32hi)__A, (__v32qi)__O,
                                                  __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_cvtusepi16_epi8(__mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovuswb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_cvtepi16_epi8(__m512i __A) {
  return (__m256i)__builtin_ia32_pmovwb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), (__mmask32)-1);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_cvtepi16_epi8(__m256i __O, __mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovwb512_mask((__v32hi)__A, (__v32qi)__O,
                                                __M);
}

static __inline__ __m256i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_cvtepi16_epi8(__mmask32 __M, __m512i __A) {
  return (__m256i)__builtin_ia32_pmovwb512_mask(
      (__v32hi)__A, (__v32qi)_mm256_setzero_si256(), __M);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_unpackhi_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_unpackhi_epi8(__m512i __W, __mmask64 __U, __m512i __A,
                              __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_unpackhi_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_unpackhi_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_unpackhi_epi16(__m512i __W, __mmask32 __U, __m512i __A,
                               __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_unpackhi_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpckhwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_unpacklo_epi8(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_unpacklo_epi8(__m512i __W, __mmask64 __U, __m512i __A,
                              __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)__W, (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_unpacklo_epi8(__mmask64 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklbw512_mask(
      (__v64qi)__A, (__v64qi)__B, (__v64qi)_mm512_setzero_qi(), (__mmask64)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_unpacklo_epi16(__m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_mask_unpacklo_epi16(__m512i __W, __mmask32 __U, __m512i __A,
                               __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)__W, (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512bw")))
    _mm512_maskz_unpacklo_epi16(__mmask32 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_punpcklwd512_mask(
      (__v32hi)__A, (__v32hi)__B, (__v32hi)_mm512_setzero_hi(), (__mmask32)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_conflict_epi64(__m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictdi_512_mask(
      (__v8di)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_mask_conflict_epi64(__m512i __W, __mmask8 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictdi_512_mask((__v8di)__A, (__v8di)__W,
                                                       (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_maskz_conflict_epi64(__mmask8 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictdi_512_mask(
      (__v8di)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_conflict_epi32(__m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictsi_512_mask(
      (__v16si)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_mask_conflict_epi32(__m512i __W, __mmask16 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictsi_512_mask(
      (__v16si)__A, (__v16si)__W, (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_maskz_conflict_epi32(__mmask16 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vpconflictsi_512_mask(
      (__v16si)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_lzcnt_epi32(__m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntd_512_mask(
      (__v16si)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_mask_lzcnt_epi32(__m512i __W, __mmask16 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntd_512_mask((__v16si)__A, (__v16si)__W,
                                                   (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_maskz_lzcnt_epi32(__mmask16 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntd_512_mask(
      (__v16si)__A, (__v16si)_mm512_setzero_si512(), (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_lzcnt_epi64(__m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntq_512_mask(
      (__v8di)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_mask_lzcnt_epi64(__m512i __W, __mmask8 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntq_512_mask((__v8di)__A, (__v8di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512cd")))
    _mm512_maskz_lzcnt_epi64(__mmask8 __U, __m512i __A) {
  return (__m512i)__builtin_ia32_vplzcntq_512_mask(
      (__v8di)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mullo_epi64(__m512i __A, __m512i __B) {
  return (__m512i)((__v8di)__A * (__v8di)__B);
}

static __inline__ __m512i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512dq")))
_mm512_mask_mullo_epi64(__m512i __W, __mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmullq512_mask((__v8di)__A, (__v8di)__B,
                                                (__v8di)__W, (__mmask8)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_mullo_epi64(__mmask8 __U, __m512i __A, __m512i __B) {
  return (__m512i)__builtin_ia32_pmullq512_mask(
      (__v8di)__A, (__v8di)__B, (__v8di)_mm512_setzero_si512(), (__mmask8)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_xor_pd(__m512d __A, __m512d __B) {
  return (__m512d)((__v8di)__A ^ (__v8di)__B);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_xor_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_xorpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)__W, (__mmask8)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_xor_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_xorpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_xor_ps(__m512 __A, __m512 __B) {
  return (__m512)((__v16si)__A ^ (__v16si)__B);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_xor_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_xorps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)__W, (__mmask16)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_xor_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_xorps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_or_pd(__m512d __A, __m512d __B) {
  return (__m512d)((__v8di)__A | (__v8di)__B);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_or_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_orpd512_mask((__v8df)__A, (__v8df)__B,
                                              (__v8df)__W, (__mmask8)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_or_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_orpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_or_ps(__m512 __A, __m512 __B) {
  return (__m512)((__v16si)__A | (__v16si)__B);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_or_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_orps512_mask((__v16sf)__A, (__v16sf)__B,
                                             (__v16sf)__W, (__mmask16)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_or_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_orps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_and_pd(__m512d __A, __m512d __B) {
  return (__m512d)((__v8di)__A & (__v8di)__B);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_and_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_andpd512_mask((__v8df)__A, (__v8df)__B,
                                               (__v8df)__W, (__mmask8)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_and_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_andpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_and_ps(__m512 __A, __m512 __B) {
  return (__m512)((__v16si)__A & (__v16si)__B);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_and_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_andps512_mask((__v16sf)__A, (__v16sf)__B,
                                              (__v16sf)__W, (__mmask16)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_and_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_andps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_andnot_pd(__m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_andnpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)_mm512_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_andnot_pd(__m512d __W, __mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_andnpd512_mask((__v8df)__A, (__v8df)__B,
                                                (__v8df)__W, (__mmask8)__U);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_andnot_pd(__mmask8 __U, __m512d __A, __m512d __B) {
  return (__m512d)__builtin_ia32_andnpd512_mask(
      (__v8df)__A, (__v8df)__B, (__v8df)_mm512_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_andnot_ps(__m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_andnps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)_mm512_setzero_ps(), (__mmask16)-1);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_andnot_ps(__m512 __W, __mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_andnps512_mask((__v16sf)__A, (__v16sf)__B,
                                               (__v16sf)__W, (__mmask16)__U);
}

static __inline__ __m512
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_andnot_ps(__mmask16 __U, __m512 __A, __m512 __B) {
  return (__m512)__builtin_ia32_andnps512_mask(
      (__v16sf)__A, (__v16sf)__B, (__v16sf)_mm512_setzero_ps(), (__mmask16)__U);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtpd_epi64(__m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2qq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtpd_epi64(__m512i __W, __mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2qq512_mask((__v8df)__A, (__v8di)__W,
                                                  (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtpd_epi64(__mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2qq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtpd_epu64(__m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2uqq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtpd_epu64(__m512i __W, __mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2uqq512_mask((__v8df)__A, (__v8di)__W,
                                                   (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtpd_epu64(__mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvtpd2uqq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtps_epi64(__m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2qq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtps_epi64(__m512i __W, __mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2qq512_mask((__v8sf)__A, (__v8di)__W,
                                                  (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtps_epi64(__mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2qq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtps_epu64(__m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2uqq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtps_epu64(__m512i __W, __mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2uqq512_mask((__v8sf)__A, (__v8di)__W,
                                                   (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtps_epu64(__mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvtps2uqq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtepi64_pd(__m512i __A) {
  return (__m512d)__builtin_ia32_cvtqq2pd512_mask(
      (__v8di)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtepi64_pd(__m512d __W, __mmask8 __U, __m512i __A) {
  return (__m512d)__builtin_ia32_cvtqq2pd512_mask((__v8di)__A, (__v8df)__W,
                                                  (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtepi64_pd(__mmask8 __U, __m512i __A) {
  return (__m512d)__builtin_ia32_cvtqq2pd512_mask(
      (__v8di)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtepi64_ps(__m512i __A) {
  return (__m256)__builtin_ia32_cvtqq2ps512_mask(
      (__v8di)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)-1, 0x04);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtepi64_ps(__m256 __W, __mmask8 __U, __m512i __A) {
  return (__m256)__builtin_ia32_cvtqq2ps512_mask((__v8di)__A, (__v8sf)__W,
                                                 (__mmask8)__U, 0x04);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtepi64_ps(__mmask8 __U, __m512i __A) {
  return (__m256)__builtin_ia32_cvtqq2ps512_mask(
      (__v8di)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvttpd_epi64(__m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2qq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvttpd_epi64(__m512i __W, __mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2qq512_mask((__v8df)__A, (__v8di)__W,
                                                   (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvttpd_epi64(__mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2qq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvttpd_epu64(__m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2uqq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvttpd_epu64(__m512i __W, __mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2uqq512_mask((__v8df)__A, (__v8di)__W,
                                                    (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvttpd_epu64(__mmask8 __U, __m512d __A) {
  return (__m512i)__builtin_ia32_cvttpd2uqq512_mask(
      (__v8df)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvttps_epi64(__m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2qq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvttps_epi64(__m512i __W, __mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2qq512_mask((__v8sf)__A, (__v8di)__W,
                                                   (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvttps_epi64(__mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2qq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvttps_epu64(__m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2uqq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)-1, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvttps_epu64(__m512i __W, __mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2uqq512_mask((__v8sf)__A, (__v8di)__W,
                                                    (__mmask8)__U, 0x04);
}

static __inline__ __m512i
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvttps_epu64(__mmask8 __U, __m256 __A) {
  return (__m512i)__builtin_ia32_cvttps2uqq512_mask(
      (__v8sf)__A, (__v8di)_mm512_setzero_si512(), (__mmask8)__U, 0x04);
}
static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtepu64_pd(__m512i __A) {
  return (__m512d)__builtin_ia32_cvtuqq2pd512_mask(
      (__v8di)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)-1, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtepu64_pd(__m512d __W, __mmask8 __U, __m512i __A) {
  return (__m512d)__builtin_ia32_cvtuqq2pd512_mask((__v8di)__A, (__v8df)__W,
                                                   (__mmask8)__U, 0x04);
}

static __inline__ __m512d
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtepu64_pd(__mmask8 __U, __m512i __A) {
  return (__m512d)__builtin_ia32_cvtuqq2pd512_mask(
      (__v8di)__A, (__v8df)_mm512_setzero_pd(), (__mmask8)__U, 0x04);
}
static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_cvtepu64_ps(__m512i __A) {
  return (__m256)__builtin_ia32_cvtuqq2ps512_mask(
      (__v8di)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)-1, 0x04);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_mask_cvtepu64_ps(__m256 __W, __mmask8 __U, __m512i __A) {
  return (__m256)__builtin_ia32_cvtuqq2ps512_mask((__v8di)__A, (__v8sf)__W,
                                                  (__mmask8)__U, 0x04);
}

static __inline__ __m256
    __attribute__((__always_inline__, __nodebug__, __target__("avx512dq")))
    _mm512_maskz_cvtepu64_ps(__mmask8 __U, __m512i __A) {
  return (__m256)__builtin_ia32_cvtuqq2ps512_mask(
      (__v8di)__A, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U, 0x04);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpeq_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqb128_mask((__v16qi)__a, (__v16qi)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpeq_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqb128_mask((__v16qi)__a, (__v16qi)__b,
                                                   __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpeq_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 0,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpeq_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 0,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpeq_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_pcmpeqb256_mask((__v32qi)__a, (__v32qi)__b,
                                                   (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpeq_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_pcmpeqb256_mask((__v32qi)__a, (__v32qi)__b,
                                                   __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpeq_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 0,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpeq_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 0,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpeq_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqw128_mask((__v8hi)__a, (__v8hi)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpeq_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpeqw128_mask((__v8hi)__a, (__v8hi)__b,
                                                  __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpeq_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 0,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpeq_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 0,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpeq_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqw256_mask((__v16hi)__a, (__v16hi)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpeq_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_pcmpeqw256_mask((__v16hi)__a, (__v16hi)__b,
                                                   __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpeq_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 0,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpeq_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 0,
                                                 __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpge_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 5,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpge_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 5,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpge_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 5,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpge_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 5,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpge_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 5,
                                                (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpge_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 5,
                                                __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpge_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 5,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpge_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 5,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpge_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 5,
                                               (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpge_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 5,
                                               __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpge_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 5,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpge_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 5,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpge_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 5,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpge_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 5,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpge_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 5,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpge_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 5,
                                                 __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpgt_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtb128_mask((__v16qi)__a, (__v16qi)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpgt_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtb128_mask((__v16qi)__a, (__v16qi)__b,
                                                   __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpgt_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 6,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpgt_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 6,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpgt_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_pcmpgtb256_mask((__v32qi)__a, (__v32qi)__b,
                                                   (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpgt_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_pcmpgtb256_mask((__v32qi)__a, (__v32qi)__b,
                                                   __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpgt_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 6,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpgt_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 6,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpgt_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtw128_mask((__v8hi)__a, (__v8hi)__b,
                                                  (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpgt_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_pcmpgtw128_mask((__v8hi)__a, (__v8hi)__b,
                                                  __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpgt_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 6,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpgt_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 6,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpgt_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtw256_mask((__v16hi)__a, (__v16hi)__b,
                                                   (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpgt_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_pcmpgtw256_mask((__v16hi)__a, (__v16hi)__b,
                                                   __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpgt_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 6,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpgt_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 6,
                                                 __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmple_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 2,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmple_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 2,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmple_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 2,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmple_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 2,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmple_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 2,
                                                (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmple_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 2,
                                                __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmple_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 2,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmple_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 2,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmple_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 2,
                                               (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmple_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 2,
                                               __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmple_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 2,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmple_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 2,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmple_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 2,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmple_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 2,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmple_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 2,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmple_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 2,
                                                 __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmplt_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 1,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmplt_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 1,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmplt_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 1,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmplt_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 1,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmplt_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 1,
                                                (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmplt_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 1,
                                                __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmplt_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 1,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmplt_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 1,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmplt_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 1,
                                               (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmplt_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 1,
                                               __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmplt_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 1,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmplt_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 1,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmplt_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 1,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmplt_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 1,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmplt_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 1,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmplt_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 1,
                                                 __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpneq_epi8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 4,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpneq_epi8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_cmpb128_mask((__v16qi)__a, (__v16qi)__b, 4,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_cmpneq_epu8_mask(__m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 4,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm_mask_cmpneq_epu8_mask(__mmask16 __u, __m128i __a, __m128i __b) {
  return (__mmask16)__builtin_ia32_ucmpb128_mask((__v16qi)__a, (__v16qi)__b, 4,
                                                 __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpneq_epi8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 4,
                                                (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpneq_epi8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_cmpb256_mask((__v32qi)__a, (__v32qi)__b, 4,
                                                __u);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpneq_epu8_mask(__m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 4,
                                                 (__mmask32)-1);
}

static __inline__ __mmask32 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpneq_epu8_mask(__mmask32 __u, __m256i __a, __m256i __b) {
  return (__mmask32)__builtin_ia32_ucmpb256_mask((__v32qi)__a, (__v32qi)__b, 4,
                                                 __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpneq_epi16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 4,
                                               (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpneq_epi16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_cmpw128_mask((__v8hi)__a, (__v8hi)__b, 4,
                                               __u);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_cmpneq_epu16_mask(__m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 4,
                                                (__mmask8)-1);
}

static __inline__ __mmask8 __attribute__((__always_inline__, __nodebug__,
                                          __target__("avx512vl,avx512bw")))
_mm_mask_cmpneq_epu16_mask(__mmask8 __u, __m128i __a, __m128i __b) {
  return (__mmask8)__builtin_ia32_ucmpw128_mask((__v8hi)__a, (__v8hi)__b, 4,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpneq_epi16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 4,
                                                (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpneq_epi16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_cmpw256_mask((__v16hi)__a, (__v16hi)__b, 4,
                                                __u);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_cmpneq_epu16_mask(__m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 4,
                                                 (__mmask16)-1);
}

static __inline__ __mmask16 __attribute__((__always_inline__, __nodebug__,
                                           __target__("avx512vl,avx512bw")))
_mm256_mask_cmpneq_epu16_mask(__mmask16 __u, __m256i __a, __m256i __b) {
  return (__mmask16)__builtin_ia32_ucmpw256_mask((__v16hi)__a, (__v16hi)__b, 4,
                                                 __u);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_add_epi8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_add_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)_mm256_setzero_si256(),
                                               (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_add_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_add_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)_mm256_setzero_si256(),
                                               (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_sub_epi8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_sub_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)_mm256_setzero_si256(),
                                               (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_sub_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_sub_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)_mm256_setzero_si256(),
                                               (__mmask16)__U);
}
static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_add_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddb128_mask((__v16qi)__A, (__v16qi)__B,
                                               (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_add_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_add_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddw128_mask((__v8hi)__A, (__v8hi)__B,
                                               (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_add_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_sub_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubb128_mask((__v16qi)__A, (__v16qi)__B,
                                               (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_sub_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_sub_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubw128_mask((__v8hi)__A, (__v8hi)__B,
                                               (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_sub_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_mullo_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmullw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_mullo_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmullw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_mullo_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmullw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_mullo_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmullw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_blend_epi8(__mmask16 __U, __m128i __A, __m128i __W) {
  return (__m128i)__builtin_ia32_blendmb_128_mask((__v16qi)__A, (__v16qi)__W,
                                                  (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_blend_epi8(__mmask32 __U, __m256i __A, __m256i __W) {
  return (__m256i)__builtin_ia32_blendmb_256_mask((__v32qi)__A, (__v32qi)__W,
                                                  (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_blend_epi16(__mmask8 __U, __m128i __A, __m128i __W) {
  return (__m128i)__builtin_ia32_blendmw_128_mask((__v8hi)__A, (__v8hi)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_blend_epi16(__mmask16 __U, __m256i __A, __m256i __W) {
  return (__m256i)__builtin_ia32_blendmw_256_mask((__v16hi)__A, (__v16hi)__W,
                                                  (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_abs_epi8(__m128i __W, __mmask16 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsb128_mask((__v16qi)__A, (__v16qi)__W,
                                               (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_abs_epi8(__mmask16 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsb128_mask(
      (__v16qi)__A, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_abs_epi8(__m256i __W, __mmask32 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsb256_mask((__v32qi)__A, (__v32qi)__W,
                                               (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_abs_epi8(__mmask32 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsb256_mask(
      (__v32qi)__A, (__v32qi)_mm256_setzero_si256(), (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_abs_epi16(__m128i __W, __mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsw128_mask((__v8hi)__A, (__v8hi)__W,
                                               (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_abs_epi16(__mmask8 __U, __m128i __A) {
  return (__m128i)__builtin_ia32_pabsw128_mask(
      (__v8hi)__A, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_abs_epi16(__m256i __W, __mmask16 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsw256_mask((__v16hi)__A, (__v16hi)__W,
                                               (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_abs_epi16(__mmask16 __U, __m256i __A) {
  return (__m256i)__builtin_ia32_pabsw256_mask(
      (__v16hi)__A, (__v16hi)_mm256_setzero_si256(), (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_packs_epi32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packssdw128_mask(
      (__v4si)__A, (__v4si)__B, (__v8hi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_packs_epi32(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packssdw128_mask((__v4si)__A, (__v4si)__B,
                                                  (__v8hi)__W, __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_packs_epi32(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packssdw256_mask(
      (__v8si)__A, (__v8si)__B, (__v16hi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_packs_epi32(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packssdw256_mask((__v8si)__A, (__v8si)__B,
                                                  (__v16hi)__W, __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_packs_epi16(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packsswb128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_packs_epi16(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packsswb128_mask((__v8hi)__A, (__v8hi)__B,
                                                  (__v16qi)__W, __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_packs_epi16(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packsswb256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v32qi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_packs_epi16(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packsswb256_mask((__v16hi)__A, (__v16hi)__B,
                                                  (__v32qi)__W, __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_packus_epi32(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packusdw128_mask(
      (__v4si)__A, (__v4si)__B, (__v8hi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_packus_epi32(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packusdw128_mask((__v4si)__A, (__v4si)__B,
                                                  (__v8hi)__W, __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_packus_epi32(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packusdw256_mask(
      (__v8si)__A, (__v8si)__B, (__v16hi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_packus_epi32(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packusdw256_mask((__v8si)__A, (__v8si)__B,
                                                  (__v16hi)__W, __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_packus_epi16(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packuswb128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_packus_epi16(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_packuswb128_mask((__v8hi)__A, (__v8hi)__B,
                                                  (__v16qi)__W, __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_packus_epi16(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packuswb256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v32qi)_mm256_setzero_si256(), __M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_packus_epi16(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_packuswb256_mask((__v16hi)__A, (__v16hi)__B,
                                                  (__v32qi)__W, __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_adds_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddsb128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_adds_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddsb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_adds_epi8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_adds_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_adds_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddsw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_adds_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddsw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_adds_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_adds_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_adds_epu8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddusb128_mask((__v16qi)__A, (__v16qi)__B,
                                                 (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_adds_epu8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddusb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_adds_epu8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddusb256_mask((__v32qi)__A, (__v32qi)__B,
                                                 (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_adds_epu8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddusb256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)_mm256_setzero_si256(),
      (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_adds_epu16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddusw128_mask((__v8hi)__A, (__v8hi)__B,
                                                 (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_adds_epu16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_paddusw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_adds_epu16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddusw256_mask((__v16hi)__A, (__v16hi)__B,
                                                 (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_adds_epu16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_paddusw256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_avg_epu8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pavgb128_mask((__v16qi)__A, (__v16qi)__B,
                                               (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_avg_epu8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pavgb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_avg_epu8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pavgb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_avg_epu8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pavgb256_mask((__v32qi)__A, (__v32qi)__B,
                                               (__v32qi)_mm256_setzero_si256(),
                                               (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_avg_epu16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pavgw128_mask((__v8hi)__A, (__v8hi)__B,
                                               (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_avg_epu16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pavgw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_avg_epu16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pavgw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_avg_epu16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pavgw256_mask((__v16hi)__A, (__v16hi)__B,
                                               (__v16hi)_mm256_setzero_si256(),
                                               (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_max_epi8(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_max_epi8(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsb128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_max_epi8(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_max_epi8(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_max_epi16(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_max_epi16(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxsw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_max_epi16(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_max_epi16(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_max_epu8(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxub128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_max_epu8(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxub128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_max_epu8(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxub256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_max_epu8(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxub256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_max_epu16(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxuw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_max_epu16(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaxuw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_max_epu16(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxuw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_max_epu16(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaxuw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_min_epi8(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_min_epi8(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsb128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_min_epi8(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_min_epi8(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_min_epi16(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_min_epi16(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminsw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_min_epi16(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_min_epi16(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_min_epu8(__mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminub128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_min_epu8(__m128i __W, __mmask16 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminub128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_min_epu8(__mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminub256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_min_epu8(__m256i __W, __mmask32 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminub256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_min_epu16(__mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminuw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_min_epu16(__m128i __W, __mmask8 __M, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pminuw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_min_epu16(__mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminuw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__M);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_min_epu16(__m256i __W, __mmask16 __M, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pminuw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_shuffle_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pshufb128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_shuffle_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pshufb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_shuffle_epi8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pshufb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_shuffle_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pshufb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_subs_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubsb128_mask((__v16qi)__A, (__v16qi)__B,
                                                (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_subs_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubsb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_subs_epi8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_subs_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubsb256_mask((__v32qi)__A, (__v32qi)__B,
                                                (__v32qi)_mm256_setzero_si256(),
                                                (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_subs_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubsw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_subs_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubsw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_subs_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_subs_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubsw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_subs_epu8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubusb128_mask((__v16qi)__A, (__v16qi)__B,
                                                 (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_subs_epu8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubusb128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_subs_epu8(__m256i __W, __mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubusb256_mask((__v32qi)__A, (__v32qi)__B,
                                                 (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_subs_epu8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubusb256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)_mm256_setzero_si256(),
      (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_subs_epu16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubusw128_mask((__v8hi)__A, (__v8hi)__B,
                                                 (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_subs_epu16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_psubusw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_subs_epu16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubusw256_mask((__v16hi)__A, (__v16hi)__B,
                                                 (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_subs_epu16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_psubusw256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask2_permutex2var_epi16(__m128i __A, __m128i __I, __mmask8 __U,
                             __m128i __B) {
  return (__m128i)__builtin_ia32_vpermi2varhi128_mask(
      (__v8hi)__A, (__v8hi)__I, (__v8hi)__B, (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask2_permutex2var_epi16(__m256i __A, __m256i __I, __mmask16 __U,
                                __m256i __B) {
  return (__m256i)__builtin_ia32_vpermi2varhi256_mask(
      (__v16hi)__A, (__v16hi)__I, (__v16hi)__B, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_permutex2var_epi16(__m128i __A, __m128i __I, __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varhi128_mask(
      (__v8hi)__I, (__v8hi)__A, (__v8hi)__B, (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_permutex2var_epi16(__m128i __A, __mmask8 __U, __m128i __I,
                            __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varhi128_mask(
      (__v8hi)__I, (__v8hi)__A, (__v8hi)__B, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_permutex2var_epi16(__mmask8 __U, __m128i __A, __m128i __I,
                             __m128i __B) {
  return (__m128i)__builtin_ia32_vpermt2varhi128_maskz(
      (__v8hi)__I, (__v8hi)__A, (__v8hi)__B, (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_permutex2var_epi16(__m256i __A, __m256i __I, __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varhi256_mask(
      (__v16hi)__I, (__v16hi)__A, (__v16hi)__B, (__mmask16)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_permutex2var_epi16(__m256i __A, __mmask16 __U, __m256i __I,
                               __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varhi256_mask(
      (__v16hi)__I, (__v16hi)__A, (__v16hi)__B, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_permutex2var_epi16(__mmask16 __U, __m256i __A, __m256i __I,
                                __m256i __B) {
  return (__m256i)__builtin_ia32_vpermt2varhi256_maskz(
      (__v16hi)__I, (__v16hi)__A, (__v16hi)__B, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_maddubs_epi16(__m128i __W, __mmask8 __U, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmaddubsw128_mask((__v16qi)__X, (__v16qi)__Y,
                                                   (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_maddubs_epi16(__mmask8 __U, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmaddubsw128_mask(
      (__v16qi)__X, (__v16qi)__Y, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_maddubs_epi16(__m256i __W, __mmask16 __U, __m256i __X,
                          __m256i __Y) {
  return (__m256i)__builtin_ia32_pmaddubsw256_mask(
      (__v32qi)__X, (__v32qi)__Y, (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_maddubs_epi16(__mmask16 __U, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmaddubsw256_mask(
      (__v32qi)__X, (__v32qi)__Y, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_madd_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaddwd128_mask((__v8hi)__A, (__v8hi)__B,
                                                 (__v4si)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_madd_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmaddwd128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v4si)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_madd_epi16(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaddwd256_mask((__v16hi)__A, (__v16hi)__B,
                                                 (__v8si)__W, (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_madd_epi16(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmaddwd256_mask((__v16hi)__A, (__v16hi)__B,
                                                 (__v8si)_mm256_setzero_si256(),
                                                 (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_cvtsepi16_epi8(__m128i __A) {
  return (__m128i)__builtin_ia32_pmovswb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_cvtsepi16_epi8(__m128i __O, __mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovswb128_mask((__v8hi)__A, (__v16qi)__O,
                                                 __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_cvtsepi16_epi8(__mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovswb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_cvtsepi16_epi8(__m256i __A) {
  return (__m128i)__builtin_ia32_pmovswb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask16)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_cvtsepi16_epi8(__m128i __O, __mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovswb256_mask((__v16hi)__A, (__v16qi)__O,
                                                 __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_cvtsepi16_epi8(__mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovswb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_cvtusepi16_epi8(__m128i __A) {
  return (__m128i)__builtin_ia32_pmovuswb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_cvtusepi16_epi8(__m128i __O, __mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovuswb128_mask((__v8hi)__A, (__v16qi)__O,
                                                  __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_cvtusepi16_epi8(__mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovuswb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_cvtusepi16_epi8(__m256i __A) {
  return (__m128i)__builtin_ia32_pmovuswb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask16)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_cvtusepi16_epi8(__m128i __O, __mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovuswb256_mask((__v16hi)__A, (__v16qi)__O,
                                                  __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_cvtusepi16_epi8(__mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovuswb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_cvtepi16_epi8(__m128i __A) {

  return (__m128i)__builtin_ia32_pmovwb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_cvtepi16_epi8(__m128i __O, __mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovwb128_mask((__v8hi)__A, (__v16qi)__O, __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_cvtepi16_epi8(__mmask8 __M, __m128i __A) {
  return (__m128i)__builtin_ia32_pmovwb128_mask(
      (__v8hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_cvtepi16_epi8(__m256i __A) {
  return (__m128i)__builtin_ia32_pmovwb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), (__mmask16)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_cvtepi16_epi8(__m128i __O, __mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovwb256_mask((__v16hi)__A, (__v16qi)__O,
                                                __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_cvtepi16_epi8(__mmask16 __M, __m256i __A) {
  return (__m128i)__builtin_ia32_pmovwb256_mask(
      (__v16hi)__A, (__v16qi)_mm_setzero_si128(), __M);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_mulhrs_epi16(__m128i __W, __mmask8 __U, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmulhrsw128_mask((__v8hi)__X, (__v8hi)__Y,
                                                  (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_mulhrs_epi16(__mmask8 __U, __m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_pmulhrsw128_mask(
      (__v8hi)__X, (__v8hi)__Y, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_mulhrs_epi16(__m256i __W, __mmask16 __U, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmulhrsw256_mask((__v16hi)__X, (__v16hi)__Y,
                                                  (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_mulhrs_epi16(__mmask16 __U, __m256i __X, __m256i __Y) {
  return (__m256i)__builtin_ia32_pmulhrsw256_mask(
      (__v16hi)__X, (__v16hi)__Y, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_mulhi_epu16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulhuw128_mask((__v8hi)__A, (__v8hi)__B,
                                                 (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_mulhi_epu16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulhuw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_mulhi_epu16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulhuw256_mask((__v16hi)__A, (__v16hi)__B,
                                                 (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_mulhi_epu16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulhuw256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_mulhi_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulhw128_mask((__v8hi)__A, (__v8hi)__B,
                                                (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_mulhi_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmulhw128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_mulhi_epi16(__m256i __W, __mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulhw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_mulhi_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmulhw256_mask((__v16hi)__A, (__v16hi)__B,
                                                (__v16hi)_mm256_setzero_si256(),
                                                (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_unpackhi_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpckhbw128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_unpackhi_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpckhbw128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_unpackhi_epi8(__m256i __W, __mmask32 __U, __m256i __A,
                          __m256i __B) {
  return (__m256i)__builtin_ia32_punpckhbw256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_unpackhi_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_punpckhbw256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)_mm256_setzero_si256(),
      (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_unpackhi_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpckhwd128_mask((__v8hi)__A, (__v8hi)__B,
                                                   (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_unpackhi_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpckhwd128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_unpackhi_epi16(__m256i __W, __mmask16 __U, __m256i __A,
                           __m256i __B) {
  return (__m256i)__builtin_ia32_punpckhwd256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_unpackhi_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_punpckhwd256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_unpacklo_epi8(__m128i __W, __mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpcklbw128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)__W, (__mmask16)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_unpacklo_epi8(__mmask16 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpcklbw128_mask(
      (__v16qi)__A, (__v16qi)__B, (__v16qi)_mm_setzero_si128(), (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_unpacklo_epi8(__m256i __W, __mmask32 __U, __m256i __A,
                          __m256i __B) {
  return (__m256i)__builtin_ia32_punpcklbw256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)__W, (__mmask32)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_unpacklo_epi8(__mmask32 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_punpcklbw256_mask(
      (__v32qi)__A, (__v32qi)__B, (__v32qi)_mm256_setzero_si256(),
      (__mmask32)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_mask_unpacklo_epi16(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpcklwd128_mask((__v8hi)__A, (__v8hi)__B,
                                                   (__v8hi)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm_maskz_unpacklo_epi16(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_punpcklwd128_mask(
      (__v8hi)__A, (__v8hi)__B, (__v8hi)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_mask_unpacklo_epi16(__m256i __W, __mmask16 __U, __m256i __A,
                           __m256i __B) {
  return (__m256i)__builtin_ia32_punpcklwd256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)__W, (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512bw")))
_mm256_maskz_unpacklo_epi16(__mmask16 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_punpcklwd256_mask(
      (__v16hi)__A, (__v16hi)__B, (__v16hi)_mm256_setzero_si256(),
      (__mmask16)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mullo_epi64(__m256i __A, __m256i __B) {
  return (__m256i)((__v4di)__A * (__v4di)__B);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_mullo_epi64(__m256i __W, __mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmullq256_mask((__v4di)__A, (__v4di)__B,
                                                (__v4di)__W, (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_mullo_epi64(__mmask8 __U, __m256i __A, __m256i __B) {
  return (__m256i)__builtin_ia32_pmullq256_mask(
      (__v4di)__A, (__v4di)__B, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mullo_epi64(__m128i __A, __m128i __B) {
  return (__m128i)((__v2di)__A * (__v2di)__B);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_mullo_epi64(__m128i __W, __mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmullq128_mask((__v2di)__A, (__v2di)__B,
                                                (__v2di)__W, (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_mullo_epi64(__mmask8 __U, __m128i __A, __m128i __B) {
  return (__m128i)__builtin_ia32_pmullq128_mask(
      (__v2di)__A, (__v2di)__B, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_andnot_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_andnpd256_mask((__v4df)__A, (__v4df)__B,
                                                (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_andnot_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_andnpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_andnot_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_andnpd128_mask((__v2df)__A, (__v2df)__B,
                                                (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_andnot_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_andnpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_andnot_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_andnps256_mask((__v8sf)__A, (__v8sf)__B,
                                               (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_andnot_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_andnps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_andnot_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_andnps128_mask((__v4sf)__A, (__v4sf)__B,
                                               (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_andnot_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_andnps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_and_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_andpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_and_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_andpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_and_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_andpd128_mask((__v2df)__A, (__v2df)__B,
                                               (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_and_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_andpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_and_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_andps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_and_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_andps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_and_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_andps128_mask((__v4sf)__A, (__v4sf)__B,
                                              (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_and_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_andps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_xor_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_xorpd256_mask((__v4df)__A, (__v4df)__B,
                                               (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_xor_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_xorpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_xor_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_xorpd128_mask((__v2df)__A, (__v2df)__B,
                                               (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_xor_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_xorpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_xor_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_xorps256_mask((__v8sf)__A, (__v8sf)__B,
                                              (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_xor_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_xorps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_xor_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_xorps128_mask((__v4sf)__A, (__v4sf)__B,
                                              (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_xor_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_xorps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_or_pd(__m256d __W, __mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_orpd256_mask((__v4df)__A, (__v4df)__B,
                                              (__v4df)__W, (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_or_pd(__mmask8 __U, __m256d __A, __m256d __B) {
  return (__m256d)__builtin_ia32_orpd256_mask(
      (__v4df)__A, (__v4df)__B, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_or_pd(__m128d __W, __mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_orpd128_mask((__v2df)__A, (__v2df)__B,
                                              (__v2df)__W, (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_or_pd(__mmask8 __U, __m128d __A, __m128d __B) {
  return (__m128d)__builtin_ia32_orpd128_mask(
      (__v2df)__A, (__v2df)__B, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_or_ps(__m256 __W, __mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_orps256_mask((__v8sf)__A, (__v8sf)__B,
                                             (__v8sf)__W, (__mmask8)__U);
}

static __inline__ __m256 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_or_ps(__mmask8 __U, __m256 __A, __m256 __B) {
  return (__m256)__builtin_ia32_orps256_mask(
      (__v8sf)__A, (__v8sf)__B, (__v8sf)_mm256_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_or_ps(__m128 __W, __mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_orps128_mask((__v4sf)__A, (__v4sf)__B,
                                             (__v4sf)__W, (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_or_ps(__mmask8 __U, __m128 __A, __m128 __B) {
  return (__m128)__builtin_ia32_orps128_mask(
      (__v4sf)__A, (__v4sf)__B, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtpd_epi64(__m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2qq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtpd_epi64(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2qq128_mask((__v2df)__A, (__v2di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtpd_epi64(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2qq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtpd_epi64(__m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2qq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtpd_epi64(__m256i __W, __mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2qq256_mask((__v4df)__A, (__v4di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtpd_epi64(__mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2qq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtpd_epu64(__m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2uqq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtpd_epu64(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2uqq128_mask((__v2df)__A, (__v2di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtpd_epu64(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvtpd2uqq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtpd_epu64(__m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2uqq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtpd_epu64(__m256i __W, __mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2uqq256_mask((__v4df)__A, (__v4di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtpd_epu64(__mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvtpd2uqq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtps_epi64(__m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2qq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtps_epi64(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2qq128_mask((__v4sf)__A, (__v2di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtps_epi64(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2qq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtps_epi64(__m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2qq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtps_epi64(__m256i __W, __mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2qq256_mask((__v4sf)__A, (__v4di)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtps_epi64(__mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2qq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtps_epu64(__m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2uqq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtps_epu64(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2uqq128_mask((__v4sf)__A, (__v2di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtps_epu64(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvtps2uqq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtps_epu64(__m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2uqq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtps_epu64(__m256i __W, __mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2uqq256_mask((__v4sf)__A, (__v4di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtps_epu64(__mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvtps2uqq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtepi64_pd(__m128i __A) {
  return (__m128d)__builtin_ia32_cvtqq2pd128_mask(
      (__v2di)__A, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtepi64_pd(__m128d __W, __mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtqq2pd128_mask((__v2di)__A, (__v2df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtepi64_pd(__mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtqq2pd128_mask(
      (__v2di)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtepi64_pd(__m256i __A) {
  return (__m256d)__builtin_ia32_cvtqq2pd256_mask(
      (__v4di)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtepi64_pd(__m256d __W, __mmask8 __U, __m256i __A) {
  return (__m256d)__builtin_ia32_cvtqq2pd256_mask((__v4di)__A, (__v4df)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtepi64_pd(__mmask8 __U, __m256i __A) {
  return (__m256d)__builtin_ia32_cvtqq2pd256_mask(
      (__v4di)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_cvtepi64_ps(__m128i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps128_mask(
      (__v2di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_cvtepi64_ps(__m128 __W, __mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps128_mask((__v2di)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_cvtepi64_ps(__mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps128_mask(
      (__v2di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_cvtepi64_ps(__m256i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps256_mask(
      (__v4di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_cvtepi64_ps(__m128 __W, __mmask8 __U, __m256i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps256_mask((__v4di)__A, (__v4sf)__W,
                                                 (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtepi64_ps(__mmask8 __U, __m256i __A) {
  return (__m128)__builtin_ia32_cvtqq2ps256_mask(
      (__v4di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvttpd_epi64(__m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2qq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvttpd_epi64(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2qq128_mask((__v2df)__A, (__v2di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvttpd_epi64(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2qq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvttpd_epi64(__m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2qq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvttpd_epi64(__m256i __W, __mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2qq256_mask((__v4df)__A, (__v4di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvttpd_epi64(__mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2qq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvttpd_epu64(__m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2uqq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvttpd_epu64(__m128i __W, __mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2uqq128_mask((__v2df)__A, (__v2di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvttpd_epu64(__mmask8 __U, __m128d __A) {
  return (__m128i)__builtin_ia32_cvttpd2uqq128_mask(
      (__v2df)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvttpd_epu64(__m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2uqq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvttpd_epu64(__m256i __W, __mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2uqq256_mask((__v4df)__A, (__v4di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvttpd_epu64(__mmask8 __U, __m256d __A) {
  return (__m256i)__builtin_ia32_cvttpd2uqq256_mask(
      (__v4df)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvttps_epi64(__m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2qq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvttps_epi64(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2qq128_mask((__v4sf)__A, (__v2di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvttps_epi64(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2qq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvttps_epi64(__m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2qq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvttps_epi64(__m256i __W, __mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2qq256_mask((__v4sf)__A, (__v4di)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvttps_epi64(__mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2qq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvttps_epu64(__m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2uqq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)-1);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvttps_epu64(__m128i __W, __mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2uqq128_mask((__v4sf)__A, (__v2di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m128i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvttps_epu64(__mmask8 __U, __m128 __A) {
  return (__m128i)__builtin_ia32_cvttps2uqq128_mask(
      (__v4sf)__A, (__v2di)_mm_setzero_si128(), (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvttps_epu64(__m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2uqq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)-1);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvttps_epu64(__m256i __W, __mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2uqq256_mask((__v4sf)__A, (__v4di)__W,
                                                    (__mmask8)__U);
}

static __inline__ __m256i __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvttps_epu64(__mmask8 __U, __m128 __A) {
  return (__m256i)__builtin_ia32_cvttps2uqq256_mask(
      (__v4sf)__A, (__v4di)_mm256_setzero_si256(), (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_cvtepu64_pd(__m128i __A) {
  return (__m128d)__builtin_ia32_cvtuqq2pd128_mask(
      (__v2di)__A, (__v2df)_mm_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_mask_cvtepu64_pd(__m128d __W, __mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtuqq2pd128_mask((__v2di)__A, (__v2df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m128d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm_maskz_cvtepu64_pd(__mmask8 __U, __m128i __A) {
  return (__m128d)__builtin_ia32_cvtuqq2pd128_mask(
      (__v2di)__A, (__v2df)_mm_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_cvtepu64_pd(__m256i __A) {
  return (__m256d)__builtin_ia32_cvtuqq2pd256_mask(
      (__v4di)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)-1);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_mask_cvtepu64_pd(__m256d __W, __mmask8 __U, __m256i __A) {
  return (__m256d)__builtin_ia32_cvtuqq2pd256_mask((__v4di)__A, (__v4df)__W,
                                                   (__mmask8)__U);
}

static __inline__ __m256d __attribute__((__always_inline__, __nodebug__,
                                         __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtepu64_pd(__mmask8 __U, __m256i __A) {
  return (__m256d)__builtin_ia32_cvtuqq2pd256_mask(
      (__v4di)__A, (__v4df)_mm256_setzero_pd(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_cvtepu64_ps(__m128i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps128_mask(
      (__v2di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_mask_cvtepu64_ps(__m128 __W, __mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps128_mask((__v2di)__A, (__v4sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm_maskz_cvtepu64_ps(__mmask8 __U, __m128i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps128_mask(
      (__v2di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_cvtepu64_ps(__m256i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps256_mask(
      (__v4di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)-1);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_mask_cvtepu64_ps(__m128 __W, __mmask8 __U, __m256i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps256_mask((__v4di)__A, (__v4sf)__W,
                                                  (__mmask8)__U);
}

static __inline__ __m128 __attribute__((__always_inline__, __nodebug__,
                                        __target__("avx512vl,avx512dq")))
_mm256_maskz_cvtepu64_ps(__mmask8 __U, __m256i __A) {
  return (__m128)__builtin_ia32_cvtuqq2ps256_mask(
      (__v4di)__A, (__v4sf)_mm_setzero_ps(), (__mmask8)__U);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("pku")))
    _rdpkru_u32(void) {
  return __builtin_ia32_rdpkru();
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("pku")))
    _wrpkru(unsigned int val) {
  return __builtin_ia32_wrpkru(val);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("rdrnd")))
    _rdrand16_step(unsigned short *__p) {
  return __builtin_ia32_rdrand16_step(__p);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("rdrnd")))
    _rdrand32_step(unsigned int *__p) {
  return __builtin_ia32_rdrand32_step(__p);
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("rdrnd")))
    _rdrand64_step(unsigned long long *__p) {
  return __builtin_ia32_rdrand64_step(__p);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _readfsbase_u32(void) {
  return __builtin_ia32_rdfsbase32();
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _readfsbase_u64(void) {
  return __builtin_ia32_rdfsbase64();
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _readgsbase_u32(void) {
  return __builtin_ia32_rdgsbase32();
}

static __inline__ unsigned long long
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _readgsbase_u64(void) {
  return __builtin_ia32_rdgsbase64();
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _writefsbase_u32(unsigned int __V) {
  return __builtin_ia32_wrfsbase32(__V);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _writefsbase_u64(unsigned long long __V) {
  return __builtin_ia32_wrfsbase64(__V);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _writegsbase_u32(unsigned int __V) {
  return __builtin_ia32_wrgsbase32(__V);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fsgsbase")))
    _writegsbase_u64(unsigned long long __V) {
  return __builtin_ia32_wrgsbase64(__V);
}

static __inline__ unsigned int
    __attribute__((__always_inline__, __nodebug__, __target__("rtm")))
    _xbegin(void) {
  return __builtin_ia32_xbegin();
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("rtm")))
    _xend(void) {
  __builtin_ia32_xend();
}

static __inline__ int
    __attribute__((__always_inline__, __nodebug__, __target__("rtm")))
    _xtest(void) {
  return __builtin_ia32_xtest();
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha1nexte_epu32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_sha1nexte((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha1msg1_epu32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_sha1msg1((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha1msg2_epu32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_sha1msg2((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha256rnds2_epu32(__m128i __X, __m128i __Y, __m128i __Z) {
  return (__m128i)__builtin_ia32_sha256rnds2((__v4si)__X, (__v4si)__Y,
                                             (__v4si)__Z);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha256msg1_epu32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_sha256msg1((__v4si)__X, (__v4si)__Y);
}

static __inline__ __m128i
    __attribute__((__always_inline__, __nodebug__, __target__("sha")))
    _mm_sha256msg2_epu32(__m128i __X, __m128i __Y) {
  return (__m128i)__builtin_ia32_sha256msg2((__v4si)__X, (__v4si)__Y);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fxsr")))
    _fxsave(void *__p) {
  return __builtin_ia32_fxsave(__p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fxsr")))
    _fxsave64(void *__p) {
  return __builtin_ia32_fxsave64(__p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fxsr")))
    _fxrstor(void *__p) {
  return __builtin_ia32_fxrstor(__p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("fxsr")))
    _fxrstor64(void *__p) {
  return __builtin_ia32_fxrstor64(__p);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsave")))
    _xsave(void *__p, unsigned long long __m) {
  return __builtin_ia32_xsave(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsave")))
    _xrstor(void *__p, unsigned long long __m) {
  return __builtin_ia32_xrstor(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsave")))
    _xsave64(void *__p, unsigned long long __m) {
  return __builtin_ia32_xsave64(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsave")))
    _xrstor64(void *__p, unsigned long long __m) {
  return __builtin_ia32_xrstor64(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaveopt")))
    _xsaveopt(void *__p, unsigned long long __m) {
  return __builtin_ia32_xsaveopt(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaveopt")))
    _xsaveopt64(void *__p, unsigned long long __m) {
  return __builtin_ia32_xsaveopt64(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsavec")))
    _xsavec(void *__p, unsigned long long __m) {
  __builtin_ia32_xsavec(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsavec")))
    _xsavec64(void *__p, unsigned long long __m) {
  __builtin_ia32_xsavec64(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaves")))
    _xsaves(void *__p, unsigned long long __m) {
  __builtin_ia32_xsaves(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaves")))
    _xrstors(void *__p, unsigned long long __m) {
  __builtin_ia32_xrstors(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaves")))
    _xrstors64(void *__p, unsigned long long __m) {
  __builtin_ia32_xrstors64(__p, __m);
}

static __inline__ void
    __attribute__((__always_inline__, __nodebug__, __target__("xsaves")))
    _xsaves64(void *__p, unsigned long long __m) {
  __builtin_ia32_xsaves64(__p, __m);
}

static __inline unsigned char
    __attribute__((__always_inline__, __nodebug__, __target__("adx")))
    _addcarryx_u32(unsigned char __cf, unsigned int __x, unsigned int __y,
                   unsigned int *__p) {
  return __builtin_ia32_addcarryx_u32(__cf, __x, __y, __p);
}

static __inline unsigned char
    __attribute__((__always_inline__, __nodebug__, __target__("adx")))
    _addcarryx_u64(unsigned char __cf, unsigned long long __x,
                   unsigned long long __y, unsigned long long *__p) {
  return __builtin_ia32_addcarryx_u64(__cf, __x, __y, __p);
}

static __inline unsigned char __attribute__((__always_inline__, __nodebug__))
_addcarry_u32(unsigned char __cf, unsigned int __x, unsigned int __y,
              unsigned int *__p) {
  return __builtin_ia32_addcarry_u32(__cf, __x, __y, __p);
}

static __inline unsigned char __attribute__((__always_inline__, __nodebug__))
_addcarry_u64(unsigned char __cf, unsigned long long __x,
              unsigned long long __y, unsigned long long *__p) {
  return __builtin_ia32_addcarry_u64(__cf, __x, __y, __p);
}

static __inline unsigned char __attribute__((__always_inline__, __nodebug__))
_subborrow_u32(unsigned char __cf, unsigned int __x, unsigned int __y,
               unsigned int *__p) {
  return __builtin_ia32_subborrow_u32(__cf, __x, __y, __p);
}

static __inline unsigned char __attribute__((__always_inline__, __nodebug__))
_subborrow_u64(unsigned char __cf, unsigned long long __x,
               unsigned long long __y, unsigned long long *__p) {
  return __builtin_ia32_subborrow_u64(__cf, __x, __y, __p);
}

typedef unsigned char __u_char;
typedef unsigned short int __u_short;
typedef unsigned int __u_int;
typedef unsigned long int __u_long;

typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;

typedef signed long int __int64_t;
typedef unsigned long int __uint64_t;

typedef long int __quad_t;
typedef unsigned long int __u_quad_t;

typedef unsigned long int __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long int __ino_t;
typedef unsigned long int __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long int __nlink_t;
typedef long int __off_t;
typedef long int __off64_t;
typedef int __pid_t;
typedef struct { int __val[2]; } __fsid_t;
typedef long int __clock_t;
typedef unsigned long int __rlim_t;
typedef unsigned long int __rlim64_t;
typedef unsigned int __id_t;
typedef long int __time_t;
typedef unsigned int __useconds_t;
typedef long int __suseconds_t;

typedef int __daddr_t;
typedef int __key_t;

typedef int __clockid_t;

typedef void *__timer_t;

typedef long int __blksize_t;

typedef long int __blkcnt_t;
typedef long int __blkcnt64_t;

typedef unsigned long int __fsblkcnt_t;
typedef unsigned long int __fsblkcnt64_t;

typedef unsigned long int __fsfilcnt_t;
typedef unsigned long int __fsfilcnt64_t;

typedef long int __fsword_t;

typedef long int __ssize_t;

typedef long int __syscall_slong_t;

typedef unsigned long int __syscall_ulong_t;

typedef __off64_t __loff_t;
typedef __quad_t *__qaddr_t;
typedef char *__caddr_t;

typedef long int __intptr_t;

typedef unsigned int __socklen_t;

struct _IO_FILE;

typedef struct _IO_FILE FILE;
typedef struct _IO_FILE __FILE;

typedef struct {
  int __count;
  union {

    unsigned int __wch;

    char __wchb[4];
  } __value;
} __mbstate_t;
typedef struct {
  __off_t __pos;
  __mbstate_t __state;
} _G_fpos_t;
typedef struct {
  __off64_t __pos;
  __mbstate_t __state;
} _G_fpos64_t;
typedef __builtin_va_list va_list;
typedef __builtin_va_list __gnuc_va_list;
struct _IO_jump_t;
struct _IO_FILE;
typedef void _IO_lock_t;

struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;

  int _pos;
};

enum __codecvt_result {
  __codecvt_ok,
  __codecvt_partial,
  __codecvt_error,
  __codecvt_noconv
};
struct _IO_FILE {
  int _flags;

  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;

  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;

  struct _IO_marker *_markers;

  struct _IO_FILE *_chain;

  int _fileno;

  int _flags2;

  __off_t _old_offset;

  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];

  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;

  int _mode;

  char _unused2[15 * sizeof(int) - 4 * sizeof(void *) - sizeof(size_t)];
};

typedef struct _IO_FILE _IO_FILE;

struct _IO_FILE_plus;

extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
typedef __ssize_t __io_read_fn(void *__cookie, char *__buf, size_t __nbytes);

typedef __ssize_t __io_write_fn(void *__cookie, const char *__buf, size_t __n);

typedef int __io_seek_fn(void *__cookie, __off64_t *__pos, int __w);

typedef int __io_close_fn(void *__cookie);
extern int __underflow(_IO_FILE *);
extern int __uflow(_IO_FILE *);
extern int __overflow(_IO_FILE *, int);
extern int _IO_getc(_IO_FILE *__fp);
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern int _IO_feof(_IO_FILE *__fp) __attribute__((__nothrow__));
extern int _IO_ferror(_IO_FILE *__fp) __attribute__((__nothrow__));

extern int _IO_peekc_locked(_IO_FILE *__fp);

extern void _IO_flockfile(_IO_FILE *) __attribute__((__nothrow__));
extern void _IO_funlockfile(_IO_FILE *) __attribute__((__nothrow__));
extern int _IO_ftrylockfile(_IO_FILE *) __attribute__((__nothrow__));
extern int _IO_vfscanf(_IO_FILE *__restrict, const char *__restrict,
                       __gnuc_va_list, int *__restrict);
extern int _IO_vfprintf(_IO_FILE *__restrict, const char *__restrict,
                        __gnuc_va_list);
extern __ssize_t _IO_padn(_IO_FILE *, int, __ssize_t);
extern size_t _IO_sgetn(_IO_FILE *, void *, size_t);

extern __off64_t _IO_seekoff(_IO_FILE *, __off64_t, int, int);
extern __off64_t _IO_seekpos(_IO_FILE *, __off64_t, int);

extern void _IO_free_backup_area(_IO_FILE *) __attribute__((__nothrow__));
typedef _G_fpos_t fpos_t;

extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;

extern int remove(const char *__filename) __attribute__((__nothrow__));

extern int rename(const char *__old, const char *__new)
    __attribute__((__nothrow__));
extern FILE *tmpfile(void);
extern char *tmpnam(char *__s) __attribute__((__nothrow__));
extern int fclose(FILE *__stream);

extern int fflush(FILE *__stream);
extern FILE *fopen(const char *__restrict __filename,
                   const char *__restrict __modes);

extern FILE *freopen(const char *__restrict __filename,
                     const char *__restrict __modes, FILE *__restrict __stream);
extern void setbuf(FILE *__restrict __stream, char *__restrict __buf)
    __attribute__((__nothrow__));

extern int setvbuf(FILE *__restrict __stream, char *__restrict __buf,
                   int __modes, size_t __n) __attribute__((__nothrow__));
extern int fprintf(FILE *__restrict __stream, const char *__restrict __format,
                   ...);

extern int printf(const char *__restrict __format, ...);

extern int sprintf(char *__restrict __s, const char *__restrict __format, ...)
    __attribute__((__nothrow__));

extern int vfprintf(FILE *__restrict __s, const char *__restrict __format,
                    __gnuc_va_list __arg);

extern int vprintf(const char *__restrict __format, __gnuc_va_list __arg);

extern int vsprintf(char *__restrict __s, const char *__restrict __format,
                    __gnuc_va_list __arg) __attribute__((__nothrow__));

extern int snprintf(char *__restrict __s, size_t __maxlen,
                    const char *__restrict __format, ...)
    __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 4)));

extern int vsnprintf(char *__restrict __s, size_t __maxlen,
                     const char *__restrict __format, __gnuc_va_list __arg)
    __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 0)));
extern int fscanf(FILE *__restrict __stream, const char *__restrict __format,
                  ...);

extern int scanf(const char *__restrict __format, ...);

extern int sscanf(const char *__restrict __s, const char *__restrict __format,
                  ...) __attribute__((__nothrow__));
extern int fscanf(FILE *__restrict __stream, const char *__restrict __format,
                  ...) __asm__(""
                               "__isoc99_fscanf");

extern int scanf(const char *__restrict __format,
                 ...) __asm__(""
                              "__isoc99_scanf");

extern int sscanf(const char *__restrict __s, const char *__restrict __format,
                  ...) __asm__(""
                               "__isoc99_sscanf") __attribute__((__nothrow__));
extern int vfscanf(FILE *__restrict __s, const char *__restrict __format,
                   __gnuc_va_list __arg)
    __attribute__((__format__(__scanf__, 2, 0)));

extern int vscanf(const char *__restrict __format, __gnuc_va_list __arg)
    __attribute__((__format__(__scanf__, 1, 0)));

extern int vsscanf(const char *__restrict __s, const char *__restrict __format,
                   __gnuc_va_list __arg) __attribute__((__nothrow__))
__attribute__((__format__(__scanf__, 2, 0)));
extern int vfscanf(FILE *__restrict __s, const char *__restrict __format,
                   __gnuc_va_list __arg) __asm__(""
                                                 "__isoc99_vfscanf")

    __attribute__((__format__(__scanf__, 2, 0)));
extern int vscanf(const char *__restrict __format,
                  __gnuc_va_list __arg) __asm__(""
                                                "__isoc99_vscanf")

    __attribute__((__format__(__scanf__, 1, 0)));
extern int vsscanf(const char *__restrict __s, const char *__restrict __format,
                   __gnuc_va_list __arg) __asm__(""
                                                 "__isoc99_vsscanf")
    __attribute__((__nothrow__))

    __attribute__((__format__(__scanf__, 2, 0)));
extern int fgetc(FILE *__stream);
extern int getc(FILE *__stream);

extern int getchar(void);
extern int fputc(int __c, FILE *__stream);
extern int putc(int __c, FILE *__stream);

extern int putchar(int __c);
extern char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream);
extern int fputs(const char *__restrict __s, FILE *__restrict __stream);

extern int puts(const char *__s);

extern int ungetc(int __c, FILE *__stream);

extern size_t fread(void *__restrict __ptr, size_t __size, size_t __n,
                    FILE *__restrict __stream);

extern size_t fwrite(const void *__restrict __ptr, size_t __size, size_t __n,
                     FILE *__restrict __s);
extern int fseek(FILE *__stream, long int __off, int __whence);

extern long int ftell(FILE *__stream);

extern void rewind(FILE *__stream);
extern int fgetpos(FILE *__restrict __stream, fpos_t *__restrict __pos);

extern int fsetpos(FILE *__stream, const fpos_t *__pos);
extern void clearerr(FILE *__stream) __attribute__((__nothrow__));

extern int feof(FILE *__stream) __attribute__((__nothrow__));

extern int ferror(FILE *__stream) __attribute__((__nothrow__));
extern void perror(const char *__s);

typedef union {
  __m128i x;
  char a[16];
} union128i_b;

typedef union {
  __m128i x;
  unsigned char a[16];
} union128i_ub;

typedef union {
  __m128i x;
  short a[8];
} union128i_w;

typedef union {
  __m128i x;
  unsigned short a[8];
} union128i_uw;

typedef union {
  __m128i x;
  int a[4];
} union128i_d;

typedef union {
  __m128i x;
  unsigned int a[4];
} union128i_ud;

typedef union {
  __m128i x;
  long long a[2];
} union128i_q;

typedef union {
  __m128i x;
  unsigned long long a[2];
} union128i_uq;

typedef union {
  __m128d x;
  double a[2];
} union128d;

typedef union {
  __m128 x;
  float a[4];
} union128;
static int __attribute__((noinline, unused))
check_union128i_b(union128i_b u, const char *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_ub(union128i_ub u, const unsigned char *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_w(union128i_w u, const short *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_uw(union128i_uw u, const unsigned short *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_d(union128i_d u, const int *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_ud(union128i_ud u, const unsigned int *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_q(union128i_q u, const long long *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128i_uq(union128i_uq u, const unsigned long long *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union128d(union128d u, const double *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}

static int __attribute__((noinline, unused))
check_union128(union128 u, const float *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVc(const char *v, const char *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVs(const short *v, const short *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVi(const int *v, const int *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVl(const long long *v, const long long *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVuc(const unsigned char *v, const unsigned char *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVus(const unsigned short *v, const unsigned short *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVui(const unsigned int *v, const unsigned int *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVul(const unsigned long long *v, const unsigned long long *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] != e[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
checkVd(const double *v, const double *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] > (e[i] + (0.000001)) || v[i] < (e[i] - (0.000001)))
      if (e[i] != v[i]) {
        err++;
        ;
      }
  return err;
}
static int __attribute__((noinline, unused))
checkVf(const float *v, const float *e, int n) {
  int i;
  int err = 0;
  for (i = 0; i < n; i++)
    if (v[i] > (e[i] + (0.000001)) || v[i] < (e[i] - (0.000001)))
      if (e[i] != v[i]) {
        err++;
        ;
      }
  return err;
}
static int __attribute__((noinline, unused))
check_fp_union128(union128 u, const float *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] > (v[i] + (0.000001)) || u.a[i] < (v[i] - (0.000001))) {
      err++;
      ;
    }
  return err;
}

static int __attribute__((noinline, unused))
check_fp_union128d(union128d u, const double *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] > (v[i] + (0.000001)) || u.a[i] < (v[i] - (0.000001))) {
      err++;
      ;
    }
  return err;
}

typedef union {
  __m256i x;
  char a[32];
} union256i_b;

typedef union {
  __m256i x;
  short a[16];
} union256i_w;

typedef union {
  __m256i x;
  int a[8];
} union256i_d;

typedef union {
  __m256i x;
  long long a[4];
} union256i_q;

typedef union {
  __m256 x;
  float a[8];
} union256;

typedef union {
  __m256d x;
  double a[4];
} union256d;

typedef union {
  __m256i x;
  unsigned char a[32];
} union256i_ub;

typedef union {
  __m256i x;
  unsigned short a[16];
} union256i_uw;

typedef union {
  __m256i x;
  unsigned int a[8];
} union256i_ud;

typedef union {
  __m256i x;
  unsigned long long a[4];
} union256i_uq;

static int __attribute__((noinline, unused))
check_union256i_b(union256i_b u, const char *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_w(union256i_w u, const short *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_d(union256i_d u, const int *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_q(union256i_q u, const long long *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256(union256 u, const float *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256d(union256d u, const double *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_ub(union256i_ub u, const unsigned char *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_uw(union256i_uw u, const unsigned short *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_ud(union256i_ud u, const unsigned int *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_union256i_uq(union256i_uq u, const unsigned long long *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] != v[i]) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_fp_union256(union256 u, const float *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] > (v[i] + (0.000001)) || u.a[i] < (v[i] - (0.000001))) {
      err++;
      ;
    }
  return err;
}
static int __attribute__((noinline, unused))
check_fp_union256d(union256d u, const double *v) {
  int i;
  int err = 0;
  for (i = 0; i < (sizeof(u.a) / sizeof((u.a)[0])); i++)
    if (u.a[i] > (v[i] + (0.000001)) || u.a[i] < (v[i] - (0.000001))) {
      err++;
      ;
    }
  return err;
}

static int avx_os_support(void) {
  unsigned int eax, edx;
  unsigned int ecx = 0x0;

  __asm__("xgetbv" : "=a"(eax), "=d"(edx) : "c"(ecx));

  return (eax & (0x2 | 0x4)) == (0x2 | 0x4);
}

static void avx_test(void);

static void __attribute__((noinline)) do_test(void) { avx_test(); }

int main() {
  unsigned int eax, ebx, ecx, edx;

  if (!__get_cpuid(1, &eax, &ebx, &ecx, &edx))
    return 0;

  if (((ecx & (0x10000000 | 0x08000000)) == (0x10000000 | 0x08000000)) &&
      avx_os_support()) {
    do_test();
  }

  return 0;
}

unsigned int a[1024] __attribute__((aligned(32)));

__attribute__((noinline, noclone)) void foo(void) {
  int i, j = 3;
  for (i = 0; i < 1024; i++)
    a[i] = (a[i] << j) | (a[i] >> ((-j) & 31));
}

static void __attribute__((noinline)) avx_test(void) {
  int i;
  for (i = 0; i < 1024; i++)
    a[i] = i * 1073741789U;
  foo();
  for (i = 0; i < 1024; i++) {
    unsigned int x = i * 1073741789U;
    if (a[i] != ((x << 3) | (x >> ((-3) & 31))))
      abort();
  }
}
