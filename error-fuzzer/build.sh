#!/usr/bin/env bash

set -o pipefail
set -o nounset

############################################################
# build the script 
############################################################
echo "building the project"
ant -f error-fuzzer.xml > /dev/null

readonly ROOT=$(pwd)

############################################################
# compute jar class path
############################################################
readonly JAR_FOLDER="${ROOT}/out/artifacts/error_fuzzer_jar/"
JAR_CLASSPATH=""
for jar in $(find "${JAR_FOLDER}" -name "*.jar") ; do
  JAR_CLASSPATH="${jar}:${JAR_CLASSPATH}"
done


############################################################
# export run script: run error fuzzer 
############################################################
readonly RUN_SCRIPT=${ROOT}/run-error-fuzzer.sh
readonly MAIN_CLASS="edu.ucdavis.error.fuzzer.core.Main"
echo "#!/usr/bin/env bash" > $RUN_SCRIPT
echo "java -cp ${JAR_CLASSPATH} ${MAIN_CLASS} \$@" >> $RUN_SCRIPT
echo "export script ${RUN_SCRIPT}"
chmod +x $RUN_SCRIPT

############################################################
# export run script: default run error fuzzer 
############################################################
readonly DEFAULT_RUN_SCRIPT=${ROOT}/default-run-error-fuzzer.sh
echo "#!/usr/bin/env bash" > ${DEFAULT_RUN_SCRIPT}
cmd="${RUN_SCRIPT} "
cmd="${cmd} --seed-folder ${ROOT}/gcc-seed-programs "
cmd="${cmd} --num-threads 1 "
cmd="${cmd} --engine crazy "
cmd="${cmd} -max-instances-per-error-msg 1000 "
echo "${cmd}" >> ${DEFAULT_RUN_SCRIPT}
chmod +x ${DEFAULT_RUN_SCRIPT}
echo "export script ${DEFAULT_RUN_SCRIPT}"


############################################################
# export run script: generate report 
############################################################
readonly GEN_REPORT=${ROOT}/gen-report.sh
echo "#!/usr/bin/env bash" > ${GEN_REPORT}
echo "java -cp ${JAR_CLASSPATH} edu.ucdavis.error.analyzer.TextualErrorReportGenerator \$@" >> $GEN_REPORT
chmod +x ${GEN_REPORT}
echo "export script ${GEN_REPORT}"


############################################################
# export run script: reduce all instances
############################################################
readonly REDUCER=${ROOT}/reduce-all-instances.sh
echo "#!/usr/bin/env bash" > ${REDUCER}
echo "java -cp ${JAR_CLASSPATH} edu.ucdavis.error.fuzzer.reducer.ReductionMain \$@" >> $REDUCER
chmod +x ${REDUCER}
echo "export script ${REDUCER}"


############################################################
# export run script: reduce all instances
############################################################
readonly FORMATTER=${ROOT}/format-all-instances.sh
echo "#!/usr/bin/env bash" > ${FORMATTER}
echo "java -cp ${JAR_CLASSPATH} edu.ucdavis.error.fuzzer.reducer.ReductionFormatterMain \$@" >> $FORMATTER
chmod +x ${FORMATTER}
echo "export script ${FORMATTER}"



