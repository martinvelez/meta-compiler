package edu.ucdavis.error.fuzzer.reducer;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.token.segment.EmptyProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.NonTokenProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/** Created by Chengnian Sun on 4/28/17. */
@RunWith(JUnit4.class)
public class NonTokenPreservedTokenizerTest {

  private static void assertTokenizerCanReconstructSourceFile(File sourceFile)
      throws ExpansionOverlapsBoundaryException, IOException, ParsingFailureException {
    final ImmutableList<IProgramSegment> tokens = NonTokenPreservedTokenizer.tokenize(sourceFile);
    for (int i = 0, size = tokens.size(); i < size; ++i) {
      IProgramSegment segment = tokens.get(i);
      assertThat(segment).isNotInstanceOf(EmptyProgramSegment.class);
      if (segment instanceof TokenProgramSegment) {
        continue;
      }
      assertThat(segment).isInstanceOf(NonTokenProgramSegment.class);
      if (i - 1 >= 0) {
        assertThat(tokens.get(i - 1)).isInstanceOf(TokenProgramSegment.class);
      }
      if (i + 1 < size) {
        assertThat(tokens.get(i + 1)).isInstanceOf(TokenProgramSegment.class);
      }
    }

    final StringBuilder builder = new StringBuilder();
    tokens.forEach(token -> builder.append(token.getLexeme()));
    final String sourceText = Files.asCharSource(sourceFile, Charsets.UTF_8).read();
    assertThat(builder.toString()).isEqualTo(sourceText);
  }

  @Test
  public void testTokenizedProgramCanReassembleOriginalSourceProgram()
      throws ExpansionOverlapsBoundaryException, IOException, ParsingFailureException {
    final List<File> files = new ArrayList<>();
    Arrays.stream(new File("test-data/tiny-seed-repo").listFiles())
        .filter(SourceFile::isSourceFile)
        .forEach(files::add);
    Arrays.stream(new File("test-data/tokenized-program/original-programs").listFiles())
        .filter(SourceFile::isSourceFile)
        .forEach(files::add);
    files.add(
        new File(
            "test-data/error-instances/MSG_no_type_named_.__in_._/000/reduction/good_reduced_formatted.C"));
    files.add(new File("test-data/error-instances/MSG_no_type_named_.__in_._/000/good.C"));
    files.add(new File("test-data/error-instances/MSG_array_type_.__is_not_assignable/000/good.c"));
    files.add(
        new File(
            "test-data/error-instances/MSG_second_parameter_of__main____argument_array___must_be_of_type_._/001/good.c"));

    files.remove(new File("test-data/tiny-seed-repo/rotate-5a.c"));
    for (File file : files) {
      assertTokenizerCanReconstructSourceFile(file);
    }
  }
}
