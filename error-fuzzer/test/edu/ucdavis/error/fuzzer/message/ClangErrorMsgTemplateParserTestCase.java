package edu.ucdavis.error.fuzzer.message;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class ClangErrorMsgTemplateParserTestCase {

  @Test
  public void testPreEscape() {
    final String s = "cannot find start of regex ('{{') in %0";
    System.out.println(s.replaceAll("\\{", Matcher.quoteReplacement("\\{")));
  }

  @Test
  public void test2() {
    final String s = "%select{" + "%diff{assigning to $ from incompatible type $|assigning to " +
            "type from incompatible type}0,1" + "|%diff{passing $ to parameter of incompatible "
            + "type $|passing type to parameter of incompatible type}0,1" + "|%diff{returning $ "
            + "from a function with incompatible result type $|returning type from a function " +
            "with " + "incompatible result type}0,1" + "|%diff{converting $ to incompatible type " +
            "" + "" + "" + "$|converting type to incompatible type}0,1" + "|%diff{initializing $ " +
            "with " + "an " + "expression of incompatible type $|initializing type with an " +
            "expression of " + "incompatible type}0,1" + "|%diff{sending $ to parameter of " +
            "incompatible type " + "$|sending type to parameter of incompatible type}0,1" +
            "|%diff{casting $ to " + "incompatible type $|casting type to incompatible type}0,1}2";
    List<Pattern> list = new ClangErrorMsgTemplateParser(s).getPatterns();
    for (Pattern p : list) {
      System.out.println(p.pattern());
    }
    assertEquals(14, list.size());
    assertEquals("assigning to .+ from incompatible type .+", list.get(0).pattern());
    assertEquals("assigning to type from incompatible type", list.get(1).pattern());
    assertEquals("casting .+ to incompatible type .+", list.get(2).pattern());
    assertEquals("casting type to incompatible type", list.get(3).pattern());
    assertEquals("converting .+ to incompatible type .+", list.get(4).pattern());
    assertEquals("converting type to incompatible type", list.get(5).pattern());
    assertEquals("initializing .+ with an expression of incompatible type .+", list.get(6)
            .pattern());
    assertEquals("initializing type with an expression of incompatible type", list.get(7).pattern
            ());
    assertEquals("passing .+ to parameter of incompatible type .+", list.get(8).pattern());
    assertEquals("passing type to parameter of incompatible type", list.get(9).pattern());
    assertEquals("returning .+ from a function with incompatible result type .+", list.get(10)
            .pattern());
    assertEquals("returning type from a function with incompatible result type", list.get(11)
            .pattern());
    assertEquals("sending .+ to parameter of incompatible type .+", list.get(12).pattern());
    assertEquals("sending type to parameter of incompatible type", list.get(13).pattern());
  }

  @Test
  public void test1() {
    final String s = "invalid block pointer conversion " + "%select{" + "%diff{assigning to $ " +
            "from $|assigning to different types}0,1" + "|%diff{passing $ to parameter of type "
            + "$|passing to parameter of different type}0,1" + "|%diff{returning $ from a " +
            "function " + "with result type $|returning from function with different return " +
            "type}0,1" + "|%diff{converting $ to type $|converting between types}0,1" +
            "|%diff{initializing $" + " with an expression of type $|initializing with " +
            "expression" + " of different type}0,1" + "|%diff{sending $ to parameter of type " +
            "$|sending to " + "parameter of different type}0,1" + "|%diff{casting $ to type " +
            "$|casting between " + "types}0,1" + "}2";
    List<Pattern> list = new ClangErrorMsgTemplateParser(s).getPatterns();
    assertEquals(14, list.size());

    assertEquals("invalid block pointer conversion assigning to .+ from .+", list.get(0).pattern());
    assertEquals("invalid block pointer conversion assigning to different types", list.get(1)
            .pattern());
    assertEquals("invalid block pointer conversion casting .+ to type .+", list.get(2).pattern());
    assertEquals("invalid block pointer conversion casting between types", list.get(3).pattern());
    assertEquals("invalid block pointer conversion converting .+ to type .+", list.get(4).pattern
            ());
    assertEquals("invalid block pointer conversion converting between types", list.get(5).pattern
            ());
    assertEquals("invalid block pointer conversion initializing .+ with an expression of type " +
            ".+", list.get(6).pattern());
    assertEquals("invalid block pointer conversion initializing with expression of different " +
            "type", list.get(7).pattern());
    assertEquals("invalid block pointer conversion passing .+ to parameter of type .+", list.get
            (8).pattern());
    assertEquals("invalid block pointer conversion passing to parameter of different type", list
            .get(9).pattern());
    assertEquals("invalid block pointer conversion returning .+ from a function with result type " +
            "" + "" + "" + ".+", list.get(10).pattern());
    assertEquals("invalid block pointer conversion returning from function with different return " +
            "" + "" + "" + "type", list.get(11).pattern());
    assertEquals("invalid block pointer conversion sending .+ to parameter of type .+", list.get
            (12).pattern());
    assertEquals("invalid block pointer conversion sending to parameter of different type", list
            .get(13).pattern());
  }

  @Test
  public void testSelect() {
    final String s = "type trait requires %0%select{| or more}1 argument%select{|s}2; have %3 " +
            "argument%s3";
    List<Pattern> list = new ClangErrorMsgTemplateParser(s).getPatterns();
    assertEquals(4, list.size());
    assertEquals("type trait requires .+ argument; have .+ argument.?", list.get(0).pattern());
    assertEquals("type trait requires .+ arguments; have .+ argument.?", list.get(1).pattern());
    assertEquals("type trait requires .+ or more argument; have .+ argument.?", list.get(2)
            .pattern());
    assertEquals("type trait requires .+ or more arguments; have .+ argument.?", list.get(3)
            .pattern());
  }

  @Test
  public void testS() {
    final String s = "requires %1 parameter%s1";
    List<String> list = ClangErrorMsgTemplateParser.expandS(Arrays.asList(s));
    assertEquals(1, list.size());
    assertEquals("requires %1 parameter.?", list.get(0));
  }

  @Test
  public void testQ() {
    final String s = "candidate found by name lookup is %q0";
    List<String> list = ClangErrorMsgTemplateParser.expandQ(Arrays.asList(s));
    assertEquals(1, list.size());
    assertEquals("candidate found by name lookup is .+", list.get(0));
  }

  @Test
  public void testObjcclass() {
    final String s = "method %objcclass0 not found";
    List<String> list = ClangErrorMsgTemplateParser.expandObjcclass(Arrays.asList(s));
    assertEquals(1, list.size());
    assertEquals("method .+ not found", list.get(0));
  }

  @Test
  public void testObjcinstance() {
    final String s = "method %objcinstance0 not found";
    List<String> list = ClangErrorMsgTemplateParser.expandObjcinstance(Arrays.asList(s));
    assertEquals(1, list.size());
    assertEquals("method .+ not found", list.get(0));
  }

  @Test
  public void testOrdinal1() {
    final String s = "ambiguity in %ordinal0 argument";
    List<String> list = ClangErrorMsgTemplateParser.expandOrdinal(Arrays.asList(s));
    assertEquals(1, list.size());
    assertEquals("ambiguity in .+ argument", list.get(0));
  }

  @Test
  public void testDiff1() {
    final String s = "no known conversion %diff{from $ to $|from argument type to parameter " +
            "type}1,2";
    List<String> list = ClangErrorMsgTemplateParser.expandDiff(Arrays.asList(s));
    assertEquals(2, list.size());
    assertEquals("no known conversion from .+ to .+", list.get(0));
    assertEquals("no known conversion from argument type to parameter type", list.get(1));
  }

  @Test
  public void testPlural1() {
    final String s = "%0 attribute parameter %1 is out of bounds: " + "%plural{0:no parameters "
            + "to" + " index into" + "|1:can only be 1, since there is one parameter" + "|:must "
            + "be " + "between" + " 1 and %2}2";
    List<String> list = ClangErrorMsgTemplateParser.expandPlural(Arrays.asList(s));
    assertEquals(3, list.size());
    assertEquals("%0 attribute parameter %1 is out of bounds: no parameters to index into", list
            .get(0));
    assertEquals("%0 attribute parameter %1 is out of bounds: can only be 1, since there is one "
            + "parameter", list.get(1));
    assertEquals("%0 attribute parameter %1 is out of bounds: must be between 1 and %2", list.get
            (2));
  }

  @Test
  public void testSelect4() {
    final String s = "previous command '%select{\\|@}0%1' here";
    List<String> list = ClangErrorMsgTemplateParser.expandSelect(Arrays.asList(s));
    assertEquals(2, list.size());
  }

  @Test
  public void testSelect1() {
    final String s = "array size expression must have integral or %select{|unscoped " +
            "}0enumeration" + " type, not %1";

    List<String> list = ClangErrorMsgTemplateParser.expandSelect(Arrays.asList(s));
    assertEquals(2, list.size());
    assertEquals("array size expression must have integral or enumeration type, not %1", list.get
            (0));
    assertEquals("array size expression must have integral or unscoped enumeration type, not %1",
            list.get(1));
  }

  @Test
  public void testSelect2() {
    final String s = "%select{overloaded function|redeclaration of}0 %1 must have the " +
            "'overloadable' attribute";

    List<String> list = ClangErrorMsgTemplateParser.expandSelect(Arrays.asList(s));
    assertEquals(2, list.size());
    assertEquals("overloaded function %1 must have the 'overloadable' attribute", list.get(0));
    assertEquals("redeclaration of %1 must have the 'overloadable' attribute", list.get(1));
  }

  @Test
  public void testSelect3() {
    final String s = "%select{'auto'|'decltype(auto)'|'__auto_type'}0 not allowed " +
            "%select{in" + " function prototype|in non-static struct member|in struct member" +
            "|in non-static " + "union member|in union member|in non-static class member" + "|in " +
            "" + "" + "interface member|in " + "exception declaration|in template parameter" +
            "|in " + "block " + "literal|in template " + "argument|in typedef|in type alias" +
            "|in " + "function return " + "type|in conversion " + "function type|here" + "|in " +
            "lambda " + "parameter|in type " + "allocated by 'new'|in K&R-style " + "function " +
            "parameter}1";

    List<String> list = ClangErrorMsgTemplateParser.expandSelect(Arrays.asList(s));
    assertEquals(3 * 19, list.size());

    assertEquals("'auto' not allowed in function prototype", list.get(0));
    assertEquals("'auto' not allowed in non-static struct member", list.get(1));
    assertEquals("'auto' not allowed in struct member", list.get(2));
    assertEquals("'auto' not allowed in non-static union member", list.get(3));
    assertEquals("'auto' not allowed in union member", list.get(4));
    assertEquals("'auto' not allowed in non-static class member", list.get(5));
    assertEquals("'auto' not allowed in interface member", list.get(6));
    assertEquals("'auto' not allowed in exception declaration", list.get(7));
    assertEquals("'auto' not allowed in template parameter", list.get(8));
    assertEquals("'auto' not allowed in block literal", list.get(9));
    assertEquals("'auto' not allowed in template argument", list.get(10));
    assertEquals("'auto' not allowed in typedef", list.get(11));
    assertEquals("'auto' not allowed in type alias", list.get(12));
    assertEquals("'auto' not allowed in function return type", list.get(13));
    assertEquals("'auto' not allowed in conversion function type", list.get(14));
    assertEquals("'auto' not allowed here", list.get(15));
    assertEquals("'auto' not allowed in lambda parameter", list.get(16));
    assertEquals("'auto' not allowed in type allocated by 'new'", list.get(17));
    assertEquals("'auto' not allowed in K&R-style function parameter", list.get(18));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
    // Assert.assertEquals("", list.get(0));
  }

}
