package edu.ucdavis.error.fuzzer.token;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

/** Created by Chengnian Sun on 3/28/17. */
@RunWith(JUnit4.class)
public class TokenizedProgramTestCase {

  private static void testTokenizer(File sourceFile) {
    try {
      final File tokenFile = new File("temp/tokenizer/tokens", sourceFile.toString());
      TokenizedProgram program =
          TokenizedProgram.tokenizeSourceProgram(sourceFile).writeTokensToFile(tokenFile);

      final File newSourceFile = new File("temp/tokenizer/program", sourceFile.toString());
      TokenizedProgram program2 =
          TokenizedProgram.readTokenizedProgram(tokenFile).outputSourceProgram(newSourceFile);

      TokenizedProgram program3 = TokenizedProgram.tokenizeSourceProgram(newSourceFile);
      Assert.assertTrue(program.equals(program2));
      Assert.assertTrue(program.equals(program3));
    } catch (Throwable e) {
      throw new AssertionError(e);
    }
  }

  @Test
  public void testReadTokenizedProgramFromFile() {
    final String folder = "test-data/tokenized-program/original-programs/";
    ImmutableList.of(
            "assumed_rank_8_c.c",
            "bind_c_dts_2_driver.c",
            "bind_c_usage_17_c.c",
            "c_f_pointer_logical_driver.c",
            "c_loc_tests_2_funcs.c",
            "explicit-spec6.C",
            "partial-spec.C",
            "pr56417.c",
            "pr63845.c",
            "pr64170.c")
        .stream()
        .map(s -> new File(folder, s))
        .forEach(TokenizedProgramTestCase::testTokenizer);
  }

  @Test
  public void test() throws ExpansionOverlapsBoundaryException, ParsingFailureException {
    TokenizedProgram p =
        TokenizedProgram.tokenizeSourceProgram(
            new File("test-data/tokenized-program/original-programs/t1.cc"));
    p.getTokens().forEach(t -> System.out.println(t.getLexeme()));
  }
}
