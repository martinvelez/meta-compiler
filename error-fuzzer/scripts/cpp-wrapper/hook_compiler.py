#!/usr/bin/env python2.7
#
# a wrapper for the clang compiler. As it compiles source code, it also 
# generates its LLVM IR representation (both .bc files and .ll files)
#
import sys
import re
import os
import subprocess

bitcode_farm = "/home/neo/bitcode_farm/"
compiler = "gcc"

opt_levels = ["0", "1", "s", "2", "3"]
opt_flags = ["-O" + x for x in opt_levels]

def strip_opt_flag(flags) :
  for opt in opt_flags:
    if (opt in flags):
      index = flags.index(opt)
      flags.pop(index)


def is_source_file(s):
  return s.endswith(".c") or x.endswith(".cc") or x.endswith(".cpp") \
           or x.endswith(".C")
def is_object_file(s):
  return s.endswith(".o")


def extract_output_names(flags):
  # given the flags, extract a list containing the output file paths 
  #   (1) if '-o' is given, then 
  #   (2) if '-o' is NOT given, then compute the output file paths
  if ('-o' in flags):
    index = flags.index('-o')
    obj_file = flags[index + 1]
    if (not is_object_file(obj_file)):
      return []
    flags[index + 1] = obj_file + ".bc"
    return [flags[index + 1]]
  else:
    source_files = [os.path.basename(x) for x in flags \
      if is_source_file(x)] 
    output_files = map(lambda x: x[0:x.rindex('.')] + ".bc", source_files)     
    return output_files




#def extract_output_name(orig_output_name):
#  abs_path = os.path.abspath(orig_output_name)
#  file_name = os.path.basename(abs_path)
#
#  return os.path.basename(abs_path)


def call_clang(cmd):
  cmd = cmd[:]
  print " \033[1;31m :: invoking clang with -emit-llvm\033[0m"#, " ".join(llvm_args)
  #subprocess.call(cmd)
  while True: 
    #print "cmd=", cmd
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ret = proc.returncode
    if (ret == 0):
      return
    output, err = proc.communicate()
    #print "stderr: ", err
    match = re.match("clang: error: unknown argument: '(.*)'", err)
    if match:
      unknown_flag = match.group(1)
      #print "unknown flag=", unknown_flag
      cmd.remove(unknown_flag)
      #print "new flags:", cmd
    else:
      return

    




def run_wo_opt(compiler):
  orig_args = sys.argv[:]
  llvm_args = orig_args[1:]
  ret = subprocess.call([compiler] + llvm_args)

  if ret != 0: 
    return ret

  if (not ('-c' in llvm_args)):
    return ret

  if ('-o' in llvm_args):
    index = llvm_args.index('-o')
    llvm_args[index + 1] = llvm_args[index + 1] + ".llvm.bc"
  else:
    return ret

  llvm_args.append('-emit-llvm')
  call_clang([clang_compiler] + llvm_args)

  return ret



