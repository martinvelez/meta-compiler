package edu.ucdavis.error.fuzzer.core.scheduler;

import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;

/** Created by neo on 4/5/17. */
public interface IFuzzingScheduler {

  default boolean hasNext() {
    return true;
  }

  AbstractFuzzingEngine nextFuzzingTask();

}
