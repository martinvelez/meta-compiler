Pilot Study #2 Results
=====================

On a Friday, I performed a pilot study with the new type of examples with two pilot testers.  I uploaded the recordings of the sessions to Downloads (pilot3.ogv and pilot4.ogv).  Each tester received a series of C++ debugging tasks.  For each task, one pilot tester was randomly shown the list of example fixes while the other was not.

Let E = participant with list of suggestions.
Let C = participant without list of suggestions.

Task #1: Both pilot testers solved this. 
* "Insert ," was the 6th suggestion.  But E did not seem to need its help.

Task #2: Both pilot testers solved this. 
* "Delete &" was the 1st suggestion. 
* E took a longer time to solve this.  This suggests he was having difficulty with this error message.  However, from the video, it is unclear whether he read the 1st suggestion and used it.

Task #3: Both pilot testers solved this.
* "Insert struct" was the 17th suggestion (4th page of suggestions).  E reviewed pages 1, 2, and 3 of the suggestions.  E also visited the 4th page,  However, based on the the video, he navigated away quickly (in ~5 seconds).  His next edit was to delete "*".  Then to insert "struct".  Then to insert "::" (a suggestion on screen).  He reviewed a few more examples and finally applied the right fix.  It is unclear whether the examples helped and how much. 
* In contrast, C renamed the identified "node" to "new_node" everywhere.  Participants were informed that the tasks could be completed with one-token insertion or deletions.


    struct node {
      int data;
      struct node* left;
      struct node* right;
    };

    // New empty tree
    struct node* newTreeNode(int data) {
      // New tree nodes
      node* node = new node;
      // New data node
      node->data = data;
      // New left node
      node->left = nullptr;
      // New right node
      node->right = nullptr;

      return node;
    }


Task #4: Both participants solved this task. C quicker than E.
* "Delete ..." was the 1st suggestion.
* C also deleted "..." from line 2 (the right location).  C reverted.  C tried a few more edits.  Eventually, I had to remind C that we only expected him to fix the first error which he had done.
* E read the first example.  E deleted "..." from line 1 (the wrong location).  E reverted.  E deleted "..." from line 2 (the right location) and line 3.  E tried a many more edits trying to get the code to compile without error.  Eventually, I had to remind the user that we only expected him to fix the first error encountered. 

Task #5: Both participants solved this.
* "Delete &" was the 2nd suggestion.  From the video, it appears as if E relied only on the compiler output.
* C fixed it correctly then made a few incorrect edits.   He was probably experimenting with the reference operator ("&").

Task #6: Both participants solved this by inserting "if", turning "else (cond)" into "else if (cond)".
* "Insert if" was the 17th suggestion.
* E turned an "else (cond)" into an "else if (cond)", a valid fix.  Then, using the 2nd sugggestion, he inserted ";" after the else block of code, an invalid fix.  Then he tried some sort of ternary expression, not in the list of suggestions..  He finally settled on the original fix, "else" to "else if".

Discussion

* Some of the tasks are simple where basic knowledge of C++ syntax is sufficient.
* The compiler helps users pinpoint the error line but they do not necessary do as the compiler suggests, e.g., Task #1, #3, #4, and #6.

* Except for Task #2, our list included the fix that the C participant ended up performing.
* Half of the time, the fix was on the first page.

* However, the fix examples may be difficult to parse and understand quickly.  To help, we could try to:
1) assume that the line indicated by the compiler is the line that needs to be mutated.  With this assumpation, we can add location information to the suggestion.  For example, "Delete & from line 15" instead of "Delete &".
2) filter suggestions that would surely not work.  For example, inserting a "(" to code that already has matching parentheses.

* Aside from cursor movement, we have no way of determining whether the user is reading and benefiting from the examples.
** To gather this info, we could add a question after each task: "If applicable, the example fixes provided helped me fix the code: 1) Not at all ... 4) Very much".
