# Meta-compiler

This meta-compiler project is an effort to automatically generate program
inputs that trigger all compiler errors.  The main idea is to generate compiler
errors using small edits to correct programs.  One possible application is to
help compiler users quickly understand and fix their compiler errors.

The link below contains a list of program pairs.  The first program is correct
and does not trigger an error. The second program has a small edit and triggers
a compiler error.

[http://chengniansun.bitbucket.org/projects/meta-compiler/triggered-errors.html](http://chengniansun.bitbucket.org/projects/meta-compiler/triggered-errors.html)


