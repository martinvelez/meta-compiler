#include <iostream>
constexpr int foo(int i, int j) { return i + j; }
// FIX: delete constexpr
using TConstExprFunction  = constexpr int (*)(int i, int j);

int main() {
  constexpr TConstExprFunction f = foo;
  constexpr int i = f(1, 2);
  std::cout << i << std::endl;
}