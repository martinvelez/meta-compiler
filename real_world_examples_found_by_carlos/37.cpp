#include<iostream>
#include <list>

class Animal {
private:
    std::string name;
public:
    Animal(std::string n) {
        name = n;
    }
};

// FIX: CHANGE TO
// class Cat :  public Animal {
class Cat :  Animal {
public:
    Cat(std::string n) : Animal(n) {}
};

class AnimalQueue {
private:
    std::list<Animal> cats;
    std::list<Animal> dogs;
public:
    void enqueue(Animal a) {
          if (typeid(a) == typeid(Cat)) {
                printf("I'm a cat\n");
          }
     }
};

int main() {
    AnimalQueue animalQ;
    Cat cat = Cat("kitty");
    animalQ.enqueue(cat); 
    return 0;
}